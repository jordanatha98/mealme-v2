import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscriber } from '@ubud/sate';
import { first, tap } from 'rxjs/operators';
import { IonTabs } from '@ionic/angular';
import { ScrollPointService, ScrollPointTargets } from '@mealme/app/modules/app/common/services/scroll-point.service';

@Component({
    selector: 'mealme-app-template-page',
    templateUrl: './app-template.page.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['./app-template.page.scss'],
})
export class AppTemplatePage implements OnInit, OnDestroy {
    @ViewChild(IonTabs) public tabs: IonTabs;
    public random: number = Math.random();

    public constructor(
        private subscriber: Subscriber,
        private scrollPoint: ScrollPointService,
    ) {
    }

    public get scrollPointTargets(): typeof ScrollPointTargets {
        return ScrollPointTargets;
    }

    public handleTabClicked(target: string): void {
        this.subscriber.subscribe(
            this,
            this.scrollPoint.scrollPoint$(target).pipe(
                first(),
                tap(() => {
                    if (target === this.tabs.getSelected()) {
                        this.scrollPoint.update(target, 0);
                    }
                }),
            ),
        );
    }

    public ngOnInit(): void {
        this.subscriber.flush(this);

        /*this.subscriber.subscribe(
            this,
            this.repository.getUser$().pipe(
                tap(() => {
                    this.random = Math.random();
                }),
            ),
        );*/
    }

    public ngOnDestroy(): void {
        this.subscriber.flush(this);
    }
}

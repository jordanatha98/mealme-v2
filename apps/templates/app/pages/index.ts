import { AppTemplatePage } from '@mealme/app/templates/app/pages/app-template.page';
import { BlankAppTemplatePage } from '@mealme/app/templates/app/pages/blank-app-template.page';

export const APP_TEMPLATE_PAGES: any[] = [
    AppTemplatePage,
    BlankAppTemplatePage,
];

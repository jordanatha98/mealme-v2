import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'mealme-blank-app-template-page',
    templateUrl: './blank-app-template.page.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BlankAppTemplatePage {}

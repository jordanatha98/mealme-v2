import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { APP_TEMPLATE_PAGES } from '@mealme/app/templates/app/pages';

const MODULES: any[] = [
    CommonModule,
    IonicModule,
];

@NgModule({
    declarations: [...APP_TEMPLATE_PAGES],
    imports: [...MODULES],
})
export class MealmeAppTemplateModule {}

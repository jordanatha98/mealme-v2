import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'mealme-blank-template',
    templateUrl: './blank.template.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BlankTemplate {
}

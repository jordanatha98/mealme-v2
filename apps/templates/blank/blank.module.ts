import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { MealmeAccountAuthDomainModule } from '@mealme/app/domains/auth/module';
import { BlankTemplate } from '@mealme/app/templates/blank/blank.template';

@NgModule({
    declarations: [BlankTemplate],
    imports: [
        CommonModule,
        IonicModule,
        MealmeAccountAuthDomainModule,
        RouterModule.forChild([
            {
                path: '',
                children: [
                    {
                        path: 'meals',
                        loadChildren: () => import('../../modules/app/meal/pages/meal-app-blank.module').then((m) => m.MealmeMealAppBlankModule),
                    },
                    {
                        path: 'account',
                        loadChildren: () => import('../../modules/app/account/pages/account-app-blank.module').then((m) => m.MealmeAccountAppBlankModule),
                    },
                    {
                        path: 'planner',
                        loadChildren: () => import('../../modules/app/planner/pages/planner-app-blank.module').then((m) => m.MealmePlannerAppBlankModule),
                    },
                ],
            }
        ])
    ],
})
export class MealmeBlankTemplateModule {
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthBlankTemplate } from './auth-blank.template';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { MealmeAccountAuthDomainModule } from '@mealme/app/domains/auth/module';
import { UserAuthenticatedGuard } from '@mealme/src/domain/modules/auth/guards/user-authenticated.guard';
import {GuestGuard} from "@mealme/app/domains/auth/guards/guest.guard";

@NgModule({
    declarations: [AuthBlankTemplate],
    imports: [
        CommonModule,
        IonicModule,
        MealmeAccountAuthDomainModule,
        RouterModule.forChild([
            {
                path: '',
                children: [
                    {
                        path: 'auth',
                        canActivate: [GuestGuard],
                        loadChildren: () => import('../../modules/app/auth/pages/auth-auth-blank.module').then(m => m.MealmeAuthAuthBlankModule),
                    },
                    /*{
                        path: 'user',
                        /!*canActivate: [UserAuthenticatedGuard],
                        loadChildren: () => import('@ammana/account-mobile/bootstrap/modules/user/pages/user-blank.module').then(m => m.AmmanaAccountUserBlankModule),*!/
                    },*/
                    {
                        path: 'meals',
                        loadChildren: () => import('../../modules/app/meal/pages/meal-app-blank.module').then((m) => m.MealmeMealAppBlankModule),
                    },
                    {
                        path: '',
                        pathMatch: 'full',
                        redirectTo: 'auth',
                    },
                ],
            }
        ])
    ],
})
export class MealmeAuthBlankTemplateModule {
}

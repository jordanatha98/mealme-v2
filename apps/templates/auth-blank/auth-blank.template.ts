import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'mealme-auth-blank-template',
    templateUrl: './auth-blank.template.html',
    styleUrls: ['./auth-blank.template.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuthBlankTemplate {
}

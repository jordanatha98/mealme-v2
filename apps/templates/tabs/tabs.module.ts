import { NgModule }                       from '@angular/core';
import { TabsTemplate }                   from './tabs.template';
import { RouterModule }                   from '@angular/router';
import { CommonModule }                   from '@angular/common';
import { IonicModule }                    from '@ionic/angular';
import { MealmeSharedFeatherIconsModule } from '@shared/modules/feather-icons/feather-icons.module';
import { MealmeDomainMealmeStoreModule }  from '@mealme/src/domain/stores/module';
import {AuthenticatedGuard}               from "@mealme/app/domains/auth/guards/authenticated.guard";
import {MealmeAccountAuthDomainModule}    from "@mealme/app/domains/auth/module";

@NgModule({
    declarations: [TabsTemplate],
    imports: [
        CommonModule,
        IonicModule,
        MealmeSharedFeatherIconsModule,
        MealmeDomainMealmeStoreModule,
        MealmeAccountAuthDomainModule,
        RouterModule.forChild([
            {
                path: '',
                component: TabsTemplate,
                canActivate: [AuthenticatedGuard],
                canActivateChild: [AuthenticatedGuard],
                children: [
                    {
                        path: 'home',
                        loadChildren: () => import('../../modules/app/home/pages/home-app-tabs.module').then((m) => m.MealmeHomeAppTabsModule),
                    },
                    {
                        path: 'search',
                        loadChildren: () => import('../../modules/app/search/pages/search-app-tabs.module').then((m) => m.MealmeSearchAppTabsModule),
                    },
                    {
                        path: '',
                        pathMatch: 'full',
                        redirectTo: 'home',
                    }
                ]
            },
        ])
    ],
})
export class MealmeTabsTemplateModule {
}

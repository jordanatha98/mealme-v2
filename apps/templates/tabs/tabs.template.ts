import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ModalController } from '@ionic/angular';
import { HttpUtil } from '@shared/utils/http.util';
import Signature = HttpUtil.Signature;
import { AuthRepository } from '@mealme/src/domain/modules/auth/repositories/auth.repository';

enum Pages {
    HOME = 'home',
    SEARCH = 'search',
    PLANNER = 'planner',
    ACCOUNT = 'account',
    AUTH_TRANSACTION = 'auth-transaction',
    AUTH_ACCOUNT = 'auth-account',
}

@Component({
    selector: 'mealme-tabs-template',
    templateUrl: './tabs.template.html',
    styleUrls: ['./tabs.template.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [AuthRepository]
})
export class TabsTemplate implements OnInit {
    private readonly selectedSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);
    public selected$: Observable<string>;
    public pagesEnum: typeof Pages = Pages;

    public signature$: Observable<Signature>;

    public constructor(
        private modalCtrl: ModalController,
        private authRepo: AuthRepository
    ) {
        this.selected$ = this.selectedSubject.asObservable();
        this.signature$ = this.authRepo.getSignature$();
    }

    public handleTabsChanged(data: { tab: string }): void {
        if (data) {
            this.selectedSubject.next(data.tab);
        }
    }

    public ngOnInit(): void {
    }
}

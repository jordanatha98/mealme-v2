import { NgModule } from '@angular/core';
import { ItemCard } from './item.card';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { MealmeSharedFeatherIconsModule } from '@shared/modules/feather-icons/feather-icons.module';

@NgModule({
    declarations: [ItemCard],
    imports: [IonicModule, CommonModule, MealmeSharedFeatherIconsModule],
    exports: [ItemCard],
})
export class MealmeCommonItemCardModule {}

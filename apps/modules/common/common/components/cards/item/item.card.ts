import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'mealme-common-item-card',
    templateUrl: './item.card.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['./item.card.scss']
})
export class ItemCard {
    @Input() public title: string;
    @Input() public logo: string;
    @Input() public nativeClass: string;
    @Input() public customArrow: boolean;
    @Input() public disabled: boolean;
    @Input() public customBorder: boolean;
    @Input() public color: string;
    @Input() public nativeStyle: any;
    @Input() public pxSmaller: boolean;
    @Input() public icon: string;
    @Input() public iconClass: any;
    @Input() public logoClass: any;
    @Output() public itemClicked: EventEmitter<void>;

    public constructor() {
        this.itemClicked = new EventEmitter<void>();
    }
}

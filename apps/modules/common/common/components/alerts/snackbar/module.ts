import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { SnackbarAlert } from './snackbar.alert';
import { MealmeSharedStickyModule } from '@shared/modules/sticky/sticky.module';
import { MealmeSharedFeatherIconsModule } from '@shared/modules/feather-icons/feather-icons.module';

@NgModule({
    declarations: [SnackbarAlert],
    imports: [
        CommonModule,
        IonicModule,
        MealmeSharedStickyModule,
        MealmeSharedFeatherIconsModule,
    ],
    exports: [SnackbarAlert],
})
export class MealmeCommonSnackbarAlertModule {}

import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    OnChanges,
    Output,
    SimpleChanges
} from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
    selector: 'mealme-common-snackbar-alert',
    templateUrl: './snackbar.alert.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['./snackbar.alert.scss'],
})
export class SnackbarAlert implements OnChanges {
    public intVal: any = null;

    private readonly activeSubject: BehaviorSubject<string>;

    @Input() public error: string;
    @Input() public success: string;
    @Input() public primary: string;
    @Input('timer') public timerInput: number;
    @Input() public cancelable: boolean;
    @Input() public floating: boolean = true;
    @Input() public floatingSm: boolean;
    @Input() public nativeClass: any;

    @Output() public open: EventEmitter<any> = new EventEmitter<any>();
    @Output() public close: EventEmitter<any> = new EventEmitter<any>();

    public message: string;
    public type: 'error' | 'success' | 'primary';
    public timer: number;

    public constructor() {
        this.activeSubject = new BehaviorSubject<string>(null);
    }

    public get isActive$(): Observable<string> {
        return this.activeSubject.asObservable();
    }

    public handleOpen(message: string, type: 'error' | 'success' | 'primary'): void {
        this.message = message;
        this.type = type;
        this.handleSnack(true);
    }

    public handleClose(): void {
        this.handleSnack(false);
    }

    public handleSnack(show: boolean): void {
        this.handleAlert(show);
    }

    public handleAlert(show: boolean): void {
        if (show) {
            this.open.emit();

            if (this.timer && this.timer > 0 && Math.floor(this.timer / 1000) > 0) {
                this.intVal = setTimeout(() => {
                    this.handleSnack(false);
                }, this.timer);
            }

            this.activeSubject.next('active');
        } else {
            this.close.emit();

            if (this.intVal) {
                clearTimeout(this.intVal);
                this.intVal = null;
            }

            this.activeSubject.next(null);
        }
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (changes) {
            const { error, success, primary, timerInput } = changes;

            if (timerInput && timerInput.currentValue) {
                this.timer = timerInput.currentValue;
            }

            if (error && error.currentValue) {
                this.handleOpen(error.currentValue, 'error');
            }

            if (success && success.currentValue) {
                this.handleOpen(success.currentValue, 'success');
            }

            if (primary && primary.currentValue) {
                this.handleOpen(primary.currentValue, 'primary');
            }
        }
    }
}

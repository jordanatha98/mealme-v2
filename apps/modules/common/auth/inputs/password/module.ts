import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { PasswordInput } from './password.input';

@NgModule({
    declarations: [
        PasswordInput,
    ],
    imports: [
        CommonModule,
        IonicModule,
    ],
    exports: [
        PasswordInput,
    ],
})
export class MealmeCommonAuthPasswordInputModule {}

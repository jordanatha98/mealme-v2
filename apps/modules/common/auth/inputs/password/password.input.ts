import { ChangeDetectionStrategy, Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    selector: 'mealme-common-auth-password-input',
    templateUrl: './password.input.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => PasswordInput),
            multi: true,
        },
    ],
    styleUrls: ['./password.input.scss'],
})
export class PasswordInput implements ControlValueAccessor {
    @Input() public nativeClass: any;
    @Input() public nativeStyle: any;
    @Input() public placeholder: string;
    @Input() public toggleColor: string;
    @Input()
    public set color(color: string) {
        if (color) {
            this.nativeClass = this.nativeClass + ' form-control-' + color;
        }
    }

    public showPassword: boolean = false;
    public disabled: boolean;

    public propagateChange = (_: any) => {};

    public registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }

    public registerOnTouched(fn: any): void {}

    public setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    public writeValue(obj: any): void {
        this.propagateChange(obj);
    }

    public handleInput(data: any): void {
        const { target } = data;

        this.writeValue(target.value);
    }
}

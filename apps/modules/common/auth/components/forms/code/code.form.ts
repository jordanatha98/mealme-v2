import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormComponent } from '@ubud/form';
import { CodeDto } from '@mealme/src/domain/modules/auth/dtos/code-dto';

@Component({
    selector: 'mealme-common-auth-code-form',
    templateUrl: './code.form.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CodeForm extends FormComponent<CodeDto> {
}

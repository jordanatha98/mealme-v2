import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { UbudFormModule } from '@ubud/form';
import { CodeForm } from '@mealme/app/modules/common/auth/components/forms/code/code.form';
import { MealmeCommonAuthPasswordInputModule } from '@mealme/app/modules/common/auth/inputs/password/module';

@NgModule({
    declarations: [CodeForm],
    imports: [
        CommonModule,
        IonicModule,
        UbudFormModule,
        MealmeCommonAuthPasswordInputModule,
    ],
    exports: [CodeForm],
})
export class MealmeCommonAuthCodeFormModule {}

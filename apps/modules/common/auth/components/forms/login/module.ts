import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { LoginForm } from './login.form';
import { UbudFormModule } from '@ubud/form';
import { MealmeCommonAuthPasswordInputModule } from '@mealme/app/modules/common/auth/inputs/password/module';

@NgModule({
    declarations: [LoginForm],
    imports: [
        CommonModule,
        IonicModule,
        UbudFormModule,
        MealmeCommonAuthPasswordInputModule,
    ],
    exports: [LoginForm],
})
export class MealmeCommonAuthLoginFormModule {}

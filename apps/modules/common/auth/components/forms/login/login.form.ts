import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FormComponent, FormState } from '@ubud/form';
import { LoginDto } from '@mealme/src/domain/modules/auth/dtos/login-dto';

@Component({
    selector: 'mealme-common-auth-login-form',
    templateUrl: './login.form.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginForm extends FormComponent<LoginDto> {
    @Input() public formState: FormState<LoginDto>;
}

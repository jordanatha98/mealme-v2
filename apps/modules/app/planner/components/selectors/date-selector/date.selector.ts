import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    forwardRef,
    Input,
    Output
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MonthName } from '@shared/enums/month-name';
import { DateUtil } from '@shared/utils/date.util';

@Component({
    selector: 'mealme-planner-date-selector',
    templateUrl: './date.selector.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DateSelector),
            multi: true,
        },
    ],
})
export class DateSelector implements ControlValueAccessor {
    @Output() public changed: EventEmitter<any> = new EventEmitter<any>();
    @Input() public max: string;
    @Input() public min: string;

    public today: Date = new Date();
    public changedToday = null;

    public day: string;
    public days: string[] = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
    public months: string [] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    public date: string;

    public doneText: string = 'Done';
    public labelFormat: string = 'dd MMMM yyyy';
    public displayFormat: string = 'DD MMMM YYYY';
    public pickerFormat: string = 'DD MMMM YYYY';
    public type: 'date' | 'time' | 'both' = 'date';

    @Input() public disabled: boolean;
    public value: string;
    public monthNames: string[];

    public openModal: boolean = false;

    public constructor(private cdRef: ChangeDetectorRef) {
        this.monthNames = MonthName.getValues().map(item => item.text);
        this.day = this.days[this.today.getDay()]
        this.date = this.today.getDate() + ' ' + this.monthNames[this.today.getMonth()] + ' ' + this.today.getFullYear()
    }

    public propagateChange = (_: any) => {
    };

    public registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }

    public registerOnTouched(fn: any): void {
    }

    public setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    public pad(value) {
        return value.toString().padStart(2, '0');
    }

    public writeValue(obj: string): void {
        if (obj) {
            const date = DateUtil.iosDate(new Date(obj).toString());

            const iso = date.toISOString().split('T');
            const time = date.toTimeString().split(' ');

            this.value = date.toISOString();

            if (iso && iso.length > 0 && time && time.length > 0) {
                let value = iso[0] + ' ' + time[0];

                if (this.type === 'date') {
                    value = iso[0];
                } else if (this.type === 'time') {
                    value = time[0];
                }

                this.propagateChange(value);
                this.changed.emit(value);
                this.changedToday = new Date(value);
                this.day = this.days[this.changedToday.getDay()]
                this.date = this.changedToday.getDate() + ' ' + this.monthNames[this.changedToday.getMonth()] + ' ' + this.changedToday.getFullYear()
            }
            this.cdRef.markForCheck()
        }

        /*this.value = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()} ${date.getHours()}`;*/
    }

    public handleModalOperation(): void {
        this.openModal = !this.openModal;
    }

    public changeDate(acc: string): void {
        const val = new Date(this.value)
        if (acc === 'plus') {
            if (this.value) {
                this.changedToday = new Date(val.setDate(val.getDate() + 1))
            } else {
                this.changedToday = new Date(this.today.setDate(this.today.getDate() + 1))
            }
        } else {
            if (this.value) {
                this.changedToday = new Date(val.setDate(val.getDate() - 1))
            } else {
                this.changedToday = new Date(this.today.setDate(this.today.getDate() - 1))
            }
        }
        if (this.changedToday.getHours() >= 20) {
            this.changedToday.setDate(this.changedToday.getDate() + 1);
        }
        let formatted = `${this.changedToday.getFullYear()}-${this.pad(this.changedToday.getMonth() + 1)}-${this.pad(this.changedToday.getDate())}`;
        this.writeValue(formatted);
        this.day = this.days[this.changedToday.getDay()]
        this.date = this.changedToday.getDate() + ' ' + this.months[this.changedToday.getMonth()] + ' ' + this.changedToday.getFullYear()
    }

    public handleDoneClick(): void {
        const val = new Date(this.value)
        if (val.getHours() >= 20) {
            val.setDate(val.getDate() + 1);
        }
        let formatted = `${val.getFullYear()}-${this.pad(val.getMonth() + 1)}-${this.pad(val.getDate())}`;
        this.writeValue(formatted);
        this.changedToday = new Date(this.value);
        this.openModal = false;
    }
}

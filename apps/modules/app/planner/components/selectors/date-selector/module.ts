import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { MealmeSharedFeatherIconsModule } from '@shared/modules/feather-icons/feather-icons.module';
import { FormsModule } from '@angular/forms';
import { DateSelector } from '@mealme/app/modules/app/planner/components/selectors/date-selector/date.selector';

@NgModule({
    declarations: [
        DateSelector
    ],
    imports: [
        CommonModule,
        IonicModule,
        MealmeSharedFeatherIconsModule,
        FormsModule
    ],
    exports: [
        DateSelector
    ],
})
export class MealmePlannerDateSelectorModule {
}

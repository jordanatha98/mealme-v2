import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { SharedCommonDirectiveModule } from '@shared/modules/common/directives/module';
import { SharedCommonComponentModule } from '@shared/modules/common/components/module';
import { MealmeSharedFeatherIconsModule } from '@shared/modules/feather-icons/feather-icons.module';
import { FormsModule } from '@angular/forms';
import { MealmeInteractionMealModule } from '@mealme/src/interaction/modules/meal/module';
import { PlanPage } from '@mealme/app/modules/app/planner/pages/plan/plan.page';
import { MelameCommonDatePickerInputModule } from '@mealme/app/modules/app/common/components/inputs/date-picker/module';
import { MealmePlannerDateSelectorModule } from '@mealme/app/modules/app/planner/components/selectors/date-selector/module';
import { MealmeMealPlanCardModule } from '@mealme/app/modules/app/meal/components/cards/meal-plan/module';
import { MealmeMealActionModalModule } from '@mealme/app/modules/app/meal/modals/meal-action/module';
import { MealmeCommonSnackbarAlertModule } from '@mealme/app/modules/common/common/components/alerts/snackbar/module';

@NgModule({
    declarations: [PlanPage],
    imports: [
        CommonModule,
        IonicModule,
        SharedCommonDirectiveModule,
        SharedCommonComponentModule,
        MelameCommonDatePickerInputModule,
        RouterModule.forChild([
            {
                path: '',
                component: PlanPage
            },
        ]),
        MealmeSharedFeatherIconsModule,
        FormsModule,
        MealmePlannerDateSelectorModule,
        MealmeMealPlanCardModule,
        MealmeInteractionMealModule,
        MealmeMealActionModalModule,
        MealmeCommonSnackbarAlertModule,
    ],
})
export class MealmePlannerPlanPageModule {}

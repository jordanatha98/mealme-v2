import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { MealmeStore } from '@mealme/src/domain/stores/mealme.store';
import { MealRepository } from '@mealme/src/domain/modules/meal/repositories/meal.repository';
import { combineLatest, Observable, of } from 'rxjs';
import { Menu } from '@mealme/src/domain/modules/meal/models/menu';
import { MealmeInteractionStore } from '@mealme/src/interaction/stores/mealme-interaction.store';
import { MealMealsInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/meal-meals-interaction-changed';
import { Subscriber } from '@ubud/sate';
import { FetchMeals } from '@mealme/src/domain/modules/meal/messages/commands/fetch-meals';
import { ActionSheetController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { User } from '@mealme/src/domain/modules/auth/models/user';
import { UserRepository } from '@mealme/src/domain/modules/auth/repositories/user.repository';
import { filter, first, mergeMap, tap } from 'rxjs/operators';
import { FetchPlans } from '@mealme/src/domain/modules/meal/messages/commands/fetch-plans';
import { Plan } from '@mealme/src/domain/modules/plan/models/plan';
import { PlanMenusInteractionRepository } from '@mealme/src/interaction/modules/meal/repositories/plan-menus-interaction.repository';
import { PlanMenu } from '@mealme/src/domain/modules/plan/models/plan-menu';
import { PlanMenusGenerateInteractionRepository } from '@mealme/src/interaction/modules/meal/repositories/plan-menus-generate-interaction.repository';
import { GeneratePlanInteraction } from '@mealme/src/interaction/modules/meal/enums/generate-plan-interaction';
import { GeneratePlanInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/generate-plan-interaction-changed';
import { GeneratePlan } from '@mealme/src/domain/modules/meal/messages/commands/generate-plan';

@Component({
    selector: 'mealme-planner-plan-page',
    templateUrl: './plan.page.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [MealRepository, PlanMenusInteractionRepository, UserRepository, PlanMenusGenerateInteractionRepository]
})
export class PlanPage implements OnInit, OnDestroy {
    public meals$: Observable<Menu[]>;
    public loading$: Observable<boolean>;
    public error$: Observable<string>;

    public plan$: Observable<Plan>;

    public authenticatedUser$: Observable<User>;

    public generateLoading$: Observable<boolean>;
    public generateError$: Observable<string>;
    public generateSuccess$: Observable<string>;
    public generateInteraction$: Observable<GeneratePlanInteraction>;

    public selectedDate: any;

    public constructor(
        private store: MealmeStore,
        private interactionStore: MealmeInteractionStore,
        private repo: MealRepository,
        private interactionRepo: PlanMenusInteractionRepository,
        private subscriber: Subscriber,
        private modalCtrl: ModalController,
        private router: Router,
        private actionSheetController: ActionSheetController,
        private userRepo: UserRepository,
        private generateInteractionRepo: PlanMenusGenerateInteractionRepository,
    ) {
        this.meals$ = this.repo.getMeals$();
        this.loading$ = this.interactionRepo.isProcess$();
        this.error$ = this.interactionRepo.getError$();
        this.authenticatedUser$ = this.userRepo.getAuthenticatedUser$();
        this.plan$ = this.repo.getPlans$().pipe(
            mergeMap(plan => {
                if (plan.data.length > 0) {
                    return of(plan.data[0]);
                } else {
                    return of(null)
                }
            })
        );
        this.generateLoading$ = this.generateInteractionRepo.isProcess$();
        this.generateError$ = this.generateInteractionRepo.getError$();
        this.generateSuccess$ = this.generateInteractionRepo.getSuccess$();
        this.generateInteraction$ = this.generateInteractionRepo.getInteraction$();
    }

    public async handleMealClicked(meal: PlanMenu) {
        await this.router.navigateByUrl(`/app/blank/meals/${meal.menuId}`);
    }

    public handleGenerateClicked(): void {
        this.subscriber.subscribe(
            this,
            this.authenticatedUser$.pipe(
                first(),
                tap(user => {
                    this.store.dispatch(new GeneratePlan({ customer: user.customer.id, payload: { dates: [this.selectedDate]} }))
                })
            )
        )
    }

    public async handleOpenAction() {
        const actionSheet = await this.actionSheetController.create({
            header: 'Actions',
            mode: 'ios',
            buttons: [
                {
                    text: 'Regenerate',
                    cssClass: 'text-secondary',
                    handler: () => {
                        console.log('Play clicked');
                    }
                },
                {
                    text: 'Cancel',
                    cssClass: 'text-danger',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        await actionSheet.present();

        const { role, data } = await actionSheet.onDidDismiss();
        console.log('onDidDismiss resolved with role and data', role, data);
    }

    public handleChangeDate(date: any): void {
        this.selectedDate = date;
        this.subscriber.subscribe(
            this,
            this.authenticatedUser$.pipe(
                filter(user => !!user),
                tap(user => {
                    this.store.dispatch(new FetchPlans({ customer: user.customer.id, queries: { startDate: date, endDate: date } }))
                })
            )
        )
        this.store.dispatch(new FetchMeals());
    }

    public handleInteraction(): void {
        this.subscriber.subscribe(
            this,
            combineLatest([this.generateInteraction$, this.authenticatedUser$]).pipe(
                tap(([interaction, user]) => {
                    if (interaction === GeneratePlanInteraction.GENERATE_SUCCEED) {
                        this.store.dispatch(new FetchPlans({ customer: user.customer.id, queries: { startDate: this.selectedDate, endDate: this.selectedDate }}));
                    }

                    if (interaction === GeneratePlanInteraction.GENERATE_FAILED ) {
                        setTimeout(() => {
                            this.handleResetInteraction();
                        }, 3000);
                    }
                }),
            )
        );
    }

    public handleResetInteraction(): void {
        this.interactionStore.dispatch(new MealMealsInteractionChanged({
            changes: {
                process: false,
                error: null,
            }
        }));
        this.interactionStore.dispatch(new GeneratePlanInteractionChanged({
            changes: {
                process: false,
                error: null,
                success: null,
                interaction: GeneratePlanInteraction.IDLE
            }
        }))
    }

    public ngOnDestroy(): void {
        this.subscriber.flush(this);
        this.handleResetInteraction();
    }

    public ngOnInit(): void {
        this.handleInteraction();
        const today = new Date();
        const startDate = today.getFullYear() + '-' + (((today.getMonth() + 1).toString().length < 2) ? '0' + (today.getMonth() + 1) : (today.getMonth() + 1)) + '-' + ((today.getDate().toString().length < 2) ? '0' + today.getDate() : today.getDate());
        this.subscriber.subscribe(
            this,
            this.authenticatedUser$.pipe(
                filter(user => !!user),
                tap(user => {
                    this.store.dispatch(new FetchPlans({ customer: user.customer.id, queries: { startDate: startDate, endDate: startDate } }))
                })
            )
        )
    }
}

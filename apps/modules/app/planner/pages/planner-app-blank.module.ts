import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                loadChildren: () => import('./plan/plan.module').then(m => m.MealmePlannerPlanPageModule),

            },
        ]),
    ],
})
export class MealmePlannerAppBlankModule {}

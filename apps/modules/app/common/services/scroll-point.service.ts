import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Subscriber } from '@ubud/sate';
import { map, tap } from 'rxjs/operators';
import { IonContent } from '@ionic/angular';

export enum ScrollPointTargets {
    HOME = 'home',
    NEWS = 'news',
    PORTFOLIOS = 'portfolios',
    PROHUB = 'prohub',
}

@Injectable({ providedIn: 'root' })
export class ScrollPointService {
    private targets: any = {
        home: 0,
        news: 0,
        portfolios: 0,
        prohub: 0,
    };

    private readonly scrollPointSubject: BehaviorSubject<any> = new BehaviorSubject<any>(this.targets);

    public constructor(
        private subscriber: Subscriber,
    ) {
    }

    public scrollPoint$(target: string): Observable<number> {
        return this.scrollPointSubject.asObservable().pipe(
            map(data => data[target]),
        );
    }

    public update(target: string, point: number): void {
        this.targets = { ...this.targets, [target]: point };
        this.scrollPointSubject.next(this.targets);
    }

    public init(context: any, target: string, content: IonContent): void {
        this.subscriber.subscribe(
            context,
            this.scrollPoint$(target).pipe(
                tap(point => {
                    if (content && !point) {
                        content.scrollToTop(500).then();
                    }
                }),
            ),
        );
    }
}

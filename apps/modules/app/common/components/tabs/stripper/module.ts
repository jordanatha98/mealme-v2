import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { StripperTab } from '@mealme/app/modules/app/common/components/tabs/stripper/stripper.tab';

@NgModule({
    declarations: [StripperTab],
    imports: [
        CommonModule,
        IonicModule,
        RouterModule,
    ],
    exports: [StripperTab],
})
export class MealmeCommonStripperTabModule {
}

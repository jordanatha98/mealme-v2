import { ChangeDetectionStrategy, Component } from '@angular/core';
import { StripperTabBase } from './stripper.tab.base';

@Component({
    selector: 'mealme-common-stripper-tab',
    templateUrl: './stripper.tab.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['./stripper.tab.scss']
})
export class StripperTab extends StripperTabBase {
}

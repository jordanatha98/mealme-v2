import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

export interface TabRouting {
    title: string;
    path: string;
    queryParams?: any;
    code?: string;
}

@Component({
    selector: 'mealme-common-stripper-tab-base',
    template: ``,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StripperTabBase {
    @Input() public tabs: TabRouting[];
    @Input() public nativeClass: string;
    @Input() public listClass: string;
    @Input() public centerTabs: boolean = true;
    @Input() public isRoute: boolean = true;
    @Input() public currentActive: string = '';
    @Output() public clicked: EventEmitter<any> = new EventEmitter<any>();

    public handleChangeActive(tab: string): void {
        this.currentActive = tab;
        this.clicked.emit(tab);
    }

    public constructor() {
    }
}

import { NgModule } from '@angular/core';
import { DatePickerInput } from './date-picker.input';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { MealmeSharedFeatherIconsModule } from '@shared/modules/feather-icons/feather-icons.module';
import { MealmeCommonItemCardModule } from '@mealme/app/modules/common/common/components/cards/item/module';

@NgModule({
    declarations: [DatePickerInput],
    imports: [
        CommonModule,
        IonicModule,
        MealmeCommonItemCardModule,
        FormsModule,
        MealmeSharedFeatherIconsModule
    ],
    exports: [DatePickerInput],
})
export class MelameCommonDatePickerInputModule {}

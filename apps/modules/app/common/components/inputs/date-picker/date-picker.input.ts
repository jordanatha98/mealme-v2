import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    forwardRef,
    Input,
    Output
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MonthName } from '@shared/enums/month-name';
import { DateUtil } from '@shared/utils/date.util';

@Component({
    selector: 'ammana-element-common-date-picker-input-ionic',
    templateUrl: './date-picker.input.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DatePickerInput),
            multi: true,
        },
    ],
})
export class DatePickerInput implements ControlValueAccessor {
    @Output() public changed: EventEmitter<any> = new EventEmitter<any>();
    @Input() public nativeClass: any;
    @Input() public pickerNativeClass: any;
    @Input() public nativeStyle: any;
    @Input() public placeholder: string;
    @Input() public max: string;
    @Input() public min: string;
    @Input() public doneText: string = 'Simpan';
    @Input() public labelFormat: string = 'dd MMMM yyyy';
    @Input() public displayFormat: string = 'DD MMMM YYYY';
    @Input() public pickerFormat: string = 'DD MMMM YYYY';
    @Input() public type: 'date' | 'time' | 'both' = 'date';
    @Input() public switchLogo: boolean;
    @Input() public customBorder: boolean;
    @Input() public color: string;
    @Input() public pxContentSmaller: boolean;

    @Input() public disabled: boolean;
    public value: string;
    public monthNames: string[];

    public constructor(private cdRef: ChangeDetectorRef) {
        this.monthNames = MonthName.getValues().map(item => item.text);
    }

    public propagateChange = (_: any) => {};

    public registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }

    public registerOnTouched(fn: any): void {}

    public setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    public writeValue(obj: string): void {
        if (obj) {
            const date = DateUtil.iosDate(new Date(obj).toString());

            const iso = date.toISOString().split('T');
            const time = date.toTimeString().split(' ');

            this.value = date.toISOString();

            if (iso && iso.length > 0 && time && time.length > 0) {
                let value = iso[0] + ' ' + time[0];

                if (this.type === 'date') {
                    value = iso[0];
                } else if (this.type === 'time') {
                    value = time[0];
                }

                this.propagateChange(value);
                this.changed.emit(value);
            }
            this.cdRef.markForCheck()
        }

        /*this.value = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()} ${date.getHours()}`;*/
    }
}

import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'mealme-common-list-item',
    templateUrl: './list.item.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['./list.item.scss'],
})
export class ListItem {
    @Input() public isButton: boolean = true;
    @Input() public title: string;
    @Input() public description: string;
    @Input() public logo: string;
    @Input() public customArrow: boolean;
    @Input() public withArrow: boolean = true;
    @Input() public nativeClass: string;
    @Input() public nativeLogoClass: any;
    @Input() public icon: string;
    @Input() public iconClass: any;
    @Input() public iconStyle: any;
    @Output() public clicked: EventEmitter<any> = new EventEmitter<any>();
}

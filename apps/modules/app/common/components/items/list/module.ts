import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { ListItem } from './list.item';
import { MealmeSharedFeatherIconsModule } from '@shared/modules/feather-icons/feather-icons.module';

@NgModule({
    declarations: [
        ListItem
    ],
    imports: [
        CommonModule,
        IonicModule,
        MealmeSharedFeatherIconsModule,
    ],
    exports: [
        ListItem
    ],
})
export class MealmeCommonListItemModule {
}

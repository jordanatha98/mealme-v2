import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { MealmeSharedFeatherIconsModule } from '@shared/modules/feather-icons/feather-icons.module';
import { DataListItem } from '@mealme/app/modules/app/common/components/items/data-list/data-list.item';

@NgModule({
    declarations: [
        DataListItem
    ],
    imports: [
        CommonModule,
        IonicModule,
        MealmeSharedFeatherIconsModule,
    ],
    exports: [
        DataListItem
    ],
})
export class MealmeCommonDataListItemModule {
}

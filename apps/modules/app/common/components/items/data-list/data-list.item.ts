import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 'mealme-common-data-list-item',
    templateUrl: './data-list.item.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DataListItem {
    @Input() public title: string;
    @Input() public data: string;
    @Input() public nativeClass: string;
    @Input() public nativeLogoClass: any;
}

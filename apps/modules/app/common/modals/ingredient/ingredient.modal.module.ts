import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { MealmeSharedStickyModule } from '@shared/modules/sticky/sticky.module';
import { SharedCommonComponentModule } from '@shared/modules/common/components/module';
import { FormsModule } from '@angular/forms';
import { IngredientModal } from '@mealme/app/modules/app/common/modals/ingredient/ingredient.modal';
import { MealmeCommonSnackbarAlertModule } from '@mealme/app/modules/common/common/components/alerts/snackbar/module';
import { MealmeCommonListItemModule } from '@mealme/app/modules/app/common/components/items/list/module';
import { IngredientModalService } from '@mealme/app/modules/app/common/modals/ingredient/ingredient-modal.service';

@NgModule({
    declarations: [IngredientModal],
    imports: [
        CommonModule,
        IonicModule,
        MealmeCommonSnackbarAlertModule,
        MealmeSharedStickyModule,
        SharedCommonComponentModule,
        FormsModule,
        MealmeCommonListItemModule,
    ],
    exports: [IngredientModal],
    providers: [IngredientModalService]
})
export class MealmeCommonIngredientModalModule {
}

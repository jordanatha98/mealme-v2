import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MealmeStore } from '@mealme/src/domain/stores/mealme.store';
import { UserRepository } from '@mealme/src/domain/modules/auth/repositories/user.repository';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { Subscriber } from '@ubud/sate';
import { ModalController } from '@ionic/angular';
import { MealmeInteractionStore } from '@mealme/src/interaction/stores/mealme-interaction.store';
import { Ingredient } from '@mealme/src/domain/modules/common/models/ingredient';
import { filter, mergeMap, take, tap } from 'rxjs/operators';
import { IngredientModalService } from '@mealme/app/modules/app/common/modals/ingredient/ingredient-modal.service';
import { SetPreferences } from '@mealme/src/domain/modules/account/messages/commands/set-preferences';
import { User } from '@mealme/src/domain/modules/auth/models/user';
import { PreferencesManageInteraction } from '@mealme/src/interaction/modules/account/enums/preferences-manage-interaction';
import { PreferencesManageRepository } from '@mealme/src/interaction/modules/account/repositories/preferences-manage.repository';
import { PreferencesManageInteractionChanged } from '@mealme/src/interaction/modules/account/messages/events/preferences-manage-interaction-changed';
import { FetchAuthenticatedUser } from '@mealme/src/domain/modules/auth/messages/commands/user/fetch-authenticated-user';

@Component({
    selector: 'mealme-common-ingredient-modal',
    templateUrl: './ingredient.modal.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        MealmeStore,
        UserRepository,
        PreferencesManageRepository
    ],
})
export class IngredientModal implements OnInit, OnDestroy {
    @Input() public chosenItems: Ingredient[] = [];
    @Input() public chosenLikes: Ingredient[];
    @Input() public chosenDislikes: Ingredient[];
    @Input() public chosenAllergies: Ingredient[];
    @Input() public type: string;
    @Input() public withRequest: boolean = false;
    @Input() public customerId: string;

    public keyword: string;
    public keyword$: Observable<string>;
    private readonly keywordSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

    public ingredients$: Observable<Ingredient[]>;
    public ingredients: Ingredient[] = null;

    public authenticatedUser$: Observable<User>;

    public loading$: Observable<boolean>;
    public error$: Observable<string>;
    public success$: Observable<string>;
    public interaction$: Observable<PreferencesManageInteraction>;

    public constructor(
        private subscriber: Subscriber,
        private cdRef: ChangeDetectorRef,
        private modalCtrl: ModalController,
        private store: MealmeStore,
        private interactionStore: MealmeInteractionStore,
        private service: IngredientModalService,
        private userRepo: UserRepository,
        private interactionRepo: PreferencesManageRepository,
    ) {
        this.keyword$ = this.keywordSubject.asObservable();
        this.ingredients$ = this.service.ingredients$.pipe(
            filter(item => !!item)
        )
        this.authenticatedUser$ = this.userRepo.getAuthenticatedUser$();
        this.loading$ = this.interactionRepo.isProcess$();
        this.error$ = this.interactionRepo.getError$();
        this.success$ = this.interactionRepo.getSuccess$();
        this.interaction$ = this.interactionRepo.getInteraction$();
    }

    public handleSelected(ingredient: Ingredient): void {
        this.chosenItems.push(ingredient);
        this.ingredients = this.ingredients.filter(item => item.id !== ingredient.id);
        this.cdRef.markForCheck();
    }

    public handleDeleteItem(ingredient: Ingredient): void {
        const deleted = this.chosenItems.filter(item => ingredient.id !== item.id);
        this.cdRef.markForCheck();
        this.ingredients.push(ingredient);
        this.chosenItems = deleted;
    }

    public handleKeywordChanged(keyword: string): void {
        if (keyword === '') {
            this.keywordSubject.next(null);
            this.keyword = null;
        } else if (keyword) {
            this.keywordSubject.next(keyword);
            this.keyword = keyword;
        }
    }

    public async dismiss(apply?: boolean) {
        if (this.withRequest && apply === true) {
            let payload = {};
            this.subscriber.subscribe(
                this,
                this.authenticatedUser$.pipe(
                    take(1),
                    filter(user => !!user),
                    tap(user => {
                        if (this.type === 'likes') {
                            payload = {
                                likes: this.chosenItems.map(item => item.id),
                                dislikes: user.customer.dislikes.map(item => item.ingredientId),
                                allergies: user.customer.allergies.map(item => item.ingredientId),
                                dietType: user.customer.dietType ? user.customer.dietType : null,
                            }
                        } else if (this.type === 'dislikes') {
                            payload = {
                                likes: user.customer.likes.map(item => item.ingredientId),
                                dislikes: this.chosenItems.map(item => item.id),
                                allergies: user.customer.allergies.map(item => item.ingredient.id),
                                dietType: user.customer.dietType ? user.customer.dietType : null,
                            }
                        } else if (this.type === 'allergies') {
                            payload = {
                                likes: user.customer.likes.map(item => item.ingredientId),
                                dislikes: user.customer.dislikes.map(item => item.ingredient.id),
                                allergies: this.chosenItems.map(item => item.id),
                                dietType: user.customer.dietType ? user.customer.dietType : null,
                            }
                        }
                        this.store.dispatch(new SetPreferences({ customer: user.customer.id, payload }))
                    })
                )
            )
            this.cdRef.markForCheck();
        } else {
            if (apply) {
                await this.modalCtrl.dismiss(this.chosenItems);
            } else {
                await this.modalCtrl.dismiss();
            }
        }
    }

    public handleResetInteraction(): void {
        this.interactionStore.dispatch(new PreferencesManageInteractionChanged({
            changes: {
                process: false,
                error: null,
                success: null,
                interaction: PreferencesManageInteraction.IDLE,
                response: null,
            },
        }));
    }

    public handleInteraction(): void {
        this.subscriber.subscribe(
            this,
            this.interaction$.pipe(
                tap(interaction => {
                    if (interaction === PreferencesManageInteraction.SET_SUCCEED) {
                        this.store.dispatch(new FetchAuthenticatedUser());
                        setTimeout(() => {
                            this.modalCtrl.dismiss(false).then();
                        }, 1000);
                    }

                    if (interaction === PreferencesManageInteraction.SET_FAILED ) {
                        setTimeout(() => {
                            this.handleResetInteraction();
                        }, 3000);
                    }
                }),
            )
        );
    }

    public ngOnInit(): void {
        this.handleInteraction();
        this.subscriber.subscribe(
            this,
            combineLatest([this.keyword$]).pipe(
                mergeMap(([keyword]) => {
                    let excludes = [];
                    if (this.type === 'likes') {
                        if (this.chosenAllergies && this.chosenAllergies.length > 0) {
                            this.chosenAllergies.map(item => item.id).forEach(likes => {
                                excludes.push(likes);
                            })
                        }
                        if (this.chosenDislikes && this.chosenDislikes.length > 0) {
                            this.chosenDislikes.map(item => item.id).forEach(dislikes => {
                                excludes.push(dislikes);
                            })
                        }
                    }
                    if (this.type === 'dislikes') {
                        if (this.chosenLikes.length > 0) {
                            this.chosenLikes.map(item => item.id).forEach(likes => {
                                excludes.push(likes);
                            })
                        }
                    } else if (this.type === 'allergies') {
                        if (this.chosenLikes.length > 0) {
                            this.chosenLikes.map(item => item.id).forEach(likes => {
                                excludes.push(likes);
                            })
                        }
                        if (this.chosenDislikes.length > 0) {
                            this.chosenDislikes.map(item => item.id).forEach(dislikes => {
                                excludes.push(dislikes);
                            })
                        }
                    }
                    return this.service.getIngredients({ keyword: keyword ? keyword : null, excludes: excludes.length > 0 ? excludes : null })
                })
            )
        )
        this.subscriber.subscribe(
            this,
            this.ingredients$.pipe(
                tap(ingredients => {
                    this.ingredients = ingredients
                    if (this.chosenItems.length > 0) {
                        this.chosenItems.forEach(item => {
                            this.ingredients = this.ingredients.filter(ingr => ingr.id !== item.id);
                        })
                    }
                    this.cdRef.markForCheck()
                })
            )
        )
    }

    public ngOnDestroy() {
        this.subscriber.flush(this);
        this.chosenItems = [];
        this.chosenLikes = [];
        this.chosenDislikes = [];
        this.handleResetInteraction();
    }

    public ionViewDidLeave(): void {
        this.subscriber.flush(this);
    }
}

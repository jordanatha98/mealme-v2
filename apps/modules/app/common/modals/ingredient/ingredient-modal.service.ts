import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { Ingredient } from '@mealme/src/domain/modules/common/models/ingredient';
import { MealmeApiClient } from '@mealme/src/api/clients/api.client';
import { catchError, filter, take, tap } from 'rxjs/operators';
import { HttpUtil } from '@shared/utils/http.util';
import { mapToArrayClass, mapToData } from '@shared/transformers/responses.transformer';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class IngredientModalService {
    private readonly ingredientsSubject: BehaviorSubject<Ingredient[]> = new BehaviorSubject<Ingredient[]>(undefined);
    public ingredients$: Observable<Ingredient[]> = this.ingredientsSubject.asObservable();

    private readonly finalIngredientsSubject: BehaviorSubject<Ingredient[]> = new BehaviorSubject<Ingredient[]>(undefined);
    public finalIngredients$: Observable<Ingredient[]> = this.finalIngredientsSubject.asObservable();

    private readonly ingredientsLoadingSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public ingredientsLoading$: Observable<boolean> = this.ingredientsLoadingSubject.asObservable();

    public constructor(private client: MealmeApiClient) {
    }

    public searchIngredients(keyword?: string): Observable<any> {
        return this.ingredients$.pipe(
            filter(ingredients => !!ingredients),
            take(1),
            tap(ingredients => {
                if (ingredients) {
                    if (keyword) {
                        this.finalIngredientsSubject.next(ingredients.filter(
                            (s) => s.name?.toLowerCase().indexOf(keyword ? keyword.toLowerCase() : '') !== -1
                        ));
                    } else {
                        this.finalIngredientsSubject.next(ingredients);
                    }
                }
            }),
        );
    }

    public getIngredients(params?: any): Observable<any> {
        this.ingredientsLoadingSubject.next(true);

        return this.client.get(`ingredients`, { params: HttpUtil.queryParamsExtractor(params) }).pipe(
            mapToData(),
            mapToArrayClass(Ingredient),
            tap(response => {
                this.ingredientsLoadingSubject.next(false);

                if (response && Array.isArray(response)) {
                    this.ingredientsSubject.next(response);
                    this.finalIngredientsSubject.next(response);
                } else {
                    this.ingredientsSubject.next(null);
                    this.finalIngredientsSubject.next(null);
                }
            }),
            catchError((e: HttpErrorResponse) => of(e).pipe(
                tap(() => {
                    this.ingredientsLoadingSubject.next(false);
                    this.ingredientsSubject.next(null);
                    this.finalIngredientsSubject.next(null);
                }),
            )),
        );
    }
}

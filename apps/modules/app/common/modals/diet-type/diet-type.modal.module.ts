import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { MealmeSharedStickyModule } from '@shared/modules/sticky/sticky.module';
import { FormsModule } from '@angular/forms';
import { DietTypeModal } from '@mealme/app/modules/app/common/modals/diet-type/diet-type.modal';
import { MealmeCommonSnackbarAlertModule } from '@mealme/app/modules/common/common/components/alerts/snackbar/module';
import { SharedCommonComponentModule } from '@shared/modules/common/components/module';
import { MealmeCommonListItemModule } from '@mealme/app/modules/app/common/components/items/list/module';
import { DietTypeModalService } from '@mealme/app/modules/app/common/modals/diet-type/diet-type-modal.service';

@NgModule({
    declarations: [DietTypeModal],
    imports: [
        CommonModule,
        IonicModule,
        MealmeCommonSnackbarAlertModule,
        MealmeSharedStickyModule,
        SharedCommonComponentModule,
        FormsModule,
        MealmeCommonListItemModule,
    ],
    exports: [DietTypeModal],
    providers: [DietTypeModalService]
})
export class MealmeCommonDietTypeModalModule {
}

import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MealmeStore } from '@mealme/src/domain/stores/mealme.store';
import { UserRepository } from '@mealme/src/domain/modules/auth/repositories/user.repository';
import { BehaviorSubject, combineLatest, Observable, of } from 'rxjs';
import { Subscriber } from '@ubud/sate';
import { ModalController } from '@ionic/angular';
import { MealmeInteractionStore } from '@mealme/src/interaction/stores/mealme-interaction.store';
import { filter, mergeMap, tap } from 'rxjs/operators';
import { DietType } from '@mealme/src/domain/modules/common/models/diet-type';
import { DietTypeModalService } from '@mealme/app/modules/app/common/modals/diet-type/diet-type-modal.service';

@Component({
    selector: 'mealme-common-diet-type-modal',
    templateUrl: './diet-type.modal.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        MealmeStore,
        UserRepository,
    ],
})
export class DietTypeModal implements OnInit, OnDestroy {
    @Input() public chosenItems: DietType[] = [];

    public keyword: string;
    public keyword$: Observable<string>;
    private readonly keywordSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

    public dietTypes$: Observable<DietType[]>;
    public dietTypes: DietType[] = null;

    public constructor(
        private subscriber: Subscriber,
        private cdRef: ChangeDetectorRef,
        private modalCtrl: ModalController,
        private store: MealmeStore,
        private interactionStore: MealmeInteractionStore,
        private service: DietTypeModalService,
    ) {
        this.keyword$ = this.keywordSubject.asObservable();
        this.dietTypes$ = this.service.dietTypes$.pipe(
            filter(item => !!item)
        )
    }

    public handleSelected(dietType: DietType): void {
        this.chosenItems.push(dietType);
        this.dietTypes = this.dietTypes.filter(item => item.id !== dietType.id);
        this.cdRef.markForCheck();
    }

    public handleDeleteItem(dietType: DietType): void {
        const deleted = this.chosenItems.filter(item => dietType.id !== item.id);
        this.cdRef.markForCheck();
        this.dietTypes.push(dietType);
        this.chosenItems = deleted;
    }

    public handleKeywordChanged(keyword: string): void {
        if (keyword === '') {
            this.keywordSubject.next(null);
            this.keyword = null;
        } else if (keyword) {
            this.keywordSubject.next(keyword);
            this.keyword = keyword;
        }
    }

    public async dismiss(apply?: boolean) {
        if (apply) {
            await this.modalCtrl.dismiss(this.chosenItems);
        } else {
            await this.modalCtrl.dismiss();
        }
    }

    public ngOnInit(): void {
        this.subscriber.subscribe(
            this,
            combineLatest([this.keyword$]).pipe(
                mergeMap(([keyword]) => {
                    return this.service.getDietTypes({ keyword: keyword ? keyword : null })
                })
            )
        )
        this.subscriber.subscribe(
            this,
            this.dietTypes$.pipe(
                tap(dietTypes => {
                    this.dietTypes = dietTypes
                    if (this.chosenItems.length > 0) {
                        this.chosenItems.forEach(item => {
                            this.dietTypes = this.dietTypes.filter(ingr => ingr.id !== item.id);
                        })
                    }
                    this.cdRef.markForCheck()
                })
            )
        )
    }

    public ngOnDestroy() {
        this.subscriber.flush(this);
        this.chosenItems = [];
    }

    public ionViewDidLeave(): void {
        this.subscriber.flush(this);
    }
}

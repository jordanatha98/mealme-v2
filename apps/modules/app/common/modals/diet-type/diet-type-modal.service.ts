import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { MealmeApiClient } from '@mealme/src/api/clients/api.client';
import { catchError, filter, take, tap } from 'rxjs/operators';
import { HttpUtil } from '@shared/utils/http.util';
import { mapToArrayClass, mapToData } from '@shared/transformers/responses.transformer';
import { HttpErrorResponse } from '@angular/common/http';
import { DietType } from '@mealme/src/domain/modules/common/models/diet-type';

@Injectable()
export class DietTypeModalService {
    private readonly dietTypesSubject: BehaviorSubject<DietType[]> = new BehaviorSubject<DietType[]>(undefined);
    public dietTypes$: Observable<DietType[]> = this.dietTypesSubject.asObservable();

    private readonly finalDietTypesSubject: BehaviorSubject<DietType[]> = new BehaviorSubject<DietType[]>(undefined);
    public finalDietTypes$: Observable<DietType[]> = this.finalDietTypesSubject.asObservable();

    private readonly dietTypesLoadingSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public dietTypesLoading$: Observable<boolean> = this.dietTypesLoadingSubject.asObservable();

    public constructor(private client: MealmeApiClient) {
    }

    public searchDietTypes(keyword?: string): Observable<any> {
        return this.dietTypes$.pipe(
            filter(dietTypes$ => !!dietTypes$),
            take(1),
            tap(dietTypes => {
                if (dietTypes) {
                    if (keyword) {
                        this.finalDietTypesSubject.next(dietTypes.filter(
                            (s) => s.name?.toLowerCase().indexOf(keyword ? keyword.toLowerCase() : '') !== -1
                        ));
                    } else {
                        this.finalDietTypesSubject.next(dietTypes);
                    }
                }
            }),
        );
    }

    public getDietTypes(params?: any): Observable<any> {
        this.dietTypesLoadingSubject.next(true);

        return this.client.get(`diet-types`, { params: HttpUtil.queryParamsExtractor(params) }).pipe(
            mapToData(),
            mapToArrayClass(DietType),
            tap(response => {
                this.dietTypesLoadingSubject.next(false);

                if (response && Array.isArray(response)) {
                    this.dietTypesSubject.next(response);
                    this.finalDietTypesSubject.next(response);
                } else {
                    this.dietTypesSubject.next(null);
                    this.finalDietTypesSubject.next(null);
                }
            }),
            catchError((e: HttpErrorResponse) => of(e).pipe(
                tap(() => {
                    this.dietTypesLoadingSubject.next(false);
                    this.dietTypesSubject.next(null);
                    this.finalDietTypesSubject.next(null);
                }),
            )),
        );
    }
}

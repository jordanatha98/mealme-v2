import { AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MealRepository } from '@mealme/src/domain/modules/meal/repositories/meal.repository';
import { ActionSheetController, AnimationController, IonContent, ModalController } from '@ionic/angular';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscriber } from '@ubud/sate';
import { MealmeStore } from '@mealme/src/domain/stores/mealme.store';
import { filter, first, mergeMap, tap } from 'rxjs/operators';
import { TabRouting } from '@mealme/app/modules/app/common/components/tabs/stripper/stripper.tab.base';
import { MenuDetailInteractionRepository } from '@mealme/src/interaction/modules/meal/repositories/menu-detail-interaction-repository';
import { FetchMenu } from '@mealme/src/domain/modules/meal/messages/commands/fetch-menu';
import { FetchMenuIngredients } from '@mealme/src/domain/modules/meal/messages/commands/fetch-menu-ingredients';
import { FetchMenuNutrients } from '@mealme/src/domain/modules/meal/messages/commands/fetch-menu-nutrients';
import { FetchMenuInstructions } from '@mealme/src/domain/modules/meal/messages/commands/fetch-menu-instructions';
import { Menu } from '@mealme/src/domain/modules/meal/models/menu';
import { MenuInstruction } from '@mealme/src/domain/modules/meal/models/menu-instruction';
import { MenuIngredient } from '@mealme/src/domain/modules/meal/models/menu-ingredient';
import { MenuNutrient } from '@mealme/src/domain/modules/meal/models/menu-nutrient';
import { BookmarkManageInteractionRepository } from '@mealme/src/interaction/modules/meal/repositories/bookmark-manage-interaction.repository';
import { BookmarkInteraction } from '@mealme/src/interaction/modules/meal/enums/bookmark-interaction';
import { MealmeInteractionStore } from '@mealme/src/interaction/stores/mealme-interaction.store';
import { BookmarkManageInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/bookmark-manage-interaction-changed';
import { User } from '@mealme/src/domain/modules/auth/models/user';
import { UserRepository } from '@mealme/src/domain/modules/auth/repositories/user.repository';
import { CreateBookmark } from '@mealme/src/domain/modules/meal/messages/commands/create-bookmark';
import { DietTypeModal } from '@mealme/app/modules/app/common/modals/diet-type/diet-type.modal';
import { NutrientModal } from '@mealme/app/modules/app/meal/modals/nutrient/nutrient.modal';

@Component({
    selector: 'mealme-meal-detail-page',
    templateUrl: './detail.page.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [MealRepository, MenuDetailInteractionRepository, BookmarkManageInteractionRepository, UserRepository],
    styleUrls: ['./detail.page.scss']
})
export class DetailPage implements AfterViewInit, OnInit, OnDestroy {
    @ViewChild(IonContent) ionContent: IonContent;
    @ViewChild('toolbarEl') public toolbarEl: any;
    @ViewChild('tabContent') public tabContent: ElementRef;

    @ViewChild('ingredientsContent') public ingredientsContent: ElementRef;
    @ViewChild('instructionsContent') public instructionsContent: ElementRef;
    @ViewChild('reviewsContent') public reviewsContent: ElementRef;

    private readonly showTitleSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public showTitleDelta = 145;
    public showTitle$: Observable<boolean>;

    public tabs: TabRouting[];

    public activeTab = 'ingredients';

    public isScrolled: boolean = false;

    private readonly changeToolbarBackgroundSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    public changeToolbarBackground$: Observable<boolean>;

    public menu$: Observable<Menu>;
    public menuInstructions$: Observable<MenuInstruction[]>;
    public menuIngredients$: Observable<MenuIngredient[]>;
    public menuNutrients$: Observable<MenuNutrient[]>;
    public trimmedMenuNutrients$: Observable<MenuNutrient[]>;

    public loading$: Observable<boolean>;
    public nutrientsLoading$: Observable<boolean>;
    public ingredientsLoading$: Observable<boolean>;
    public instructionsLoading$: Observable<boolean>;

    public bookmarkLoading$: Observable<boolean>;
    public bookmarkError$: Observable<string>;
    public bookmarkSuccess$: Observable<string>;
    public bookmarkInteraction$: Observable<BookmarkInteraction>;

    public authenticatedUser$: Observable<User>;

    public constructor(
        private router: Router,
        public activatedRoute: ActivatedRoute,
        private subscriber: Subscriber,
        private animationCtrl: AnimationController,
        private store: MealmeStore,
        private repository: MealRepository,
        private modalCtrl: ModalController,
        private interactionRepo: MenuDetailInteractionRepository,
        private actionSheetController: ActionSheetController,
        private bookmarkInteractionRepo: BookmarkManageInteractionRepository,
        private interactionStore: MealmeInteractionStore,
        private userRepo: UserRepository,
    ) {
        this.showTitle$ = this.showTitleSubject.asObservable();
        this.changeToolbarBackground$ = this.changeToolbarBackgroundSubject.asObservable();

        this.menu$ = this.repository.getMenu$();
        this.menuIngredients$ = this.repository.getMenuIngredients$();
        this.menuInstructions$ = this.repository.getMenuInstructions$();
        this.menuNutrients$ = this.repository.getMenuNutrients$();
        this.trimmedMenuNutrients$ = this.repository.getMenuNutrients$().pipe(
            filter(item => !!item),
            mergeMap(item => {
                item[2].name = 'Carbohydrate'
                return of([item[0], item[2], item[5], item[6], item[7]])
            })
        )

        this.loading$ = this.interactionRepo.isProcess$();
        this.nutrientsLoading$ = this.interactionRepo.isNutrientsProcess$();
        this.ingredientsLoading$ = this.interactionRepo.isIngredientsProcess$();
        this.instructionsLoading$ = this.interactionRepo.isInstructionsProcess$();

        this.bookmarkLoading$ = this.bookmarkInteractionRepo.isProcess$();
        this.bookmarkError$ = this.bookmarkInteractionRepo.getError$();
        this.bookmarkSuccess$ = this.bookmarkInteractionRepo.getSuccess$();
        this.bookmarkInteraction$ = this.bookmarkInteractionRepo.getInteraction$();

        this.authenticatedUser$ = this.userRepo.getAuthenticatedUser$();

        this.tabs = [
            {
                title: 'Ingredients',
                path: `/app/blank/meals/${ this.activatedRoute.snapshot.params.meal }`,
                code: 'ingredients',
            },
            {
                title: 'Instructions',
                path: `/app/blank/meals/${ this.activatedRoute.snapshot.params.meal }`,
                code: 'instructions',
            },
            {
                title: 'Reviews',
                path: `/app/blank/meals/${ this.activatedRoute.snapshot.params.meal }`,
                code: 'reviews',
            },
        ]

        this.activeTab = 'ingredients';
    }

    public async handleOpenAction() {
        const { meal } = this.activatedRoute.snapshot.params;
        const actionSheet = await this.actionSheetController.create({
            header: 'Actions',
            mode: 'ios',
            buttons: [
                {
                    text: 'Bookmark',
                    cssClass: 'text-secondary',
                    handler: () => {
                        this.subscriber.subscribe(
                            this,
                            this.authenticatedUser$.pipe(
                                first(),
                                tap((user) => {
                                    this.store.dispatch(new CreateBookmark({ customer: user.customer.id, menu: meal }))
                                })
                            )
                        )
                        return false;
                    }
                },
                {
                    text: 'Cancel',
                    cssClass: 'text-danger',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        await actionSheet.present();

        const { role, data } = await actionSheet.onDidDismiss();
        console.log('onDidDismiss resolved with role and data', role, data);
    }

    public handleInteraction(): void {
        this.subscriber.subscribe(
            this,
            this.bookmarkInteraction$.pipe(
                tap(interaction => {
                    if (interaction === BookmarkInteraction.CREATE_SUCCEED) {
                        this.actionSheetController.dismiss().then();
                    }

                    if (interaction === BookmarkInteraction.CREATE_FAILED) {
                        setTimeout(() => {
                            this.handleResetInteraction();
                        }, 3000);
                    }
                }),
            )
        );
    }

    public handleResetInteraction(): void {
        this.interactionStore.dispatch(new BookmarkManageInteractionChanged({
            changes: {
                process: false,
                error: null,
                success: null,
                interaction: BookmarkInteraction.IDLE,
                response: null,
            },
        }));
    }

    public handleScroll(content: CustomEvent): void {
        const { detail } = content;

        if (detail) {
            const { currentY } = detail;

            const value = this.showTitleSubject.value;

            if (currentY >= this.showTitleDelta) {
                if (!value) {
                    this.showTitleSubject.next(true);
                    this.changeToolbarBackgroundSubject.next(true);
                    this.isScrolled = true;
                }
            } else {
                if (value) {
                    this.showTitleSubject.next(false);
                    this.changeToolbarBackgroundSubject.next(false);
                    this.isScrolled = false;
                }
            }
        }
    }

    public async handleOpenNutrientModal() {
        this.subscriber.subscribe(
            this,
            this.menuNutrients$.pipe(
                first(),
                filter(item => !!item),
                tap(async nutrients => {
                    const modal = await this.modalCtrl.create({
                        component: NutrientModal,
                        mode: 'ios',
                        cssClass: 'modal-h-full',
                        swipeToClose: true,
                        componentProps: {
                            nutrients: nutrients
                        }
                    });

                    await modal.present();
                })
            )
        )
    }

    public async handleTabClicked(type: string) {
        /*const ingredientsTop = this.ingredientsContent.nativeElement.offsetTop;
        const instructionsTop = this.instructionsContent.nativeElement.offsetTop;

        if (type === 'ingredients') {
            await this.ionContent.scrollToPoint(0, ingredientsTop, 300);
        } else if (type === 'instructions') {
            await this.ionContent.scrollToPoint(0, instructionsTop, 300);
        }*/
        this.activeTab = type;
    }

    public ngOnDestroy(): void {
        this.handleResetInteraction();
    }

    public ngOnInit(): void {
        this.handleInteraction();
        const { meal } = this.activatedRoute.snapshot.params;
        this.store.dispatch(new FetchMenu({ menu: meal }))
        this.store.dispatch(new FetchMenuIngredients({ menu: meal }))
        this.store.dispatch(new FetchMenuNutrients({ menu: meal }))
        this.store.dispatch(new FetchMenuInstructions({ menu: meal }))
    }

    public ngAfterViewInit(): void {
        const toolbarEnterAnimation = this.animationCtrl.create()
            .addElement(this.toolbarEl.el)
            .beforeAddClass('bg-primary-contrast')
            .keyframes([
                { offset: 0, background: 'transparent' },
                { offset: 1, background: 'white' },
            ])
            .duration(200)
        ;
        const toolbarLeaveAnimation = this.animationCtrl.create()
            .addElement(this.toolbarEl.el)
            .afterRemoveClass('bg-primary-contrast')
            .keyframes([
                { offset: 0, background: 'white' },
                { offset: 1, background: 'transparent' },
            ])
            .duration(200)
        ;

        this.subscriber.subscribe(
            this,
            this.changeToolbarBackground$.pipe(
                tap(show => {
                    if (show) {
                        toolbarEnterAnimation.play().then();
                        toolbarLeaveAnimation.stop();
                    } else {
                        toolbarLeaveAnimation.play().then();
                        toolbarEnterAnimation.stop();
                    }
                }),
            ),
        );
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { SharedCommonDirectiveModule } from '@shared/modules/common/directives/module';
import { SharedCommonComponentModule } from '@shared/modules/common/components/module';
import { MealmeSharedFeatherIconsModule } from '@shared/modules/feather-icons/feather-icons.module';
import { FormsModule } from '@angular/forms';
import { MelameCommonDatePickerInputModule } from '@mealme/app/modules/app/common/components/inputs/date-picker/module';
import { DetailPage } from '@mealme/app/modules/app/meal/pages/detail/detail.page';
import { MealmePlannerDateSelectorModule } from '@mealme/app/modules/app/planner/components/selectors/date-selector/module';
import { MealmeMealPlanCardModule } from '@mealme/app/modules/app/meal/components/cards/meal-plan/module';
import { MealmeInteractionMealModule } from '@mealme/src/interaction/modules/meal/module';
import { MealmeMealActionModalModule } from '@mealme/app/modules/app/meal/modals/meal-action/module';
import { MealmeCommonStripperTabModule } from '@mealme/app/modules/app/common/components/tabs/stripper/module';
import { MealmeIngredientItemModule } from '@mealme/app/modules/app/meal/components/items/ingredient/module';
import { MealmeCommonSnackbarAlertModule } from '@mealme/app/modules/common/common/components/alerts/snackbar/module';
import { SharedCommonPipeModule } from '@shared/modules/common/pipes/module';
import { MealmeMealNutrientModalModule } from '@mealme/app/modules/app/meal/modals/nutrient/module';
import { MealmeReviewItemModule } from '@mealme/app/modules/app/meal/components/items/review/module';

@NgModule({
    declarations: [DetailPage],
    imports: [
        CommonModule,
        IonicModule,
        SharedCommonDirectiveModule,
        SharedCommonComponentModule,
        MelameCommonDatePickerInputModule,
        RouterModule.forChild([
            {
                path: '',
                component: DetailPage
            },
        ]),
        MealmeSharedFeatherIconsModule,
        FormsModule,
        MealmePlannerDateSelectorModule,
        MealmeMealPlanCardModule,
        MealmeInteractionMealModule,
        MealmeMealActionModalModule,
        MealmeCommonStripperTabModule,
        MealmeIngredientItemModule,
        MealmeCommonSnackbarAlertModule,
        SharedCommonPipeModule,
        MealmeMealNutrientModalModule,
        MealmeReviewItemModule,
    ],
})
export class MealmeMealDetailPageModule {}

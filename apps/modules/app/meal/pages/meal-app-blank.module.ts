import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: ':meal',
                loadChildren: () => import('./detail/module').then(m => m.MealmeMealDetailPageModule),
            },
        ]),
    ],
})
export class MealmeMealAppBlankModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { UbudFormModule } from '@ubud/form';
import { BrMaskerModule } from 'br-mask';
import { MenuBasicForm } from '@mealme/app/modules/app/meal/components/forms/menu-basic/menu-basic.form';
import { MealmeSharedFeatherIconsModule } from '@shared/modules/feather-icons/feather-icons.module';

@NgModule({
    declarations: [MenuBasicForm],
    imports: [
        CommonModule,
        IonicModule,
        UbudFormModule,
        BrMaskerModule,
        MealmeSharedFeatherIconsModule,
    ],
    exports: [MenuBasicForm],
})
export class MealmeMealMenuBasicFormModule {}

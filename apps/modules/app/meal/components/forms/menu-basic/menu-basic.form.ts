import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormComponent } from '@ubud/form';
import { MenuDto } from '@mealme/src/domain/modules/meal/dtos/menu-dto';
import { LoadingController, Platform, ToastController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Directory, Filesystem } from '@capacitor/filesystem';
import { Camera, CameraResultType, CameraSource, Photo } from '@capacitor/camera';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
    selector: 'mealme-meal-menu-basic-form',
    templateUrl: './menu-basic.form.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuBasicForm extends FormComponent<MenuDto> implements OnInit {
    public image: File;
    public image$: Observable<any>;
    public imageSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

    constructor(
        private plt: Platform,
        private http: HttpClient,
        private loadingCtrl: LoadingController,
        private toastCtrl: ToastController,
        private cdRef: ChangeDetectorRef,
    ) {
        super();
        this.image$ = this.imageSubject.asObservable();
    }

    // Little helper
    public async presentToast(text) {
        const toast = await this.toastCtrl.create({
            message: text,
            duration: 3000
        });
        toast.present();
    }

    public async selectImage() {
        const image = await Camera.getPhoto({
            quality: 90,
            allowEditing: false,
            resultType: CameraResultType.Uri,
            source: CameraSource.Photos // Camera, Photos or Prompt!
        });

        if (image) {
            this.saveImage(image)
        }
    }

    public async saveImage(photo: Photo) {
        const base64Data = await this.readAsBase64(photo);
        const response = await fetch(photo.webPath);
        const blob = await response.blob();
        this.image = await new File([blob], "example", { type: "image/jpeg" })
        this.imageSubject.next({
            name: 'image',
            data: base64Data
        })
        this.form.formGroup.get('image').reset(this.image);
        this.cdRef.markForCheck();
    }

    private async readAsBase64(photo: Photo) {
        if (this.plt.is('hybrid')) {
            const file = await Filesystem.readFile({
                path: photo.path
            });

            return file.data;
        }
        else {
            const response = await fetch(photo.webPath);
            const blob = await response.blob();

            return await this.convertBlobToBase64(blob) as string;
        }
    }

    public convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
        const reader = new FileReader;
        reader.onerror = reject;
        reader.onload = () => {
            resolve(reader.result);
        };
        reader.readAsDataURL(blob);
    });

    public async ngOnInit() {
    }
}

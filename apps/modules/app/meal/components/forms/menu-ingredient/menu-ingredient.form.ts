import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormComponent } from '@ubud/form';
import { MenuIngredientDto } from '@mealme/src/domain/modules/meal/dtos/menu-ingredient-dto';

@Component({
    selector: 'mealme-meal-menu-ingredient-form',
    templateUrl: './menu-ingredient.form.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuIngredientForm extends FormComponent<MenuIngredientDto> {}

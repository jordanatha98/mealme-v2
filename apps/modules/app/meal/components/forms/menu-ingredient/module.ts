import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { UbudFormModule } from '@ubud/form';
import { MealmeSharedFeatherIconsModule } from '@shared/modules/feather-icons/feather-icons.module';
import { MenuIngredientForm } from '@mealme/app/modules/app/meal/components/forms/menu-ingredient/menu-ingredient.form';

@NgModule({
    declarations: [MenuIngredientForm],
    imports: [
        CommonModule,
        IonicModule,
        UbudFormModule,
        MealmeSharedFeatherIconsModule,
    ],
    exports: [MenuIngredientForm],
})
export class MealmeMealMenuIngredientFormModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { MealmeSharedFeatherIconsModule } from '@shared/modules/feather-icons/feather-icons.module';
import { ReviewItem } from '@mealme/app/modules/app/meal/components/items/review/review.item';

@NgModule({
    declarations: [
        ReviewItem
    ],
    imports: [
        CommonModule,
        IonicModule,
        MealmeSharedFeatherIconsModule
    ],
    exports: [
        ReviewItem
    ],
})
export class MealmeReviewItemModule {
}

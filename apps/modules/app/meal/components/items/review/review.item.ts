import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'mealme-review-item',
    templateUrl: './review.item.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReviewItem {
    @Input() public loading: boolean;
    @Input() public review: any;
}

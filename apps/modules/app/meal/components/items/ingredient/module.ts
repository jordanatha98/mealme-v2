import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { MealmeSharedFeatherIconsModule } from '@shared/modules/feather-icons/feather-icons.module';
import { IngredientItem } from '@mealme/app/modules/app/meal/components/items/ingredient/ingredient.item';

@NgModule({
    declarations: [
        IngredientItem
    ],
    imports: [
        CommonModule,
        IonicModule,
        MealmeSharedFeatherIconsModule
    ],
    exports: [
        IngredientItem
    ],
})
export class MealmeIngredientItemModule {
}

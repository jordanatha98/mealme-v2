import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { MenuIngredient } from '@mealme/src/domain/modules/meal/models/menu-ingredient';

@Component({
    selector: 'mealme-ingredient-item',
    templateUrl: './ingredient.item.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['./ingredient.item.scss']
})
export class IngredientItem {
    @Input() public loading: boolean;
    @Input() public ingredient: MenuIngredient;
}

import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Menu } from '@mealme/src/domain/modules/meal/models/menu';
import { Plan } from '@mealme/src/domain/modules/plan/models/plan';
import { PlanMenu } from '@mealme/src/domain/modules/plan/models/plan-menu';

@Component({
    selector: 'mealme-meal-plan-card',
    templateUrl: './meal-plan.card.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['./meal-plan.card.scss']
})
export class MealPlanCard {
    @Output() public clicked: EventEmitter<any> = new EventEmitter<any>();
    @Output() public actionClicked: EventEmitter<any> = new EventEmitter<any>();
    @Input() public meal: Menu;
    @Input() public plan: Plan;
    @Input() public planMenu: PlanMenu;
    @Input() public loading: boolean;
    @Input() public bigMode: boolean;
}

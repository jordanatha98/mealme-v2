import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { MealmeSharedFeatherIconsModule } from '@shared/modules/feather-icons/feather-icons.module';
import { MealPlanCard } from '@mealme/app/modules/app/meal/components/cards/meal-plan/meal-plan.card';
import { SharedCommonPipeModule } from '@shared/modules/common/pipes/module';

@NgModule({
    declarations: [
        MealPlanCard
    ],
    imports: [
        CommonModule,
        IonicModule,
        MealmeSharedFeatherIconsModule,
        SharedCommonPipeModule
    ],
    exports: [
        MealPlanCard
    ],
})
export class MealmeMealPlanCardModule {
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { MealmeSharedFeatherIconsModule } from '@shared/modules/feather-icons/feather-icons.module';
import { NextMealCard } from '@mealme/app/modules/app/meal/components/cards/next-meal/next-meal.card';
import { SharedCommonPipeModule } from '@shared/modules/common/pipes/module';

@NgModule({
    declarations: [
        NextMealCard
    ],
    imports: [
        CommonModule,
        IonicModule,
        MealmeSharedFeatherIconsModule,
        SharedCommonPipeModule
    ],
    exports: [
        NextMealCard
    ],
})
export class MealmeNextMealCardModule {
}

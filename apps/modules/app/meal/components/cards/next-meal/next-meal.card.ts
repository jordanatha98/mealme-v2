import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Menu } from '@mealme/src/domain/modules/meal/models/menu';
import { PlanMenu } from '@mealme/src/domain/modules/plan/models/plan-menu';
import { Plan } from '@mealme/src/domain/modules/plan/models/plan';
import { Bookmark } from '@mealme/src/domain/modules/meal/models/bookmark';

@Component({
    selector: 'mealme-next-meal-card',
    templateUrl: './next-meal.card.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['./next-meal.card.scss']
})
export class NextMealCard {
    @Output() public itemClicked: EventEmitter<any> = new EventEmitter<any>();
    @Input() public meal: Menu;
    @Input() public loading: boolean;
    @Input() public planMenu: PlanMenu;
    @Input() public plan: Plan;
    @Input() public isBookmark: boolean = false;
    @Input() public bookmark: Bookmark;
}

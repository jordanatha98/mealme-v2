import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { MealmeSharedFeatherIconsModule } from '@shared/modules/feather-icons/feather-icons.module';
import { BookmarkCard } from '@mealme/app/modules/app/meal/components/cards/bookmark/bookmark.card';

@NgModule({
    declarations: [
        BookmarkCard
    ],
    imports: [
        CommonModule,
        IonicModule,
        MealmeSharedFeatherIconsModule
    ],
    exports: [
        BookmarkCard
    ],
})
export class MealmeBookmarkCardModule {
}

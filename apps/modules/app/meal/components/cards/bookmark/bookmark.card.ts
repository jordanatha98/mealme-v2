import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Menu } from '@mealme/src/domain/modules/meal/models/menu';
import { Plan } from '@mealme/src/domain/modules/plan/models/plan';
import { PlanMenu } from '@mealme/src/domain/modules/plan/models/plan-menu';

@Component({
    selector: 'mealme-bookmark-card',
    templateUrl: './bookmark.card.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['./bookmark.card.scss']
})
export class BookmarkCard {
    @Output() public clicked: EventEmitter<any> = new EventEmitter<any>();
    @Output() public actionClicked: EventEmitter<any> = new EventEmitter<any>();
    @Input() public loading: boolean;
    @Input() public meal: Menu;
}

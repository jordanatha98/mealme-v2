import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
    selector: 'mealme-meal-action-modal',
    templateUrl: './meal-action.modal.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MealActionModal {
    public constructor(
        private modalCtrl: ModalController
    ) {
    }

    public async dismiss() {
        await this.modalCtrl.dismiss();
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { MealmeSharedFeatherIconsModule } from '@shared/modules/feather-icons/feather-icons.module';
import { MealActionModal } from '@mealme/app/modules/app/meal/modals/meal-action/meal-action.modal';

@NgModule({
    declarations: [
        MealActionModal
    ],
    imports: [
        CommonModule,
        IonicModule,
        MealmeSharedFeatherIconsModule
    ],
    exports: [
        MealActionModal
    ],
})
export class MealmeMealActionModalModule {
}

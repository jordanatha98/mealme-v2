import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { MealmeSharedFeatherIconsModule } from '@shared/modules/feather-icons/feather-icons.module';
import { NutrientModal } from '@mealme/app/modules/app/meal/modals/nutrient/nutrient.modal';

@NgModule({
    declarations: [
        NutrientModal
    ],
    imports: [
        CommonModule,
        IonicModule,
        MealmeSharedFeatherIconsModule
    ],
    exports: [
        NutrientModal
    ],
})
export class MealmeMealNutrientModalModule {
}

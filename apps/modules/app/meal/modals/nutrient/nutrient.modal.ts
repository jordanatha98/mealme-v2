import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { MenuNutrient } from '@mealme/src/domain/modules/meal/models/menu-nutrient';

@Component({
    selector: 'mealme-nutrient-modal',
    templateUrl: './nutrient.modal.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NutrientModal {
    @Input() nutrients: MenuNutrient[];

    public constructor(
        private modalCtrl: ModalController
    ) {
    }

    public async dismiss() {
        await this.modalCtrl.dismiss();
    }
}

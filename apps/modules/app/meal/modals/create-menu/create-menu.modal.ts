import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { BehaviorSubject, Observable } from 'rxjs';
import { MenuInteraction } from '@mealme/src/interaction/modules/meal/enums/menu-interaction';
import { MealRepository } from '@mealme/src/domain/modules/meal/repositories/meal.repository';
import { MenuManageInteractionRepository } from '@mealme/src/interaction/modules/meal/repositories/menu-manage-interaction.repository';
import { first, take, tap } from 'rxjs/operators';
import { MealmeInteractionStore } from '@mealme/src/interaction/stores/mealme-interaction.store';
import { Subscriber } from '@ubud/sate';
import { MenuManageInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/menu-manage-interaction-changed';
import { Form, FormValue } from '@ubud/form';
import { MenuBasicFormFactory } from '@mealme/src/domain/modules/meal/factories/menu-basic-form.factory';
import { MenuInstructionFormFactory } from '@mealme/src/domain/modules/meal/factories/menu-instruction-form.factory';
import { MenuIngredientFormFactory } from '@mealme/src/domain/modules/meal/factories/menu-ingredient-form.factory';
import { MenuNutrientFormFactory } from '@mealme/src/domain/modules/meal/factories/menu-nutrient-form.factory';
import { MenuDto } from '@mealme/src/domain/modules/meal/dtos/menu-dto';
import { MenuBasicForm } from '@mealme/app/modules/app/meal/components/forms/menu-basic/menu-basic.form';
import { MealmeStore } from '@mealme/src/domain/stores/mealme.store';
import { CreateMenu } from '@mealme/src/domain/modules/meal/messages/commands/menu/create-menu';
import { User } from '@mealme/src/domain/modules/auth/models/user';
import { UserRepository } from '@mealme/src/domain/modules/auth/repositories/user.repository';

@Component({
    selector: 'mealme-meal-create-menu-modal',
    templateUrl: './create-menu.modal.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [MealRepository, MenuManageInteractionRepository, MenuBasicFormFactory, MenuInstructionFormFactory, MenuIngredientFormFactory, MenuNutrientFormFactory, UserRepository]
})
export class CreateMenuModal implements OnInit, OnDestroy {
    @ViewChild('basicFormTpl') public basicFormTpl: MenuBasicForm;
    public step$: Observable<number>;
    public stepSubject: BehaviorSubject<number> = new BehaviorSubject<number>(2);
    public lastStep = 4;

    public user$: Observable<User>;

    public loading$: Observable<boolean>;
    public error$: Observable<string>;
    public success$: Observable<string>;
    public interaction$: Observable<MenuInteraction>;

    public basicForm: Form;
    public instructionForm: Form;
    public ingredientForm: Form;
    public nutrientForm: Form;

    public constructor(
        basicFormFactory: MenuBasicFormFactory,
        instructionFormFactory: MenuInstructionFormFactory,
        ingredientFormFactory: MenuIngredientFormFactory,
        nutrientFormFactory: MenuNutrientFormFactory,
        private subscriber: Subscriber,
        private modalCtrl: ModalController,
        private interactionRepo: MenuManageInteractionRepository,
        private interactionStore: MealmeInteractionStore,
        private store: MealmeStore,
        private cdRef: ChangeDetectorRef,
        private userRepo: UserRepository,
    ) {
        this.basicForm = basicFormFactory.create();
        this.instructionForm = instructionFormFactory.create();
        this.ingredientForm = ingredientFormFactory.create();
        this.nutrientForm = nutrientFormFactory.create();
        this.step$ = this.stepSubject.asObservable();
        this.loading$ = this.interactionRepo.isProcess$();
        this.error$ = this.interactionRepo.getError$();
        this.success$ = this.interactionRepo.getSuccess$();
        this.interaction$ = this.interactionRepo.getInteraction$();
        this.user$ = this.userRepo.getAuthenticatedUser$();
    }

    public async dismiss() {
        await this.modalCtrl.dismiss();
    }


    public async handlePrev() {
        this.subscriber.subscribe(
            this,
            this.step$.pipe(
                first(),
                tap(step => {
                    if (step > 1) {
                        this.stepSubject.next(step - 1);
                    } else {
                        this.modalCtrl.dismiss().then();
                    }
                    this.cdRef.markForCheck();
                }),
            ),
        );
    }

    public async handleNext() {
        this.cdRef.markForCheck();

        this.subscriber.subscribe(
            this,
            this.step$.pipe(
                first(),
                tap((step) => {
                    if (step !== this.lastStep) {
                        this.stepSubject.next(step + 1);
                    }
                    this.cdRef.markForCheck();
                })
            )
        )
    }

    public handleInteraction(): void {
        this.subscriber.subscribe(
            this,
            this.interaction$.pipe(
                tap(interaction => {
                    if (interaction === MenuInteraction.CREATE_SUCCEED) {
                        this.stepSubject.next(2);
                    }
                }),
            )
        );
    }

    public handleResetInteraction(): void {
        this.interactionStore.dispatch(new MenuManageInteractionChanged({
            changes: {
                process: false,
                error: null,
                success: null,
                interaction: MenuInteraction.IDLE,
                response: null,
            },
        }));
    }

    public handleBasicSubmit(payload: FormValue<MenuDto>): void {
        if (payload.status === 'VALID') {
            this.subscriber.subscribe(
                this,
                this.user$.pipe(
                    first(),
                    tap(user => {
                        this.store.dispatch(new CreateMenu({ customer: user.customer.id, payload: payload.data }))
                    })
                )
            )
        }
    }

    public handleSubmits(): void {
        this.subscriber.subscribe(
            this,
            this.step$.pipe(
                take(1),
                tap(step => {
                    if (step === 1) {
                        this.basicFormTpl.submit();
                    }
                })
            )
        )
    }

    public ngOnInit(): void {
        this.handleInteraction();
    }

    public ngOnDestroy(): void {
        this.subscriber.flush(this);
        this.handleResetInteraction();
    }
}

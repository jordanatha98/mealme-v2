import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
    selector: 'mealme-meal-ingredient-form-modal',
    templateUrl: './ingredient-form.modal.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngredientFormModal {
    public constructor(
        private modalCtrl: ModalController
    ) {
    }

    public async dismiss() {
        await this.modalCtrl.dismiss();
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { MealmeSharedFeatherIconsModule } from '@shared/modules/feather-icons/feather-icons.module';
import { MealmeCommonSnackbarAlertModule } from '@mealme/app/modules/common/common/components/alerts/snackbar/module';
import { MealmeSharedStickyModule } from '@shared/modules/sticky/sticky.module';
import { MealmeMealMenuBasicFormModule } from '@mealme/app/modules/app/meal/components/forms/menu-basic/module';
import { MealmeMealMenuIngredientFormModule } from '@mealme/app/modules/app/meal/components/forms/menu-ingredient/module';
import { IngredientFormModal } from '@mealme/app/modules/app/meal/modals/ingredient-form/ingredient-form.modal';

@NgModule({
    declarations: [
        IngredientFormModal
    ],
    imports: [
        CommonModule,
        IonicModule,
        MealmeSharedFeatherIconsModule,
        MealmeCommonSnackbarAlertModule,
        MealmeSharedStickyModule,
        MealmeMealMenuBasicFormModule,
        MealmeMealMenuIngredientFormModule,
    ],
    exports: [
        IngredientFormModal
    ],
})
export class MealmeMealIngredientFormModalModule {
}

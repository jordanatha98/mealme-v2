import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscriber } from '@ubud/sate';
import { combineLatest, Observable, of } from 'rxjs';
import { delay, first, mergeMap, tap } from 'rxjs/operators';
import { IonContent, IonRefresher, ModalController, NavController, Platform } from '@ionic/angular';
import { ScrollPointService, ScrollPointTargets } from '../../../common/services/scroll-point.service';
import { ProductRepository } from '@mealme/src/domain/modules/products/repositories/product.repository';
import { ProductIndexInteractionRepository } from '@mealme/src/interaction/modules/product/repositories/product-index-interaction.repository';
import { MealmeStore } from '@mealme/src/domain/stores/mealme.store';
import { FetchNextMeal } from '@mealme/src/domain/modules/meal/messages/commands/fetch-next-meal';
import { MealRepository } from '@mealme/src/domain/modules/meal/repositories/meal.repository';
import { Menu } from '@mealme/src/domain/modules/meal/models/menu';
import { UserRepository } from '@mealme/src/domain/modules/auth/repositories/user.repository';
import { User } from '@mealme/src/domain/modules/auth/models/user';
import { AppStore } from '@mealme/app/bootstrap/app.store';
import { PersonalizationModal } from '@mealme/app/modules/app/account/modals/personalization/personalization.modal';
import { FetchPlans } from '@mealme/src/domain/modules/meal/messages/commands/fetch-plans';
import { Router } from '@angular/router';
import { Plan } from '@mealme/src/domain/modules/plan/models/plan';
import { PlanMenusInteractionRepository } from '@mealme/src/interaction/modules/meal/repositories/plan-menus-interaction.repository';
import { GeneratePlan } from '@mealme/src/domain/modules/meal/messages/commands/generate-plan';
import { GeneratePlanInteraction } from '@mealme/src/interaction/modules/meal/enums/generate-plan-interaction';
import { GeneratePlanInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/generate-plan-interaction-changed';
import { PlanMenusGenerateInteractionRepository } from '@mealme/src/interaction/modules/meal/repositories/plan-menus-generate-interaction.repository';
import { MealmeInteractionStore } from '@mealme/src/interaction/stores/mealme-interaction.store';
import { CreateMenuModal } from '@mealme/app/modules/app/meal/modals/create-menu/create-menu.modal';

@Component({
    selector: 'mealme-home-page',
    templateUrl: './home.page.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['./home.page.scss'],
    providers: [
        ProductRepository,
        ProductIndexInteractionRepository,
        MealRepository,
        UserRepository,
        PlanMenusInteractionRepository,
        PlanMenusGenerateInteractionRepository,
    ],
})
export class HomePage implements OnInit, OnDestroy {
    @ViewChild(IonRefresher) public refresher: IonRefresher;
    @ViewChild(IonContent) public content: IonContent;

    public random: number = Math.random();
    public loading$: Observable<boolean>;
    public meals$: Observable<Menu[]>;
    public plan$: Observable<Plan>;
    public yesterdayPlans$: Observable<Plan>;

    public generateLoading$: Observable<boolean>;
    public generateError$: Observable<string>;
    public generateSuccess$: Observable<string>;
    public generateInteraction$: Observable<GeneratePlanInteraction>;

    public today: Date;

    public authenticatedUser$: Observable<User>;

    public coverflowEffect: any = {
        rotate: 50,
        stretch: 0,
        depth: 100,
        modifier: 1,
        slideShadows: true
    }

    public constructor(
        private scrollPoint: ScrollPointService,
        private appStore: AppStore,
        private subscriber: Subscriber,
        private navCtrl: NavController,
        public platform: Platform,
        private modalCtrl: ModalController,
        private repo: MealRepository,
        private interactionRepo: PlanMenusInteractionRepository,
        private store: MealmeStore,
        private userRepo: UserRepository,
        private router: Router,
        private generateInteractionRepo: PlanMenusGenerateInteractionRepository,
        private interactionStore: MealmeInteractionStore,
    ) {
        this.meals$ = this.repo.getMeals$();
        this.loading$ = this.interactionRepo.isProcess$();
        this.authenticatedUser$ = this.userRepo.getAuthenticatedUser$();
        this.plan$ = this.repo.getHomePlans$().pipe(
            mergeMap(plans => {
                if (plans) {
                    return of(plans.data[0])
                } else {
                    return of(null)
                }
            })
        );
        this.yesterdayPlans$ = this.repo.getYesterdayPlans$().pipe(
            mergeMap(plans => {
                if (plans) {
                    return of(plans.data[0])
                } else {
                    return of(null)
                }
            })
        );
        this.today = new Date();
        this.generateLoading$ = this.generateInteractionRepo.isProcess$();
        this.generateError$ = this.generateInteractionRepo.getError$();
        this.generateSuccess$ = this.generateInteractionRepo.getSuccess$();
        this.generateInteraction$ = this.generateInteractionRepo.getInteraction$();
    }

    public async handleNavigate(path: string, root?: boolean) {
        if (root) {
            await this.navCtrl.navigateRoot(path);
        } else {
            await this.navCtrl.navigateForward(path);
        }
    }

    public async redirectAccount() {
        await this.router.navigateByUrl('/app/tabs/account');
    }

    public pad(value) {
        return value.toString().padStart(2, '0');
    }

    public handleGenerateClicked(): void {
        const today = new Date();
        if (today.getHours() >= 20) {
            today.setDate(today.getDate() + 1);
        }
        let formatted = `${today.getFullYear()}-${this.pad(today.getMonth() + 1)}-${this.pad(today.getDate())}`;
        this.subscriber.subscribe(
            this,
            this.authenticatedUser$.pipe(
                first(),
                tap(user => {
                    this.store.dispatch(new GeneratePlan({ customer: user.customer.id, payload: { dates: [formatted]} }))
                })
            )
        )
    }


    public handleInteraction(): void {
        this.subscriber.subscribe(
            this,
            combineLatest([this.generateInteraction$, this.authenticatedUser$]).pipe(
                tap(([interaction, user]) => {
                    if (interaction === GeneratePlanInteraction.GENERATE_SUCCEED) {
                        this.store.dispatch(new FetchPlans({ customer: user.customer.id, queries: { startDate: this.today, endDate: this.today }, home: true }));
                    }

                    if (interaction === GeneratePlanInteraction.GENERATE_FAILED ) {
                        setTimeout(() => {
                            this.handleResetInteraction();
                        }, 3000);
                    }
                }),
            )
        );
    }

    public handleResetInteraction(): void {
        this.interactionStore.dispatch(new GeneratePlanInteractionChanged({
            changes: {
                process: false,
                error: null,
                success: null,
                interaction: GeneratePlanInteraction.IDLE
            }
        }))
    }

    public async handleOpenAction() {
        const modal = await this.modalCtrl.create({
            component: PersonalizationModal,
            mode: 'ios',
            cssClass: 'modal-h-full',
            swipeToClose: true,
        });

        await modal.present();
    }

    public async handleCreateMenu() {
        const modal = await this.modalCtrl.create({
            component: CreateMenuModal,
            mode: 'ios',
            cssClass: 'modal-h-full',
            swipeToClose: true,
        });

        await modal.present();
    }

    public handleReload(refresher?: any): void {
        this.appStore.flush();

        if (refresher) {
            this.subscriber.subscribe(
                this,
                of(null).pipe(
                    delay(2000),
                    tap(() => {
                        refresher.target.complete();
                    }),
                ),
            );
        }
    }

    public ngOnInit(): void {
        this.handleCreateMenu();
        this.handleInteraction();
        const today = new Date();
        const startDate = today.getFullYear() + '-' + (((today.getMonth() + 1).toString().length < 2) ? '0' + (today.getMonth() + 1) : (today.getMonth() + 1)) + '-' + ((today.getDate().toString().length < 2) ? '0' + today.getDate() : today.getDate());
        const yesterdayDate = today.getFullYear() + '-' + (((today.getMonth() + 1).toString().length < 2) ? '0' + (today.getMonth() + 1) : (today.getMonth() + 1)) + '-' + ((today.getDate().toString().length < 2) ? '0' + (today.getDate() - 1) : (today.getDate() - 1));
        this.handleReload();
        this.store.dispatch(new FetchNextMeal());
        this.subscriber.subscribe(
            this,
            this.authenticatedUser$.pipe(
                first(),
                tap(user => {
                    this.store.dispatch(new FetchPlans({ customer: user.customer.id, queries: { startDate: startDate, endDate: startDate }, home: true }));
                    this.store.dispatch(new FetchPlans({ customer: user.customer.id, queries: { startDate: yesterdayDate, endDate: yesterdayDate }, yesterday: true }));
                    if (user.customer.preferencesSet === false) {
                        this.handleOpenAction().then();
                    }
                })
            )
        )
    }

    public ngOnDestroy(): void {
        this.handleResetInteraction();
        this.subscriber.flush(this);
    }

    public ionViewDidEnter() {
        this.random = Math.random();
        this.scrollPoint.init(this, ScrollPointTargets.HOME, this.content);
    }

    public ionViewWillLeave() {
        this.subscriber.flush(this);
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { MealmeInteractionMealmeProductModule } from '@mealme/src/interaction/modules/product/module';
import { SharedCommonDirectiveModule } from '@shared/modules/common/directives/module';
import { SharedCommonComponentModule } from '@shared/modules/common/components/module';
import { MealmeInteractionMealModule } from '@mealme/src/interaction/modules/meal/module';
import { HomePage } from '@mealme/app/modules/app/home/pages/home/home.page';
import { MealmeMealPlanCardModule } from '@mealme/app/modules/app/meal/components/cards/meal-plan/module';
import { MealmeAccountPersonalizationModalModule } from '@mealme/app/modules/app/account/modals/personalization/personalization.modal.module';
import { MealmeNextMealCardModule } from '@mealme/app/modules/app/meal/components/cards/next-meal/module';
import { SwiperModule } from 'swiper/angular';
import { MealmeSharedFeatherIconsModule } from '@shared/modules/feather-icons/feather-icons.module';
import { SharedCommonPipeModule } from '@shared/modules/common/pipes/module';
import { MealmeMealCreateMenuModalModule } from '@mealme/app/modules/app/meal/modals/create-menu/module';

@NgModule({
    declarations: [HomePage],
    imports: [
        CommonModule,
        IonicModule,
        MealmeInteractionMealmeProductModule,
        SharedCommonDirectiveModule,
        SharedCommonComponentModule,
        MealmeMealPlanCardModule,
        MealmeInteractionMealModule,
        SharedCommonPipeModule,
        RouterModule.forChild([
            {
                path: '',
                component: HomePage,
            },
        ]),
        MealmeNextMealCardModule,
        MealmeAccountPersonalizationModalModule,
        SwiperModule,
        MealmeSharedFeatherIconsModule,
        MealmeMealCreateMenuModalModule,
    ],
})
export class MealmeHomeHomePageModule {}

import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { ModalController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscriber } from '@ubud/sate';
import { filter, map, take, tap } from 'rxjs/operators';
import { Form, FormValue } from '@ubud/form';
import { LoginFormFactory } from '@mealme/src/domain/modules/auth/factories/login-form.factory';
import { LoginDto } from '@mealme/src/domain/modules/auth/dtos/login-dto';
import { Authenticate } from '@mealme/src/domain/modules/auth/messages/commands/auth/authenticate';
import { MealmeStore } from '@mealme/src/domain/stores/mealme.store';
import { AuthLoginInteraction } from '@mealme/src/interaction/modules/auth/enums/auth-login-interaction';
import { AuthRepository } from '@mealme/src/domain/modules/auth/repositories/auth.repository';
import { LoginInteractionRepository } from '@mealme/src/interaction/modules/auth/repositories/auth/login-interaction.repository';
import { MealmeInteractionStore } from '@mealme/src/interaction/stores/mealme-interaction.store';
import { LoginInteractionChanged } from '@mealme/src/interaction/modules/auth/messages/events/login-interaction-changed';
import { FetchAuthenticatedUser } from '@mealme/src/domain/modules/auth/messages/commands/user/fetch-authenticated-user';
import { SignUpModal } from '@mealme/app/modules/app/auth/modals/sign-up.modal';

@Component({
    selector: 'mealme-auth-landing-container',
    templateUrl: './landing.container.html',
    styleUrls: ['./landing.container.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [LoginFormFactory, AuthRepository, LoginInteractionRepository]
})
export class LandingContainer implements OnInit, OnDestroy {
    @Input() public hideBanner: boolean;
    @Output() public callbackTriggered: EventEmitter<void | string> = new EventEmitter<void | string>();

    public form: Form;

    public error$: Observable<string>;
    public interaction$: Observable<AuthLoginInteraction>;
    public loading$: Observable<boolean>;
    public success$: Observable<string>;

    // public user$: Observable<User>;
    public logoutProcess$: Observable<boolean>;

    public constructor(
        formFactory: LoginFormFactory,
        protected modalCtrl: ModalController,
        protected route: ActivatedRoute,
        protected subscriber: Subscriber,
        protected store: MealmeStore,
        protected authRepo: AuthRepository,
        protected loginInteractionRepo: LoginInteractionRepository,
        protected interactionStore: MealmeInteractionStore,
        protected router: Router,
    ) {
        this.form = formFactory.create();
        this.error$ = this.loginInteractionRepo.getError$();
        this.interaction$ = this.loginInteractionRepo.getInteraction$();
        this.loading$ = this.loginInteractionRepo.isProcess$();
        this.success$ = this.loginInteractionRepo.getSuccess$();
    }

    public get callback$(): Observable<string> {
        return this.route.data.pipe(
            map(({ callback }) => {
                return callback;
            }),
        );
    }

    public handleCallback(callback?: string): void {
        this.callbackTriggered.emit(callback);
    }

    public handleSubmit(payload: FormValue<LoginDto>): void {
        if ('VALID' === payload.status) {
            const { data } = payload;

            this.store.dispatch(new Authenticate({
                payload: data,
            }));
        }
    }

    public async handleRegisterClicked() {
        this.subscriber.subscribe(
            this,
            this.callback$.pipe(
                take(1),
                tap(callback => {
                    this.modalCtrl.create({
                        component: SignUpModal,
                        mode: 'ios',
                        swipeToClose: false,
                        cssClass: 'modal-h-full',
                    }).then(modal => {
                        modal.present().then();

                        modal.onDidDismiss().then(({ data }) => {
                            if (data) {
                                const { succeed } = data;

                                if (succeed) {
                                    this.handleCallback(callback);
                                }
                            }
                        });
                    });
                }),
            ),
        );
    }

    public async redirectAuth() {
        await this.router.navigateByUrl('/app/tabs/home');
    }

    public handleInteraction(): void {
        this.subscriber.subscribe(
            this,
            this.interaction$.pipe(
                tap(interaction => {
                    if (interaction === AuthLoginInteraction.AUTHENTICATE_SUCCEED) {
                        this.redirectAuth().then();
                    }
                }),
            )
        );
    }

    public handleResetInteraction(): void {
        this.interactionStore.dispatch(new LoginInteractionChanged({
            changes: {
                process: false,
                error: null,
                success: null,
                interaction: AuthLoginInteraction.IDLE,
            },
        }));
    }

    public handleLogout(): void {
        // this.store.dispatch(new SignOut());
    }

    public ngOnInit(): void {
        this.handleInteraction();
        this.subscriber.subscribe(
            this,
            this.authRepo.getSignature$().pipe(
                filter(signature => !!signature),
                take(1),
                tap(signature => {
                    this.store.dispatch(new FetchAuthenticatedUser({
                        cacheable: true,
                    }));
                }),
            ),
        );
        /*this.subscriber.subscribe(
            this,
            this.authRepo.getSignature$().pipe(
                filter(signature => !!signature),
                take(1),
                tap(signature => {
                    this.store.dispatch(new FetchAuthenticatedUser({
                        cacheable: true,
                    }));
                }),
            ),
        );*/

        /*this.subscriber.subscribe(
            this,
            this.accountInteractionRepo.getInteraction$().pipe(
                tap(interaction => {
                    if (interaction === AuthLogoutInteraction.SIGN_OUT_SUCCEED) {
                        this.accountInteractionStore.dispatch(new AuthLogoutInteractionChanged({
                            changes: {
                                process: false,
                                interaction: AuthLogoutInteraction.IDLE,
                            },
                        }));
                    }
                }),
            ),
        );*/
    }

    public ngOnDestroy(): void {
        this.subscriber.flush(this);
        this.handleResetInteraction();
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { MealmeSharedFeatherIconsModule } from '@shared/modules/feather-icons/feather-icons.module';
import { LandingContainer } from '@mealme/app/modules/app/auth/containers/landing/landing.container';
import { MealmeCommonAuthLoginFormModule } from '@mealme/app/modules/common/auth/components/forms/login/module';
import { MealmeAuthBasicMerchantSignupModule } from '@mealme/app/modules/app/auth/components/basic-signup/module';
import { MealmeAuthSignUpModalModule } from '@mealme/app/modules/app/auth/modals/sign-up-modal.module';
import { MealmeCommonSnackbarAlertModule } from '@mealme/app/modules/common/common/components/alerts/snackbar/module';

@NgModule({
    declarations: [LandingContainer],
    imports: [
        CommonModule,
        IonicModule,
        MealmeSharedFeatherIconsModule,
        MealmeCommonAuthLoginFormModule,
        MealmeAuthBasicMerchantSignupModule,
        MealmeAuthSignUpModalModule,
        MealmeCommonSnackbarAlertModule,
    ],
    exports: [LandingContainer],
})
export class MealmeAuthLandingContainerModule {}

import { ChangeDetectionStrategy, Component, OnDestroy, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { LandingContainer } from '@mealme/app/modules/app/auth/containers/landing/landing.container';

@Component({
    selector: 'mealme-auth-landing-page',
    templateUrl: './landing.page.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LandingPage implements OnDestroy {
    @ViewChild(LandingContainer) public containerTpl: LandingContainer;

    public constructor(
        private router: Router,
    ) {
    }

    public handleCallback(callback: string): void {
        this.router.navigateByUrl(callback).then(() => {
            this.ngOnDestroy();
        });
    }

    public ngOnDestroy(): void {
        if (this.containerTpl) {
            this.containerTpl.ngOnDestroy();
        }
    }
}

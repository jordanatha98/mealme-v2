import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { LandingPage } from '@mealme/app/modules/app/auth/pages/landing/landing.page';
import { MealmeAuthLandingContainerModule } from '@mealme/app/modules/app/auth/containers/landing/module';

@NgModule({
    declarations: [LandingPage],
    imports: [
        CommonModule,
        IonicModule,
        MealmeAuthLandingContainerModule,
        RouterModule.forChild([
            {
                path: '',
                component: LandingPage
            },
        ]),
    ],
})
export class MealmeAuthLandingPageModule {}

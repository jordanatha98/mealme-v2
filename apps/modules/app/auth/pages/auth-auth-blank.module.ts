import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: 'landing',
                loadChildren: () => import('../../../../modules/app/auth/pages/landing/landing.module').then(m => m.MealmeAuthLandingPageModule),
            },
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'landing',
            },
        ]),
    ],
})
export class MealmeAuthAuthBlankModule {
}

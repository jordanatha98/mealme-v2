import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

export interface OptionCard {
    title: string;
    text: string;
    icon: string;
}

@Component({
    selector: 'mealme-auth-signup-option-card',
    templateUrl: './signup-option.card.html',
    styleUrls: ['./signup-option.card.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class SignupOptionCard {
    @Input() public data: OptionCard;
    @Output() public itemClicked: EventEmitter<OptionCard> = new EventEmitter<OptionCard>();
}

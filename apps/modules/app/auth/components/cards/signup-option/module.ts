import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { UbudFormModule } from '@ubud/form';
import { BrMaskerModule } from 'br-mask';
import { MealmeSharedFeatherIconsModule } from '@shared/modules/feather-icons/feather-icons.module';
import { SignupOptionCard } from '@mealme/app/modules/app/auth/components/cards/signup-option/signup-option.card';
import { MealmeCommonItemCardModule } from '@mealme/app/modules/common/common/components/cards/item/module';

@NgModule({
    declarations: [SignupOptionCard],
    imports: [
        CommonModule,
        IonicModule,
        UbudFormModule,
        BrMaskerModule,
        MealmeCommonItemCardModule,
        MealmeSharedFeatherIconsModule,
    ],
    exports: [SignupOptionCard],
})
export class MealmeAuthSignupOptionCardModule {}

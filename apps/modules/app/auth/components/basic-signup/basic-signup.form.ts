import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormComponent } from '@ubud/form';
import { MerchantBasicSignupDto } from '@mealme/src/domain/modules/auth/dtos/merchant-basic-signup-dto';

@Component({
    selector: 'mealme-auth-basic-signup-form',
    templateUrl: './basic-signup.form.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class BasicSignupForm extends FormComponent<MerchantBasicSignupDto> {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { UbudFormModule } from '@ubud/form';
import { BrMaskerModule } from 'br-mask';
import { BasicSignupForm } from '@mealme/app/modules/app/auth/components/basic-signup/basic-signup.form';

@NgModule({
    declarations: [BasicSignupForm],
    imports: [
        CommonModule,
        IonicModule,
        UbudFormModule,
        BrMaskerModule,
    ],
    exports: [BasicSignupForm],
})
export class MealmeAuthBasicMerchantSignupModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { UbudFormModule } from '@ubud/form';
import { BrMaskerModule } from 'br-mask';
import { FormsModule } from '@angular/forms';
import { PasswordSignupForm } from '@mealme/app/modules/app/auth/components/password-signup/password-signup.form';
import { MealmeCommonItemCardModule } from '@mealme/app/modules/common/common/components/cards/item/module';
import { MealmeCommonAuthPasswordInputModule } from '@mealme/app/modules/common/auth/inputs/password/module';

@NgModule({
    declarations: [PasswordSignupForm],
    imports: [
        CommonModule,
        IonicModule,
        UbudFormModule,
        BrMaskerModule,
        MealmeCommonItemCardModule,
        MealmeCommonAuthPasswordInputModule,
        FormsModule,
    ],
    exports: [PasswordSignupForm],
})
export class MealmeAuthPasswordMerchantSignupModule {}

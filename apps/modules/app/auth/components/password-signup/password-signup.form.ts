import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormComponent } from '@ubud/form';
import { MerchantPasswordSignupDto } from '@mealme/src/domain/modules/auth/dtos/merchant-password-signup-dto';

@Component({
    selector: 'mealme-auth-password-signup-form',
    templateUrl: './password-signup.form.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PasswordSignupForm extends FormComponent<MerchantPasswordSignupDto> {
    public receiveOffer: boolean;

    public handleToggleOffer(show: boolean): void {
        this.form.formGroup.get('receiveOffer').reset(show);
    }
}

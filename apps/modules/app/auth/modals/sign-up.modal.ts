import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { IonContent, ModalController } from '@ionic/angular';
import { Subscriber } from '@ubud/sate';
import { filter, first, tap } from 'rxjs/operators';
import { Form, FormValue } from '@ubud/form';
import { BasicSignupFormFactory } from '@mealme/src/domain/modules/auth/factories/basic-signup-form.factory';
import { CodeFormFactory } from '@mealme/src/domain/modules/auth/factories/code-form.factory';
import { PasswordSignupFormFactory } from '@mealme/src/domain/modules/auth/factories/password-signup-form.factory';
import { UserManageInteractionRepository } from '@mealme/src/interaction/modules/auth/repositories/user/user-manage-interaction.repository';
import { MealmeInteractionStore } from '@mealme/src/interaction/stores/mealme-interaction.store';
import { MealmeStore } from '@mealme/src/domain/stores/mealme.store';
import { UserManageInteraction } from '@mealme/src/interaction/modules/auth/enums/user-manage-interaction';
import { UserDto } from '@mealme/src/domain/modules/auth/dtos/user-dto';
import { CreateUser } from '@mealme/src/domain/modules/auth/messages/commands/user/create-user';
import { UserManageInteractionChanged } from '@mealme/src/interaction/modules/auth/messages/events/user-manage-interaction-changed';
import { UserRepository } from '@mealme/src/domain/modules/auth/repositories/user.repository';
import { User } from '@mealme/src/domain/modules/auth/models/user';
import { CodeDto } from '@mealme/src/domain/modules/auth/dtos/code-dto';
import { VerifyUser } from '@mealme/src/domain/modules/auth/messages/commands/user/verify-user';
import { MerchantPasswordSignupDto } from '@mealme/src/domain/modules/auth/dtos/merchant-password-signup-dto';
import { CreateCustomer } from '@mealme/src/domain/modules/auth/messages/commands/user/create-customer';
import { BasicSignupForm } from '@mealme/app/modules/app/auth/components/basic-signup/basic-signup.form';
import { CodeForm } from '@mealme/app/modules/common/auth/components/forms/code/code.form';
import { PasswordSignupForm } from '@mealme/app/modules/app/auth/components/password-signup/password-signup.form';

@Component({
    selector: 'mealme-auth-sign-up-modal',
    templateUrl: './sign-up.modal.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        BasicSignupFormFactory,
        CodeFormFactory,
        PasswordSignupFormFactory,
        UserManageInteractionRepository,
        MealmeInteractionStore,
        MealmeStore,
        UserRepository,
    ],
})
export class SignUpModal implements OnInit, OnDestroy {
    @ViewChild(IonContent) public ionContent: IonContent;
    @ViewChild('basicFormTpl', { static: false }) public basicFormTpl: BasicSignupForm;
    @ViewChild('codeFormTpl', { static: false }) public codeFormTpl: CodeForm;
    @ViewChild('passwordFormTpl', { static: false }) public passwordFormTpl: PasswordSignupForm;

    public basicForm: Form;
    public codeForm: Form;
    public passwordForm: Form;

    public step$: Observable<number>;
    public stepSubject: BehaviorSubject<number> = new BehaviorSubject<number>(1);
    public lastStep = 3;

    public loading$: Observable<boolean>;
    public error$: Observable<string>;
    public success$: Observable<string>;
    public interaction$: Observable<UserManageInteraction>;

    public userToken: string;

    public user$: Observable<User>;

    public constructor(
        basicFormFactory: BasicSignupFormFactory,
        codeFormFactory: CodeFormFactory,
        passwordFormFactory: PasswordSignupFormFactory,
        private subscriber: Subscriber,
        private cdRef: ChangeDetectorRef,
        private modalCtrl: ModalController,
        private store: MealmeStore,
        private manageInteractionRepo: UserManageInteractionRepository,
        private interactionStore: MealmeInteractionStore,
        private userRepo: UserRepository,
    ) {
        this.basicForm = basicFormFactory.create();
        this.codeForm = codeFormFactory.create();
        this.passwordForm = passwordFormFactory.create();
        this.step$ = this.stepSubject.asObservable();
        this.loading$ = this.manageInteractionRepo.isProcess$();
        this.error$ = this.manageInteractionRepo.getError$();
        this.success$ = this.manageInteractionRepo.getSuccess$();
        this.interaction$ = this.manageInteractionRepo.getInteraction$();
        this.user$ = this.userRepo.getUser$();
    }

    public handleCreateUser(payload: FormValue<UserDto>) {
        this.store.dispatch(new CreateUser({ payload: payload.data }));
    }

    public handleVerifyUser(payload: FormValue<CodeDto>) {
        this.subscriber.subscribe(
            this,
            this.user$.pipe(
                filter(user => !!user),
                first(),
                tap(user => {
                    this.codeForm.formGroup.get('token').reset(user.emailVerificationToken);
                    this.store.dispatch(new VerifyUser({ user: user.id, payload: this.codeForm.formGroup.value }));
                }),
            )
        );
    }

    public handleCreateCustomer(payload: FormValue<MerchantPasswordSignupDto>) {
        this.subscriber.subscribe(
            this,
            this.user$.pipe(
                filter(user => !!user),
                first(),
                tap(user => {
                    this.passwordForm.formGroup.get('token').reset(user.registrationToken);
                    this.store.dispatch(new CreateCustomer({ user: user.id, payload: this.passwordForm.formGroup.value }));
                }),
            )
        );
    }

    public async handlePrev() {
        this.subscriber.subscribe(
            this,
            this.step$.pipe(
                first(),
                tap(step => {
                    if (step > 1) {
                        this.stepSubject.next(step - 1);
                        this.ionContent.scrollToTop(500).then();
                        if (step === 5) {
                        } else if (step === 4) {
                        }
                    } else {
                        this.modalCtrl.dismiss().then();
                    }

                    this.cdRef.markForCheck();
                }),
            ),
        );
    }

    public async handleNext(request?: boolean) {
        this.ionContent.scrollToTop(500).then();
        this.cdRef.markForCheck();

        this.subscriber.subscribe(
            this,
            this.step$.pipe(
                first(),
                tap((step) => {
                    if (step !== this.lastStep) {
                        if (step === 2 && request === true) {
                            this.codeFormTpl.submit();
                        } else if (step === 1 && request === true) {
                            this.basicFormTpl.submit();
                        }
                    } else {
                        this.passwordFormTpl.submit();
                    }
                    if (request === false) {
                        this.stepSubject.next(step + 1);
                    }
                    this.cdRef.markForCheck();
                })
            )
        )
    }

    public handleInteraction(): void {
        this.subscriber.subscribe(
            this,
            this.interaction$.pipe(
                tap(interaction => {
                    if (interaction === UserManageInteraction.CREATE_SUCCEED) {
                        this.handleNext(false).then();
                    }

                    if (interaction === UserManageInteraction.CREATE_FAILED || interaction === UserManageInteraction.VERIFY_FAILED ) {
                        setTimeout(() => {
                            this.handleResetInteraction();
                        }, 3000);
                    }

                    if (interaction === UserManageInteraction.VERIFY_SUCCEED) {
                        this.handleNext(false).then();
                    }

                    if (interaction === UserManageInteraction.CREATE_CUSTOMER_SUCCEED) {
                        setTimeout(() => {
                            this.modalCtrl.dismiss().then();
                        }, 1000);
                    }

                    if (interaction === UserManageInteraction.CREATE_MERCHANT_SUCCEED) {
                        setTimeout(() => {
                            this.modalCtrl.dismiss().then();
                        }, 1000);
                    }
                }),
            )
        );
    }

    public handleResetInteraction(): void {
        this.interactionStore.dispatch(new UserManageInteractionChanged({
            changes: {
                process: false,
                error: null,
                success: null,
                interaction: UserManageInteraction.IDLE,
                response: null,
            },
        }));
    }

    public ngOnInit(): void {
        this.handleInteraction();
    }

    public ngOnDestroy(): void {
        this.handleResetInteraction();
    }
}

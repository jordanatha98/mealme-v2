import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { MealmeInteractionAuthModule } from '@mealme/src/interaction/modules/auth/module';
import { MealmeSharedStickyModule } from '@shared/modules/sticky/sticky.module';
import { SignUpModal } from '@mealme/app/modules/app/auth/modals/sign-up.modal';
import { MealmeAuthBasicMerchantSignupModule } from '@mealme/app/modules/app/auth/components/basic-signup/module';
import { MealmeCommonAuthCodeFormModule } from '@mealme/app/modules/common/auth/components/forms/code/module';
import { MealmeAuthPasswordMerchantSignupModule } from '@mealme/app/modules/app/auth/components/password-signup/module';
import { MealmeAuthSignupOptionCardModule } from '@mealme/app/modules/app/auth/components/cards/signup-option/module';
import { MealmeCommonSnackbarAlertModule } from '@mealme/app/modules/common/common/components/alerts/snackbar/module';

@NgModule({
    declarations: [SignUpModal],
    imports: [
        CommonModule,
        IonicModule,
        MealmeAuthBasicMerchantSignupModule,
        MealmeCommonAuthCodeFormModule,
        MealmeAuthPasswordMerchantSignupModule,
        MealmeAuthSignupOptionCardModule,
        MealmeInteractionAuthModule,
        MealmeCommonSnackbarAlertModule,
        MealmeSharedStickyModule,
    ],
    exports: [SignUpModal]
})
export class MealmeAuthSignUpModalModule {
}

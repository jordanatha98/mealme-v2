import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { SharedCommonDirectiveModule } from '@shared/modules/common/directives/module';
import { SharedCommonComponentModule } from '@shared/modules/common/components/module';
import { MealmeSharedFeatherIconsModule } from "@shared/modules/feather-icons/feather-icons.module";
import { BookmarkPage } from '@mealme/app/modules/app/account/pages/bookmark/bookmark.page';
import { MealmeInteractionMealmeStoreModule } from '@mealme/src/interaction/stores/module';
import { MealmeCommonSnackbarAlertModule } from '@mealme/app/modules/common/common/components/alerts/snackbar/module';
import { MealmeInteractionMealModule } from '@mealme/src/interaction/modules/meal/module';
import { MealmeNextMealCardModule } from '@mealme/app/modules/app/meal/components/cards/next-meal/module';

@NgModule({
    declarations: [BookmarkPage],
    imports: [
        CommonModule,
        IonicModule,
        SharedCommonDirectiveModule,
        SharedCommonComponentModule,
        MealmeInteractionMealmeStoreModule,
        MealmeInteractionMealModule,
        RouterModule.forChild([
            {
                path: '',
                component: BookmarkPage
            },
        ]),
        MealmeSharedFeatherIconsModule,
        MealmeCommonSnackbarAlertModule,
        MealmeNextMealCardModule,
    ],
})
export class MealmeAccountBookmarkPageModule {}

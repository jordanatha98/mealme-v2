import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { UserRepository } from '@mealme/src/domain/modules/auth/repositories/user.repository';
import { Observable } from 'rxjs';
import { User } from '@mealme/src/domain/modules/auth/models/user';
import { Subscriber } from '@ubud/sate';
import { first, tap } from 'rxjs/operators';
import { ActionSheetController, ModalController, NavController } from '@ionic/angular';
import { PreferencesModal } from '@mealme/app/modules/app/account/modals/preferences/preferences.modal';
import { MealmeStore } from '@mealme/src/domain/stores/mealme.store';
import { MealmeInteractionStore } from '@mealme/src/interaction/stores/mealme-interaction.store';
import { AuthLogoutInteraction } from '@mealme/src/interaction/modules/auth/enums/auth-logout-interaction';
import { LogoutInteractionRepository } from '@mealme/src/interaction/modules/auth/repositories/auth/logout-interaction.repository';
import { SignOut } from '@mealme/src/domain/modules/auth/messages/commands/auth/sign-out';
import { LogoutInteractionChanged } from '@mealme/src/interaction/modules/auth/messages/events/logout-interaction-changed';
import { FetchBookmarks } from '@mealme/src/domain/modules/meal/messages/commands/fetch-bookmarks';
import { BookmarkIndexInteractionRepository } from '@mealme/src/interaction/modules/meal/repositories/bookmark-index-interaction.repository';
import { BookmarkManageInteractionRepository } from '@mealme/src/interaction/modules/meal/repositories/bookmark-manage-interaction.repository';
import { MealRepository } from '@mealme/src/domain/modules/meal/repositories/meal.repository';
import { BookmarkInteraction } from '@mealme/src/interaction/modules/meal/enums/bookmark-interaction';
import { Bookmark } from '@mealme/src/domain/modules/meal/models/bookmark';
import { Collection } from '@shared/types/collection';
import { CreateBookmark } from '@mealme/src/domain/modules/meal/messages/commands/create-bookmark';
import { DeleteBookmark } from '@mealme/src/domain/modules/meal/messages/commands/delete-bookmark';
import { Book } from 'angular-feather/icons';
import { BookmarkManageInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/bookmark-manage-interaction-changed';

@Component({
    selector: 'mealme-account-bookmark-page',
    templateUrl: './bookmark.page.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [UserRepository, LogoutInteractionRepository, BookmarkIndexInteractionRepository, BookmarkManageInteractionRepository, MealRepository]
})
export class BookmarkPage implements OnInit, OnDestroy {
    public authenticatedUser$: Observable<User>;

    public loading$: Observable<boolean>;

    public manageLoading$: Observable<boolean>;
    public manageSuccess$: Observable<string>;
    public manageError$: Observable<string>;
    public interaction$: Observable<BookmarkInteraction>;

    public bookmarks$: Observable<Collection<Bookmark>>;

    public constructor(
        private userRepo: UserRepository,
        private subscriber: Subscriber,
        private modalCtrl: ModalController,
        private store: MealmeStore,
        private interactionStore: MealmeInteractionStore,
        private interactionRepo: BookmarkIndexInteractionRepository,
        private manageInteractionRepo: BookmarkManageInteractionRepository,
        private repo: MealRepository,
        private navCtrl: NavController,
        private actionSheetController: ActionSheetController,
    ) {
        this.authenticatedUser$ = this.userRepo.getAuthenticatedUser$();
        this.loading$ = this.interactionRepo.isProcess$();
        this.interaction$ = this.manageInteractionRepo.getInteraction$();
        this.manageLoading$ = this.manageInteractionRepo.isProcess$();
        this.manageSuccess$ = this.manageInteractionRepo.getSuccess$();
        this.manageError$ = this.manageInteractionRepo.getError$();
        this.bookmarks$ = this.repo.getBookmarks$();
    }

    public async handleNavigate(path: string, root?: boolean) {
        if (root) {
            await this.navCtrl.navigateRoot(path);
        } else {
            await this.navCtrl.navigateForward(path);
        }
    }

    public async handleOpenAction(bookmark: Bookmark) {
        const actionSheet = await this.actionSheetController.create({
            header: 'Actions',
            mode: 'ios',
            buttons: [
                {
                    text: 'View Menu',
                    cssClass: 'text-secondary',
                    handler: () => {
                        this.handleNavigate(`app/blank/meals/${bookmark.menuId}`)
                    }
                },
                {
                    text: 'Remove from bookmark',
                    cssClass: 'text-danger',
                    handler: () => {
                        this.store.dispatch(new DeleteBookmark({ bookmark: bookmark.id }))
                        return false;
                    }
                },
                {
                    text: 'Cancel',
                    cssClass: 'text-danger',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        await actionSheet.present();

        const { role, data } = await actionSheet.onDidDismiss();
        console.log('onDidDismiss resolved with role and data', role, data);
    }

    public handleInteraction(): void {
        this.subscriber.subscribe(
            this,
            this.interaction$.pipe(
                tap(interaction => {
                    if (interaction === BookmarkInteraction.DELETE_SUCCEED) {
                        this.subscriber.subscribe(
                            this,
                            this.authenticatedUser$.pipe(
                                first(),
                                tap(user => {
                                    this.store.dispatch(new FetchBookmarks({ customer: user.customer.id }))
                                })
                            )
                        )
                        this.actionSheetController.dismiss().then();
                    }

                    if (interaction === BookmarkInteraction.DELETE_FAILED) {
                        setTimeout(() => {
                            this.handleResetInteraction();
                        }, 3000);
                    }
                }),
            )
        );
    }

    public handleResetInteraction(): void {
        this.interactionStore.dispatch(new BookmarkManageInteractionChanged({
            changes: {
                process: false,
                error: null,
                success: null,
                interaction: BookmarkInteraction.IDLE,
                response: null,
            },
        }));
    }

    public ngOnDestroy(): void {
        this.handleResetInteraction();
        this.subscriber.flush(this)
    }

    public ngOnInit(): void {
        this.handleInteraction();
        this.subscriber.subscribe(
            this,
            this.authenticatedUser$.pipe(
                first(),
                tap(user => {
                    this.store.dispatch(new FetchBookmarks({ customer: user.customer.id }))
                })
            )
        )
    }
}

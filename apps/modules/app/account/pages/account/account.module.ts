import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { MealmeInteractionMealmeProductModule } from '@mealme/src/interaction/modules/product/module';
import { SharedCommonDirectiveModule } from '@shared/modules/common/directives/module';
import { SharedCommonComponentModule } from '@shared/modules/common/components/module';
import { AccountPage } from '@mealme/app/modules/app/account/pages/account/account.page';
import { MealmeAccountPersonalizationModalModule } from '@mealme/app/modules/app/account/modals/personalization/personalization.modal.module';
import { MealmeSharedFeatherIconsModule } from "@shared/modules/feather-icons/feather-icons.module";
import { MealmeCommonListItemModule } from "@mealme/app/modules/app/common/components/items/list/module";
import { MealmeCommonDataListItemModule } from '@mealme/app/modules/app/common/components/items/data-list/module';
import { MealmeAccountPreferencesModalModule } from '@mealme/app/modules/app/account/modals/preferences/preferences.modal.module';
import { MealmeInteractionAuthModule } from '@mealme/src/interaction/modules/auth/module';

@NgModule({
    declarations: [AccountPage],
    imports: [
        CommonModule,
        IonicModule,
        MealmeInteractionMealmeProductModule,
        SharedCommonDirectiveModule,
        SharedCommonComponentModule,
        MealmeAccountPersonalizationModalModule,
        MealmeAccountPreferencesModalModule,
        RouterModule.forChild([
            {
                path: '',
                component: AccountPage
            },
        ]),
        MealmeSharedFeatherIconsModule,
        MealmeCommonListItemModule,
        MealmeCommonDataListItemModule,
        MealmeInteractionAuthModule,
    ],
})
export class MealmeAccountAccountPageModule {}

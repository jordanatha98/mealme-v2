import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { UserRepository } from '@mealme/src/domain/modules/auth/repositories/user.repository';
import { Observable } from 'rxjs';
import { User } from '@mealme/src/domain/modules/auth/models/user';
import { Subscriber } from '@ubud/sate';
import { tap } from 'rxjs/operators';
import { ModalController, NavController } from '@ionic/angular';
import { PreferencesModal } from '@mealme/app/modules/app/account/modals/preferences/preferences.modal';
import { MealmeStore } from '@mealme/src/domain/stores/mealme.store';
import { MealmeInteractionStore } from '@mealme/src/interaction/stores/mealme-interaction.store';
import { AuthLogoutInteraction } from '@mealme/src/interaction/modules/auth/enums/auth-logout-interaction';
import { LogoutInteractionRepository } from '@mealme/src/interaction/modules/auth/repositories/auth/logout-interaction.repository';
import { SignOut } from '@mealme/src/domain/modules/auth/messages/commands/auth/sign-out';
import { LogoutInteractionChanged } from '@mealme/src/interaction/modules/auth/messages/events/logout-interaction-changed';

@Component({
    selector: 'mealme-account-account-page',
    templateUrl: './account.page.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [UserRepository, LogoutInteractionRepository]
})
export class AccountPage implements OnInit, OnDestroy {
    public authenticatedUser$: Observable<User>;

    public loading$: Observable<boolean>;
    public interaction$: Observable<AuthLogoutInteraction>;

    public constructor(
        private userRepo: UserRepository,
        private subscriber: Subscriber,
        private modalCtrl: ModalController,
        private store: MealmeStore,
        private interactionStore: MealmeInteractionStore,
        private interactionRepo: LogoutInteractionRepository,
        private navCtrl: NavController,
    ) {
        this.authenticatedUser$ = this.userRepo.getAuthenticatedUser$();
        this.loading$ = this.interactionRepo.isProcess$();
        this.interaction$ = this.interactionRepo.getInteraction$();
    }

    public async handleNavigate(path: string, root?: boolean) {
        if (root) {
            await this.navCtrl.navigateRoot(path);
        } else {
            await this.navCtrl.navigateForward(path);
        }
    }

    public async handleOpenAction() {
        const modal = await this.modalCtrl.create({
            component: PreferencesModal,
            mode: 'ios',
            cssClass: 'bottom-modal modal-h-auto modal-rounded',
            swipeToClose: true,
        });

        await modal.present();
    }

    public handleLogout(): void {
        this.store.dispatch(new SignOut());
    }

    public handleInteraction(): void {
        this.subscriber.subscribe(
            this,
            this.interaction$.pipe(
                tap(interaction => {
                    if (interaction === AuthLogoutInteraction.SIGN_OUT_SUCCEED) {
                        this.handleNavigate('/app/auth-blank/auth/landing').then();
                    }
                }),
            )
        );
    }

    public handleResetInteraction(): void {
        this.interactionStore.dispatch(new LogoutInteractionChanged({
            changes: {
                process: false,
                error: null,
                interaction: AuthLogoutInteraction.IDLE,
            },
        }));
    }

    public ngOnDestroy(): void {
        this.handleResetInteraction();
        this.subscriber.flush(this)
    }

    public ngOnInit(): void {
        this.handleInteraction();
    }
}

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                loadChildren: () => import('./account/account.module').then(m => m.MealmeAccountAccountPageModule),
            },
            {
                path: 'bookmark',
                loadChildren: () => import('./bookmark/bookmark.module').then(m => m.MealmeAccountBookmarkPageModule),
            },
        ]),
    ],
})
export class MealmeAccountAppBlankModule {}

import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MealmeStore } from '@mealme/src/domain/stores/mealme.store';
import { UserRepository } from '@mealme/src/domain/modules/auth/repositories/user.repository';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { Subscriber } from '@ubud/sate';
import { IonContent, ModalController } from '@ionic/angular';
import { MealmeInteractionStore } from '@mealme/src/interaction/stores/mealme-interaction.store';
import { Ingredient } from '@mealme/src/domain/modules/common/models/ingredient';
import { first, tap } from 'rxjs/operators';
import { DietType } from '@mealme/src/domain/modules/common/models/diet-type';
import { SetPreferences } from '@mealme/src/domain/modules/account/messages/commands/set-preferences';
import { User } from '@mealme/src/domain/modules/auth/models/user';
import { observableToBeFn } from 'rxjs/internal/testing/TestScheduler';
import { PreferencesManageInteraction } from '@mealme/src/interaction/modules/account/enums/preferences-manage-interaction';
import { UserManageInteractionRepository } from '@mealme/src/interaction/modules/auth/repositories/user/user-manage-interaction.repository';
import { PreferencesManageRepository } from '@mealme/src/interaction/modules/account/repositories/preferences-manage.repository';
import { UserManageInteraction } from '@mealme/src/interaction/modules/auth/enums/user-manage-interaction';
import { UserManageInteractionChanged } from '@mealme/src/interaction/modules/auth/messages/events/user-manage-interaction-changed';
import { PreferencesManageInteractionChanged } from '@mealme/src/interaction/modules/account/messages/events/preferences-manage-interaction-changed';
import { IngredientModal } from '@mealme/app/modules/app/common/modals/ingredient/ingredient.modal';
import { DietTypeModal } from '@mealme/app/modules/app/common/modals/diet-type/diet-type.modal';
import {
    FetchAuthenticatedUser
} from '@mealme/src/domain/modules/auth/messages/commands/user/fetch-authenticated-user';

@Component({
    selector: 'mealme-account-personalization-modal',
    templateUrl: './personalization.modal.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        MealmeStore,
        UserRepository,
        PreferencesManageRepository,
    ],
})
export class PersonalizationModal implements OnInit, OnDestroy {
    @ViewChild(IonContent) public ionContent: IonContent;

    public step$: Observable<number>;
    public stepSubject: BehaviorSubject<number> = new BehaviorSubject<number>(1);
    public lastStep = 4;

    public chosenLikes: Ingredient[] = [];
    public chosenDislikes: Ingredient[] = [];
    public chosenAllergies: Ingredient[] = [];
    public chosenDietTypes: DietType[] = [];

    public authenticatedUser$: Observable<User>;

    public loading$: Observable<boolean>;
    public error$: Observable<string>;
    public success$: Observable<string>;
    public interaction$: Observable<PreferencesManageInteraction>;

    public constructor(
        private subscriber: Subscriber,
        private cdRef: ChangeDetectorRef,
        private modalCtrl: ModalController,
        private store: MealmeStore,
        private interactionStore: MealmeInteractionStore,
        private userRepo: UserRepository,
        private interactionRepo: PreferencesManageRepository,
    ) {
        this.step$ = this.stepSubject.asObservable();
        this.authenticatedUser$ = this.userRepo.getAuthenticatedUser$();
        this.loading$ = this.interactionRepo.isProcess$();
        this.error$ = this.interactionRepo.getError$();
        this.success$ = this.interactionRepo.getSuccess$();
        this.interaction$ = this.interactionRepo.getInteraction$();
    }

    public async handleOpenModal(type: string) {
        let typeByChosen;
        if (type === 'likes') {
            typeByChosen = this.chosenLikes
        } else if (type === 'dislikes') {
            typeByChosen = this.chosenDislikes;
        } else if (type === 'allergies') {
            typeByChosen = this.chosenAllergies;
        }
        const modal = await this.modalCtrl.create({
            component: IngredientModal,
            mode: 'ios',
            cssClass: 'modal-h-full',
            swipeToClose: false,
            componentProps: {
                chosenItems: typeByChosen,
                chosenLikes: this.chosenLikes,
                chosenDislikes: this.chosenDislikes,
                type,
            }
        });

        await modal.present();
        await modal.onDidDismiss().then(({ data }) => {
            if (data) {
                if (type === 'likes') {
                    this.chosenLikes = data
                } else if (type === 'dislikes') {
                    this.chosenDislikes = data
                } else if (type === 'allergies') {
                    this.chosenAllergies = data
                }
                this.cdRef.markForCheck();
            }
        });
    }

    public async handleOpenDietModal() {
        const modal = await this.modalCtrl.create({
            component: DietTypeModal,
            mode: 'ios',
            cssClass: 'modal-h-full',
            swipeToClose: false,
            componentProps: {
                chosenItems: this.chosenDietTypes,
            }
        });

        await modal.present();
        await modal.onDidDismiss().then(({ data }) => {
            if (data) {
                this.chosenDietTypes = data;
                this.cdRef.markForCheck();
            }
        });
    }

    public handleNext(): void {
        this.ionContent.scrollToTop(500).then();
        this.cdRef.markForCheck();

        this.subscriber.subscribe(
            this,
            combineLatest([this.step$, this.userRepo.getAuthenticatedUser$()]).pipe(
                first(),
                tap(([step, user]) => {
                    if (step !== this.lastStep) {
                        this.stepSubject.next(step + 1)
                    } else {
                        const payload = {
                            likes: this.chosenLikes.map(item => item.id),
                            dislikes: this.chosenDislikes.map(item => item.id),
                            allergies: this.chosenAllergies.map(item => item.id),
                            dietTypes: this.chosenDietTypes.map(item => item.id),
                        }
                        this.store.dispatch(new SetPreferences({ customer: user.customer.id, payload }))
                    }
                })
            )
        )
    }

    public handlePrev(): void {
        this.ionContent.scrollToTop(500).then();
        this.cdRef.markForCheck();

        this.subscriber.subscribe(
            this,
            this.step$.pipe(
                first(),
                tap(step => {
                    if (step !== 1) {
                        this.stepSubject.next(step - 1);
                    } else {
                        this.modalCtrl.dismiss().then();
                    }
                })
            )
        )
    }

    public handleInteraction(): void {
        this.subscriber.subscribe(
            this,
            this.interaction$.pipe(
                tap(interaction => {
                    if (interaction === PreferencesManageInteraction.SET_SUCCEED) {
                        this.store.dispatch(new FetchAuthenticatedUser());
                        setTimeout(() => {
                            this.modalCtrl.dismiss().then();
                        }, 1000);
                    }

                    if (interaction === PreferencesManageInteraction.SET_FAILED ) {
                        setTimeout(() => {
                            this.handleResetInteraction();
                        }, 3000);
                    }
                }),
            )
        );
    }

    public handleResetInteraction(): void {
        this.interactionStore.dispatch(new PreferencesManageInteractionChanged({
            changes: {
                process: false,
                error: null,
                success: null,
                interaction: PreferencesManageInteraction.IDLE,
                response: null,
            },
        }));
    }

    public ngOnInit(): void {
        this.handleInteraction();
    }

    public ngOnDestroy(): void {
        this.handleResetInteraction();
    }
}

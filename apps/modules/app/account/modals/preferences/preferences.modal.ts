import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { MealmeStore } from '@mealme/src/domain/stores/mealme.store';
import { UserRepository } from '@mealme/src/domain/modules/auth/repositories/user.repository';
import {
    PreferencesManageRepository
} from '@mealme/src/interaction/modules/account/repositories/preferences-manage.repository';
import { Observable } from 'rxjs';
import { User } from '@mealme/src/domain/modules/auth/models/user';
import { IngredientModal } from '@mealme/app/modules/app/common/modals/ingredient/ingredient.modal';
import { Ingredient } from '@mealme/src/domain/modules/common/models/ingredient';
import { DietType } from '@mealme/src/domain/modules/common/models/diet-type';
import { ModalController } from '@ionic/angular';
import { Subscriber } from '@ubud/sate';
import { filter, first, tap } from 'rxjs/operators';

@Component({
    selector: 'mealme-account-preferences-modal',
    templateUrl: './preferences.modal.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        MealmeStore,
        UserRepository,
        PreferencesManageRepository,
    ],
})
export class PreferencesModal implements OnInit, OnDestroy {
    public authenticatedUser$: Observable<User>;

    public chosenLikes: Ingredient[] = [];
    public chosenDislikes: Ingredient[] = [];
    public chosenAllergies: Ingredient[] = [];
    public chosenDietTypes: DietType[] = [];

    public chosenLikesText: string = null;
    public chosenDislikeText: string = null;
    public chosenAllergiesText: string = null;
    public chosenDietTypeText: string;

    public constructor(
        private userRepo: UserRepository,
        private modalCtrl: ModalController,
        private cdRef: ChangeDetectorRef,
        private subscriber: Subscriber,
    ) {
        this.authenticatedUser$ = this.userRepo.getAuthenticatedUser$();
    }

    public async handleOpenModal(type: string) {
        let typeByChosen;
        if (type === 'likes') {
            typeByChosen = this.chosenLikes
        } else if (type === 'dislikes') {
            typeByChosen = this.chosenDislikes;
        } else if (type === 'allergies') {
            typeByChosen = this.chosenAllergies;
        }

        const modal = await this.modalCtrl.create({
            component: IngredientModal,
            mode: 'ios',
            cssClass: 'modal-h-full',
            swipeToClose: false,
            componentProps: {
                chosenItems: typeByChosen,
                chosenLikes: this.chosenLikes,
                chosenDislikes: this.chosenDislikes,
                chosenAllergies: this.chosenAllergies,
                type,
                withRequest: true,
            }
        });

        await modal.present();
        await modal.onDidDismiss().then(({ data }) => {
        });
    }

    public ngOnDestroy(): void {
        this.subscriber.flush(this);
    }

    public ngOnInit(): void {
        this.subscriber.subscribe(
            this,
            this.authenticatedUser$.pipe(
                filter(user => !!user),
                tap(user => {
                    if (user.customer.likes.length > 0) {
                        const arr = user.customer.likes.map(item => item.ingredient.name).slice(0,3).join(', ')
                        this.chosenLikesText = arr + (user.customer.likes.length > 3 ? ` and ${user.customer.likes.length - 3} more` : '');
                        this.chosenLikes = user.customer.likes.map(item => item.ingredient);
                        this.cdRef.markForCheck();
                    }
                    if (user.customer.dislikes.length > 0) {
                        const arr = user.customer.dislikes.map(item => item.ingredient.name).slice(0,3).join(', ')
                        this.chosenDislikeText = arr + (user.customer.dislikes.length > 3 ? ` and ${user.customer.dislikes.length - 3} more` : '');
                        this.chosenDislikes = user.customer.dislikes.map(item => item.ingredient);
                        this.cdRef.markForCheck();
                    }
                    if (user.customer.allergies.length > 0) {
                        const arr = user.customer.allergies.map(item => item.ingredient.name).slice(0,3).join(', ')
                        this.chosenAllergiesText = arr + (user.customer.allergies.length > 3 ? ` and ${user.customer.allergies.length - 3} more` : '');
                        this.chosenAllergies = user.customer.allergies.map(item => item.ingredient);
                        this.cdRef.markForCheck();
                    }
                })
            )
        )
    }
}
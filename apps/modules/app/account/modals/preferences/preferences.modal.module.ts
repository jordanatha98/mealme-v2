import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { MealmeSharedStickyModule } from '@shared/modules/sticky/sticky.module';
import { MealmeDomainCommonModule } from '@mealme/src/domain/modules/common/module';
import { MealmeDomainAccountModule } from '@mealme/src/domain/modules/account/module';
import { MealmeInteractionAccountModule } from '@mealme/src/interaction/modules/account/module';
import { PersonalizationModal } from '@mealme/app/modules/app/account/modals/personalization/personalization.modal';
import { MealmeCommonSnackbarAlertModule } from '@mealme/app/modules/common/common/components/alerts/snackbar/module';
import { MealmeCommonListItemModule } from '@mealme/app/modules/app/common/components/items/list/module';
import { MealmeCommonIngredientModalModule } from '@mealme/app/modules/app/common/modals/ingredient/ingredient.modal.module';
import { MealmeCommonDietTypeModalModule } from '@mealme/app/modules/app/common/modals/diet-type/diet-type.modal.module';
import { PreferencesModal } from '@mealme/app/modules/app/account/modals/preferences/preferences.modal';
import { MealmeAccountAuthDomainModule } from '@mealme/app/domains/auth/module';

@NgModule({
    declarations: [PreferencesModal],
    imports: [
        CommonModule,
        IonicModule,
        MealmeCommonListItemModule,
        MealmeCommonIngredientModalModule,
        MealmeCommonDietTypeModalModule,
        MealmeDomainAccountModule,
        MealmeInteractionAccountModule,
        MealmeAccountAuthDomainModule,
    ],
    exports: [PreferencesModal]
})
export class MealmeAccountPreferencesModalModule {
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { IndexPage } from '@mealme/app/modules/app/search/pages/index/index.page';

@NgModule({
    declarations: [IndexPage],
    imports: [
        CommonModule,
        IonicModule,
        RouterModule.forChild([
            {
                path: '',
                component: IndexPage
            },
        ]),
    ],
})
export class MealmeSearchIndexPageModule {}

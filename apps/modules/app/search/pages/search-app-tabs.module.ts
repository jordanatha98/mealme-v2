import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                loadChildren: () => import('./index/module').then(m => m.MealmeSearchIndexPageModule),
            },
        ]),
    ],
})
export class MealmeSearchAppTabsModule {}

import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class AppStore {
    public data: any = {};

    public save(key: any, value: any): void {
        const k = `[${window.location.href}]|${key}`;

        if (this.data[k]) {
            this.data[k] = [
                ...this.data[k],
                value,
            ];
        } else {
            this.data[k] = [value];
        }
    }

    public get(key: any): any {
        return this.data[`[${window.location.href}]|${key.toString()}`];
    }

    public getFirst(key: any): any {
        const data = this.get(key);

        if (data) {
            return data[0];
        }

        return null;
    }

    public getLast(key: any): any {
        const data = this.get(key);

        if (data) {
            return data[data.length - 1];
        }

        return null;
    }

    public flush(): void {
        const prefix = `[${window.location.href}]`;
        for (const key in this.data) {
            const str = key.split('|');

            if (str.length >= 0) {
                if (str[0] === prefix) {
                    delete this.data[key];
                }
            }
        }
    }
}

import { Component, NgZone, OnInit } from '@angular/core';
import { IonRouterOutlet, LoadingController, ModalController, NavController, Platform } from '@ionic/angular';
import { SplashScreen } from '@capacitor/splash-screen';
import { StatusBar, Style } from '@capacitor/status-bar';
import { Subscriber } from '@ubud/sate';
import { Router } from '@angular/router';
import { filter, tap } from 'rxjs/operators';
import { AppService } from '@mealme/app/bootstrap/app.service';

@Component({
    selector: 'mealme-app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
    public constructor(
        private platform: Platform,
        private service: AppService,
        private zone: NgZone,
        private subscriber: Subscriber,
        private router: Router,
        private loadingCtrl: LoadingController,
        private navCtrl: NavController,
        private modalCtrl: ModalController,
    ) {
    }

    public ngOnInit(): void {
        this.bindingNative();
    }

    public handleActivatedRouter(): void {
        this.platform.ready().then(() => {
            SplashScreen.hide().then();
        });
    }

    public bindingNative(): void {
        if (this.platform.is('cordova')) {
            window.screen.orientation.lock('portrait');

            if (this.platform.is('ios')) {
                StatusBar.setStyle({style: Style.Light});
            } else {
                StatusBar.setStyle({style: Style.Light});
                StatusBar.setOverlaysWebView({overlay: false});
                StatusBar.setBackgroundColor({color: '00aa13'});
            }
        }
    }


    public bindingDeeplink(): void {
        this.subscriber.subscribe(
            this,
            this.service.deeplinkData$.pipe(
                filter(url => !!url),
                tap(url => {
                    if (url) {
                        this.loadingCtrl.create({
                            message: 'Please Wait',
                            duration: 1000,
                        }).then(loading => {
                            loading.present();

                            loading.onDidDismiss().then(() => {
                                this.router.navigateByUrl(url).then();
                            });
                        });
                    }
                }),
            ),
        );
    }
}

import { ActionReducer, META_REDUCERS } from '@ngrx/store';
import { Provider } from '@angular/core';
import { AppStore } from '@mealme/app/bootstrap/app.store';

export function appReducer(store: AppStore) {
    return (reducer: ActionReducer<any>) => {
        /*return (state, action) => {
            const { loggable, TYPE } = action;

            if (action instanceof SignOutSucceed || action instanceof AuthenticateSucceed) {
                store.flush();
            }

            if (loggable) {
                const data = MessageUtil.getPayload(action, ['loggable']);

                if (data && Object.keys(data).length > 0) {
                    store.save(TYPE.toString(), data);
                }
            }

            return reducer(state, action);
        };*/
    };
}

export const MEALME_APP_REDUCER: Provider = {
    useFactory: appReducer,
    provide: META_REDUCERS,
    deps: [AppStore],
    multi: true,
};

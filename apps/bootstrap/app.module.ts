import { registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import localeId from '@angular/common/locales/id';
import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { IonicStorageModule } from '@ionic/storage';
import { MealmeDomainAuthModule } from '@mealme/src/domain/modules/auth/module';
import { MealmeApiModule } from '@mealme/src/api/api.module';
import { MEALME_APP_INITIALIZER } from '@mealme/src/domain/modules/common/core/initializers/app-initializer';
import { IonicStorageAdapter, UbudStorageModule } from '@ubud/storage';
import { AppComponent } from '@mealme/app/bootstrap/app.component';
import { AppRoutingModule } from '@mealme/app/bootstrap/app-routing.module';
import { MealmeAppTemplateModule } from '@mealme/app/templates/app/app-template.module';
import { environment } from '@mealme/app/environments/environment';
import { AppService } from '@mealme/app/bootstrap/app.service';
import { MealmeSharedConfirmationModule } from '@shared/modules/confirmation/confirmation.module';

registerLocaleData(localeId, 'en-US');

@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [
        BrowserModule,
        IonicModule.forRoot({
            mode: 'md',
            swipeBackEnabled: false,
        }),
        StoreModule.forRoot({}, {
            runtimeChecks: {
                strictStateImmutability: false,
                strictActionImmutability: false,
            },
        }),
        EffectsModule.forRoot([]),
        IonicStorageModule.forRoot(),
        UbudStorageModule.forRoot(IonicStorageAdapter),
        MealmeSharedConfirmationModule,
        HttpClientModule,
        AppRoutingModule,
        MealmeAppTemplateModule,
        MealmeDomainAuthModule,
        MealmeApiModule.forRoot(environment.mealmeEndpoint),
    ],
    providers: [
        MEALME_APP_INITIALIZER,
        StatusBar,
        SplashScreen,
        Clipboard,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
        {
            provide: LOCALE_ID,
            useValue: 'en-US',
        },
        AppService,
    ],
    bootstrap: [AppComponent],
})
export class AppModule {
}

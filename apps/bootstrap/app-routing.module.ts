import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { UserAuthenticatedGuard } from "@mealme/src/domain/modules/auth/guards/user-authenticated.guard";
import { MealmeAccountAuthDomainModule } from "@mealme/app/domains/auth/module";

const routes: Routes = [
    {
        path: 'app',
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'tabs',
            },
            {
                path: 'tabs',
                canActivate: [UserAuthenticatedGuard],
                loadChildren: () => import('../templates/tabs/tabs.module').then(m => m.MealmeTabsTemplateModule),
            },
            {
                path: 'auth-blank',
                loadChildren: () => import('../templates/auth-blank/auth-blank.module').then(m => m.MealmeAuthBlankTemplateModule),
            },
            {
                canActivate: [UserAuthenticatedGuard],
                path: 'blank',
                loadChildren: () => import('../templates/blank/blank.module').then(m => m.MealmeBlankTemplateModule),
            },
        ]
    },
    {
        path: '',
        redirectTo: 'app',
        pathMatch: 'full',
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules}),
        MealmeAccountAuthDomainModule,
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}

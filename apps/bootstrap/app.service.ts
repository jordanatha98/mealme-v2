import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

const DEEPLINK_MAPS: any = {

};

@Injectable()
export class AppService {
    private readonly deeplinkDataSubject: BehaviorSubject<string | undefined | null> = new BehaviorSubject<string | undefined | null>(undefined);
    public deeplinkData$: Observable<string | undefined | null> = this.deeplinkDataSubject.asObservable();
    private readonly deeplinkUrlSubject: BehaviorSubject<string | undefined | null> = new BehaviorSubject<string | undefined | null>(undefined);
    public deeplinkUrl$: Observable<string | undefined | null> = this.deeplinkUrlSubject.asObservable();
    private readonly deeplinkParamsSubject: BehaviorSubject<URLSearchParams | undefined | null> = new BehaviorSubject<URLSearchParams | undefined | null>(undefined);
    public deeplinkParams$: Observable<URLSearchParams | undefined | null> = this.deeplinkParamsSubject.asObservable();

    private readonly retriesSubject: BehaviorSubject<any[] | undefined | null> = new BehaviorSubject<any[] | undefined | null>(undefined);
    public retries$: Observable<any[] | undefined | null> = this.retriesSubject.asObservable();

    public handleChangeDeeplinkData(data: string | undefined | null): void {
        this.deeplinkDataSubject.next(data);
    }

    public handleChangeDeeplinkUrl(url: string | undefined | null): void {
        this.deeplinkUrlSubject.next(url);
    }

    public handleChangeDeeplinkParams(params: URLSearchParams | undefined | null): void {
        this.deeplinkParamsSubject.next(params);
    }

    public handleChangeDeeplink(slug: string): void {
        const cleanUrl = slug.split('?');
        const url = cleanUrl && cleanUrl.length > 0 ? cleanUrl[0] : slug;
        const params = cleanUrl && cleanUrl.length > 0 ? new URLSearchParams(cleanUrl[1]) : null;

        if (url) {
            const data = DEEPLINK_MAPS[url];

            if (data) {
                if (params) {
                    this.handleChangeDeeplinkData(data + '?' + params.toString());
                } else {
                    this.handleChangeDeeplinkData(data);
                }
                this.handleChangeDeeplinkUrl(data);
                this.handleChangeDeeplinkParams(params);
            }
        }
    }

    public handlePushRetries(retries: any[] | undefined | null): void {
        if (retries && Array.isArray(retries)) {
            if (this.retriesSubject.getValue() && Array.isArray(this.retriesSubject.getValue())) {
                this.retriesSubject.next([
                    ...this.retriesSubject.getValue() as any,
                    ...retries,
                ]);
            } else {
                this.retriesSubject.next([
                    ...retries,
                ]);
            }
        }
    }
}

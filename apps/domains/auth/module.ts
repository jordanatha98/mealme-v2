import { NgModule }             from '@angular/core';
import { AuthRepository }       from '@mealme/src/domain/modules/auth/repositories/auth.repository';
import { AuthenticatedGuard }   from '@mealme/app/domains/auth/guards/authenticated.guard';
import { GuestGuard }           from '@mealme/app/domains/auth/guards/guest.guard';
import {UserAuthenticatedGuard} from "@mealme/src/domain/modules/auth/guards/user-authenticated.guard";

@NgModule({
    providers: [
        AuthenticatedGuard,
        GuestGuard,
        AuthRepository,
    ],
})
export class MealmeAccountAuthDomainModule {}

import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
    appId: 'io.ionic.starter',
    appName: 'mealme',
    webDir: 'www/mealme',
    bundledWebRuntime: false,
    plugins: {
        SplashScreen: {
            backgroundColor: "#00aa13",
            launchAutoHide: false,
            spinnerColor: "#ffffff",
            androidScaleType: "CENTER_CROP",
            splashFullScreen: true,
            splashImmersive: false,
            showSpinner: true
        }
    }
};

export default config;

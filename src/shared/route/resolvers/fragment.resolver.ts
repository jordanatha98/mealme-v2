import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { first, tap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class FragmentResolver implements Resolve<boolean> {
    public constructor(private router: Router) {}

    public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        const fragment = window.location.hash;

        return of(true).pipe(
            first(),
            tap(() => {
                if (fragment && !route.fragment) {
                    const url = state.url.split('#')[0];
                    this.router.navigate([url], {
                        fragment: fragment.replace('#', ''),
                    });
                }
            }),
        );
    }
}

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { filter, take } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class FragmentGuard implements CanActivate, CanActivateChild {
    public fragments$: Observable<string | null>;
    public fragmentsSubject: BehaviorSubject<string | null> = new BehaviorSubject<string | null>(null);

    public constructor() {
        this.fragments$ = this.fragmentsSubject.asObservable();
    }

    public canActivate(route: ActivatedRouteSnapshot, routerState: RouterStateSnapshot): Observable<boolean> {
        if (route.fragment) {
            this.fragmentsSubject.next(route.fragment.toString());
            setTimeout(() => {
                if (route.fragment) {
                    window.location.hash = route.fragment.toString();
                }
            }, 100);
        } else {
            this.fragments$
                .pipe(
                    filter((fragments) => !!fragments),
                    take(1),
                )
                .subscribe((fragments) => {
                    setTimeout(() => {
                        if (fragments) {
                            window.location.hash = fragments.toString();
                        }
                    }, 100);
                })
                .unsubscribe();
        }

        return of(true);
    }

    public canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.canActivate(childRoute, state);
    }
}

import * as uuidLib from 'uuid';

// tslint:disable:no-construct
export class UUID extends String {
    public constructor() {
        super();
        return new String(uuidLib.v4());
    }
}

import { HttpErrorResponse } from '@angular/common/http';
import sha256 from 'crypto-js/sha256';
import hmacSHA256 from 'crypto-js/hmac-sha256';
import Base64 from 'crypto-js/enc-base64';
import Hex from 'crypto-js/enc-hex';
import { DateUtil } from './date.util';

export namespace HttpUtil {
    export function queryParamsExtractor(obj: any): any {
        const data: any = {};

        for (const key in obj) {
            if (undefined !== obj[key] && null !== obj[key]) {
                if (Array.isArray(obj[key])) {
                    data[`${key}[]`] = [];
                    obj[key].forEach((item: any) => {
                        data[`${key}[]`].push(item);
                    });
                } else if (typeof obj[key] === 'object') {
                    for (const objKey in obj[key]) {
                        data[`${key}[${objKey}]`] = obj[key][objKey];
                    }
                } else {
                    data[key] = obj[key];
                }
            }
        }
        return HttpUtil.requestToSnake(data);
    }

    export function requestToSnake(data: any): any {
        if (data instanceof FormData) {
            const arr: any[] = Array.from((<any>data).entries());

            return arr.reduce((acc, [key, val]) => {
                const newKey = key.replace(/([A-Z])/g, (m: string) => '_' + m.toLowerCase());
                if (val) {
                    if (val instanceof File) {
                        acc.append(newKey, val, val.name);
                    } else {
                        acc.append(newKey, val);
                    }
                }

                return acc;
            }, new FormData());
        }
        if (data instanceof Date || data instanceof Boolean || data instanceof String) {
            return data;
        }
        if (data instanceof Array) {
            return data.map((item: any) => HttpUtil.requestToSnake(item));
        }

        if (null !== data && 'object' === typeof data) {
            const transformed: any = {};
            Object.keys(data).forEach((key: any) => {
                let transformedKey = key;
                let transformedItem = data[key];

                if ('string' === typeof transformedKey) {
                    transformedKey = transformedKey.replace(/([A-Z])/g, (m: string) => '_' + m.toLowerCase());
                }

                if ('object' === typeof transformedItem) {
                    transformedItem = HttpUtil.requestToSnake(transformedItem);
                }

                transformed[transformedKey] = transformedItem;
            });

            return transformed;
        }

        return data;
    }

    export function errorExtractor(err: HttpErrorResponse): string {
        let message: any = '';

        if (err.error && err.message) {
            if (err.error instanceof ProgressEvent) {
                message = err.message;
            } else {
                message = err.error.message;
            }
        }

        return message;
    }

    export function payloadToFormData(payload: any): FormData {
        let formData = new FormData();

        for (const key in payload) {
            if (payload[key]) {
                if (Array.isArray(payload[key]) || ('object' === typeof payload[key] && !(payload[key] instanceof File))) {
                    formData = HttpUtil.objectToFormData(formData, key, payload[key])
                } else {
                    formData.append(key, payload[key]);
                }
            }
        }

        return formData;
    }

    export function objectToFormData(formData: FormData, key: string, payload: any): any {
        const keys = Object.keys(payload);
        keys.forEach(item => {
            if (payload[item] !== null && payload[item] !== undefined) {
                formData.append(`${key}[${item}]`, payload[item]);
            } else {
                formData.append(`${key}[${item}]`, '');
            }
        });

        return formData;
    }

    export function buildQueryParams(params: any): string {
        if (typeof params === 'string') {
            return params;
        }

        const query = [];

        for (const key in params) {
            // eslint-disable-next-line no-prototype-builtins
            if (params.hasOwnProperty(key)) {
                query.push(encodeURIComponent(key) + '=' + encodeURIComponent(params[key]));
            }
        }

        return query.join('&');
    }

    export class Signature {
        static generateDigest(jsonBody: any) {
            const jsonStringHash256 = sha256(jsonBody);

            return Base64.stringify(jsonStringHash256);
        }

        static generateSignature(clientId: any, clientSecret: any, requestTimestamp: any, requestTarget: any, digest?: any) {
            const components = [
                clientId,
                requestTimestamp,
                requestTarget,
            ];
            if (digest) {
                components.push(digest);
            }
            const componentSignature = components.join('|');

            return hmacSHA256(componentSignature.toString(), clientSecret).toString(Hex);
        }

        static generate(clientId: any, clientSecret: any, target: any, payload?: any): any {
            let digest = null;

            if (payload) {
                const jsonBody = JSON.stringify(HttpUtil.requestToSnake(payload));
                digest = Signature.generateDigest(jsonBody);
            }

            const convertTZ = (data: any, tzString: any) => {
                return new Date((typeof data === 'string' ? new Date(data) : data).toLocaleString('en-US', {timeZone: tzString}));
            };

            const date = convertTZ(new Date(), 'Asia/Jakarta');

            const timestamp = DateUtil.toDateWithTimezone(date, true);

            const signature = Signature.generateSignature(
                clientId,
                clientSecret,
                timestamp,
                target,
                digest,
            );

            return {
                'Client-Id': clientId,
                'Request-Timestamp': timestamp,
                'Signature': `HMACSHA256=${signature}`
            };
        }
    }
}

export namespace DataUtil {
    export function toCamel(data: any): any {
        if (data instanceof Array) {
            return data.map((item: any) => DataUtil.toCamel(item));
        }

        if (null !== data && 'object' === typeof data) {
            const transformed: any = {};
            Object.keys(data).forEach((key: any) => {
                let transformedKey = key;
                let transformedItem = data[key];

                if ('string' === typeof transformedKey) {
                    transformedKey = transformedKey.replace(/(\_\w)/g, (m: string) => m[1].toUpperCase());
                }

                if ('object' === typeof transformedItem) {
                    transformedItem = DataUtil.toCamel(transformedItem);
                }

                transformed[transformedKey] = transformedItem;
            });

            return transformed;
        }

        return data;
    }

    export function convertArrayToObject(array: any[], key?: string | null, defaultValue?: any) {
        const initialValue = {};
        return array.reduce((obj, item) => {
            if (key && item) {
                return {
                    ...obj,
                    [item[key]]: item,
                };
            }

            return {
                ...obj,
                [item]: defaultValue,
            };
        }, initialValue);
    };
}

import { Message } from '@ubud/ngrx';

export namespace MessageUtil {
    export function getPayload<T>(message: Message<any>, exclude?: any[]): T {
        let excludes: string[] = ['TYPE', 'type', 'container'];

        if (exclude) {
            excludes = [...excludes, ...exclude];
        }

        let payloads: any = {};
        for (const key in message) {
            if (!excludes.find(item => item === key)) {
                payloads = {
                    ...payloads,
                    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                    // @ts-ignore
                    [key]: message[key],
                };
            }
        }

        return payloads;
    }
}

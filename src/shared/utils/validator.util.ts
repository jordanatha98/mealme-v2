import { AbstractControl, FormControl, ValidatorFn } from '@angular/forms';

export namespace ValidatorUtil {
    export function arrayMinLengthValidator(min: number): ValidatorFn {
        let a: FormControl;

        return (control: AbstractControl): { [key: string]: any } | null => {
            if (!a) {
                a = control as FormControl;
            }

            if (a && a.value && a.value.length < min) {
                return {
                    arrayminlength: min,
                };
            }

            return null;
        };
    }

    export function dateAfterValidator(matchWith: string): ValidatorFn {
        let a: FormControl;
        let b: FormControl;

        return (control: AbstractControl): { [key: string]: any } | null => {
            if (!control.parent) {
                return null;
            }

            if (!a) {
                a = control as FormControl;
                b = control.parent.get(matchWith) as FormControl;

                if (!b) {
                    throw new Error('dateAfterValidator(): other control is not found in parent group');
                }

                b.valueChanges.subscribe(() => {
                    a.updateValueAndValidity();
                });
            }

            if (!b) {
                return null;
            }

            const firstDate = new Date(a.value);
            const secondDate = new Date(b.value);

            const dateResponse = new Date(firstDate);

            if (secondDate.getTime() > firstDate.getTime()) {
                return {
                    dateafter: dateResponse,
                };
            }

            return null;
        };
    }

    export function dateBeforeValidator(matchWith: string): ValidatorFn {
        let a: FormControl;
        let b: FormControl;

        return (control: AbstractControl): { [key: string]: any } | null => {
            if (!control.parent) {
                return null;
            }

            if (!a) {
                a = control as FormControl;
                b = control.parent.get(matchWith) as FormControl;

                if (!b) {
                    throw new Error('dateBeforeValidator(): other control is not found in parent group');
                }

                b.valueChanges.subscribe(() => {
                    a.updateValueAndValidity();
                });
            }

            if (!b) {
                return null;
            }

            const firstDate = new Date(a.value);
            const secondDate = new Date(b.value);

            const dateResponse = new Date(secondDate);

            if (firstDate.getTime() > secondDate.getTime()) {
                return {
                    datebefore: dateResponse,
                };
            }

            return null;
        };
    }

    export function dateMaxValidator(date?: Date): ValidatorFn {
        let a: FormControl;

        return (control: AbstractControl): { [key: string]: any } | null => {
            if (!a) {
                a = control as FormControl;
            }

            if (!date) {
                date = new Date();
                date.setDate(date.getDate() - 1);
            }

            const dateResponse = new Date(date);

            if (a.value > date) {
                return {
                    datemax: dateResponse,
                };
            }

            return null;
        };
    }

    export function dateMinValidator(date?: Date): ValidatorFn {
        let a: FormControl;

        return (control: AbstractControl): { [key: string]: any } | null => {
            if (!a) {
                a = control as FormControl;
            }

            if (!date) {
                date = new Date();
                date.setDate(date.getDate() - 1);
            }

            const val = a.value;
            if (val) {
                val.setHours(0, 0, 0, 0);
            }

            const dateResponse = new Date(date);

            if (date) {
                date.setHours(0, 0, 0, 0);
            }

            if (val < date) {
                return {
                    datemin: dateResponse,
                };
            }

            return null;
        };
    }

    export function lessWithValidator(lessWith: string): ValidatorFn {
        let a: FormControl;
        let b: FormControl;

        return (control: AbstractControl): { [key: string]: any } | null => {
            if (!control.parent) {
                return null;
            }

            if (!a) {
                a = control as FormControl;
                b = control.parent.get(lessWith) as FormControl;

                if (!b) {
                    throw new Error('lessWithValidator(): other control is not found in parent group');
                }

                b.valueChanges.subscribe(() => {
                    a.updateValueAndValidity();
                });
            }

            if (!b) {
                return null;
            }

            if (b.value < a.value) {
                return {
                    lesswith: b.value,
                };
            }

            return null;
        };
    }

    export function moreWithValidator(moreWith: string, field?: string): ValidatorFn {
        let a: FormControl;
        let b: FormControl;

        return (control: AbstractControl): { [key: string]: any } | null => {
            if (!control.parent) {
                return null;
            }

            if (!a) {
                a = control as FormControl;
                b = control.parent.get(moreWith) as FormControl;

                if (!b) {
                    throw new Error('moreWithValidator(): other control is not found in parent group');
                }

                /*b.valueChanges.subscribe(() => {
                    a.updateValueAndValidity();
                });*/
            }

            if (!b) {
                return null;
            }

            if (field) {
                if (a.value instanceof Object && b.value instanceof Object) {
                    if (b.value[field] > a.value[field]) {
                        return {
                            morewith: b.value[field],
                        };
                    }
                }
                return null;
            } else {
                if (b.value > a.value) {
                    return {
                        morewith: b.value,
                    };
                }
            }

            return null;
        };
    }

    export function matchValidator(matchWith: string): ValidatorFn {
        let a: FormControl;
        let b: FormControl;

        return (control: AbstractControl): { [key: string]: any } | null => {
            if (!control.parent) {
                return null;
            }

            if (!a) {
                a = control as FormControl;
                b = control.parent.get(matchWith) as FormControl;

                if (!b) {
                    throw new Error('matchValidator(): other control is not found in parent group');
                }

                b.valueChanges.subscribe(() => {
                    a.updateValueAndValidity();
                });
            }

            if (!b) {
                return null;
            }

            if (b.value !== a.value) {
                return {
                    match: true,
                };
            }

            return null;
        };
    }

    export function notMatchValidator(notMatchWith: string): ValidatorFn {
        let a: FormControl;
        let b: FormControl;

        return (control: AbstractControl): { [key: string]: any } | null => {
            if (!control.parent) {
                return null;
            }

            if (!a) {
                a = control as FormControl;
                b = control.parent.get(notMatchWith) as FormControl;

                if (!b) {
                    throw new Error('notMatchValidator(): other control is not found in parent group');
                }

                b.valueChanges.subscribe(() => {
                    a.updateValueAndValidity();
                });
            }

            if (!b) {
                return null;
            }

            if (b.value !== a.value) {
                return null;
            }

            return {
                notMatch: true,
            };
        };
    }

    export function requiredIf(relatedControl: string): ValidatorFn {
        let a: FormControl;
        let b: FormControl;

        return (control: AbstractControl): { [key: string]: any } | null => {
            if (!control.parent) {
                return null;
            }

            if (!a) {
                a = control as FormControl;
                b = control.parent.get(relatedControl) as FormControl;

                if (!b) {
                    throw new Error('requiredIfValidator(): other control is not found in parent group');
                }

                b.valueChanges.subscribe(() => {
                    a.updateValueAndValidity();
                });
            }

            if (a.value == null && b.value != null) {
                return {
                    requiredif: true,
                };
            }

            return null;
        };
    }
}

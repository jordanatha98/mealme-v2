import { TransformFnParams } from 'class-transformer';

export enum DiffType {
    SECONDS = 'seconds',
    MINUTES = 'minutes',
    HOURS = 'hours',
    DAYS = 'days',
}

export namespace DateUtil {
    export function toISOString(date: Date): string {
        return new Date(date.getTime() - date.getTimezoneOffset() * 60 * 1000).toISOString();
    }

    export function toISODateString(date: Date): string {
        return new Date(date.getTime() - date.getTimezoneOffset() * 60 * 1000).toISOString().split('T')[0];
    }

    export function toISOTimeString(date: Date | string): string {
        if (date instanceof Date) {
            return new Date(date.getTime() - date.getTimezoneOffset() * 60 * 1000).toISOString().split('T')[1].split('.')[0];
        }

        const time = date.split('T')[1].split('.')[0];
        const hasTimezone = time.split('+');
        return hasTimezone.length > 0 ? hasTimezone[0] : time;
    }

    export function toISODateTimeTz(date: Date): string {
        function formatString(data: string): string {
            if (data.length < 2) {
                return `0${data}`;
            }

            return data;
        }

        const tz = date.getTimezoneOffset() / 60;
        let strTz = `-${formatString(Math.abs(tz).toString())}`;
        if (tz < 0) {
            strTz = `+${formatString(Math.abs(tz).toString())}`;
        }

        return DateUtil.toISODateString(date) + 'T' + DateUtil.toISOTimeString(date) + `${strTz}:00`;
    }

    export function toDateWithTimezone(date: Date, hideTimezone?: boolean): string | null {
        function formatString(data: string): string {
            if (data.length < 2) {
                return `0${data}`;
            }

            return data;
        }

        if (date) {
            const tz = date.getTimezoneOffset() / 60;
            let strTz = `-${Math.abs(tz)}`;
            if (tz < 0) {
                strTz = `+${Math.abs(tz)}`;
            }

            return `${formatString(date.getFullYear().toString())}-${formatString((date.getMonth() + 1).toString())}-${formatString(
                date.getDate().toString(),
            )} ${formatString(date.getHours().toString())}:${formatString(date.getMinutes().toString())}:${formatString(
                date.getSeconds().toString(),
            )}${!!strTz && !hideTimezone ? ' ' + strTz : ''}`;
        }

        return null;
    }

    export function getMonthDuration(firstDate: Date, secondDate: Date): number {
        let timeDiff = (secondDate.getTime() - firstDate.getTime()) / 1000;
        timeDiff /= 60 * 60 * 24 * 7 * 4;
        if (timeDiff < 1) {
            timeDiff = 1;
        }

        return Math.abs(Math.round(timeDiff));
    }

    export function getCountDown(date: Date, dateNow: Date, type: DiffType = DiffType.SECONDS): number | undefined {
        let diffTime = date.getTime() - dateNow.getTime();

        let diff;

        switch (type) {
            case DiffType.SECONDS:
                diff = (diffTime / 1000) % 60;
                diff = Math.floor(diff);
                break;
            case DiffType.MINUTES:
                diff = (diffTime / (1000 * 60)) % 60;
                diff = Math.floor(diff);
                break;
            case DiffType.HOURS:
                diff = (diffTime / (1000 * 60 * 60)) % 24;
                diff = Math.floor(diff);
                break;
            case DiffType.DAYS:
                diff = diffTime / (1000 * 60 * 60 * 24);
                diff = Math.floor(diff);
                break;
            default:
                break;
        }

        return diff;
    }

    export function getDiff(dateThen: Date, dateNow: Date, type: DiffType = DiffType.SECONDS): number | undefined {
        let diffTime = dateNow.getTime() - dateThen.getTime();

        let diff;

        switch (type) {
            case DiffType.SECONDS:
                diff = (diffTime / 1000) % 60;
                diff = Math.floor(diff);
                break;
            case DiffType.MINUTES:
                diff = (diffTime / (1000 * 60)) % 60;
                diff = Math.floor(diff);
                break;
            case DiffType.HOURS:
                diff = (diffTime / (1000 * 60 * 60)) % 24;
                diff = Math.floor(diff);
                break;
            case DiffType.DAYS:
                diff = diffTime / (1000 * 60 * 60 * 24);
                diff = Math.floor(diff);
                break;
            default:
                break;
        }

        return diff;
    }

    export function iosDate(value: string): Date | null {
        if (value) {
            return new Date(value.toString().replace(/-/g, '/'));
        }

        return null;
    }
}

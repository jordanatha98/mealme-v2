export namespace TextUtil {
    export function toLiteral(data: any): any {
        return toSnake(data).replace(/_/g, ' ');
    }

    export function toKebab(data: string): string {
        return data.replace(/([A-Z])/g, (m: string) => '-' + m.toLowerCase());
    }

    export function toSnake(data: string): string {
        return data.replace(/([A-Z])/g, (m: string) => '_' + m.toLowerCase());
    }

    export function toCamel(data: string): string {
        return data.replace(/(\_\w)/g, (m: string) => m[1].toUpperCase());
    }

    export function extractUrlValue(key: string, url: string): string | null {
        if (typeof(url) === 'undefined') url = window.location.href;

        const match = url.match('[?&]' + key + '=([^&]+)');

        return match ? match[1] : null;
    }
}

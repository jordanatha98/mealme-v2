import { TransformFnParams } from 'class-transformer';

export namespace NumberUtil {
    export function toFloat(obj: TransformFnParams, suffix?: string): number | null {
        const value = obj ? obj.value : null;

        if (value) {
            if (suffix) {
                return parseFloat(value.replace(new RegExp(suffix, 'gi'), ''));
            }

            return parseFloat(value);
        }

        return 0;
    }

    export function toInt(obj: TransformFnParams, suffix?: string): number | null {
        const value = obj ? (obj.value === 0 ? 0 : obj.value) : null;

        if (value !== null) {
            if (suffix) {
                return parseInt(value.replace(new RegExp(suffix, 'gi'), ''), 0);
            }

            return parseInt(value, 0);
        }

        return null;
    }
}

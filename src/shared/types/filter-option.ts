export interface FilterOption {
    key: string;
    text: string;
    value: any;
    source: any;
}

export interface Collection<T> {
    data: T[];
    limit: number;
    page: number;
    total: number;
    lastPage?: number;
}

export interface SingleCollection<T> {
    data: T;
    limit: number;
    page: number;
    total: number;
    lastPage?: number;
}

export class NullCollection {
    public data!: any[];
    public limit!: number;
    public page!: number;
    public total!: number;
    public lastPage?: number;
}

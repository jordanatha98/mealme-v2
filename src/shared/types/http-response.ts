export interface MetaResponse {
    hostname: string;
    clientIp: string;
}

export class HttpArrayResponse {
    public meta!: MetaResponse;
    public statusCode!: number;
}

import { NgModule } from '@angular/core';
import { FeatherModule } from 'angular-feather';
import * as featherIcons from 'angular-feather/icons';
import { Wallet } from './svg/wallet';
import { History } from './svg/history';
import { Shop } from './svg/shop';
import { Virus } from './svg/virus';
import { HeadphoneWithMic } from './svg/headphone-with-mic';
import { FacebookFill } from './svg/facebook-fill';
import { TwitterFill } from './svg/twitter-fill';
import { FoodAndBeverage } from './svg/food-and-beverage';
import { Ritel } from './svg/ritel';
import { UserCircle } from './svg/user-circle';
import { Laundry } from './svg/laundry';
import { RealEstate } from './svg/real-estate';
import { Union } from './svg/union';
import { ShoppingCart } from './svg/shopping-cart';
import { AccountTree } from './svg/account-tree';
import { Payment } from './svg/payment';
import { ShowChart } from './svg/show-chart';
import { RecordVoiceOver } from './svg/record-voice-over';
import { TrackChanges } from './svg/track-changes';
import { People } from './svg/people';
import { Whatsapp } from './svg/whatsapp';
import { LinkedinFill } from './svg/linkedin-fill';
import { FormatQuote } from './svg/format-quote';
import { Money } from './svg/money';
import { ChatAltDouble } from './svg/chat-alt-double';
import { PencilAlt } from './svg/pencil-alt';
import { PencilAltFill } from './svg/pencil-alt-fill';
import { DocumentAlt } from './svg/document-alt';
import { CheckCircle } from './svg/check-circle';
import { ThumbsUp } from './svg/thumbs-up';
import { Library } from './svg/library';
import { Users } from './svg/users';
import { StarFill } from './svg/star-fill';
import { QuoteAltRight } from './svg/quote-alt-right';
import { CheckCircleFill } from './svg/check-circle-fill';
import { HeartFill } from './svg/heart-fill';
import { Share2Fill } from './svg/share-2-fill';
import { MapPinFill } from './svg/map-pin-fill';
import { StarHalfFill } from './svg/star-half-fill';
import { Exclamation } from './svg/exclamation';
import { StarBadge } from './svg/star-badge';
import { PhoneFill } from './svg/phone-fill';
import { MailFill } from './svg/mail-fill';
import { OfficeBuilding } from './svg/office-building';
import { OfficeBuildingFill } from './svg/office-building-fill';
import { ClockFill } from './svg/clock-fill';
import { ChatAltDoubleFill } from './svg/chat-alt-double-fill';
import { AcademicCapFill } from './svg/academic-cap-fill';
import { Support } from './svg/support';
import { OfferFill } from './svg/offer-fill';
import { StreamFill } from './svg/stream-fill';
import { MosaicFill } from './svg/mosaic-fill';
import { InfoFill } from './svg/info-fill';
import { BpjsFill } from './svg/BPJS-fill';
import { MarketInfoFill } from './svg/market-info-fill';
import { WorkTrainingFill } from './svg/work-training-fill';
import { GlobeFill } from './svg/globe-fill';
import { SearchFill } from './svg/search-fill';
import { LocationMarkerFill } from './svg/location-marker-fill';
import { ChevronRightFill } from './svg/chevron-right-fill';
import { ChevronDownFill } from './svg/chevron-down-fill';
import { ChevronUpFill } from './svg/chevron-up-fill';
import { HelpCircleFill } from './svg/help-circle-fill';
import { UserCircleFill } from './svg/user-circle-fill';
import { ClipboardListFill } from './svg/clipboard-list-fill';
import { ViewGridFill } from './svg/view-grid-fill';
import { BriefcaseFill } from './svg/briefcase-fill';
import { BriefcaseCheckedFill } from './svg/briefcase-checked-fill';
import { LibraryFill } from './svg/library-fill';
import { LockClosedFill } from './svg/lock-closed-fill';
import { DocumentTextFill } from './svg/document-text-fill';
import { CameraFill } from './svg/camera-fill';
import { ExclamationFill } from './svg/exclamation-fill';
import { XCircleFill } from './svg/x-circle-fill';
import { BookmarkFill } from './svg/bookmark-fill';
import { DataExplorationFill } from './svg/data-exploration-fill';
import { TimerFill } from './svg/timer-fill';
import { VerifiedFill } from './svg/verified-fill';
import { EmojiMehFill } from './svg/emoji-meh-fill';
import { EmojiHappyFill } from './svg/emoji-happy-fill';
import { EmojiSadFill } from './svg/emoji-sad-fill';
import { TrashFill } from './svg/trash-fill';
import { Upload } from './svg/upload';
import { DocumentAdd } from './svg/document-add';
import { DocumentDuplicate } from './svg/document-duplicate';
import { UserGroup } from './svg/user-group';
import { Identification } from './svg/identification';
import { HomeFill } from './svg/home-fill';
import { NewspaperFill } from './svg/newspaper-fill';
import { BellFill } from './svg/bell-fill';
import { IdentificationFill } from './svg/identification-fill';
import { CogFill } from './svg/cog-fill';
import { QuestionMarkCircleFill } from './svg/question-mark-circle-fill';
import { LogoutFill } from './svg/logout-fill';
import { BriefcaseCrossedFill } from './svg/briefcase-crossed-fill';
import { BriefcaseSearchFill } from './svg/briefcase-search-fill';
import { StoreFill } from './svg/store-fill';
import { CalendarFill } from './svg/calendar-fill';
import { ClipboardCopyFill } from './svg/clipboard-copy-fill';
import { ClipboardCopy } from './svg/clipboard-copy';
import { PlayFill } from './svg/play-fill';
import { AdjustmentFill } from './svg/adjustment-fill';
import { UsersFill } from './svg/users-fill';
import { UserCogFill } from './svg/user-cog-fill';
import { SearchDesktopFill } from './svg/search-desktop-fill';
import { DotsVerticalFill } from './svg/dots-vertical-fill';
import { Photograph } from './svg/photograph';
import { Trash } from './svg/trash';
import { Qrcode } from './svg/qrcode';
import { Newspaper } from './svg/newspaper';
import { ClipboardList } from './svg/clipboard-list';
import { Sort } from './svg/sort';
import { SwitchVertical } from './svg/switch-vertical';
import { EyeFill } from './svg/eye-fill';
import { EyeOffFill } from './svg/eye-off-fill';
import { FilterFill } from './svg/filter-fill';
import { YoutubeFill } from './svg/youtube-fill';
import { Theme } from './svg/theme';
import { FlagFill } from './svg/flag-fill';
import { DuplicateFill } from './svg/duplicate-fill';
import { Minimize2Fill } from './svg/minimize-2-fill';
import { MaximizeFill } from './svg/maximize-fill';
import { CashFill } from './svg/cash-fill';
import { UserFill } from './svg/user-fill';
import { FireFill } from './svg/fire-fill';

/*
    How To Insert Own Feather Icon:
    1. Download the svg and make it into new file, you can see the example below, remove the width and height attribute from the svg.
    2. DON'T FORGET to remove all "stroke" attribute from the svg, otherwise the stroke color can't be controlled.
    3. If step 2 is done, but the color still can't be controlled, try to set fill="currentColor" and stroke="none"
 */

const icons = {
    ...featherIcons,
    AccountTree,
    ChatAltDouble,
    CheckCircle,
    CheckCircleFill,
    DocumentAlt,
    FacebookFill,
    FoodAndBeverage,
    FormatQuote,
    HeadphoneWithMic,
    HeartFill,
    History,
    InfoFill,
    Laundry,
    Library,
    LinkedinFill,
    MailFill,
    MapPinFill,
    Money,
    Payment,
    PencilAlt,
    People,
    PhoneFill,
    QuoteAltRight,
    RealEstate,
    RecordVoiceOver,
    Ritel,
    Share2Fill,
    Shop,
    ShoppingCart,
    ShowChart,
    StarBadge,
    StarHalfFill,
    StarFill,
    ThumbsUp,
    TrackChanges,
    TwitterFill,
    Union,
    UserCircle,
    Users,
    Virus,
    Wallet,
    Whatsapp,
    Exclamation,
    OfficeBuilding,
    ClockFill,
    ChatAltDoubleFill,
    AcademicCapFill,
    Support,
    OfferFill,
    StreamFill,
    MosaicFill,
    BpjsFill,
    MarketInfoFill,
    WorkTrainingFill,
    GlobeFill,
    SearchFill,
    LocationMarkerFill,
    ChevronRightFill,
    ChevronDownFill,
    ChevronUpFill,
    HelpCircleFill,
    UserCircleFill,
    ClipboardListFill,
    ViewGridFill,
    BriefcaseFill,
    BriefcaseCheckedFill,
    LibraryFill,
    LockClosedFill,
    DocumentTextFill,
    CameraFill,
    ExclamationFill,
    XCircleFill,
    BookmarkFill,
    DataExplorationFill,
    TimerFill,
    VerifiedFill,
    EmojiMehFill,
    EmojiHappyFill,
    EmojiSadFill,
    OfficeBuildingFill,
    PencilAltFill,
    TrashFill,
    Upload,
    DocumentAdd,
    DocumentDuplicate,
    UserGroup,
    Identification,
    HomeFill,
    NewspaperFill,
    BellFill,
    IdentificationFill,
    CogFill,
    QuestionMarkCircleFill,
    LogoutFill,
    BriefcaseCrossedFill,
    StoreFill,
    CalendarFill,
    ClipboardCopyFill,
    ClipboardCopy,
    PlayFill,
    AdjustmentFill,
    UsersFill,
    UserCogFill,
    BriefcaseSearchFill,
    SearchDesktopFill,
    DotsVerticalFill,
    Photograph,
    Trash,
    Qrcode,
    Newspaper,
    ClipboardList,
    Sort,
    SwitchVertical,
    EyeFill,
    EyeOffFill,
    FilterFill,
    YoutubeFill,
    Theme,
    FlagFill,
    DuplicateFill,
    Minimize2Fill,
    MaximizeFill,
    CashFill,
    UserFill,
    FireFill,
};

@NgModule({
    imports: [FeatherModule.pick(icons as any)],
    exports: [FeatherModule],
})
export class MealmeSharedFeatherIconsModule {}

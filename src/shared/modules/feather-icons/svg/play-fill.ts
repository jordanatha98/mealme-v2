export const PlayFill = `
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" stroke="none" fill="currentColor">
    <path d="M13.3333 11.3663V28.6329C13.3333 29.9496 14.7833 30.7496 15.9 30.0329L29.4666 21.3996C30.5 20.7496 30.5 19.2496 29.4666 18.5829L15.9 9.96627C14.7833 9.24961 13.3333 10.0496 13.3333 11.3663Z"/>
</svg>
`;

export const ShowChart = `
<svg viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M4.66675 24.6533L12.6667 16.64L18.0001 21.9733L29.3334 9.22668L27.4534 7.34668L18.0001 17.9733L12.6667 12.64L2.66675 22.6533L4.66675 24.6533Z" fill="currentColor" stroke="none"/>
</svg>
`;

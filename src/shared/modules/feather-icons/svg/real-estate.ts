export const RealEstate = `
<svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M17.0002 15.0002H19.0002V17.0002H17.0002V15.0002ZM17.0002 11.0002H19.0002V13.0002H17.0002V11.0002ZM17.0002 7.00018H19.0002V9.00018H17.0002V7.00018ZM13.7402 7.00018L15.0002 7.84018V7.00018H13.7402Z" fill="currentColor" stroke="none"/>
    <path d="M10 3V4.51L12 5.84V5H21V19H17V21H23V3H10Z" fill="currentColor" stroke="none"/>
    <path d="M8.17 5.70038L15 10.2504V21.0004H1V10.4804L8.17 5.70038ZM10 19.0004H13V11.1604L8.17 8.09038L3 11.3804V19.0004H6V13.0004H10V19.0004Z" fill="currentColor" stroke="none"/>
</svg>
`;

export const Virus = `
<svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M12 17C14.7614 17 17 14.7614 17 12C17 9.23858 14.7614 7 12 7C9.23858 7 7 9.23858 7 12C7 14.7614 9.23858 17 12 17Z" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M11 3H13M12 7V3V7Z" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M17.657 4.92896L19.071 6.34296M15.536 8.46395L18.364 5.63596L15.536 8.46395Z" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M21 11V13M17 12H21H17Z" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M19.071 17.657L17.657 19.071M15.536 15.536L18.364 18.364L15.536 15.536Z" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M13 21H11M12 17V21V17Z" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M6.34299 19.071L4.92999 17.657M8.46399 15.536L5.63599 18.364L8.46399 15.536Z" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M3 13V11M7 12H3H7Z" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M4.929 6.34305L6.343 4.93005M8.464 8.46405L5.636 5.63605L8.464 8.46405Z" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
</svg>
`;

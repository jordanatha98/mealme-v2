export const FormatQuote = `
<svg viewBox="0 0 34 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M31 0.334637H24L19.3334 9.66797V23.668H33.3334V9.66797H26.3334L31 0.334637ZM12.3334 0.334637H5.33337L0.666702 9.66797V23.668H14.6667V9.66797H7.6667L12.3334 0.334637Z"  fill="currentColor" stroke="none"/>
</svg>
`

export const History = `
<svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M2.2002 10C3.12674 5.43552 7.16224 2 12.0002 2C17.523 2 22.0002 6.47715 22.0002 12C22.0002 17.5228 17.523 22 12.0002 22C7.52252 22 3.73222 19.0571 2.45796 15" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M12 6V12L16 14" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M1 5V10H6" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
</svg>
`;

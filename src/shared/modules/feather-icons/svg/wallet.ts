export const Wallet = `
<svg viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
    <rect x="2" y="6.54541" width="20" height="14.5455" rx="1.81818" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M17.7637 15.6697C17.4422 15.4832 17.1754 15.2156 16.9899 14.8936C16.8045 14.5716 16.7068 14.2065 16.7068 13.8348C16.7068 13.4632 16.8045 13.0981 16.9899 12.7761C17.1754 12.4541 17.4422 12.1865 17.7637 12" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M18.3636 6.54545V3.81818C18.3636 2.81403 17.5496 2 16.5455 2H6.54546C4.03507 2 2 4.03507 2 6.54545V9.72727" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
</svg>
`;

import { NgModule } from '@angular/core';
import { PhoneInput } from './phone.input';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [PhoneInput],
    imports: [CommonModule, FormsModule],
    exports: [PhoneInput],
})
export class MealmeSharedPhoneInputModule {}

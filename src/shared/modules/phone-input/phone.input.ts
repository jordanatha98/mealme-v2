import { ChangeDetectorRef, Component, ElementRef, EventEmitter, forwardRef, Input, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    selector: 'mealme-phone-input',
    templateUrl: './phone.input.html',
    styleUrls: ['./phone.input.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => PhoneInput),
            multi: true,
        },
    ],
})
export class PhoneInput implements ControlValueAccessor {
    @ViewChild('telInput') public telInput!: ElementRef;

    @Input() public placeholder: any = '';
    @Input() public nativeClass: any;
    @Input() public inputNativeClass: any;
    @Input() public disabled: boolean | undefined;
    @Input() public nativeStyle: any;
    @Input() public hideDialCode: boolean | undefined;
    @Input() public dialCode: any;

    @Input() public value: number | undefined | null;

    @Output() public focused: EventEmitter<any> = new EventEmitter<any>();

    public propagateChange = (_: any) => {
        // TODO:
    }

    public registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }

    registerOnTouched(fn: any): void {}

    public writeValue(obj: any): void {
        if (this.value) {
            this.value = null;
        }
        this.propagateChange(obj);
        this.value = obj;
    }

    public handleChanged(): void {
        if (this.telInput) {
            const val = this.telInput;

            console.log(val);
        }
    }

    public constructor(private cdRef: ChangeDetectorRef) {}
}

import { MegaCurrencyPipe } from '@shared/modules/common/pipes/mega-currency.pipe';
import { TimeAgoPipe } from '@shared/modules/common/pipes/time-ago.pipe';
import { TruncateTextPipe } from '@shared/modules/common/pipes/truncate-text.pipe';
import { StripTagsPipe } from '@shared/modules/common/pipes/strip-tags.pipe';
import { GenderPipe } from '@shared/modules/common/pipes/gender.pipe';
import { MaritalStatusPipe } from '@shared/modules/common/pipes/marital-status.pipe';
import { DayPipe } from '@shared/modules/common/pipes/day.pipe';
import { HtmlSanitizerPipe } from '@shared/modules/common/pipes/html-sanitizer.pipe';
import { MonthNamePipe } from '@shared/modules/common/pipes/month-name.pipe';

export const SHARED_COMMON_PIPES: any[] = [
    TruncateTextPipe,
    TimeAgoPipe,
    MegaCurrencyPipe,
    StripTagsPipe,
    GenderPipe,
    MaritalStatusPipe,
    DayPipe,
    HtmlSanitizerPipe,
    MonthNamePipe,
];

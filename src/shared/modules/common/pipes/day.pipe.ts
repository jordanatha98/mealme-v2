import { Pipe, PipeTransform } from '@angular/core';
import { Day } from '@shared/enums/day';

@Pipe({
    name: 'day',
})
export class DayPipe implements PipeTransform {
    public transform(value: Day, key: string): string {
        return Day.find(value)[key];
    }
}

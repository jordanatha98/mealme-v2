import { CurrencyPipe, DecimalPipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'megaCurrency',
    pure: true,
})
export class MegaCurrencyPipe implements PipeTransform {
    public constructor(private currency: CurrencyPipe, private decimal: DecimalPipe) {}

    public transform(number: number, currencyCode?: string, fraction?: any, hideKey?: boolean, hideCurrency?: boolean): string {
        if (number === null) return null;
        if (number === 0) return '0';

        if (!fraction || fraction < 0) fraction = 2;

        let abs = Math.abs(number);
        let rounder = Math.pow(10, fraction);
        let isNegative = number < 0;
        let key = '';
        let powers = [
            { key: 'Q', value: Math.pow(10, 15) },
            { key: 'T', value: Math.pow(10, 12) },
            { key: 'M', value: Math.pow(10, 9) },
            { key: 'jt', value: Math.pow(10, 6) },
            { key: 'rb', value: 1000 },
        ];

        for (let i = 0; i < powers.length; i++) {
            let reduced = abs / powers[i].value;

            reduced = Math.round(reduced * rounder) / rounder;

            if (reduced >= 1) {
                if (reduced / Math.round(reduced) === Math.round(reduced) / Math.round(reduced)) {
                    reduced = Math.round(reduced * rounder) / rounder;
                    reduced = Math.round(reduced);
                }
                abs = reduced;
                key = powers[i].key;
                break;
            }
        }

        if (abs === 0) {
            fraction = 0;
        }

        const str = this.currency.transform(abs, currencyCode, 'symbol', '1.' + fraction).split(',');
        let result = hideCurrency ? this.decimal.transform(abs, '1.2-1' + fraction) : this.currency.transform(abs, currencyCode, 'symbol', '1.2-1' + fraction);

        if (str.length > 0) {
            const separator = str[1];

            if (separator && separator === '00') {
                result = hideCurrency ? this.decimal.transform(abs, '1.' + 0) : this.currency.transform(abs, currencyCode, 'symbol', '1.' + 0);
            }
        }

        return (isNegative ? '-' : '') + result + (hideKey ? '' : key);
    }
}

import { Pipe, PipeTransform } from '@angular/core';
import { MonthName } from '@shared/enums/month-name';

@Pipe({
    name: 'monthName',
})
export class MonthNamePipe implements PipeTransform {
    public transform(value: any, key: string): any {
        return MonthName.find(value)[key];
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SHARED_COMMON_PIPES } from '@shared/modules/common/pipes/index';

const MODULES: any[] = [
    CommonModule,
];

@NgModule({
    declarations: [...SHARED_COMMON_PIPES],
    imports: [...MODULES],
    exports: [...SHARED_COMMON_PIPES],
})
export class SharedCommonPipeModule {}

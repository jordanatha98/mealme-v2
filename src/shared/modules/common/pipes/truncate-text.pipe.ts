import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'truncateText',
})
export class TruncateTextPipe implements PipeTransform {
    public transform(value: any, length: number): string {
        if (value && value.length > length) {
            return value.substring(0, length) + '...';
        }

        return value;
    }
}

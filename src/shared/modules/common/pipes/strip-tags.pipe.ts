import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'stripTags',
})
export class StripTagsPipe implements PipeTransform {
    public transform(text: string): string {
        const tmp = document.createElement('div');

        tmp.innerHTML = text;

        return tmp.textContent || tmp.innerText || '';
    }
}

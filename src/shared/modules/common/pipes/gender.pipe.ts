import { Pipe, PipeTransform } from '@angular/core';
import { Gender } from '@shared/enums/gender';

@Pipe({
    name: 'gender',
})
export class GenderPipe implements PipeTransform {
    public transform(value: Gender, key: string): string {
        return Gender.find(value)[key];
    }
}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { SHARED_COMMON_COMPONENTS } from '@shared/modules/common/components/index';
import { SharedCommonPipeModule } from '@shared/modules/common/pipes/module';
import { MealmeSharedFeatherIconsModule } from '@shared/modules/feather-icons/feather-icons.module';

const MODULES: any[] = [
    CommonModule,
    IonicModule,
    SharedCommonPipeModule,
];

@NgModule({
    declarations: [...SHARED_COMMON_COMPONENTS],
    imports: [...MODULES, MealmeSharedFeatherIconsModule],
    exports: [...SHARED_COMMON_COMPONENTS],
})
export class SharedCommonComponentModule {}

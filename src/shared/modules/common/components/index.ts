import { AmountRangeContent } from '@shared/modules/common/components/contents/amount-range.content';
import { EmptyFallbackContent } from '@shared/modules/common/components/contents/empty-fallback.content';
import { RatingStatisticContent } from '@shared/modules/common/components/contents/rating-statistic.content';
import { RatingContent } from '@shared/modules/common/components/contents/rating.content';
import { OtpInput } from '@shared/modules/common/components/inputs/otp.input';
import { PasswordInput } from '@shared/modules/common/components/inputs/password.input';
import { SearchInput } from '@shared/modules/common/components/inputs/search.input';
import { PickerInput } from '@shared/modules/common/components/inputs/picker.input';
import { IonPaginatorComponent } from '@shared/modules/common/components/miscs/ion-paginator.component';
import { RatingStatisticSkeletonContent } from '@shared/modules/common/components/contents/rating-statistic-skeleton.content';

export const SHARED_COMMON_COMPONENTS: any[] = [
    AmountRangeContent,
    EmptyFallbackContent,
    RatingContent,
    RatingStatisticContent,
    OtpInput,
    PasswordInput,
    SearchInput,
    PickerInput,
    IonPaginatorComponent,
    RatingStatisticSkeletonContent,
];

import { ChangeDetectionStrategy, Component, EventEmitter, forwardRef, Input, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { IonInput } from '@ionic/angular';

@Component({
    selector: 'mealme-search-input',
    templateUrl: './search.input.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => SearchInput),
            multi: true,
        },
    ],
    styleUrls: ['./search.input.scss'],
})
export class SearchInput implements ControlValueAccessor {
    @ViewChild('ionTextTpl', { static: false }) public ionTextTpl: any;
    @ViewChild('ionResetTpl', { static: false }) public ionResetTpl: any;
    @ViewChild('ionInput', { static: false }) public ionInput: IonInput;

    @Input() public autofocus: boolean;
    @Input() public nativeClass: any;
    @Input() public nativeStyle: any;
    @Input() public placeholder: string;
    @Input() public iconColor: string;
    @Input() public readonly: boolean;
    @Input() public debounce: number;
    @Input()
    public set color(color: string) {
        if (color) {
            this.colorClass = 'form-control-' + color;
        }
    }
    @Input('disabled')
    public set setDisabled(disable: boolean) {
        this.setDisabledState(disable);
    }

    @Output() public clicked: EventEmitter<any> = new EventEmitter<any>();

    public value: string = null;
    public disabled: boolean;
    public colorClass: string;

    public propagateChange = (_: any) => {};

    public registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }

    public registerOnTouched(fn: any): void {}

    public setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    public async setFocus() {
        await this.ionInput.setFocus();
    }

    public writeValue(obj: any): void {
        this.value = obj;
        this.propagateChange(obj);
    }

    public handleInput(data: any): void {
        const { target } = data;

        this.writeValue(target.value);
    }

    public async handleReset() {
        this.updateIonTextOpacity(1);
        this.value = null;
        await this.ionInput.setFocus();
    }

    public updateIonTextOpacity(opacity: number): void {
        this.ionTextTpl.el.style.opacity = opacity;
        this.ionResetTpl.el.style.opacity = opacity;
    }
}

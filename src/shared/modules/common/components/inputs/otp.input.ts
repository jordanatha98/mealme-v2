import { Component, forwardRef, Input, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    selector: 'otp-input',
    templateUrl: './otp.input.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => OtpInput),
            multi: true,
        },
    ],
    styleUrls: ['./otp.input.scss'],
})
export class OtpInput implements ControlValueAccessor {
    @ViewChild('firstInput', { static: false }) public firstInput: any;
    @ViewChild('secondInput', { static: false }) public secondInput: any;
    @ViewChild('thirdInput', { static: false }) public thirdInput: any;
    @ViewChild('fourthInput', { static: false }) public fourthInput: any;
    @ViewChild('fifthInput', { static: false }) public fifthInput: any;
    @ViewChild('sixthInput', { static: false }) public sixthInput: any;

    @Input() public disabled: boolean;
    @Input() public nativeClass: any;
    @Input() public nativeStyle: any;

    public currIndex: number;
    public value: any;

    public constructor() {
        this.currIndex = 0;
        this.value = '';
    }

    public propagateChange = (_: any) => {};

    public registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }

    public registerOnTouched(fn: any): void {}

    public setDisabledState(isDisabled: boolean): void {}

    public writeValue(obj: any): void {
        if (obj !== '' && obj) {
            this.propagateChange(obj);
            this.value = obj;
        }
    }

    public updateValue(index: number, event: any): void {
        if (event.inputType === 'deleteContentBackward') {
            return;
        }

        if (event.inputType === 'insertFromPaste') {
            this.currIndex = 0;
            this.writeValue(event.target.value);
            const val = event.target.value.split('');
            val.forEach((item: any, i: number) => {
                this.currIndex = i;
                switch (i) {
                    case 0:
                        this.firstInput.value = item;
                        break;
                    case 1:
                        this.secondInput.value = item;
                        break;
                    case 2:
                        this.thirdInput.value = item;
                        break;
                    case 3:
                        this.fourthInput.value = item;
                        break;
                    case 4:
                        this.fifthInput.value = item;
                        break;
                    case 5:
                        this.sixthInput.value = item;
                        break;
                }
            });

            this.handleFocus();

            return;
        }

        if (event.target.value.length > 1) {
            event.target.value = '';
            return;
        }

        const code = this.value.split('');
        code[index] = event.target.value;

        this.currIndex = index;

        if (this.currIndex < 5) {
            this.currIndex += 1;
        }

        this.handleFocus();

        this.writeValue(code.join(''));
    }

    public deleteValue(index: number, event: any): void {
        this.currIndex = index;

        if (event.code === 'Backspace') {
            const code = this.value.split('');
            code[index] = '';

            event.target.value = '';
            if (this.currIndex > 0) {
                this.currIndex -= 1;
            }

            this.writeValue(code.join(''));
        }

        this.handleFocus();
    }

    public handleFocus(): void {
        switch (this.currIndex) {
            case 0:
                this.firstInput.setFocus().then();
                break;
            case 1:
                this.secondInput.setFocus().then();
                break;
            case 2:
                this.thirdInput.setFocus().then();
                break;
            case 3:
                this.fourthInput.setFocus().then();
                break;
            case 4:
                this.fifthInput.setFocus().then();
                break;
            case 5:
                this.sixthInput.setFocus().then();
                break;
        }
    }
}

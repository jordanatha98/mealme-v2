import { ChangeDetectionStrategy, Component, EventEmitter, forwardRef, Input, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { PickerController } from '@ionic/angular';

export interface PickerInputOption {
    name: string;
    selectedIndex?: number;
    options: PickerInputOptionType[];
}

export interface PickerInputOptionType {
    text: string;
    value: any;
}

@Component({
    selector: 'picker-input',
    templateUrl: './picker.input.html',
    styleUrls: ['./picker.input.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => PickerInput),
            multi: true,
        },
    ],
})
export class PickerInput implements ControlValueAccessor {
    @Input() public nativeClass: any;
    @Input() public nativeStyle: any;
    @Input() public placeholder: string;
    @Input() public toggleColor: string;
    @Input()
    public set color(color: string) {
        if (color) {
            this.nativeClass = this.nativeClass + ' form-control-' + color;
        }
    }
    @Input() public options: PickerInputOption[];
    @Input() public valuePrimitive: boolean;
    @Input('disabled')
    public set setDisabled(disabled: boolean) {
        this.setDisabledState(disabled);
    }

    @Output() public changed: EventEmitter<{ [key: string]: PickerInputOptionType }> = new EventEmitter<{[p: string]: PickerInputOptionType}>();

    public disabled: boolean;
    public label: any;
    public value: any;

    public constructor(
        private pickerCtrl: PickerController,
    ) {
    }

    public propagateChange = (_: any) => {};

    public registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }

    public registerOnTouched(fn: any): void {}

    public setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    public writeValue(obj: any): void {
        this.value = obj;
        this.handleUpdatePicker();
        this.propagateChange(obj);
    }

    public handleInput(value: { [key: string]: PickerInputOptionType }) {
        const data: PickerInputOptionType[] = [];

        for (const key in value) {
            data.push(value[key]);
        }

        if (data && data.length > 0) {
            if (data.length > 1) {
                if (this.valuePrimitive) {
                    this.writeValue(data.map(item => item.value));
                } else {
                    this.writeValue(data);
                }
            } else {
                if (this.valuePrimitive) {
                    this.writeValue(data[0].value);
                } else {
                    this.writeValue(data[0]);
                }
            }
        }
    }

    public handleUpdatePicker(): void {
        if (this.value && this.options) {
            if (Array.isArray(this.value)) {
                if (this.valuePrimitive) {
                    const data = [];
                    this.options.forEach((item, index) => {
                        const keyword = this.value[index];
                        const find = item.options.find(option => option.value === keyword);
                        item.selectedIndex = item.options.findIndex(option => option.value === keyword);
                        data.push({ value: keyword, text: find.text });
                    });

                    this.label = data.map(item => item.text).join(', ');
                } else {
                    this.options.forEach((item, index) => {
                        const keyword = this.value[index].value;
                        item.selectedIndex = item.options.findIndex(option => option.value === keyword);
                    });
                    this.label = this.value.map(item => item.text).join(', ');
                }
            } else {
                const data = this.options.map(item => item.options);
                const option = data.length > 0 ? data[0] : null;
                if (this.valuePrimitive) {
                    if (option) {
                        this.label = option.find(item => item.value === this.value).text;
                    }
                    this.options[0].selectedIndex = option.findIndex(item => item.value === this.value);
                } else {
                    this.label = this.value.text;
                    this.options[0].selectedIndex = option.findIndex(item => item.value === this.value.value);
                }
            }
        }
    }

    public async handleOpenPicker() {
        this.handleUpdatePicker();

        const picker = await this.pickerCtrl.create({
            mode: 'ios',
            columns: this.options,
            buttons: [
                {
                    text: 'Batal',
                    role: 'cancel',
                },
                {
                    text: 'Done',
                    handler: value => {
                        this.handleInput(value);
                        this.changed.emit(value);
                    },
                },
            ],
        });

        await picker.present();
    }
}


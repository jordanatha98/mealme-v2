import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnDestroy, Output, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';
import { Subscriber } from '@ubud/sate';
import { tap } from 'rxjs/operators';

@Component({
    selector: 'ion-paginator-component',
    templateUrl: './ion-paginator.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IonPaginatorComponent implements OnDestroy {
    @ViewChild('loader') public loader: ElementRef<any>;

    public offset: any;

    @Input() public pageable: boolean;
    @Input() public loading: boolean;
    @Output() public paginated: EventEmitter<boolean> = new EventEmitter<boolean>();

    @Input()
    public set content(element: IonContent) {
        if (element) {
            this.subscriber.subscribe(
                this,
                element.ionScroll.pipe(
                    tap(event => {
                        if (this.pageable) {
                            const { offsetTop, clientHeight } = this.loader.nativeElement;
                            const { detail, target } = event;
                            const scrollTop = detail.scrollTop || 0;
                            const offset = offsetTop - target.scrollHeight + clientHeight;

                            this.offset = {
                                ...detail,
                                data: JSON.stringify(this.loader.nativeElement),
                            };

                            if (scrollTop >= offset) {
                                this.paginated.emit(true);
                            }
                        }
                    }),
                ),
            );
        }
    }

    public constructor(
        private subscriber: Subscriber,
    ) {
    }

    public ngOnDestroy(): void {
        this.subscriber.flush(this);
    }
}

import { CurrencyPipe, DecimalPipe } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { MegaCurrencyPipe } from '@shared/modules/common/pipes/mega-currency.pipe';

@Component({
    selector: 'amount-range-content',
    templateUrl: './amount-range.content.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [MegaCurrencyPipe, CurrencyPipe, DecimalPipe],
})
export class AmountRangeContent {
    @Input() public min: number;
    @Input() public max: number;
    @Input() public currency: string;
    @Input() public mega: boolean;

    public constructor(private currencyPipe: CurrencyPipe, private megaCurrencyPipe: MegaCurrencyPipe) {}

    public get megaSalary(): string {
        if (this.min === this.max) {
            return this.megaCurrencyPipe.transform(this.min, this.currency);
        }

        if (this.min <= 0) {
            return `<= ${this.megaCurrencyPipe.transform(this.max, this.currency)}`;
        }

        if (this.max <= 0) {
            return `>= ${this.megaCurrencyPipe.transform(this.min, this.currency)}`;
        }

        return `${this.megaCurrencyPipe.transform(this.min, this.currency, null, true)} - ${this.megaCurrencyPipe.transform(this.max, this.currency, null, false, true)}`;
    }

    public get salary(): string {
        if (this.mega) {
            return this.megaSalary;
        }

        if (this.min === this.max) {
            return this.currencyPipe.transform(this.min, this.currency);
        }

        if (this.min <= 0) {
            return `<= ${this.currencyPipe.transform(this.max, this.currency)}`;
        }

        if (this.max <= 0) {
            return `>= ${this.currencyPipe.transform(this.min, this.currency)}`;
        }

        return `${this.currencyPipe.transform(this.min, this.currency)} - ${this.currencyPipe.transform(this.max, this.currency)}`;
    }
}

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'empty-fallback-content',
    templateUrl: './empty-fallback.content.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['./empty-fallback.content.scss'],
})
export class EmptyFallbackContent {
    @Input() public nativeStyle: any;

    public get style(): any {
        return { width: '100%', height: '300px', objectFit: 'contain', backgroundColor: 'transparent', ...this.nativeStyle };
    }
}

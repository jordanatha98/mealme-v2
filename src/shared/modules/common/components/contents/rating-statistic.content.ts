import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'rating-statistic-content',
    templateUrl: './rating-statistic.content.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['./rating-statistic.content.scss'],
})
export class RatingStatisticContent {
    @Input('data')
    public set setData(data: number[]) {
        this.totalRating = 0;
        this.averageRating = 0;
        this.ratingStatistics = [];

        if (data instanceof Array) {
            this.totalRating = data.reduce((a, b) => a + b, 0);

            this.bindStatistic(data);
            this.bindAverage();
        }
    }

    public ratingStatistics: { rate: number, total: number, percentage: number }[] = [];
    public totalRating: number = 0;
    public averageRating: number = 0;

    public bindAverage(): void {
        const rateTimesTotal = this.ratingStatistics.map((item) => {
            return item.rate * item.total;
        });

        const total = rateTimesTotal.reduce((a, b) => a + b, 0);

        this.averageRating = parseFloat((total / this.totalRating).toFixed(1));
    }

    public bindStatistic(data: number[]): void {
        for (let i = 4; i >= 0; i--) {
            this.ratingStatistics.push({
                rate: i + 1,
                total: data[i],
                percentage: parseFloat(((data[i] / this.totalRating) * 100).toFixed(0)),
            });
        }
    }
}

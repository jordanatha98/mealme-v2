import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'rating-statistic-skeleton-content',
    templateUrl: './rating-statistic-skeleton.content.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['./rating-statistic.content.scss'],
})
export class RatingStatisticSkeletonContent {}

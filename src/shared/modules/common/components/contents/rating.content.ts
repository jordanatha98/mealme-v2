import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'rating-content',
    templateUrl: './rating.content.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['./rating.content.scss'],
})
export class RatingContent implements OnInit {
    @Input() rating?: number;
    @Input() nativeClass: any;
    @Input() nativeStyle: any;
    @Input() showNumber: boolean;
    @Input() textClass: any;

    public totalRating: any[] = [];

    public ngOnInit(): void {
        if (this.rating) {
            const splitted: any[] = this.rating.toString().split('.');

            if (splitted.length === 1) {
                for (let i = 0; i < this.rating; i++) {
                    this.totalRating.push({
                        index: i + 1,
                        rating: ['fas', 'star'],
                        active: true,
                    });
                }
                for (let i = 0; i < Math.ceil(5 - this.rating); i++) {
                    this.totalRating.push({
                        index: i + 1,
                        rating: ['fas', 'star'],
                        active: false,
                    });
                }
            } else if (splitted.length > 1) {
                for (let i = 0; i < Math.floor(this.rating); i++) {
                    this.totalRating.push({
                        index: i + 1,
                        rating: ['fas', 'star'],
                        active: true,
                    });
                }
                for (let i = 0; i < Math.ceil(5 - this.rating); i++) {
                    this.totalRating.push({
                        index: i + 1,
                        rating: i === 0 ? ['fas', 'star-half-alt'] : ['far', 'star'],
                        active: i === 0,
                    });
                }
            }
        }
    }
}

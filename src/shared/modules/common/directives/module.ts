import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { SHARED_COMMON_DIRECTIVES } from '@shared/modules/common/directives/index';

const MODULES: any[] = [
    CommonModule,
    IonicModule,
];

@NgModule({
    declarations: [...SHARED_COMMON_DIRECTIVES],
    imports: [...MODULES],
    exports: [...SHARED_COMMON_DIRECTIVES],
})
export class SharedCommonDirectiveModule {}

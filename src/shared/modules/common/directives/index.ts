import { ScrollHorizontalDirective } from '@shared/modules/common/directives/scroll-horizontal.directive';
import { DisableControlDirective } from '@shared/modules/common/directives/disable-control.directive';
import { ScrollVerticalDirective } from '@shared/modules/common/directives/scroll-vertical.directive';

export const SHARED_COMMON_DIRECTIVES: any[] = [
    ScrollHorizontalDirective,
    ScrollVerticalDirective,
    DisableControlDirective,
];

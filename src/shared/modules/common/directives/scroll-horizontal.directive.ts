import { Directive, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';

@Directive({
    selector: '[scrollHorizontal]',
})
export class ScrollHorizontalDirective {
    @Input() public scrollHorizontal: boolean = true;

    @Output() public scrolled: EventEmitter<any> = new EventEmitter<any>();
    @Output() public scrolledStart: EventEmitter<any> = new EventEmitter<any>();
    @Output() public scrolledEnd: EventEmitter<any> = new EventEmitter<any>();

    public constructor(private el: ElementRef) {
    }

    @HostListener('scroll')
    public handleNativeScroll(): void {
        if (this.scrollHorizontal) {
            const element = this.el.nativeElement;

            const deltaStart = element.scrollLeft + element.offsetLeft;
            const deltaEnd = element.scrollLeft + element.offsetWidth;

            this.scrolled.emit(element.scrollLeft);

            if (deltaStart <= 0) {
                this.scrolledStart.emit();
            }

            if (deltaEnd >= element.scrollWidth) {
                this.scrolledEnd.emit();
            }
        }

        return;
    }
}

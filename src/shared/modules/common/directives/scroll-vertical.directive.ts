import { Directive, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';

@Directive({
    selector: '[scrollVertical]',
})
export class ScrollVerticalDirective {
    @Input() public scrollVertical: boolean;

    @Output() public scrolled: EventEmitter<any> = new EventEmitter<any>();
    @Output() public scrolledStart: EventEmitter<any> = new EventEmitter<any>();
    @Output() public scrolledEnd: EventEmitter<any> = new EventEmitter<any>();

    public constructor(private el: ElementRef) {
    }

    @HostListener('ionScroll', ['$event'])
    public handleIonScroll(event: CustomEvent): void {
        if (this.scrollVertical) {
            const { target, detail }: any = event;
            const first = target.firstElementChild.offsetParent ? target.firstElementChild.offsetParent.scrollHeight : (target.firstElementChild.scrollHeight || 0);
            const last = target.lastElementChild.offsetParent ? target.lastElementChild.offsetParent.scrollHeight : (target.lastElementChild.scrollHeight || 0);
            const offsetHeight = first <= target.scrollHeight ? last - target.scrollHeight : first - target.scrollHeight;

            const deltaStart = detail.scrollTop + target.offsetTop;
            const deltaEnd = detail.scrollTop;

            this.scrolled.emit(deltaEnd);

            if (deltaStart <= 0) {
                this.scrolledStart.emit();
                return;
            }

            if (deltaEnd >= offsetHeight) {
                this.scrolledEnd.emit();
                return;
            }
        }

        return;
    }

    @HostListener('scroll')
    public handleNativeScroll(): void {
        if (this.scrollVertical) {
            const element = this.el.nativeElement;

            const deltaStart = element.scrollTop + element.offsetTop;
            const deltaEnd = element.scrollTop + element.offsetHeight;

            this.scrolled.emit(element.scrollTop);

            if (deltaStart <= 0) {
                this.scrolledStart.emit();
            }

            if (deltaEnd >= element.scrollHeight) {
                this.scrolledEnd.emit();
            }
        }

        return;
    }
}

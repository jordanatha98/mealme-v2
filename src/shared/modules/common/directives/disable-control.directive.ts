import { Directive, Input } from '@angular/core';
import { NgControl } from '@angular/forms';

/* tslint:disable */
@Directive({
    selector: '[disableControl]',
})
export class DisableControlDirective {
    public constructor(private ngControl: NgControl) {}

    @Input()
    public set disableControl(condition: boolean) {
        const action = condition ? 'disable' : 'enable';
        if (this.ngControl.control) {
            this.ngControl.control![action]();
        }
    }
}

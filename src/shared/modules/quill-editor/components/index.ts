import { QuillTextComponent } from './quill-text.component';

export const QUILL_EDITOR_COMPONENTS: any[] = [QuillTextComponent];

import { ChangeDetectionStrategy, Component, forwardRef, Input, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { QuillEditorComponent } from 'ngx-quill';

@Component({
    selector: 'mealme-quill-text',
    templateUrl: './quill-text.component.html',
    changeDetection: ChangeDetectionStrategy.Default,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => QuillTextComponent),
            multi: true,
        },
    ],
    styleUrls: ['./quill-text.component.scss'],
})
export class QuillTextComponent implements ControlValueAccessor {
    @Input() public placeholder: string | undefined;
    @ViewChild('quillEditorTpl') public quillEditorTpl: QuillEditorComponent | undefined;

    public quilEditorConfig: any = {
        toolbar: [['bold', 'italic', 'underline', 'strike'], ['blockquote', 'code-block'], [{ list: 'ordered' }, { list: 'bullet' }], [{ indent: '-1' }, { indent: '+1' }], [{ color: [] }, { background: [] }], [{ align: [] }], ['link']],
    };

    public disabled: boolean | undefined;
    public value: any;

    public propagateChange = (_: any) => {
        // TODO:
    };

    public registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }

    public registerOnTouched(fn: any): void {
        // TODO:
    }

    public setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    public writeValue(obj: any): void {
        if (obj !== '' && obj && obj.html) {
            this.value = obj.html;
        } else if (typeof obj === 'string') {
            this.value = obj;
        }
    }

    public handleChanged(obj: any): void {
        if (obj !== '' && obj) {
            this.propagateChange(obj.html);
        }
    }
}

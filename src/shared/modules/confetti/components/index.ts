import { ConfettiComponent } from './confetti.component';

export const CONFETTI_COMPONENTS: any[] = [
    ConfettiComponent,
];

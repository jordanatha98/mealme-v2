import {
    AfterViewInit,
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    OnDestroy,
    ViewChild
} from '@angular/core';
import { ConfettiService } from '../services/confetti.service';
import { Subscriber } from '@ubud/sate';
import { tap } from 'rxjs/operators';
import { ConfettiType } from '../enums/confetti-type';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import * as confetti from 'canvas-confetti';

@Component({
    selector: 'mealme-shared-confetti',
    templateUrl: './confetti.component.html',
    styleUrls: ['./confetti.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfettiComponent implements OnDestroy, AfterViewInit {
    public confetti: any;

    public constructor(
        private service: ConfettiService,
        private subscriber: Subscriber
    ) {}

    @ViewChild('confettiCanvas') private confettiCanvas!: ElementRef;

    public ngAfterViewInit(): void {
        this.confetti = confetti.create(this.confettiCanvas.nativeElement, {
            resize: true
        });

        this.subscriber.subscribe(
            this,
            this.service.type$.pipe(
                tap(type => {
                    if (type) {
                        if (type === ConfettiType.REALISTIC) {
                            const count = 200;
                            const defaults = {
                                origin: { y: 0.7 }
                            };

                            const fire = (particleRatio: number, opts: { spread: number; startVelocity?: number; decay?: number; scalar?: number; }) => {
                                this.confetti(Object.assign({}, defaults, opts, {
                                    particleCount: Math.floor(count * particleRatio)
                                }));
                            };

                            fire(0.25, {
                                spread: 26,
                                startVelocity: 55,
                            });
                            fire(0.2, {
                                spread: 60,
                            });
                            fire(0.35, {
                                spread: 100,
                                decay: 0.91,
                                scalar: 0.8
                            });
                            fire(0.1, {
                                spread: 120,
                                startVelocity: 25,
                                decay: 0.92,
                                scalar: 1.2
                            });
                            fire(0.1, {
                                spread: 120,
                                startVelocity: 45,
                            });
                        } else if (type === ConfettiType.FIREWORKS) {
                            const duration = 3 * 1000;
                            const animationEnd = Date.now() + duration;
                            const defaults = { startVelocity: 30, spread: 360, ticks: 60, zIndex: 0 };

                            const randomInRange = (min: number, max: number) => {
                                return Math.random() * (max - min) + min;
                            }

                            const interval: any = setInterval(() => {
                                const timeLeft = animationEnd - Date.now();

                                if (timeLeft <= 0) {
                                    return clearInterval(interval);
                                }

                                const particleCount = 50 * (timeLeft / duration);
                                // since particles fall down, start a bit higher than random
                                this.confetti(Object.assign({}, defaults, { particleCount, origin: { x: randomInRange(0.1, 0.3), y: Math.random() - 0.2 } }));
                                this.confetti(Object.assign({}, defaults, { particleCount, origin: { x: randomInRange(0.7, 0.9), y: Math.random() - 0.2 } }));
                            }, 250);
                        }
                    }
                }),
            ),
        );
    }

    public ngOnDestroy(): void {
        this.service.reset();
        this.subscriber.flush(this);
    }
}

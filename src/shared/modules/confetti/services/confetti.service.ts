import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ConfettiType } from '../enums/confetti-type';

@Injectable({ providedIn: 'root' })
export class ConfettiService {
    private readonly typeSubject: BehaviorSubject<ConfettiType | null>;

    public constructor() {
        this.typeSubject = new BehaviorSubject<ConfettiType | null>(null);
    }

    public get type$(): Observable<ConfettiType | null> {
        return this.typeSubject.asObservable();
    }

    public show(type: ConfettiType): void {
        this.typeSubject.next(type);
    }

    public reset(): void {
        this.typeSubject.next(null);
    }
}

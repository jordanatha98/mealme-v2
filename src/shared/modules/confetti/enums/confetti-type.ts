export enum ConfettiType {
    REALISTIC = 'realistic',
    FIREWORKS = 'fireworks',
}

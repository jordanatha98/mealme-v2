import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CONFETTI_COMPONENTS } from './components';
import { IonicModule } from '@ionic/angular';

const MODULES: any[] = [CommonModule, IonicModule];

@NgModule({
    imports: [...MODULES],
    declarations: [...CONFETTI_COMPONENTS],
    exports: [IonicModule, ...CONFETTI_COMPONENTS],
})
export class MealmeSharedConfettiModule {}

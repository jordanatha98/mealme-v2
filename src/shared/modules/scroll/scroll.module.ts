import { NgModule } from '@angular/core';
import { ScrollDirective } from './directives/scroll.directive';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [
        ScrollDirective
    ],
    imports: [
        CommonModule
    ],
    exports: [
        ScrollDirective
    ]
})
export class MealmeSharedScrollModule {
}

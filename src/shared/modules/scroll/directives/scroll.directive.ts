import { Directive, EventEmitter, HostListener, Output } from '@angular/core';

@Directive({ selector: '[mealmeScrollSection]' })
export class ScrollDirective {
    @Output() scrollSection: EventEmitter<any> = new EventEmitter();

    @HostListener('(scroll)', ['$event'])
    public handleScroll(event: any): void {
        this.onScroll(event);
    }

    public onScroll(el: any) {
        this.scrollSection.emit(el.target.scrollTop);
    }
}


import { NgModule } from '@angular/core';
import { NumericInput } from './numeric.input';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [
        NumericInput,
    ],
    imports: [
        CommonModule,
    ],
    exports: [
        NumericInput,
    ],
})
export class MealmeSharedNumericInputModule {}

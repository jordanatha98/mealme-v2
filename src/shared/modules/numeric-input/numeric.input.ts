import {
    ChangeDetectorRef,
    Component,
    ElementRef,
    EventEmitter,
    forwardRef,
    Input,
    Output,
    ViewChild
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { CurrencyPipe, DecimalPipe } from '@angular/common';

@Component({
    selector: 'mealme-numeric-input',
    templateUrl: './numeric.input.html',
    styleUrls: ['./numeric.input.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => NumericInput),
            multi: true,
        },
        CurrencyPipe, DecimalPipe,
    ],
})
export class NumericInput implements ControlValueAccessor {
    @ViewChild('textInput') public textInput!: ElementRef;

    @Input() public format: any;
    @Input() public placeholder: any = '';
    @Input() public nativeClass: any;
    @Input() public inputNativeClass: any;
    @Input() public labelNativeClass: any;
    @Input() public disabled: boolean | undefined;
    @Input() public nativeStyle: any;
    @Input() public hideSymbol: boolean | undefined;

    @Input()
    public set value(val: string | null) {
        if (val !== '' && val != null) {
            // set input value with formatted [value] set
            this._formattedValue = this.formatNumber(val);
        } else {
            this._formattedValue = '';
        }

        this.cdRef.markForCheck();
    }

    @Output() public focused: EventEmitter<any> = new EventEmitter<any>();

    public _formattedValue: any = '';
    public currentValue: '' | undefined;

    public propagateChange = (_: any) => {
        // TODO:
    };

    public registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }

    registerOnTouched(fn: any): void {
        // TODO:
    }

    public writeValue(obj: any): void {
        if (this.value === '') {
            this.value = null;
        }
        this.propagateChange(obj);
        this.value = obj;
    }

    public constructor(private currencyPipe: CurrencyPipe, private decimalPipe: DecimalPipe, private cdRef: ChangeDetectorRef) {
    }

    // http://jsfiddle.net/1w9pm78z
    public onInput(event: any) {
        if (this.textInput) {
            const input = this.textInput.nativeElement;
            let cursorPosition = this.getCaretPosition(input);
            let inputValue = this.sanitizeNumber(input.value);
            const valueBefore = inputValue;
            const specialCharsBefore = this.getSpecialCharsOnSides(inputValue, cursorPosition);
            inputValue = this.formatNumber(this.getCleanValue(inputValue));

            if (inputValue === '' || inputValue == null) {
                input.value = '';
                this.writeValue('');
                return;
            }

            // if deleting the comma, delete it correctly
            if (
                this.currentValue === inputValue &&
                this.currentValue === valueBefore.substr(0, cursorPosition) + this.getThousandSeparator() + valueBefore.substr(cursorPosition)
            ) {
                inputValue = this.formatNumber(
                    this.removeThousandSeparators(valueBefore.substr(0, cursorPosition - 1) + valueBefore.substr(cursorPosition)),
                );
                cursorPosition--;
            }

            // if entering comma for separation, leave it in there (as well support .000)
            const commaSeparator = this.getCommaSeparator();
            if (
                valueBefore.endsWith(commaSeparator) ||
                valueBefore.endsWith(commaSeparator + '0') ||
                valueBefore.endsWith(commaSeparator + '00') ||
                valueBefore.endsWith(commaSeparator + '000')
            ) {
                inputValue = inputValue + valueBefore.substring(valueBefore.indexOf(commaSeparator));
            }

            // move cursor correctly if thousand separator got added or removed
            const specialCharsAfter = this.getSpecialCharsOnSides(inputValue, cursorPosition);
            if (specialCharsBefore[0] < specialCharsAfter[0]) {
                cursorPosition += specialCharsAfter[0] - specialCharsBefore[0];
            } else if (specialCharsBefore[0] > specialCharsAfter[0]) {
                cursorPosition -= specialCharsBefore[0] - specialCharsAfter[0];
            }

            const maxFractionDigits = this.format.maximumFractionDigits ?? 2;
            if (maxFractionDigits === 0) {
                inputValue = this.removeCommaSeparators(inputValue);
            }
            input.value = inputValue;
            this.currentValue = inputValue;
            this.writeValue(parseFloat(this.getCleanValue(inputValue)));
            this.setCaretPosition(input, cursorPosition);
        }
    }

    // get clean value string of number (e.g. Rp100.000,00 => 100000.00)
    public getCleanValue(number: any) {
        return this.removeThousandSeparators(number).replace(this.getCommaSeparator(), '.');
    }

    // get special chars on left and right
    public getSpecialCharsOnSides(x: any, cursorPosition: any) {
        const specialCharsLeft = x.substring(0, undefined).replace(/[0-9]/g, '').length;
        const specialCharsRight = x.substring(undefined).replace(/[0-9]/g, '').length;
        return [specialCharsLeft, specialCharsRight];
    }

    // call angular currency pipe, get currency info from [format] input
    public currencyTransform(x: any, forceMaximumFractionDigits: any = null) {
        const currency = this.format.style === 'currency' ? (this.format.currency ?? 'IDR') : null;
        const currencyDisplay = this.format.style === 'currency' ? (this.format.currencyDisplay ?? 'symbol') : null;
        const minIntegerDigits = this.format.minimumFractionDigits ?? 0;
        const minFractionDigits = this.format.minimumFractionDigits ?? 0;
        const maxFractionDigits = forceMaximumFractionDigits ?? this.format.maximumFractionDigits ?? 2;
        const digitsInfo = `${ minIntegerDigits }.${ minFractionDigits }-${ maxFractionDigits }`;

        if (currency && currencyDisplay) {
            return this.currencyPipe.transform(x, currency, currencyDisplay, digitsInfo);
        }

        return this.decimalPipe.transform(x, digitsInfo);
    }

    // set number format (e.g. 1000000.00 => 1.000.000,00)
    public formatNumber(x: any) {
        const thousandSeparator = this.getThousandSeparator();
        const commaSeparator = this.getCommaSeparator();

        // remove chars except number, decimal point.
        const reg = new RegExp(`[^0-9${ commaSeparator }${ thousandSeparator }]`, 'g');

        return this.currencyTransform(x)?.replace(reg, '');
    }

    /* From: https://stackoverflow.com/a/53169365 */
    public sanitizeNumber(x: any) {
        const thousandSeparator = this.getThousandSeparator();
        const commaSeparator = this.getCommaSeparator();

        // remove chars except number, decimal point.
        const firstRe = new RegExp(`[^0-9${ commaSeparator }${ thousandSeparator }]`, 'g');

        // remove multiple decimal points.
        const secondRe = new RegExp(`(${ commaSeparator }${ commaSeparator }*)${ commaSeparator }`, 'g');

        const thirdRe = new RegExp(`/${ commaSeparator }(?=${ commaSeparator }*\\${ commaSeparator })`, 'g');

        return (
            x
                .replace(firstRe, '')
                .replace(secondRe, '$1')
                // .replace(/(,*),/, a => a.replace(/,/g, '') + ',');
                .replace(/,(?=.*,)/g, '')
        );
    }

    // remove thousand separator from string number (e.g. 1.000.000,00 => 1000000,00)
    public removeThousandSeparators(x: any) {
        const thousandSeparator = this.getThousandSeparator();
        const regex = new RegExp(this.escapeRegExp(thousandSeparator), 'g');
        return x.toString().replace(regex, '');
    }

    public removeCommaSeparators(x: any) {
        const commaSeparator = this.getCommaSeparator();
        const regex = new RegExp(this.escapeRegExp(commaSeparator), 'g');
        return x.toString().replace(regex, '');
    }

    // get currency symbo from string number (e.g. IDR => Rp)
    public getCurrencySymbol() {
        const thousandSeparator = this.getThousandSeparator();
        const commaSeparator = this.getCommaSeparator();

        // remove all number and decimal point.
        const reg = new RegExp(`[0-9${ commaSeparator }${ thousandSeparator }]`, 'g');

        const currency = this.currencyTransform(0);
        if (currency) {
            return currency.replace(reg, '');
        }

        return '';
    }

    // get thousand separator from current currency (e.g. IDR => (.) dot)
    public getThousandSeparator() {
        const currency = this.currencyTransform(1000);
        if (currency) {
            const str = currency.replace(/[0-9]/g, '');
            return str.charAt(str.length - 1);
        }

        return '';
    }

    // get decimal point separator from current currency (e.g. IDR => (,) comma)
    public getCommaSeparator() {
        const currency = this.currencyTransform(0.01, 2);

        if (currency) {
            const str = currency.replace(/[0-9]/g, '');
            return str.charAt(str.length - 1);
        }

        return '';
    }

    /* From: http://stackoverflow.com/a/6969486/496992 */
    public escapeRegExp(str: string) {
        //eslint-disable-line
        //eslint-disable-next-line
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
    }

    /*
     ** Returns the caret (cursor) position of the specified text field.
     ** Return value range is 0-oField.value.length.
     ** From: http://stackoverflow.com/a/2897229/496992
     */
    public getCaretPosition(oField: any) {
        // Initialize
        let iCaretPos = 0;

        // IE Support
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        if (document.selection) {
            // Set focus on the element
            oField.focus();

            // To get cursor position, get empty selection range
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            const oSel = document.selection.createRange();

            // Move selection start to 0 position
            oSel.moveStart('character', -oField.value.length);

            // The caret position is selection length
            iCaretPos = oSel.text.length;
        } else if (oField.selectionStart || oField.selectionStart === '0') {
            iCaretPos = oField.selectionStart;
        }

        return iCaretPos;
    }

    /* From: http://stackoverflow.com/a/512542/496992 */
    public setCaretPosition(elem: any, caretPos: any) {
        if (elem != null) {
            if (elem.createTextRange) {
                const range = elem.createTextRange();
                range.move('character', caretPos);
                range.select();
            } else {
                if (elem.selectionStart) {
                    elem.focus();
                    elem.setSelectionRange(caretPos, caretPos);
                } else {
                    elem.focus();
                }
            }
        }
    }

    public handleFocus(): void {
        if (this.textInput && this.textInput.nativeElement) {
            if (this.disabled) {
                this.textInput.nativeElement.click();
            } else {
                this.textInput.nativeElement.focus();
            }
        }
    }

    public setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }
}

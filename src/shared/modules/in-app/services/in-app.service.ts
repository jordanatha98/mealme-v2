import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Subscriber } from '@ubud/sate';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class InAppService {
    public fragments$: Observable<URLSearchParams>;
    public isInApp$: Observable<boolean>;
    public accessToken$: Observable<string | null>;
    public refreshToken$: Observable<string | null>;

    public constructor(private route: ActivatedRoute, private subscriber: Subscriber) {
        this.fragments$ = this.route.fragment.pipe(
            map((fragment) => {
                return new URLSearchParams(fragment || '');
            }),
        );

        this.isInApp$ = this.fragments$.pipe(
            map((params) => {
                if (params) {
                    return !!params.get('in_app');
                }

                return false;
            }),
        );

        this.accessToken$ = this.fragments$.pipe(
            map((params) => {
                return params.get('access_token');
            }),
        );

        this.refreshToken$ = this.fragments$.pipe(
            map((params) => {
                return params.get('refresh_token');
            }),
        );
    }

    public bindOauth(): void {
    }
}

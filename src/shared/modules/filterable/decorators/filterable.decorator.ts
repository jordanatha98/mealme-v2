export function FilterableDecorator(props: { keys: string[], configs: { [key: string]: { type: any, fields: string[] } }}) {
    return function <T>(target: any) {
        target.filterableKeys = props.keys;
        target.prototype.filterableKeys = props.keys;
        target.filterableConfigs = props.configs;
        target.prototype.filterableConfigs = props.configs;
    };
}

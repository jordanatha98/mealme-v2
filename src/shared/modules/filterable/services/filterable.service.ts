import { Injectable } from '@angular/core';
import { Subscriber } from '@ubud/sate';
import { BehaviorSubject, Observable } from 'rxjs';
import { FilterOption } from '../types/filter-option';
import { DataUtil } from '../../../utils/data.util';
import { first, map, tap } from 'rxjs/operators';
import { plainToClass } from 'class-transformer';

@Injectable()
export class FilterableService {
    public filterValues: { [key: string]: any } = {};
    public filterValues$: Observable<{ [key: string]: FilterOption[] }>;
    public filterPrimitives: { [key: string]: any } = {};

    private readonly filterValuesSubject: BehaviorSubject<{ [key: string]: FilterOption[] }> = new BehaviorSubject<{ [p: string]: FilterOption[] }>({});

    public constructor(
        private subscriber: Subscriber
    ) {
        this.filterValues$ = this.filterValuesSubject.asObservable();
    }

    public printFilterOptions(options: any): any[] {
        return options.map((item: any) => item.text).join(', ');
    }

    public filterOptions$(excludes?: string[], only?: string[]): Observable<(FilterOption | FilterOption[] | any)[]> {
        return this.filterValues$.pipe(
            map(data => {
                const arr: any = [];

                for (const key in data) {
                    if (data[key]) {
                        if (Array.isArray(data[key])) {
                            arr.push(...data[key]);
                        } else {
                            arr.push(data[key]);
                        }
                    }
                }

                if (excludes) {
                    return arr.filter((item: { key: string; }) => {
                        return !excludes.find(excludeItem => excludeItem === item.key);
                    });
                }

                if (only) {
                    return arr.filter((item: { key: string; }) => {
                        return only.find(onlyItem => onlyItem === item.key);
                    });
                }

                return arr;
            })
        );
    }

    public handleFilter(key: string, data: any, valueField?: string, textField?: string): void {
        this.subscriber.subscribe(
            this,
            this.filterValues$.pipe(
                first(),
                tap(values => {
                    const obj = { ...values };
                    obj[key] = this.mapValue(key, data, valueField || 'id', textField || 'name');

                    this.filterValuesSubject.next(obj);
                })
            )
        );
        this.filterValues[key] = data;
    }

    public handleBulkFilter(items: { key: string, data: any, valueField?: string, textField?: string }[]): void {
        this.subscriber.subscribe(
            this,
            this.filterValues$.pipe(
                first(),
                tap(values => {
                    const obj: any = { ...values };
                    items.forEach(item => {
                        if (item.data) {
                            obj[item.key] = this.mapValue(item.key, item.data, item.valueField || 'id', item.textField || 'name');
                        } else {
                            obj[item.key] = null;
                        }
                    });
                    this.filterValuesSubject.next(obj);
                })
            )
        );

        items.forEach(item => {
            this.filterValues[item.key] = item.data;
        });
    }

    public handleMultipleFilter(key: string, data: any, valueField: string = 'id', textField: string = 'text', bulk?: any): void {
        const filterValue = this.filterValues[key];

        if (filterValue) {
            if (Array.isArray(filterValue)) {
                if (bulk) {
                    this.handleBulkFilter([
                        {
                            key,
                            data: [
                                ...filterValue,
                                data
                            ],
                            valueField,
                            textField
                        },
                        {
                            ...bulk,
                        }
                    ]);
                } else {
                    this.handleFilter(
                        key,
                        [
                            ...filterValue,
                            data
                        ],
                        valueField,
                        textField
                    );
                }
            } else {
                if (bulk) {
                    this.handleBulkFilter([
                        {
                            key,
                            data: [
                                filterValue,
                                data
                            ],
                            valueField,
                            textField
                        },
                        {
                            ...bulk,
                        }
                    ]);
                } else {
                    this.handleFilter(
                        key,
                        [
                            filterValue,
                            data
                        ],
                        valueField,
                        textField
                    );
                }
            }
        } else {
            if (bulk) {
                this.handleBulkFilter([
                    {
                        key,
                        data,
                        valueField,
                        textField
                    },
                    {
                        ...bulk,
                    }
                ]);
            } else {
                this.handleFilter(
                    key,
                    data,
                    valueField,
                    textField
                );
            }
        }
    }

    public getFirst(key: string): any {
        if (this.filterValues && this.filterValues[key]) {
            const data = this.filterValues[key];

            if (Array.isArray(data) && data.length > 0) {
                return data[0];
            } else if (Array.isArray(data) && data.length <= 0) {
                return null;
            }

            return data;
        }

        return null;
    }

    public handleFilterPaginate(target: any, data: any, value: any, valueField: string = 'id', textField: string = 'text'): void {
        const checked = value.detail.checked;

        if (checked) {
            this.handleBulkFilter([
                {
                    key: target,
                    data,
                    valueField,
                    textField,
                },
                {
                    key: 'page',
                    data: null,
                    valueField: 'value',
                    textField: 'text',
                },
            ]);
        } else {
            this.handleRemove(target, data[valueField]);
        }
    }

    public handleFilterPaginateMultiple(target: any, data: any, value: any, valueField: string = 'id', textField: string = 'text'): void {
        const checked = value.detail.checked;

        if (checked) {
            this.handleMultipleFilter(target, data, valueField, textField, {
                key: 'page',
                data: null,
                valueField: 'value',
                textField: 'text',
            });
        } else {
            this.handleRemove(target, data[valueField]);
        }
    }

    public isExists(key: string, predicate?: any, valueField?: string): boolean {
        const obj = { ...this.filterValues };
        const data: any = obj[key];
        if (!data) {
            return !!obj[key];
        } else {
            if (Array.isArray(data)) {
                const finded = data.find(item => item[valueField || 'value'] === predicate);

                return !!finded;
            }

            return data[valueField || 'value'] === predicate;
        }
    }

    public handleRemove(key: string, predicate?: any): void {
        this.subscriber.subscribe(
            this,
            this.filterValues$.pipe(
                first(),
                tap(values => {
                    const obj = { ...values };
                    const data = obj[key];
                    if (Array.isArray(data) && predicate) {
                        const finded = data.findIndex(item => item.value === predicate);
                        data.splice(finded, 1);
                        this.filterValues[key] = data.map(item => item.source);
                    } else {
                        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                        // @ts-ignore
                        obj[key] = null;
                        this.filterValues[key] = null;
                    }

                    this.filterValuesSubject.next(obj);
                })
            )
        );
    }

    public handleReset(key?: string | any[]): void {
        if (key) {
            if (typeof key === 'string') {
                this.handleFilter(key, []);
            } else if (Array.isArray(key)) {
                const arr: any = key;

                this.handleBulkFilter(arr.map((item: any) => {
                    return {
                        key: item,
                        data: [],
                    };
                }));
            }
        } else {
            this.filterValuesSubject.next({});
            this.filterValues = {};
        }
    }

    public bind(str: string, configs: { [key: string]: { type: any, fields: string[] } }): void {
        const data = this.normalizeToValues(str, configs);
        this.filterValuesSubject.next(data);

        for (const key in data) {
            this.filterValues[key] = data[key].map(item => item.source);
        }
    }

    public init(keys: string[]): void {
        this.filterValuesSubject.next(DataUtil.convertArrayToObject(keys, null, []));

        this.subscriber.subscribe(
            this,
            this.filterValues$.pipe(
                tap(values => {
                    for (const key in values) {
                        if (typeof values[key] === 'object') {
                            const data: any = values[key];
                            if (data) {
                                if (Array.isArray(data)) {
                                    if (data.length > 0) {
                                        this.filterPrimitives[key] = data[0]['value'];
                                    } else {
                                        this.filterPrimitives[key] = null;
                                    }
                                } else {
                                    if (data['value']) {
                                        this.filterPrimitives[key] = data['value'];
                                    } else {
                                        this.filterPrimitives[key] = null;
                                    }
                                }
                            } else {
                                this.filterPrimitives[key] = null;
                            }
                        } else {
                            this.filterPrimitives[key] = null;
                        }
                    }
                }),
            ),
        );
    }

    public flush(obj: any): void {
        this.subscriber.flush(obj);
    }

    public normalizeOptions(options: FilterOption[]): any {
        const obj = {};

        options.forEach(item => {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            if (obj[item.key]) {
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                obj[item.key].push({
                    value: item.value,
                    text: item.text
                });
            } else {
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                obj[item.key] = [{
                    value: item.value,
                    text: item.text
                }];
            }
        });

        return obj;
    }

    public normalizeToValues(str: string, configs?: { [key: string]: { type: any, fields: string[] } }): { [key: string]: FilterOption[] } {
        if (str) {
            const arr = str.split('|').filter(item => !!item && item !== '');
            const data = arr.map(item => {
                if (item && item !== '') {
                    const obj = item.split(':');
                    if (obj.length > 0) {
                        const key = obj[0];
                        const valueObj = obj[1].split(';');
                        const value = valueObj.map(valueItem => {
                            const valueData = valueItem.split('#');
                            let source = null;
                            if (configs) {
                                const config = configs[key];
                                const generatedCls = {};
                                config.fields.forEach(item => {
                                    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                                    // @ts-ignore
                                    generatedCls[item] = null;
                                });

                                if (config.fields.length === 2) {
                                    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                                    // @ts-ignore
                                    generatedCls[config.fields[0]] = valueData[0];
                                    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                                    // @ts-ignore
                                    generatedCls[config.fields[1]] = valueData[1];
                                }

                                source = plainToClass(config.type, generatedCls);
                            }

                            return {
                                value: valueData[0],
                                text: valueData[1],
                                key,
                                source: source || {}
                            };
                        });

                        return {
                            key,
                            value: value
                        };
                    }

                    return null;
                }

                return null;
            });

            const obj = {};
            data.forEach(item => {
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                obj[item.key] = item.value;
            });

            return obj;
        }

        return {};
    }

    public normalizeToString(obj: { [key: string]: FilterOption[] }): string | null {
        let length = 0;
        let max = 0;
        let str = '';

        for (const key in obj) {
            max++;
        }

        for (const key in obj) {
            const value = obj[key].map(item => {
                if (item.value !== null && item.text !== null) {
                    return `${ item.value }#${ item.text }`;
                }

                return null;
            }).join(';');

            length++;

            if (value) {
                if (length < max) {
                    str += `${ key }:${ value }|`;
                } else {
                    str += `${ key }:${ value }`;
                }
            }
        }

        return length > 0 ? str : null;
    }

    public mapValue(key: string, data: any, valueField: string, textField: string): any {
        if (Array.isArray(data)) {
            const mapped = data.map(item => {
                const value = valueField.split('.')
                    .reduce((acc, cur) => {
                        return acc[cur];
                    }, item) || null;

                const text = textField.split('.')
                    .reduce((acc, cur) => {
                        return acc[cur];
                    }, item) || null;

                return {
                    key,
                    value,
                    text,
                    source: item
                };
            });

            return mapped;
        }

        const value = valueField.split('.')
            .reduce((acc, cur) => {
                return acc[cur];
            }, data) || null;

        const text = textField.split('.')
            .reduce((acc, cur) => {
                return acc[cur];
            }, data) || null;

        return {
            key,
            value,
            text,
            source: data
        };
    }
}

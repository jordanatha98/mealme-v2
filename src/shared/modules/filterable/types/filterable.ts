export interface Filterable {
    filterableKeys: string[];
    filterableConfigs: { [key: string]: { type: any, fields: string[] } };
}

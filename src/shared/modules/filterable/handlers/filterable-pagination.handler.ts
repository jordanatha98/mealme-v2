import { FilterableService } from '../services/filterable.service';
import { first, tap } from 'rxjs/operators';
import { Subscriber } from '@ubud/sate';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

export class FilterablePaginationHandler {
    public filterOptionKeyEnum: any;
    public filterableConfigs!: { [p: string]: { type: any; fields: string[] } };
    public filterableKeys!: string[];

    public keyword: string | undefined | null;
    public keyword$: Observable<string | undefined | null>;
    private readonly keywordSubject: BehaviorSubject<string | undefined | null> = new BehaviorSubject<string | undefined | null>(undefined);

    public constructor(
        public filterable: FilterableService,
        protected route: ActivatedRoute,
        protected subscriber: Subscriber,
    ) {
        this.filterable.init(this.filterableKeys);

        this.keyword$ = this.keywordSubject.asObservable();
    }

    public handleKeywordChanged(keyword: string): void {
        if (typeof keyword !== 'undefined') {
            if (keyword === '') {
                this.keywordSubject.next(null);
            } else {
                this.keywordSubject.next(keyword);
            }
        }
    }

    public handleFilter(target: any, data: any, value: any, valueField: string = 'id', textField: string = 'text'): void {
        this.filterable.handleFilterPaginate(target, data, value, valueField, textField);
    }

    public handleFilterMultiple(target: any, data: any, value: any, valueField: string = 'id', textField: string = 'text'): void {
        this.filterable.handleFilterPaginateMultiple(target, data, value, valueField, textField);
    }

    public handleBinds(async?: boolean): void {}

    public handleRemove(key: any, predicate?: any): void {
        this.filterable.handleRemove(key, predicate);
        this.handleBinds(true);
    }

    public handlePrepareFilter(): void {
        this.subscriber.subscribe(
            this,
            this.route.queryParams.pipe(
                first(),
                tap(({ filters }) => {
                    this.filterable.bind(filters, this.filterableConfigs);
                }),
            ),
        );

        this.subscriber.subscribe(
            this,
            this.keyword$.pipe(
                tap(keyword => {
                    if (keyword) {
                        this.filterable.handleBulkFilter([
                            {
                                key: this.filterOptionKeyEnum.KEYWORD,
                                data: { id: keyword, text: keyword },
                                valueField: 'id',
                                textField: 'text',
                            },
                            {
                                key: this.filterOptionKeyEnum.PAGE,
                                data: null,
                                valueField: 'value',
                                textField: 'text',
                            },
                        ]);
                    } else {
                        this.filterable.handleReset(this.filterOptionKeyEnum.KEYWORD);
                    }

                    if (keyword !== undefined) {
                        this.handleBinds(true);
                    }
                }),
            ),
        );
    }
}

import { NgModule } from '@angular/core';
import { StickyDirective } from './directives/sticky.directive';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [
        StickyDirective,
    ],
    imports: [
        CommonModule,
    ],
    exports: [
        StickyDirective,
    ]
})
export class MealmeSharedStickyModule {}

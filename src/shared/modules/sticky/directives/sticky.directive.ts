import { Directive, ElementRef, HostListener, Input, OnInit } from '@angular/core';

@Directive({
    selector: '[mealmeSticky]',
})
export class StickyDirective implements OnInit{
    public scrollTop: number | undefined;

    public constructor(
        private el: ElementRef,
    ) {}

    @Input() public stickingClass = 'sticking';

    public ngOnInit(): void {
        this.checkSticking();
    }

    public checkSticking() {
        const el = this.el.nativeElement;
        const top = parseInt(window.getComputedStyle(el).getPropertyValue('top').replace('px', ''));
        el.classList.toggle(this.stickingClass, el.getBoundingClientRect().top <= top);

        // Not Working in Safari < 12.1
        // const observer = new IntersectionObserver(
        //     ([e]) => {
        //         e.target.classList.toggle('sticking', e.intersectionRatio < 1)
        //     },
        //     {
        //         rootMargin: `${-top-1}px 100% 100% 100%`,
        //         threshold: [1],
        //     }
        // );
        // observer.observe(el);
    }

    @HostListener('document:scroll', ['$event'])
    public handleScroll(): void {
        this.scrollTop = window.scrollY;
        this.checkSticking();
    }
}

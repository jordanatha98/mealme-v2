import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ConfirmationService {
    private readonly confirmingSubject: BehaviorSubject<boolean>;
    private readonly confirmedSubject: BehaviorSubject<boolean>;
    private readonly titleSubject: BehaviorSubject<string>;
    private readonly questionSubject: BehaviorSubject<string>;
    private readonly cancelButtonTextSubject: BehaviorSubject<string>;
    private readonly confirmButtonTextSubject: BehaviorSubject<string>;
    private readonly confirmButtonColorSubject: BehaviorSubject<string>;

    public constructor() {
        this.confirmingSubject = new BehaviorSubject<boolean>(false);
        this.confirmedSubject = new BehaviorSubject<boolean>(false);
        this.titleSubject = new BehaviorSubject<string>('Lanjutkan');
        this.questionSubject = new BehaviorSubject<string>('Aksi ini bersifat permanen, apakah Anda yakin ingin melanjutkan?');
        this.cancelButtonTextSubject = new BehaviorSubject<string>('Batalkan');
        this.confirmButtonTextSubject = new BehaviorSubject<string>('Ya');
        this.confirmButtonColorSubject = new BehaviorSubject<string>('primary');
    }

    public get confirming$(): Observable<boolean> {
        return this.confirmingSubject.asObservable();
    }

    public get confirmed$(): Observable<boolean> {
        return this.confirmedSubject.asObservable();
    }

    public get title$(): Observable<string> {
        return this.titleSubject.asObservable();
    }

    public get question$(): Observable<string> {
        return this.questionSubject.asObservable();
    }

    public get cancelButtonText$(): Observable<string> {
        return this.cancelButtonTextSubject.asObservable();
    }

    public get confirmButtonText$(): Observable<string> {
        return this.confirmButtonTextSubject.asObservable();
    }

    public get confirmButtonColor$(): Observable<string> {
        return this.confirmButtonColorSubject.asObservable();
    }

    public confirming(confirm: boolean, texts?: any): void {
        this.confirmingSubject.next(confirm);

        if (texts) {
            if (texts.title !== undefined) {
                this.titleSubject.next(texts.title);
            }

            if (texts.question !== undefined) {
                this.questionSubject.next(texts.question);
            }

            if (texts.cancelButtonText !== undefined) {
                this.cancelButtonTextSubject.next(texts.cancelButtonText);
            }

            if (texts.confirmButtonText !== undefined) {
                this.confirmButtonTextSubject.next(texts.confirmButtonText);
            }

            if (texts.confirmButtonColor !== undefined) {
                this.confirmButtonColorSubject.next(texts.confirmButtonColor);
            }
        }
    }

    public confirmed(yes: boolean): void {
        this.confirmedSubject.next(yes);
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CONFIRMATION_DIRECTIVES } from './directives';
import { CONFIRMATION_COMPONENTS } from './components';
import { IonicModule } from '@ionic/angular';
import { MealmeSharedFeatherIconsModule } from '@shared/modules/feather-icons/feather-icons.module';

const MODULES: any[] = [CommonModule, IonicModule, MealmeSharedFeatherIconsModule];

@NgModule({
    imports: [...MODULES],
    declarations: [...CONFIRMATION_DIRECTIVES, ...CONFIRMATION_COMPONENTS],
    exports: [IonicModule, ...CONFIRMATION_DIRECTIVES, ...CONFIRMATION_COMPONENTS],
})
export class MealmeSharedConfirmationModule {}

import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ConfirmationService } from '../../../services/confirmation.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'mealme-shared-confirmation-modal',
    templateUrl: './confirmation.modal.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfirmationModalComponent {
    public title$: Observable<string>;
    public question$: Observable<string>;
    public cancelButtonText$: Observable<string>;
    public confirmButtonText$: Observable<string>;

    public constructor(
        private service: ConfirmationService,
    ) {
        this.title$ = this.service.title$;
        this.question$ = this.service.question$;
        this.cancelButtonText$ = this.service.cancelButtonText$;
        this.confirmButtonText$ = this.service.confirmButtonText$;
    }

    public confirmed(): void {
        this.service.confirmed(true);
        this.service.confirming(false);
    }

    public close(): void {
        this.service.confirmed(false);
        this.service.confirming(false);
    }
}

import { ConfirmationDialogComponent } from './confirmation-dialog/ionic/confirmation-dialog.component';
import { ConfirmationModalComponent } from './confirmation-modal/ionic/confirmation-modal.component';

export const CONFIRMATION_COMPONENTS: any[] = [
    ConfirmationDialogComponent,
    ConfirmationModalComponent
];

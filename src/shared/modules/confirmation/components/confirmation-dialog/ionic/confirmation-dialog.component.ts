import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ConfirmationService } from '../../../services/confirmation.service';
import { ModalController } from '@ionic/angular';
import { Subscriber } from '@ubud/sate';
import { tap } from 'rxjs/operators';
import { ConfirmationModalComponent } from '@shared/modules/confirmation/components/confirmation-modal/ionic/confirmation-modal.component';

@Component({
    selector: 'mealme-shared-confirmation-dialog',
    templateUrl: './confirmation-dialog.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfirmationDialogComponent implements OnInit, OnDestroy{
    public confirming$: Observable<boolean>;
    public title$: Observable<string>;
    public modal: HTMLIonModalElement | undefined;

    public constructor(
        private service: ConfirmationService,
        private modalController: ModalController,
        private subscriber: Subscriber,
    ) {
        this.confirming$ = this.service.confirming$;
        this.title$ = this.service.title$;
    }

    public ngOnInit(): void {
        this.subscriber.subscribe(
            this,
            this.confirming$.pipe(
                tap(async (confirming: boolean) => {
                    if (confirming) {
                        this.modal = await this.modalController.create({
                            component: ConfirmationModalComponent,
                            cssClass: 'confirmation-modal mobile-modal modal-rounded px-4'
                        });

                        await this.modal.present();
                    } else {
                        if (this.modal) {
                            this.modal.dismiss().then(() => this.modal = undefined);
                        }
                    }
                })
            )
        )
    }

    public ngOnDestroy(): void {
        this.subscriber.flush(this);
    }
}

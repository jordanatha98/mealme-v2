import { ConfirmationDirective } from './confirmation.directive';

export const CONFIRMATION_DIRECTIVES: any[] = [ConfirmationDirective];

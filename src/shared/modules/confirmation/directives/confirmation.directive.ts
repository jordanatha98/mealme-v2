import { Directive, EventEmitter, HostListener, Input, OnDestroy, Output } from '@angular/core';
import { Subscriber } from '@ubud/sate';
import { tap } from 'rxjs/operators';
import { ConfirmationService } from '../services/confirmation.service';

@Directive({
    selector: '[mealmeConfirmation]',
})
export class ConfirmationDirective implements OnDestroy {
    @Output() public confirmed: EventEmitter<any>;

    @Input() public title: string | undefined;
    @Input() public question: string | undefined;
    @Input() public cancelButtonText: string | undefined;
    @Input() public confirmButtonText: string | undefined;
    @Input() public confirmButtonColor: string | undefined;

    public constructor(private subscriber: Subscriber, private service: ConfirmationService) {
        this.confirmed = new EventEmitter<any>();
    }

    @HostListener('click', ['$event'])
    public onClick(event: any): any {
        const texts = {
            title: this.title,
            question: this.question,
            cancelButtonText: this.cancelButtonText,
            confirmButtonText: this.confirmButtonText,
            confirmButtonColor: this.confirmButtonColor,
        };

        this.service.confirming(true, texts);

        this.service.confirmed(false);
        this.subscriber.subscribe(
            this,
            this.service.confirmed$.pipe(
                tap((result: any) => {
                    if (result) {
                        this.confirmed.emit();
                    }
                    this.subscriber.flush(this);
                }),
            ),
        );
    }

    public ngOnDestroy(): void {
        this.subscriber.flush(this);
    }
}

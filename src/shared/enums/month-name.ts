import { EnumOption } from '@shared/enums/enum-option';
import { NullEnumOption } from '@shared/models/null-enum-option';

export enum MonthName {
    JANUARI = '1',
    FEBRUARI = '2',
    MARET = '3',
    APRIL = '4',
    MEI = '5',
    JUNI = '6',
    JULI = '7',
    AGUSTUS = '8',
    SEPTEMBER = '9',
    OKTOBER = '10',
    NOVEMBER = '11',
    DESEMBER = '12',
}

export namespace MonthName {
    export function getValues(): EnumOption<MonthName>[] {
        return [
            { id: MonthName.JANUARI, text: 'January' },
            { id: MonthName.FEBRUARI, text: 'February' },
            { id: MonthName.MARET, text: 'March' },
            { id: MonthName.APRIL, text: 'April' },
            { id: MonthName.MEI, text: 'May' },
            { id: MonthName.JUNI, text: 'June' },
            { id: MonthName.JULI, text: 'July' },
            { id: MonthName.AGUSTUS, text: 'August' },
            { id: MonthName.SEPTEMBER, text: 'September' },
            { id: MonthName.OKTOBER, text: 'October' },
            { id: MonthName.NOVEMBER, text: 'November' },
            { id: MonthName.DESEMBER, text: 'December' },
        ];
    }

    export function find(id: MonthName): EnumOption<MonthName> {
        const search = MonthName.getValues().find(item => item.id === id.toString());

        return search ? search : new NullEnumOption();
    }
}

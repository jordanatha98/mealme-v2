import { EnumOption } from '@shared/enums/enum-option';
import { Color } from '@shared/enums/ui';
import { NullEnumOption } from '@shared/models/null-enum-option';

export enum Day {
    MONDAY = 1,
    TUESDAY = 2,
    WEDNESDAY = 3,
    THURSDAY = 4,
    FRIDAY = 5,
    SATURDAY = 6,
    SUNDAY = 7,
}

export namespace Day {
    export function getValues(): EnumOption<Day>[] {
        return [
            { id: Day.MONDAY, text: 'Monday', color: Color.Primary },
            { id: Day.TUESDAY, text: 'Tuesday', color: Color.Success },
            { id: Day.WEDNESDAY, text: 'Wednesday', color: Color.Info },
            { id: Day.THURSDAY, text: 'Thursday', color: Color.Info },
            { id: Day.FRIDAY, text: 'Friday', color: Color.Info },
            { id: Day.SATURDAY, text: 'Saturday', color: Color.Info },
            { id: Day.SUNDAY, text: 'Sunday', color: Color.Info },

        ];
    }

    export function find(status: Day): EnumOption<Day> {
        const search = Day.getValues().find(item => item.id === status);

        return search ? search : new NullEnumOption();
    }
}

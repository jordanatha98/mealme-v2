export interface EnumOption<T> {
    id: T;
    text: string;
    color?: string;
    logo?: string;
}

export interface FilterEnumOption<T> extends EnumOption<T> {
    checked: boolean;
}

export interface FilterOption<T> {
    data: T;
    checked: boolean;
}

import { EnumOption } from '@shared/enums/enum-option';
import { Color } from '@shared/enums/ui';
import { NullEnumOption } from '@shared/models/null-enum-option';

export enum MaritalStatus {
    MARRIED = 10,
    SINGLE = 20,
    WIDOWER = 30,
    WIDOW = 40,
}

export namespace MaritalStatus {
    export function getValues(): EnumOption<MaritalStatus>[] {
        return [
            { id: MaritalStatus.SINGLE, text: 'Belum Menikah', color: Color.Primary },
            { id: MaritalStatus.MARRIED, text: 'Sudah Menikah', color: Color.Success },
            { id: MaritalStatus.WIDOWER, text: 'Duda', color: Color.Info },
            { id: MaritalStatus.WIDOW, text: 'Janda', color: Color.Info },
        ];
    }

    export function find(status: MaritalStatus): EnumOption<MaritalStatus> {
        const search = MaritalStatus.getValues().find(item => item.id === status);

        return search ? search : new NullEnumOption();
    }
}

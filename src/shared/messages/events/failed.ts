import { Message } from '@ubud/ngrx';
import { HttpErrorResponse } from '@angular/common/http';

export abstract class Failed<T> extends Message<T> {
    public exception!: HttpErrorResponse;

    public handle(state: T): T {
        return { ...state };
    }
}

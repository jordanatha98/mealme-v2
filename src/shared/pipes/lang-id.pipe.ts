import { Pipe, PipeTransform } from '@angular/core';
import { LangId } from '../enums/lang-id';

@Pipe({
    name: 'langId',
})
export class LangIdPipe implements PipeTransform {
    public transform(value: any, key: string): any {
        const val: any = LangId.find(value);
        return val[key];
    }
}

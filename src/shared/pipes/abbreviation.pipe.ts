import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'abbreviate',
})
export class AbbreviationPipe implements PipeTransform {
    public transform(text: string): string {
        const words = text.split(' ');
        if (words.length > 1) {
            return words.slice(0, 2)
                .map(word => word.charAt(0))
                .join('');
        }

        return text.substring(0, 2).toUpperCase();
    }
}

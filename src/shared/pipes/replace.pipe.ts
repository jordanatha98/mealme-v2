import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'replace',
})
export class ReplacePipe implements PipeTransform {
    public transform(value: any, searchValue: any, newValue: any): string {
        return value.replace(searchValue, newValue);
    }
}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'age',
})
export class AgePipe implements PipeTransform {
    public transform(value: Date): string {
        const thisYear = (new Date()).getFullYear();
        const valueYear = value.getFullYear();
        return (thisYear - valueYear).toString();
    }
}

import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';

@Pipe({
    name: 'megaCurrency',
    pure: true,
})
export class MegaCurrencyPipe implements PipeTransform {
    public constructor(private currency: CurrencyPipe) {}

    public transform(number: number, currencyCode?: string, fraction?: any): string | null {
        if (number === null) return null;
        if (number === 0) return '0';

        if (!fraction || fraction < 0) fraction = 2;

        let abs = Math.abs(number);
        const rounder = Math.pow(10, fraction);
        const isNegative = number < 0;
        let key = '';
        const powers = [
            { key: 'Q', value: Math.pow(10, 15) },
            { key: 'T', value: Math.pow(10, 12) },
            { key: 'M', value: Math.pow(10, 9) },
            { key: 'jt', value: Math.pow(10, 6) },
            { key: 'rb', value: 1000 },
        ];

        for (let i = 0; i < powers.length; i++) {
            let reduced = abs / powers[i].value;

            reduced = Math.round(reduced * rounder) / rounder;

            if (reduced >= 1) {
                if (reduced / Math.round(reduced) === Math.round(reduced) / Math.round(reduced)) {
                    reduced = Math.round(reduced * rounder) / rounder;
                    reduced = Math.round(reduced);
                }
                abs = reduced;
                key = powers[i].key;
                break;
            }
        }

        if (abs === 0) {
            fraction = 0;
        }

        const curr = this.currency.transform(abs, currencyCode, 'symbol', '1.' + fraction);
        const str = curr ? curr.split(',') : null;
        let result = this.currency.transform(abs, currencyCode, 'symbol', '1.2-1' + fraction);

        if (str && str.length > 0) {
            const separator = str[1];

            if (separator && separator === '00') {
                result = this.currency.transform(abs, currencyCode, 'symbol', '1.' + 0);
            }
        }

        return (isNegative ? '-' : '') + result + key;
    }
}

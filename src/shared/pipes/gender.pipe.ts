import { Pipe, PipeTransform } from '@angular/core';
import { Gender } from '@sisnaker/domain/common';

@Pipe({
    name: 'gender',
})
export class GenderPipe implements PipeTransform {
    public transform(value: Gender, key: string): string {
        const val: any = Gender.find(value);
        return val[key];
    }
}

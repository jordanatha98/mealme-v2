import { Pipe, PipeTransform } from '@angular/core';
import { TermUnit } from '../enums/term-unit';

@Pipe({
    name: 'termUnit',
})
export class TermUnitPipe implements PipeTransform {
    public transform(value: TermUnit, key: string): any {
        const val: any = TermUnit.find(value);

        return val[key];
    }
}

import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';
import { LangIdPipe } from './lang-id.pipe';
import { MegaNumberPipe } from './mega-number.pipe';
import { TermUnitPipe } from './term-unit.pipe';
import { MegaCurrencyPipe } from './mega-currency.pipe';
import { NullStripPipe } from './null-strip.pipe';
import { AbbreviationPipe } from './abbreviation.pipe';
import { GenderPipe } from './gender.pipe';
import { TimeAgoPipe } from './time-ago.pipe';
import { AgePipe } from './age.pipe';
import { TruncatePipe } from './truncate.pipe';
import { ReplacePipe } from './replace.pipe';
import { MonthPipe } from './month.pipe';
import { UrlSanitizerPipe } from './url-sanitizer.pipe';
import { GenderLabelPipe } from './gender-label.pipe';

@NgModule({
    declarations: [
        LangIdPipe,
        MegaNumberPipe,
        TermUnitPipe,
        MegaCurrencyPipe,
        NullStripPipe,
        AbbreviationPipe,
        GenderPipe,
        GenderLabelPipe,
        TimeAgoPipe,
        AgePipe,
        ReplacePipe,
        MonthPipe,
        TruncatePipe,
        UrlSanitizerPipe,
    ],
    imports: [
        CommonModule,
    ],
    exports: [
        LangIdPipe,
        MegaNumberPipe,
        TermUnitPipe,
        MegaCurrencyPipe,
        NullStripPipe,
        AbbreviationPipe,
        GenderPipe,
        GenderLabelPipe,
        TimeAgoPipe,
        AgePipe,
        TruncatePipe,
        ReplacePipe,
        MonthPipe,
        UrlSanitizerPipe,
    ],
    providers: [CurrencyPipe],
})
export class MealmeSharedPipesModule {}

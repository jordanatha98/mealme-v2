import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'megaNumber',
    pure: true,
})
export class MegaNumberPipe implements PipeTransform {
    public constructor() {}

    public transform(number: number, fraction?: any): string | null {
        if (number === null) return null;
        if (number === 0) return '0';

        if ((!fraction && fraction != 0) || fraction < 0) fraction = 2;

        let abs = Math.abs(number);
        const isNegative = number < 0;
        let key = '';
        const powers = [
            { key: 'Q', value: Math.pow(10, 15) },
            { key: 'T', value: Math.pow(10, 12) },
            { key: 'M', value: Math.pow(10, 9) },
            { key: 'jt', value: Math.pow(10, 6) },
            { key: 'rb', value: Math.pow(10, 3) },
        ];

        for (let i = 0; i < powers.length; i++) {
            const reduced = abs / powers[i].value;
            if (reduced >= 1) {
                abs = reduced;
                key = powers[i].key;
                break;
            }
        }

        if (abs === 0) {
            fraction = 0;
        }

        return (isNegative ? '-' : '') + abs.toFixed(fraction).replace('.', ',') + key;
    }
}

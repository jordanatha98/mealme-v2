import { Pipe, PipeTransform } from '@angular/core';
import { GenderLabel } from '@sisnaker/domain/common';

@Pipe({
    name: 'genderLabel',
})
export class GenderLabelPipe implements PipeTransform {
    public transform(value: GenderLabel, key: string): string {
        const val: any = GenderLabel.find(value);
        return val[key];
    }
}

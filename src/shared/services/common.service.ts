import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

@Injectable({ providedIn: 'root' })
export class SharedCommonService {
    public constructor(
        @Inject(PLATFORM_ID) private platformId: any,
    ) {
    }

    public get image(): any {
        if (isPlatformBrowser(this.platformId)) {
            return new Image();
        }
    }

    public get window(): any {
        if (isPlatformBrowser(this.platformId)) {
            return window;
        }
    }
}

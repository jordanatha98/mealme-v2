import { Store, UbudStore } from '@ubud/ngrx';
import { Injectable } from '@angular/core';
import { MealmeInteractionState } from './states';

@UbudStore('mealmeInteraction')
@Injectable()
export class MealmeInteractionStore extends Store<MealmeInteractionState> {
}

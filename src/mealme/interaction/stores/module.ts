import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { mealmeInteractionReducer } from '@mealme/src/interaction/stores/mealme-interaction.reducer';
import { MealmeInteractionStore } from '@mealme/src/interaction/stores/mealme-interaction.store';

@NgModule({
    imports: [
        StoreModule.forFeature('mealmeInteraction', mealmeInteractionReducer),
    ],
    providers: [
        MealmeInteractionStore,
    ],
})
export class MealmeInteractionMealmeStoreModule {}

import { createReducer, Message } from '@ubud/ngrx';
import { MealmeInteractionState } from '@mealme/src/interaction/stores/states';
import { MEALME_INTERACTION_PRODUCT_MESSAGES } from '@mealme/src/interaction/modules/product/messages';
import { UserManageInteraction } from '@mealme/src/interaction/modules/auth/enums/user-manage-interaction';
import { MEALME_INTERACTION_AUTH_MESSAGES } from '@mealme/src/interaction/modules/auth/messages';
import { AuthLoginInteraction } from '@mealme/src/interaction/modules/auth/enums/auth-login-interaction';
import { MEALME_INTERACTION_MEAL_MESSAGES } from '@mealme/src/interaction/modules/meal/messages';
import { PreferencesManageInteraction } from '@mealme/src/interaction/modules/account/enums/preferences-manage-interaction';
import { MEALME_INTERACTION_ACCOUNT_MESSAGES } from '@mealme/src/interaction/modules/account/messages';
import { AuthLogoutInteraction } from '@mealme/src/interaction/modules/auth/enums/auth-logout-interaction';
import { GeneratePlanInteraction } from '@mealme/src/interaction/modules/meal/enums/generate-plan-interaction';
import { BookmarkInteraction } from '@mealme/src/interaction/modules/meal/enums/bookmark-interaction';
import { MenuInteraction } from '@mealme/src/interaction/modules/meal/enums/menu-interaction';

const INITIAL_STATE: MealmeInteractionState = {
    product: {
        index: {
            process: false,
            error: null,
        }
    },
    auth: {
        user: {
            manage: {
                process: false,
                error: null,
                success: null,
                interaction: UserManageInteraction.IDLE
            }
        },
        login: {
            process: false,
            error: null,
            interaction: AuthLoginInteraction.IDLE,
            success: null,
        },
        logout: {
            process: false,
            interaction: AuthLogoutInteraction.IDLE
        }
    },
    common: {
        companyPosition: {
            index: {
                process: false,
                error: null,
            }
        }
    },
    meal: {
        meal: {
            process: false,
            error: null,
        },
        meals: {
            process: false,
            error: null,
        },
        planMenus: {
            process: false,
            error: null,
            generate: {
                process: false,
                error: null,
                success: null,
                interaction: GeneratePlanInteraction.IDLE
            }
        },
        menu: {
            detail: {
                process: false,
                error: null,
            },
            nutrients: {
                process: false,
                error: null,
            },
            ingredients: {
                process: false,
                error: null,
            },
            instructions: {
                process: false,
                error: null,
            },
            manage: {
                process: false,
                error: null,
                success: null,
                interaction: MenuInteraction.IDLE,
                response: null,
            }
        },
        bookmark: {
            index: {
                process: false,
                error: null,
            },
            manage: {
                process: false,
                error: null,
                success: null,
                interaction: BookmarkInteraction.IDLE,
            }
        }
    },
    account: {
        preferences: {
            manage: {
                process: false,
                error: null,
                interaction: PreferencesManageInteraction.IDLE,
                success: null,
            }
        }
    }
};

export function mealmeInteractionReducer(state: MealmeInteractionState = INITIAL_STATE, action: Message<MealmeInteractionState>): MealmeInteractionState {
    const messages: any[] = [
        ...MEALME_INTERACTION_PRODUCT_MESSAGES,
        ...MEALME_INTERACTION_AUTH_MESSAGES,
        ...MEALME_INTERACTION_MEAL_MESSAGES,
        ...MEALME_INTERACTION_ACCOUNT_MESSAGES,
    ];

    return createReducer<MealmeInteractionState>(...messages)(state, action);
}

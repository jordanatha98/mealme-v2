import { UserManageInteraction } from '@mealme/src/interaction/modules/auth/enums/user-manage-interaction';
import { AuthLoginInteraction } from '@mealme/src/interaction/modules/auth/enums/auth-login-interaction';
import { PreferencesManageInteraction } from '@mealme/src/interaction/modules/account/enums/preferences-manage-interaction';
import { AuthLogoutInteraction } from '@mealme/src/interaction/modules/auth/enums/auth-logout-interaction';
import { GeneratePlanInteraction } from '@mealme/src/interaction/modules/meal/enums/generate-plan-interaction';
import { BookmarkInteraction } from '@mealme/src/interaction/modules/meal/enums/bookmark-interaction';
import { MenuInteraction } from '@mealme/src/interaction/modules/meal/enums/menu-interaction';
import { CustomerMenu } from '@mealme/src/domain/modules/meal/models/customer-menu';

interface ProductState {
    index: {
        process: boolean;
        error: string;
    };
}

interface AccountState {
    preferences: {
        manage: {
            process: boolean;
            error: string;
            success: string;
            interaction: PreferencesManageInteraction;
        }
    }
}

interface AuthState {
    user: {
        manage: {
            process: boolean;
            error: string;
            success: string;
            interaction: UserManageInteraction;
        };
    };

    login: {
        process: boolean;
        error: string;
        interaction: AuthLoginInteraction;
        success: string;
    };
    logout: {
        process: boolean;
        interaction: AuthLogoutInteraction;
    }
}

interface CommonState {
    companyPosition: {
        index: {
            process: boolean;
            error: string;
        }
    };
}

interface MealState {
    meal: {
        process: boolean;
        error: string;
    };
    meals: {
        process: boolean;
        error: string;
    };
    planMenus: {
        process: boolean;
        error: string;
        generate: {
            process: boolean;
            error: string;
            success: string;
            interaction: GeneratePlanInteraction
        }
    };
    menu: {
        detail: {
            process: boolean;
            error: string;
        };
        nutrients: {
            process: boolean;
            error: string;
        }
        ingredients: {
            process: boolean;
            error: string;
        };
        instructions: {
            process: boolean;
            error: string;
        };
        manage: {
            process: boolean;
            error: string;
            success: string;
            interaction: MenuInteraction;
            response: CustomerMenu;
        }
    };
    bookmark: {
        index: {
            process: boolean;
            error: string;
        };
        manage: {
            process: boolean;
            error: string;
            success: string;
            interaction: BookmarkInteraction;
        }
    }
}


export interface MealmeInteractionState {
    product: ProductState;
    auth: AuthState;
    common: CommonState;
    meal: MealState;
    account: AccountState;
}

export enum PreferencesManageInteraction {
    IDLE = 'idle',
    SET_EXECUTING = 'set_executing',
    SET_SUCCEED = 'set_succeed',
    SET_FAILED = 'set_failed',
}

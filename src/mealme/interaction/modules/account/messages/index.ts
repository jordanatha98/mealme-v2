import { PreferencesManageInteractionChanged } from '@mealme/src/interaction/modules/account/messages/events/preferences-manage-interaction-changed';

export const MEALME_INTERACTION_ACCOUNT_MESSAGES: any[] = [
    PreferencesManageInteractionChanged
];

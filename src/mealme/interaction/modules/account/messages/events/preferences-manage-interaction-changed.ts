import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeInteractionState } from '@mealme/src/interaction/stores/states';

@UbudMessage('Interaction.Account.PreferencesManageInteractionChanged')
export class PreferencesManageInteractionChanged extends Message<MealmeInteractionState> {
    public changes: any;

    public handle(state: MealmeInteractionState): MealmeInteractionState {
        return {
            ...state,
            account: {
                ...state.account,
                preferences: {
                    ...state.account.preferences,
                    manage: {
                        ...state.account.preferences.manage,
                        ...this.changes,
                    }
                },
            },
        };
    }
}

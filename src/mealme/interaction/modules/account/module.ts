import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { MealmeInteractionMealmeStoreModule } from '../../stores/module';
import { MealmeDomainAccountModule } from '@mealme/src/domain/modules/account/module';
import { PreferencesManageInteractionEffect } from '@mealme/src/interaction/modules/account/effects/preferences-manage-interaction.effect';

@NgModule({
    imports: [
        MealmeInteractionMealmeStoreModule,
        MealmeDomainAccountModule,
        EffectsModule.forFeature([
            PreferencesManageInteractionEffect,
        ]),
    ],
})
export class MealmeInteractionAccountModule {}

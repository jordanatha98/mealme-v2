import { Injectable } from '@angular/core';
import { Repository } from '@ubud/ngrx';
import { Observable } from 'rxjs';
import { MealmeInteractionState } from '@mealme/src/interaction/stores/states';
import { MealmeInteractionStore } from '@mealme/src/interaction/stores/mealme-interaction.store';
import { PreferencesManageInteraction } from '@mealme/src/interaction/modules/account/enums/preferences-manage-interaction';

@Injectable()
export class PreferencesManageRepository extends Repository<MealmeInteractionState> {
    public constructor(store: MealmeInteractionStore) {
        super(store);
    }

    public isProcess$(): Observable<boolean> {
        return this.select(state => state.account.preferences.manage.process);
    }

    public getError$(): Observable<string> {
        return this.select(state => state.account.preferences.manage.error);
    }

    public getInteraction$(): Observable<PreferencesManageInteraction> {
        return this.select(state => state.account.preferences.manage.interaction);
    }

    public getSuccess$(): Observable<string> {
        return this.select(state => state.account.preferences.manage.success);
    }
}

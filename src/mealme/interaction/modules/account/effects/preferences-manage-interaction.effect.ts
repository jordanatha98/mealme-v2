import { Injectable } from '@angular/core';
import { Effects, Message, ubudType } from '@ubud/ngrx';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Authenticate } from '@mealme/src/domain/modules/auth/messages/commands/auth/authenticate';
import { AuthenticateSucceed } from '@mealme/src/domain/modules/auth/messages/events/auth/authenticate-succeed';
import { AuthenticateFailed } from '@mealme/src/domain/modules/auth/messages/events/auth/authenticate-failed';
import { MessageUtil } from '@shared/utils/message.util';
import { HttpUtil } from '@shared/utils/http.util';
import { PreferencesManageInteractionChanged } from '@mealme/src/interaction/modules/account/messages/events/preferences-manage-interaction-changed';
import { PreferencesManageInteraction } from '@mealme/src/interaction/modules/account/enums/preferences-manage-interaction';
import { SetPreferences } from '@mealme/src/domain/modules/account/messages/commands/set-preferences';
import { SetPreferencesSucceed } from '@mealme/src/domain/modules/account/messages/events/set-preferences-succeed';
import { SetPreferencesFailed } from '@mealme/src/domain/modules/account/messages/events/set-preferences-failed';

@Injectable()
export class PreferencesManageInteractionEffect extends Effects {
    public constructor(actions$: Actions) {
        super(actions$);
    }

    @Effect()
    public setPreferences$: Observable<Message> = this.actions$.pipe(
        ubudType(SetPreferences),
        map(() => new PreferencesManageInteractionChanged({
            changes: {
                process: true,
                error: null,
                interaction: PreferencesManageInteraction.SET_EXECUTING,
                success: null,
            },
        })),
    );

    @Effect()
    public setPreferencesSucceed$: Observable<Message> = this.actions$.pipe(
        ubudType(SetPreferencesSucceed),
        map(() => new PreferencesManageInteractionChanged({
            changes: {
                process: false,
                error: null,
                interaction: PreferencesManageInteraction.SET_SUCCEED,
                success: 'Success setting your preferences',
            },
        })),
    );

    @Effect()
    public setPreferencesFailed$: Observable<Message> = this.actions$.pipe(
        ubudType(SetPreferencesFailed),
        map(message => MessageUtil.getPayload<SetPreferencesFailed>(message)),
        map(({ exception }) => new PreferencesManageInteractionChanged({
            changes: {
                process: false,
                error: HttpUtil.errorExtractor(exception),
                interaction: PreferencesManageInteraction.SET_FAILED,
                success: null,
            },
        })),
    );
}

import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeInteractionState } from '@mealme/src/interaction/stores/states';

@UbudMessage('Interaction.Auth.LogoutInteractionChanged')
export class LogoutInteractionChanged extends Message<MealmeInteractionState> {
    public changes: any;

    public handle(state: MealmeInteractionState): MealmeInteractionState {
        return {
            ...state,
            auth: {
                ...state.auth,
                logout: {
                    ...state.auth.logout,
                    ...this.changes,
                },
            },
        };
    }
}

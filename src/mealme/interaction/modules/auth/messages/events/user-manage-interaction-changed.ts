import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeInteractionState } from '@mealme/src/interaction/stores/states';

@UbudMessage('Interaction.Auth.User.UserManageInteractionChanged')
export class UserManageInteractionChanged extends Message<MealmeInteractionState> {
    public changes: any;

    public handle(state: MealmeInteractionState): MealmeInteractionState {
        return {
            ...state,
            auth: {
                ...state.auth,
                user: {
                    ...state.auth.user,
                    manage: {
                        ...state.auth.user.manage,
                        ...this.changes,
                    }
                },
            },
        };
    }
}

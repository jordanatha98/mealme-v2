import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeInteractionState } from '@mealme/src/interaction/stores/states';

@UbudMessage('Interaction.Auth.LoginInteractionChanged')
export class LoginInteractionChanged extends Message<MealmeInteractionState> {
    public changes: any;

    public handle(state: MealmeInteractionState): MealmeInteractionState {
        return {
            ...state,
            auth: {
                ...state.auth,
                login: {
                    ...state.auth.login,
                    ...this.changes,
                },
            },
        };
    }
}

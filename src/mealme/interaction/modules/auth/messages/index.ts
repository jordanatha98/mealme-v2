import { UserManageInteractionChanged } from '@mealme/src/interaction/modules/auth/messages/events/user-manage-interaction-changed';
import { LoginInteractionChanged } from '@mealme/src/interaction/modules/auth/messages/events/login-interaction-changed';
import {
    LogoutInteractionChanged
} from '@mealme/src/interaction/modules/auth/messages/events/logout-interaction-changed';

export const MEALME_INTERACTION_AUTH_MESSAGES: any[] = [
    UserManageInteractionChanged,
    LoginInteractionChanged,
    LogoutInteractionChanged,
];

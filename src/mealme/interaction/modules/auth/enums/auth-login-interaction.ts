export enum AuthLoginInteraction {
    IDLE = 'idle',
    AUTHENTICATE_EXECUTED = 'authenticate_executed',
    AUTHENTICATE_SUCCEED = 'authenticate_succeed',
    AUTHENTICATE_FAILED = 'authenticate_failed',
}

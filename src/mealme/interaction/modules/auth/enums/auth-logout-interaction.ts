export enum AuthLogoutInteraction {
    IDLE = 'idle',
    SIGN_OUT_EXECUTED = 'sign_out_executed',
    SIGN_OUT_SUCCEED = 'sign_out_succeed',
}
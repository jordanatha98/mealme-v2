export enum UserManageInteraction {
    IDLE = 'idle',
    CREATE_EXECUTING = 'create_executing',
    CREATE_SUCCEED = 'create_succeed',
    CREATE_FAILED = 'create_failed',

    VERIFY_EXECUTING = 'verify_executing',
    VERIFY_SUCCEED = 'verify_succeed',
    VERIFY_FAILED = 'verify_failed',

    CREATE_CUSTOMER_EXECUTING = 'create_customer_executing',
    CREATE_CUSTOMER_SUCCEED = 'create_customer_succeed',
    CREATE_CUSTOMER_FAILED = 'create_customer_failed',

    CREATE_MERCHANT_EXECUTING = 'create_merchant_executing',
    CREATE_MERCHANT_SUCCEED = 'create_merchant_succeed',
    CREATE_MERCHANT_FAILED = 'create_merchant_failed',
}

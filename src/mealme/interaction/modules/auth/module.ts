import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { MealmeInteractionMealmeStoreModule } from '../../stores/module';
import { MealmeDomainAuthModule } from '@mealme/src/domain/modules/auth/module';
import { UserManageInteractionEffect } from '@mealme/src/interaction/modules/auth/effects/user/user-manage-interaction.effect';
import { LoginInteractionEffect } from '@mealme/src/interaction/modules/auth/effects/auth/login-interaction.effect';
import { LogoutInteractionEffect } from '@mealme/src/interaction/modules/auth/effects/auth/logout-interaction.effect';

@NgModule({
    imports: [
        MealmeInteractionMealmeStoreModule,
        MealmeDomainAuthModule,
        EffectsModule.forFeature([
            UserManageInteractionEffect,
            LoginInteractionEffect,
            LogoutInteractionEffect,
        ]),
    ],
})
export class MealmeInteractionAuthModule {}

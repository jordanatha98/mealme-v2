import { Injectable } from '@angular/core';
import { Effects, Message, ubudType } from '@ubud/ngrx';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { CreateUser } from '@mealme/src/domain/modules/auth/messages/commands/user/create-user';
import { map } from 'rxjs/operators';
import { UserManageInteractionChanged } from '@mealme/src/interaction/modules/auth/messages/events/user-manage-interaction-changed';
import { UserManageInteraction } from '@mealme/src/interaction/modules/auth/enums/user-manage-interaction';
import { CreateUserSucceed } from '@mealme/src/domain/modules/auth/messages/events/user/create-user-succeed';
import { CreateUserFailed } from '@mealme/src/domain/modules/auth/messages/events/user/create-user-failed';
import { MessageUtil } from '@shared/utils/message.util';
import { HttpUtil } from '@shared/utils/http.util';
import { VerifyUser } from '@mealme/src/domain/modules/auth/messages/commands/user/verify-user';
import { VerifyUserSucceed } from '@mealme/src/domain/modules/auth/messages/events/user/verify-user-succeed';
import { VerifyUserFailed } from '@mealme/src/domain/modules/auth/messages/events/user/verify-user-failed';
import { CreateCustomer } from '@mealme/src/domain/modules/auth/messages/commands/user/create-customer';
import { CreateCustomerSucceed } from '@mealme/src/domain/modules/auth/messages/events/user/create-customer-succeed';
import { CreateCustomerFailed } from '@mealme/src/domain/modules/auth/messages/events/user/create-customer-failed';

@Injectable()
export class UserManageInteractionEffect extends Effects {
    public constructor(actions$: Actions) {
        super(actions$);
    }

    @Effect()
    public createUser$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateUser),
        map(() => new UserManageInteractionChanged({
            changes: {
                process: true,
                error: null,
                success: null,
                interaction: UserManageInteraction.CREATE_EXECUTING,
            },
        })),
    );

    @Effect()
    public createUserSucceed$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateUserSucceed),
        map(() => new UserManageInteractionChanged({
            changes: {
                process: false,
                error: null,
                success: 'Success creating this user',
                interaction: UserManageInteraction.CREATE_SUCCEED,
            },
        })),
    );

    @Effect()
    public createUserFailed$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateUserFailed),
        map(message => MessageUtil.getPayload<CreateUserFailed>(message)),
        map(({ exception }) => {
            return new UserManageInteractionChanged({
                changes: {
                    process: false,
                    error: HttpUtil.errorExtractor(exception),
                    success: null,
                    interaction: UserManageInteraction.CREATE_FAILED,
                },
            });
        }),
    );

    @Effect()
    public verifyUser$: Observable<Message> = this.actions$.pipe(
        ubudType(VerifyUser),
        map(() => new UserManageInteractionChanged({
            changes: {
                process: true,
                error: null,
                success: null,
                interaction: UserManageInteraction.VERIFY_EXECUTING,
            },
        })),
    );

    @Effect()
    public verifyUserSucceed$: Observable<Message> = this.actions$.pipe(
        ubudType(VerifyUserSucceed),
        map(() => new UserManageInteractionChanged({
            changes: {
                process: false,
                error: null,
                success: 'Success verifying this user',
                interaction: UserManageInteraction.VERIFY_SUCCEED,
            },
        })),
    );

    @Effect()
    public verifyUserFailed$: Observable<Message> = this.actions$.pipe(
        ubudType(VerifyUserFailed),
        map(message => MessageUtil.getPayload<VerifyUserFailed>(message)),
        map(({ exception }) => {
            return new UserManageInteractionChanged({
                changes: {
                    process: false,
                    error: HttpUtil.errorExtractor(exception),
                    success: null,
                    interaction: UserManageInteraction.IDLE,
                },
            });
        }),
    );

    @Effect()
    public createCustomer$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateCustomer),
        map(() => new UserManageInteractionChanged({
            changes: {
                process: true,
                error: null,
                success: null,
                interaction: UserManageInteraction.CREATE_CUSTOMER_EXECUTING,
            },
        })),
    );

    @Effect()
    public createCustomerSucceed$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateCustomerSucceed),
        map(() => new UserManageInteractionChanged({
            changes: {
                process: false,
                error: null,
                success: 'Success creating this customer account',
                interaction: UserManageInteraction.CREATE_CUSTOMER_SUCCEED,
            },
        })),
    );

    @Effect()
    public createCustomerFailed$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateCustomerFailed),
        map(message => MessageUtil.getPayload<CreateCustomerFailed>(message)),
        map(({ exception }) => {
            return new UserManageInteractionChanged({
                changes: {
                    process: false,
                    error: HttpUtil.errorExtractor(exception),
                    success: null,
                    interaction: UserManageInteraction.IDLE,
                },
            });
        }),
    );
}

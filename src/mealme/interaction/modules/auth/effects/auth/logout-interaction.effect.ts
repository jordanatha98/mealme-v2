import { Injectable } from '@angular/core';
import { Effects, Message, ubudType } from '@ubud/ngrx';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Authenticate } from '@mealme/src/domain/modules/auth/messages/commands/auth/authenticate';
import { LoginInteractionChanged } from '@mealme/src/interaction/modules/auth/messages/events/login-interaction-changed';
import { AuthLoginInteraction } from '@mealme/src/interaction/modules/auth/enums/auth-login-interaction';
import { AuthenticateSucceed } from '@mealme/src/domain/modules/auth/messages/events/auth/authenticate-succeed';
import { AuthenticateFailed } from '@mealme/src/domain/modules/auth/messages/events/auth/authenticate-failed';
import { MessageUtil } from '@shared/utils/message.util';
import { HttpUtil } from '@shared/utils/http.util';
import { SignOut } from '@mealme/src/domain/modules/auth/messages/commands/auth/sign-out';
import {
    LogoutInteractionChanged
} from '@mealme/src/interaction/modules/auth/messages/events/logout-interaction-changed';
import { AuthLogoutInteraction } from '@mealme/src/interaction/modules/auth/enums/auth-logout-interaction';
import { SignOutSucceed } from '@mealme/src/domain/modules/auth/messages/events/auth/sign-out-succeed';

@Injectable()
export class LogoutInteractionEffect extends Effects {
    public constructor(actions$: Actions) {
        super(actions$);
    }

    @Effect()
    public signOut$: Observable<Message> = this.actions$.pipe(
        ubudType(SignOut),
        map(() => new LogoutInteractionChanged({
            changes: {
                process: true,
                interaction: AuthLogoutInteraction.SIGN_OUT_EXECUTED,
            },
        })),
    );

    @Effect()
    public signOutSucceed$: Observable<Message> = this.actions$.pipe(
        ubudType(SignOutSucceed),
        map(() => new LogoutInteractionChanged({
            changes: {
                process: false,
                error: null,
                interaction: AuthLogoutInteraction.SIGN_OUT_SUCCEED,
                success: 'Success Logging In, Redirecting....',
            },
        })),
    );

    @Effect()
    public authenticateFailed$: Observable<Message> = this.actions$.pipe(
        ubudType(AuthenticateFailed),
        map(message => MessageUtil.getPayload<AuthenticateFailed>(message)),
        map(({ exception }) => new LoginInteractionChanged({
            changes: {
                process: false,
                error: HttpUtil.errorExtractor(exception),
                interaction: AuthLoginInteraction.AUTHENTICATE_FAILED,
                success: null,
            },
        })),
    );
}

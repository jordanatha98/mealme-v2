import { Injectable } from '@angular/core';
import { Effects, Message, ubudType } from '@ubud/ngrx';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Authenticate } from '@mealme/src/domain/modules/auth/messages/commands/auth/authenticate';
import { LoginInteractionChanged } from '@mealme/src/interaction/modules/auth/messages/events/login-interaction-changed';
import { AuthLoginInteraction } from '@mealme/src/interaction/modules/auth/enums/auth-login-interaction';
import { AuthenticateSucceed } from '@mealme/src/domain/modules/auth/messages/events/auth/authenticate-succeed';
import { AuthenticateFailed } from '@mealme/src/domain/modules/auth/messages/events/auth/authenticate-failed';
import { MessageUtil } from '@shared/utils/message.util';
import { HttpUtil } from '@shared/utils/http.util';

@Injectable()
export class LoginInteractionEffect extends Effects {
    public constructor(actions$: Actions) {
        super(actions$);
    }

    @Effect()
    public authenticate$: Observable<Message> = this.actions$.pipe(
        ubudType(Authenticate),
        map(() => new LoginInteractionChanged({
            changes: {
                process: true,
                error: null,
                interaction: AuthLoginInteraction.AUTHENTICATE_EXECUTED,
                success: null,
            },
        })),
    );

    @Effect()
    public authenticateSucceed$: Observable<Message> = this.actions$.pipe(
        ubudType(AuthenticateSucceed),
        map(() => new LoginInteractionChanged({
            changes: {
                process: false,
                error: null,
                interaction: AuthLoginInteraction.AUTHENTICATE_SUCCEED,
                success: 'Success Logging In, Redirecting....',
            },
        })),
    );

    @Effect()
    public authenticateFailed$: Observable<Message> = this.actions$.pipe(
        ubudType(AuthenticateFailed),
        map(message => MessageUtil.getPayload<AuthenticateFailed>(message)),
        map(({ exception }) => new LoginInteractionChanged({
            changes: {
                process: false,
                error: HttpUtil.errorExtractor(exception),
                interaction: AuthLoginInteraction.AUTHENTICATE_FAILED,
                success: null,
            },
        })),
    );
}

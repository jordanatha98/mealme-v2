import { Injectable } from '@angular/core';
import { Repository } from '@ubud/ngrx';
import { Observable } from 'rxjs';
import { MealmeInteractionState } from '@mealme/src/interaction/stores/states';
import { MealmeInteractionStore } from '@mealme/src/interaction/stores/mealme-interaction.store';
import { AuthLogoutInteraction } from '@mealme/src/interaction/modules/auth/enums/auth-logout-interaction';

@Injectable()
export class LogoutInteractionRepository extends Repository<MealmeInteractionState> {
    public constructor(store: MealmeInteractionStore) {
        super(store);
    }

    public isProcess$(): Observable<boolean> {
        return this.select(state => state.auth.logout.process);
    }

    public getInteraction$(): Observable<AuthLogoutInteraction> {
        return this.select(state => state.auth.logout.interaction);
    }

}

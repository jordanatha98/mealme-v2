import { Injectable } from '@angular/core';
import { Repository } from '@ubud/ngrx';
import { Observable } from 'rxjs';
import { MealmeInteractionState } from '@mealme/src/interaction/stores/states';
import { MealmeInteractionStore } from '@mealme/src/interaction/stores/mealme-interaction.store';
import { AuthLoginInteraction } from '@mealme/src/interaction/modules/auth/enums/auth-login-interaction';

@Injectable()
export class LoginInteractionRepository extends Repository<MealmeInteractionState> {
    public constructor(store: MealmeInteractionStore) {
        super(store);
    }

    public isProcess$(): Observable<boolean> {
        return this.select(state => state.auth.login.process);
    }

    public getError$(): Observable<string> {
        return this.select(state => state.auth.login.error);
    }

    public getInteraction$(): Observable<AuthLoginInteraction> {
        return this.select(state => state.auth.login.interaction);
    }

    public getSuccess$(): Observable<string> {
        return this.select(state => state.auth.login.success);
    }
}

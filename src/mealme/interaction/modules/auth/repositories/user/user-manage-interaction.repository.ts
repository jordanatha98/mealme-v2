import { Injectable } from '@angular/core';
import { Repository } from '@ubud/ngrx';
import { Observable } from 'rxjs';
import { MealmeInteractionState } from '@mealme/src/interaction/stores/states';
import { MealmeInteractionStore } from '@mealme/src/interaction/stores/mealme-interaction.store';
import { UserManageInteraction } from '@mealme/src/interaction/modules/auth/enums/user-manage-interaction';

@Injectable()
export class UserManageInteractionRepository extends Repository<MealmeInteractionState> {
    public constructor(store: MealmeInteractionStore) {
        super(store);
    }

    public isProcess$(): Observable<boolean> {
        return this.select(state => state.auth.user.manage.process);
    }

    public getError$(): Observable<string> {
        return this.select(state => state.auth.user.manage.error);
    }

    public getInteraction$(): Observable<UserManageInteraction> {
        return this.select(state => state.auth.user.manage.interaction);
    }

    public getSuccess$(): Observable<string> {
        return this.select(state => state.auth.user.manage.success);
    }
}

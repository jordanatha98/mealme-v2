import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { MealmeInteractionMealmeStoreModule } from '../../stores/module';
import { MealmeDomainMealmeProductModule } from '@mealme/src/domain/modules/products/module';
import { ProductIndexInteractionEffect } from '@mealme/src/interaction/modules/product/effects/product-index-interaction.effect';

@NgModule({
    imports: [
        MealmeInteractionMealmeStoreModule,
        MealmeDomainMealmeProductModule,
        EffectsModule.forFeature([
            ProductIndexInteractionEffect,
        ]),
    ],
})
export class MealmeInteractionMealmeProductModule {}

import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeInteractionState } from '../../../../stores/states';

@UbudMessage('Interaction.Mealme.Product.ProductIndexInteractionChanged')
export class ProductIndexInteractionChanged extends Message<MealmeInteractionState> {
    public changes: any;

    public handle(state: MealmeInteractionState): MealmeInteractionState {
        return {
            ...state,
            product: {
                ...state.product,
                index: {
                    ...state.product.index,
                    ...this.changes,
                },
            },
        };
    }
}

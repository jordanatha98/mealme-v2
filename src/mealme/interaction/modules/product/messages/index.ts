import { ProductIndexInteractionChanged } from '@mealme/src/interaction/modules/product/messages/events/product-index-interaction-changed';

export const MEALME_INTERACTION_PRODUCT_MESSAGES: any[] = [
    ProductIndexInteractionChanged,
];

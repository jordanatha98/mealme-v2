import { Injectable } from '@angular/core';
import { Effects, Message, ubudType } from '@ubud/ngrx';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FetchProducts } from '@mealme/src/domain/modules/products/messages/commands/fetch-products';
import { ProductIndexInteractionChanged } from '@mealme/src/interaction/modules/product/messages/events/product-index-interaction-changed';
import { FetchProductsSucceed } from '@mealme/src/domain/modules/products/messages/events/fetch-products-succeed';
import { FetchProductsFailed } from '@mealme/src/domain/modules/products/messages/events/fetch-products-failed';
import { MessageUtil } from '@shared/utils/message.util';
import { HttpUtil } from '@shared/utils/http.util';

@Injectable()
export class ProductIndexInteractionEffect extends Effects {
    public constructor(actions$: Actions) {
        super(actions$);
    }

    @Effect()
    public fetchProducts$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchProducts),
        map(() => new ProductIndexInteractionChanged({
            changes: {
                process: true,
                error: null,
            },
        })),
    );

    @Effect()
    public fetchProductsSucceed$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchProductsSucceed),
        map(() => new ProductIndexInteractionChanged({
            changes: {
                process: false,
                error: null,
            },
        })),
    );

    @Effect()
    public fetchProductsFailed$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchProductsFailed),
        map(message => MessageUtil.getPayload<FetchProductsFailed>(message)),
        map(({ exception }) => new ProductIndexInteractionChanged({
            changes: {
                process: false,
                error: HttpUtil.errorExtractor(exception),
            },
        })),
    );
}

import { Injectable } from '@angular/core';
import { Effects, Message, ubudType } from '@ubud/ngrx';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MessageUtil } from '@shared/utils/message.util';
import { HttpUtil } from '@shared/utils/http.util';
import { FetchBookmarks } from '@mealme/src/domain/modules/meal/messages/commands/fetch-bookmarks';
import { BookmarkIndexInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/bookmark-index-interaction-changed';
import { FetchBookmarksSucceed } from '@mealme/src/domain/modules/meal/messages/events/fetch-bookmarks-succeed';
import { FetchBookmarksFailed } from '@mealme/src/domain/modules/meal/messages/events/fetch-bookmarks-failed';

@Injectable()
export class BookmarkIndexInteractionEffect extends Effects {
    public constructor(actions$: Actions) {
        super(actions$);
    }

    @Effect()
    public fetchBookmarks$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchBookmarks),
        map(() => new BookmarkIndexInteractionChanged({
            changes: {
                process: true,
                error: null,
            },
        })),
    );

    @Effect()
    public fetchBookmarksSucceed$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchBookmarksSucceed),
        map(() => new BookmarkIndexInteractionChanged({
            changes: {
                process: false,
                error: null,
            },
        })),
    );

    @Effect()
    public fetchBookmarksFailed$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchBookmarksFailed),
        map(message => MessageUtil.getPayload<FetchBookmarksFailed>(message)),
        map(({ exception }) => new BookmarkIndexInteractionChanged({
            changes: {
                process: false,
                error: HttpUtil.errorExtractor(exception),
            },
        })),
    );
}

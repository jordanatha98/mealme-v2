import { Injectable } from '@angular/core';
import { Effects, Message, ubudType } from '@ubud/ngrx';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MessageUtil } from '@shared/utils/message.util';
import { HttpUtil } from '@shared/utils/http.util';
import { FetchMenuNutrients } from '@mealme/src/domain/modules/meal/messages/commands/fetch-menu-nutrients';
import { FetchMenuNutrientsSucceed } from '@mealme/src/domain/modules/meal/messages/events/fetch-menu-nutrients-succeed';
import { FetchMenuNutrientsFailed } from '@mealme/src/domain/modules/meal/messages/events/fetch-menu-nutrients-failed';
import { MenuNutrientsInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/menu-nutrients-interaction-changed';

@Injectable()
export class MenuNutrientsInteractionEffect extends Effects {
    public constructor(actions$: Actions) {
        super(actions$);
    }

    @Effect()
    public fetchMenuNutrients$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchMenuNutrients),
        map(() => new MenuNutrientsInteractionChanged({
            changes: {
                process: true,
                error: null,
            },
        })),
    );

    @Effect()
    public fetchMenuNutrientsSucceed$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchMenuNutrientsSucceed),
        map(() => new MenuNutrientsInteractionChanged({
            changes: {
                process: false,
                error: null,
            },
        })),
    );

    @Effect()
    public fetchMenuNutrientsFailed$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchMenuNutrientsFailed),
        map(message => MessageUtil.getPayload<FetchMenuNutrientsFailed>(message)),
        map(({ exception }) => new MenuNutrientsInteractionChanged({
            changes: {
                process: false,
                error: HttpUtil.errorExtractor(exception),
            },
        })),
    );
}

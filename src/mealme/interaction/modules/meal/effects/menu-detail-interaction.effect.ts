import { Injectable } from '@angular/core';
import { Effects, Message, ubudType } from '@ubud/ngrx';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MessageUtil } from '@shared/utils/message.util';
import { HttpUtil } from '@shared/utils/http.util';
import { FetchMenu } from '@mealme/src/domain/modules/meal/messages/commands/fetch-menu';
import { MenuDetailInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/menu-detail-interaction-changed';
import { FetchMenuSucceed } from '@mealme/src/domain/modules/meal/messages/events/fetch-menu-succeed';
import { FetchMenuFailed } from '@mealme/src/domain/modules/meal/messages/events/fetch-menu-failed';

@Injectable()
export class MenuDetailInteractionEffect extends Effects {
    public constructor(actions$: Actions) {
        super(actions$);
    }

    @Effect()
    public fetchMenu$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchMenu),
        map(() => new MenuDetailInteractionChanged({
            changes: {
                process: true,
                error: null,
            },
        })),
    );

    @Effect()
    public fetchMenuSucceed$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchMenuSucceed),
        map(() => new MenuDetailInteractionChanged({
            changes: {
                process: false,
                error: null,
            },
        })),
    );

    @Effect()
    public fetchMenuFailed$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchMenuFailed),
        map(message => MessageUtil.getPayload<FetchMenuFailed>(message)),
        map(({ exception }) => new MenuDetailInteractionChanged({
            changes: {
                process: false,
                error: HttpUtil.errorExtractor(exception),
            },
        })),
    );
}

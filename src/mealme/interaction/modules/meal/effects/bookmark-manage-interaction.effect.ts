import { Injectable } from '@angular/core';
import { Effects, Message, ubudType } from '@ubud/ngrx';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MessageUtil } from '@shared/utils/message.util';
import { HttpUtil } from '@shared/utils/http.util';
import { CreateBookmark } from '@mealme/src/domain/modules/meal/messages/commands/create-bookmark';
import { BookmarkManageInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/bookmark-manage-interaction-changed';
import { BookmarkInteraction } from '@mealme/src/interaction/modules/meal/enums/bookmark-interaction';
import { CreateBookmarkSucceed } from '@mealme/src/domain/modules/meal/messages/events/create-bookmark-succeed';
import { CreateBookmarkFailed } from '@mealme/src/domain/modules/meal/messages/events/create-bookmark-failed';
import { DeleteBookmark } from '@mealme/src/domain/modules/meal/messages/commands/delete-bookmark';
import { DeleteBookmarkSucceed } from '@mealme/src/domain/modules/meal/messages/events/delete-bookmark-succeed';
import { DeleteBookmarkFailed } from '@mealme/src/domain/modules/meal/messages/events/delete-bookmark-failed';

@Injectable()
export class BookmarkManageInteractionEffect extends Effects {
    public constructor(actions$: Actions) {
        super(actions$);
    }

    @Effect()
    public createBookmark$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateBookmark),
        map(() => new BookmarkManageInteractionChanged({
            changes: {
                process: true,
                error: null,
                success: null,
                interaction: BookmarkInteraction.CREATE_EXECUTING
            },
        })),
    );

    @Effect()
    public createBookmarkSucceed$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateBookmarkSucceed),
        map(() => new BookmarkManageInteractionChanged({
            changes: {
                process: false,
                error: null,
                success: 'Success bookmarking this menu',
                interaction: BookmarkInteraction.CREATE_SUCCEED
            },
        })),
    );

    @Effect()
    public createBookmarkFailed$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateBookmarkFailed),
        map(message => MessageUtil.getPayload<CreateBookmarkFailed>(message)),
        map(({ exception }) => new BookmarkManageInteractionChanged({
            changes: {
                process: false,
                error: HttpUtil.errorExtractor(exception),
                success: null,
                interaction: BookmarkInteraction.CREATE_FAILED
            },
        })),
    );

    @Effect()
    public deleteBookmark$: Observable<Message> = this.actions$.pipe(
        ubudType(DeleteBookmark),
        map(() => new BookmarkManageInteractionChanged({
            changes: {
                process: true,
                error: null,
                success: null,
                interaction: BookmarkInteraction.DELETE_EXECUTING
            },
        })),
    );

    @Effect()
    public deleteBookmarkSucceed$: Observable<Message> = this.actions$.pipe(
        ubudType(DeleteBookmarkSucceed),
        map(() => new BookmarkManageInteractionChanged({
            changes: {
                process: false,
                error: null,
                success: 'Success removing this menu from bookmark',
                interaction: BookmarkInteraction.DELETE_SUCCEED
            },
        })),
    );

    @Effect()
    public deleteBookmarkFailed$: Observable<Message> = this.actions$.pipe(
        ubudType(DeleteBookmarkFailed),
        map(message => MessageUtil.getPayload<DeleteBookmarkFailed>(message)),
        map(({ exception }) => new BookmarkManageInteractionChanged({
            changes: {
                process: false,
                error: HttpUtil.errorExtractor(exception),
                success: null,
                interaction: BookmarkInteraction.DELETE_FAILED
            },
        })),
    );
}

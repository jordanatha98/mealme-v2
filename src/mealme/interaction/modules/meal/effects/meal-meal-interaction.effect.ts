import { Injectable } from '@angular/core';
import { Effects, Message, ubudType } from '@ubud/ngrx';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MessageUtil } from '@shared/utils/message.util';
import { HttpUtil } from '@shared/utils/http.util';
import { FetchNextMeal } from '@mealme/src/domain/modules/meal/messages/commands/fetch-next-meal';
import { MealMealInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/meal-meal-interaction-changed';
import { FetchNextMealSucceed } from '@mealme/src/domain/modules/meal/messages/events/fetch-next-meal-succeed';
import { FetchNextMealFailed } from '@mealme/src/domain/modules/meal/messages/events/fetch-next-meal-failed';

@Injectable()
export class MealMealInteractionEffect extends Effects {
    public constructor(actions$: Actions) {
        super(actions$);
    }

    @Effect()
    public fetchNextMeal$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchNextMeal),
        map(() => new MealMealInteractionChanged({
            changes: {
                process: true,
                error: null,
            },
        })),
    );

    @Effect()
    public fetchNextMealSucceed$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchNextMealSucceed),
        map(() => new MealMealInteractionChanged({
            changes: {
                process: false,
                error: null,
            },
        })),
    );

    @Effect()
    public fetchNextMealFailed$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchNextMealFailed),
        map(message => MessageUtil.getPayload<FetchNextMealFailed>(message)),
        map(({ exception }) => new MealMealInteractionChanged({
            changes: {
                process: false,
                error: HttpUtil.errorExtractor(exception),
            },
        })),
    );
}

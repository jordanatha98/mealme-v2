import { Injectable } from '@angular/core';
import { Effects, Message, ubudType } from '@ubud/ngrx';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MessageUtil } from '@shared/utils/message.util';
import { HttpUtil } from '@shared/utils/http.util';
import { FetchMenuIngredients } from '@mealme/src/domain/modules/meal/messages/commands/fetch-menu-ingredients';
import { MenuIngredientsInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/menu-ingredients-interaction-changed';
import { FetchMenuIngredientsSucceed } from '@mealme/src/domain/modules/meal/messages/events/fetch-menu-ingredients-succeed';
import { FetchMenuIngredientsFailed } from '@mealme/src/domain/modules/meal/messages/events/fetch-menu-ingredients-failed';

@Injectable()
export class MenuIngredientsInteractionEffect extends Effects {
    public constructor(actions$: Actions) {
        super(actions$);
    }

    @Effect()
    public fetchMenuIngredients$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchMenuIngredients),
        map(() => new MenuIngredientsInteractionChanged({
            changes: {
                process: true,
                error: null,
            },
        })),
    );

    @Effect()
    public fetchMenuIngredientsSucceed$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchMenuIngredientsSucceed),
        map(() => new MenuIngredientsInteractionChanged({
            changes: {
                process: false,
                error: null,
            },
        })),
    );

    @Effect()
    public fetchMenuIngredientsFailed$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchMenuIngredientsFailed),
        map(message => MessageUtil.getPayload<FetchMenuIngredientsFailed>(message)),
        map(({ exception }) => new MenuIngredientsInteractionChanged({
            changes: {
                process: false,
                error: HttpUtil.errorExtractor(exception),
            },
        })),
    );
}

import { Injectable } from '@angular/core';
import { Effects, Message, ubudType } from '@ubud/ngrx';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MessageUtil } from '@shared/utils/message.util';
import { HttpUtil } from '@shared/utils/http.util';
import { FetchPlans } from '@mealme/src/domain/modules/meal/messages/commands/fetch-plans';
import {
    PlanMenusInteractionChanged
} from '@mealme/src/interaction/modules/meal/messages/events/plan-menus-interaction-changed';
import { FetchPlansSucceed } from '@mealme/src/domain/modules/meal/messages/events/fetch-plans-succeed';
import { FetchPlansFailed } from '@mealme/src/domain/modules/meal/messages/events/fetch-plans-failed';
import { GeneratePlan } from '@mealme/src/domain/modules/meal/messages/commands/generate-plan';
import { GeneratePlanInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/generate-plan-interaction-changed';
import { GeneratePlanInteraction } from '@mealme/src/interaction/modules/meal/enums/generate-plan-interaction';
import { GeneratePlanSucceed } from '@mealme/src/domain/modules/meal/messages/events/generate-plan-succeed';
import { GeneratePlanFailed } from '@mealme/src/domain/modules/meal/messages/events/generate-plan-failed';

@Injectable()
export class PlanMenusInteractionEffect extends Effects {
    public constructor(actions$: Actions) {
        super(actions$);
    }

    @Effect()
    public fetchPlanMenus$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchPlans),
        map(() => new PlanMenusInteractionChanged({
            changes: {
                process: true,
                error: null,
            },
        })),
    );

    @Effect()
    public fetchPlanMenusSucceed$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchPlansSucceed),
        map(() => new PlanMenusInteractionChanged({
            changes: {
                process: false,
                error: null,
            },
        })),
    );

    @Effect()
    public fetchPlanMenusFailed$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchPlansFailed),
        map(message => MessageUtil.getPayload<FetchPlansFailed>(message)),
        map(({ exception }) => new PlanMenusInteractionChanged({
            changes: {
                process: false,
                error: HttpUtil.errorExtractor(exception),
            },
        })),
    );

    @Effect()
    public generatePlan$: Observable<Message> = this.actions$.pipe(
        ubudType(GeneratePlan),
        map(() => new GeneratePlanInteractionChanged({
            changes: {
                process: true,
                error: null,
                success: null,
                interaction: GeneratePlanInteraction.GENERATE_EXECUTING
            },
        })),
    );

    @Effect()
    public generatePlanSucceed$: Observable<Message> = this.actions$.pipe(
        ubudType(GeneratePlanSucceed),
        map(() => new GeneratePlanInteractionChanged({
            changes: {
                process: false,
                error: null,
                success: 'Success generating your menu!',
                interaction: GeneratePlanInteraction.GENERATE_SUCCEED
            },
        })),
    );

    @Effect()
    public generatePlanFailed$: Observable<Message> = this.actions$.pipe(
        ubudType(GeneratePlanFailed),
        map(message => MessageUtil.getPayload<GeneratePlanFailed>(message)),
        map(({ exception }) => new GeneratePlanInteractionChanged({
            changes: {
                process: false,
                error: HttpUtil.errorExtractor(exception),
                success: null,
                interaction: GeneratePlanInteraction.GENERATE_FAILED
            },
        })),
    );
}

import { Injectable } from '@angular/core';
import { Effects, Message, ubudType } from '@ubud/ngrx';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MessageUtil } from '@shared/utils/message.util';
import { HttpUtil } from '@shared/utils/http.util';
import { FetchMeals } from '@mealme/src/domain/modules/meal/messages/commands/fetch-meals';
import { MealMealsInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/meal-meals-interaction-changed';
import { FetchMealsSucceed } from '@mealme/src/domain/modules/meal/messages/events/fetch-meals-succeed';
import { FetchMealsFailed } from '@mealme/src/domain/modules/meal/messages/events/fetch-meals-failed';

@Injectable()
export class MealMealsInteractionEffect extends Effects {
    public constructor(actions$: Actions) {
        super(actions$);
    }

    @Effect()
    public fetchMeals$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchMeals),
        map(() => new MealMealsInteractionChanged({
            changes: {
                process: true,
                error: null,
            },
        })),
    );

    @Effect()
    public fetchMealsSucceed$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchMealsSucceed),
        map(() => new MealMealsInteractionChanged({
            changes: {
                process: false,
                error: null,
            },
        })),
    );

    @Effect()
    public fetchMealsFailed$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchMealsFailed),
        map(message => MessageUtil.getPayload<FetchMealsFailed>(message)),
        map(({ exception }) => new MealMealsInteractionChanged({
            changes: {
                process: false,
                error: HttpUtil.errorExtractor(exception),
            },
        })),
    );
}

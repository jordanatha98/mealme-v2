import { Injectable } from '@angular/core';
import { Effects, Message, ubudType } from '@ubud/ngrx';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MessageUtil } from '@shared/utils/message.util';
import { HttpUtil } from '@shared/utils/http.util';
import { BookmarkManageInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/bookmark-manage-interaction-changed';
import { CreateMenu } from '@mealme/src/domain/modules/meal/messages/commands/menu/create-menu';
import { MenuManageInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/menu-manage-interaction-changed';
import { MenuInteraction } from '@mealme/src/interaction/modules/meal/enums/menu-interaction';
import { CreateMenuSucceed } from '@mealme/src/domain/modules/meal/messages/events/menu/create-menu-succeed';
import { CreateMenuFailed } from '@mealme/src/domain/modules/meal/messages/events/menu/create-menu-failed';
import { CreateMenuIngredient } from '@mealme/src/domain/modules/meal/messages/commands/menu/create-menu-ingredient';
import { CreateMenuIngredientSucceed } from '@mealme/src/domain/modules/meal/messages/events/menu/create-menu-ingredient-succeed';
import { CreateMenuIngredientFailed } from '@mealme/src/domain/modules/meal/messages/events/menu/create-menu-ingredient-failed';
import { CreateMenuInstruction } from '@mealme/src/domain/modules/meal/messages/commands/menu/create-menu-instruction';
import { CreateMenuInstructionSucceed } from '@mealme/src/domain/modules/meal/messages/events/menu/create-menu-instruction-succeed';
import { CreateMenuInstructionFailed } from '@mealme/src/domain/modules/meal/messages/events/menu/create-menu-instruction-failed';
import { CreateMenuNutrient } from '@mealme/src/domain/modules/meal/messages/commands/menu/create-menu-nutrient';
import { CreateMenuNutrientSucceed } from '@mealme/src/domain/modules/meal/messages/events/menu/create-menu-nutrient-succeed';
import { CreateMenuNutrientFailed } from '@mealme/src/domain/modules/meal/messages/events/menu/create-menu-nutrient-failed';

@Injectable()
export class MenuManageInteractionEffect extends Effects {
    public constructor(actions$: Actions) {
        super(actions$);
    }

    @Effect()
    public createMenu$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateMenu),
        map(() => new MenuManageInteractionChanged({
            changes: {
                process: true,
                error: null,
                success: null,
                interaction: MenuInteraction.CREATE_EXECUTING
            },
        })),
    );

    @Effect()
    public createMenuSucceed$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateMenuSucceed),
        map((response) => new MenuManageInteractionChanged({
            changes: {
                process: false,
                error: null,
                success: 'Success creating this menu',
                interaction: MenuInteraction.CREATE_SUCCEED,
                response
            },
        })),
    );

    @Effect()
    public createMenuFailed$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateMenuFailed),
        map(message => MessageUtil.getPayload<CreateMenuFailed>(message)),
        map(({ exception }) => new MenuManageInteractionChanged({
            changes: {
                process: false,
                error: HttpUtil.errorExtractor(exception),
                success: null,
                interaction: MenuInteraction.CREATE_FAILED
            },
        })),
    );

    @Effect()
    public createMenuIngredient$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateMenuIngredient),
        map(() => new MenuManageInteractionChanged({
            changes: {
                process: true,
                error: null,
                success: null,
                interaction: MenuInteraction.CREATE_INGREDIENT_EXECUTING
            },
        })),
    );

    @Effect()
    public createMenuIngredientSucceed$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateMenuIngredientSucceed),
        map(() => new BookmarkManageInteractionChanged({
            changes: {
                process: false,
                error: null,
                success: 'Success uploading ingredient to menu',
                interaction: MenuInteraction.CREATE_INGREDIENT_SUCCEED
            },
        })),
    );

    @Effect()
    public createMenuIngredientFailed$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateMenuIngredientFailed),
        map(message => MessageUtil.getPayload<CreateMenuIngredientFailed>(message)),
        map(({ exception }) => new BookmarkManageInteractionChanged({
            changes: {
                process: false,
                error: HttpUtil.errorExtractor(exception),
                success: null,
                interaction: MenuInteraction.CREATE_INGREDIENT_FAILED
            },
        })),
    );

    @Effect()
    public createMenuInstruction$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateMenuInstruction),
        map(() => new MenuManageInteractionChanged({
            changes: {
                process: true,
                error: null,
                success: null,
                interaction: MenuInteraction.CREATE_INSTRUCTION_EXECUTING
            },
        })),
    );

    @Effect()
    public createMenuInstructionSucceed$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateMenuInstructionSucceed),
        map(() => new BookmarkManageInteractionChanged({
            changes: {
                process: false,
                error: null,
                success: 'Success uploading instruction to menu',
                interaction: MenuInteraction.CREATE_INSTRUCTION_SUCCEED
            },
        })),
    );

    @Effect()
    public createMenuInstructionFailed$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateMenuInstructionFailed),
        map(message => MessageUtil.getPayload<CreateMenuInstructionFailed>(message)),
        map(({ exception }) => new BookmarkManageInteractionChanged({
            changes: {
                process: false,
                error: HttpUtil.errorExtractor(exception),
                success: null,
                interaction: MenuInteraction.CREATE_INSTRUCTION_FAILED
            },
        })),
    );

    @Effect()
    public createMenuNutrient$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateMenuNutrient),
        map(() => new MenuManageInteractionChanged({
            changes: {
                process: true,
                error: null,
                success: null,
                interaction: MenuInteraction.CREATE_NUTRIENT_EXECUTING
            },
        })),
    );

    @Effect()
    public createMenuNutrientSucceed$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateMenuNutrientSucceed),
        map(() => new BookmarkManageInteractionChanged({
            changes: {
                process: false,
                error: null,
                success: 'Success uploading nutrient to menu',
                interaction: MenuInteraction.CREATE_NUTRIENT_SUCCEED
            },
        })),
    );

    @Effect()
    public createMenuNutrientFailed$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateMenuNutrientFailed),
        map(message => MessageUtil.getPayload<CreateMenuNutrientFailed>(message)),
        map(({ exception }) => new BookmarkManageInteractionChanged({
            changes: {
                process: false,
                error: HttpUtil.errorExtractor(exception),
                success: null,
                interaction: MenuInteraction.CREATE_NUTRIENT_FAILED
            },
        })),
    );
}

import { Injectable } from '@angular/core';
import { Effects, Message, ubudType } from '@ubud/ngrx';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MessageUtil } from '@shared/utils/message.util';
import { HttpUtil } from '@shared/utils/http.util';
import { FetchMenuInstructions } from '@mealme/src/domain/modules/meal/messages/commands/fetch-menu-instructions';
import { FetchMenuInstructionsSucceed } from '@mealme/src/domain/modules/meal/messages/events/fetch-menu-instructions-succeed';
import { FetchMenuInstructionsFailed } from '@mealme/src/domain/modules/meal/messages/events/fetch-menu-instructions-failed';
import { MenuInstructionsInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/menu-instructions-interaction-changed';

@Injectable()
export class MenuInstructionsInteractionEffect extends Effects {
    public constructor(actions$: Actions) {
        super(actions$);
    }

    @Effect()
    public fetchMenuInstructions$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchMenuInstructions),
        map(() => new MenuInstructionsInteractionChanged({
            changes: {
                process: true,
                error: null,
            },
        })),
    );

    @Effect()
    public fetchMenuInstructionsSucceed$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchMenuInstructionsSucceed),
        map(() => new MenuInstructionsInteractionChanged({
            changes: {
                process: false,
                error: null,
            },
        })),
    );

    @Effect()
    public fetchMenuInstructionsFailed$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchMenuInstructionsFailed),
        map(message => MessageUtil.getPayload<FetchMenuInstructionsFailed>(message)),
        map(({ exception }) => new MenuInstructionsInteractionChanged({
            changes: {
                process: false,
                error: HttpUtil.errorExtractor(exception),
            },
        })),
    );
}

import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { MealmeInteractionMealmeStoreModule } from '../../stores/module';
import { MealmeDomainMealModule } from '@mealme/src/domain/modules/meal/module';
import { MealMealInteractionEffect } from '@mealme/src/interaction/modules/meal/effects/meal-meal-interaction.effect';
import { MealMealsInteractionEffect } from '@mealme/src/interaction/modules/meal/effects/meal-meals-interaction.effect';
import { PlanMenusInteractionEffect } from '@mealme/src/interaction/modules/meal/effects/plan-menus-interaction.effect';
import { MenuDetailInteractionEffect } from '@mealme/src/interaction/modules/meal/effects/menu-detail-interaction.effect';
import { MenuInstructionsInteractionEffect } from '@mealme/src/interaction/modules/meal/effects/menu-instructions-interaction.effect';
import { MenuNutrientsInteractionEffect } from '@mealme/src/interaction/modules/meal/effects/menu-nutrients-interaction.effect';
import { MenuIngredientsInteractionEffect } from '@mealme/src/interaction/modules/meal/effects/menu-ingredients-interaction.effect';
import { BookmarkIndexInteractionEffect } from '@mealme/src/interaction/modules/meal/effects/bookmark-index-interaction.effect';
import { BookmarkManageInteractionEffect } from '@mealme/src/interaction/modules/meal/effects/bookmark-manage-interaction.effect';
import { MenuManageInteractionEffect } from '@mealme/src/interaction/modules/meal/effects/menu-manage-interaction.effect';

@NgModule({
    imports: [
        MealmeInteractionMealmeStoreModule,
        MealmeDomainMealModule,
        EffectsModule.forFeature([
            MealMealInteractionEffect,
            MealMealsInteractionEffect,
            PlanMenusInteractionEffect,
            MenuDetailInteractionEffect,
            MenuInstructionsInteractionEffect,
            MenuNutrientsInteractionEffect,
            MenuIngredientsInteractionEffect,
            BookmarkIndexInteractionEffect,
            BookmarkManageInteractionEffect,
            MenuManageInteractionEffect,
        ]),
    ],
})
export class MealmeInteractionMealModule {}

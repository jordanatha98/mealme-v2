import { MealMealInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/meal-meal-interaction-changed';
import { MealMealsInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/meal-meals-interaction-changed';
import { PlanMenusInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/plan-menus-interaction-changed';
import { MenuDetailInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/menu-detail-interaction-changed';
import { MenuNutrientsInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/menu-nutrients-interaction-changed';
import { MenuIngredientsInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/menu-ingredients-interaction-changed';
import { MenuInstructionsInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/menu-instructions-interaction-changed';
import { GeneratePlanInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/generate-plan-interaction-changed';
import { BookmarkIndexInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/bookmark-index-interaction-changed';
import { BookmarkManageInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/bookmark-manage-interaction-changed';
import { MenuManageInteractionChanged } from '@mealme/src/interaction/modules/meal/messages/events/menu-manage-interaction-changed';

export const MEALME_INTERACTION_MEAL_MESSAGES: any[] = [
    MealMealInteractionChanged,
    MealMealsInteractionChanged,
    PlanMenusInteractionChanged,
    MenuDetailInteractionChanged,
    MenuNutrientsInteractionChanged,
    MenuIngredientsInteractionChanged,
    MenuInstructionsInteractionChanged,
    GeneratePlanInteractionChanged,
    BookmarkIndexInteractionChanged,
    BookmarkManageInteractionChanged,
    MenuManageInteractionChanged,
];

import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeInteractionState } from '../../../../stores/states';

@UbudMessage('Interaction.Mealme.Menu.BookmarkIndexInteractionChanged')
export class BookmarkIndexInteractionChanged extends Message<MealmeInteractionState> {
    public changes: any;

    public handle(state: MealmeInteractionState): MealmeInteractionState {
        return {
            ...state,
            meal: {
                ...state.meal,
                bookmark: {
                    ...state.meal.bookmark,
                    index: {
                        ...state.meal.bookmark.index,
                        ...this.changes
                    }
                }
            },
        };
    }
}

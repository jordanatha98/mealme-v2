import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeInteractionState } from '../../../../stores/states';

@UbudMessage('Interaction.Mealme.Menu.PlanMenusInteractionChanged')
export class PlanMenusInteractionChanged extends Message<MealmeInteractionState> {
    public changes: any;

    public handle(state: MealmeInteractionState): MealmeInteractionState {
        return {
            ...state,
            meal: {
                ...state.meal,
                planMenus: {
                    ...state.meal.planMenus,
                    ...this.changes,
                },
            },
        };
    }
}

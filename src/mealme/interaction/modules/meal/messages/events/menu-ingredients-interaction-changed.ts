import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeInteractionState } from '../../../../stores/states';

@UbudMessage('Interaction.Mealme.Menu.MenuIngredientsInteractionChanged')
export class MenuIngredientsInteractionChanged extends Message<MealmeInteractionState> {
    public changes: any;

    public handle(state: MealmeInteractionState): MealmeInteractionState {
        return {
            ...state,
            meal: {
                ...state.meal,
                menu: {
                    ...state.meal.menu,
                    ingredients: {
                        ...state.meal.menu.ingredients,
                        ...this.changes
                    }
                }
            },
        };
    }
}

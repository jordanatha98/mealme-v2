import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeInteractionState } from '../../../../stores/states';

@UbudMessage('Interaction.Mealme.Menu.MenuManageInteractionChanged')
export class MenuManageInteractionChanged extends Message<MealmeInteractionState> {
    public changes: any;

    public handle(state: MealmeInteractionState): MealmeInteractionState {
        return {
            ...state,
            meal: {
                ...state.meal,
                menu: {
                    ...state.meal.menu,
                    manage: {
                        ...state.meal.menu.manage,
                        ...this.changes
                    }
                }
            },
        };
    }
}

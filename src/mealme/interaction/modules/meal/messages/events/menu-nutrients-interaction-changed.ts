import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeInteractionState } from '../../../../stores/states';

@UbudMessage('Interaction.Mealme.Menu.MenuNutrientsInteractionChanged')
export class MenuNutrientsInteractionChanged extends Message<MealmeInteractionState> {
    public changes: any;

    public handle(state: MealmeInteractionState): MealmeInteractionState {
        return {
            ...state,
            meal: {
                ...state.meal,
                menu: {
                    ...state.meal.menu,
                    nutrients: {
                        ...state.meal.menu.nutrients,
                        ...this.changes
                    }
                }
            },
        };
    }
}

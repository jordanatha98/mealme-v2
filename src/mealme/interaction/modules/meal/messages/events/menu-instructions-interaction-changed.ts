import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeInteractionState } from '../../../../stores/states';

@UbudMessage('Interaction.Mealme.Menu.MenuInstructionsInteractionChanged')
export class MenuInstructionsInteractionChanged extends Message<MealmeInteractionState> {
    public changes: any;

    public handle(state: MealmeInteractionState): MealmeInteractionState {
        return {
            ...state,
            meal: {
                ...state.meal,
                menu: {
                    ...state.meal.menu,
                    instructions: {
                        ...state.meal.menu.instructions,
                        ...this.changes
                    }
                }
            },
        };
    }
}

import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeInteractionState } from '../../../../stores/states';

@UbudMessage('Interaction.Mealme.Menu.MenuDetailInteractionChanged')
export class MenuDetailInteractionChanged extends Message<MealmeInteractionState> {
    public changes: any;

    public handle(state: MealmeInteractionState): MealmeInteractionState {
        return {
            ...state,
            meal: {
                ...state.meal,
                menu: {
                    ...state.meal.menu,
                    detail: {
                        ...state.meal.menu.detail,
                        ...this.changes
                    }
                }
            },
        };
    }
}

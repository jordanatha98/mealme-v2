import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeInteractionState } from '../../../../stores/states';

@UbudMessage('Interaction.Mealme.Menu.GeneratePlanInteractionChanged')
export class GeneratePlanInteractionChanged extends Message<MealmeInteractionState> {
    public changes: any;

    public handle(state: MealmeInteractionState): MealmeInteractionState {
        console.log(this.changes)
        return {
            ...state,
            meal: {
                ...state.meal,
                planMenus: {
                    ...state.meal.planMenus,
                    generate: {
                        ...state.meal.planMenus.generate,
                        ...this.changes,
                    }
                },
            },
        };
    }
}

import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeInteractionState } from '../../../../stores/states';

@UbudMessage('Interaction.Mealme.Menu.BookmarkManageInteractionChanged')
export class BookmarkManageInteractionChanged extends Message<MealmeInteractionState> {
    public changes: any;

    public handle(state: MealmeInteractionState): MealmeInteractionState {
        return {
            ...state,
            meal: {
                ...state.meal,
                bookmark: {
                    ...state.meal.bookmark,
                    manage: {
                        ...state.meal.bookmark.manage,
                        ...this.changes
                    }
                }
            },
        };
    }
}

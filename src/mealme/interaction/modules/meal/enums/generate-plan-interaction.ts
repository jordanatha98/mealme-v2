export enum GeneratePlanInteraction {
    IDLE = 'idle',
    GENERATE_EXECUTING = 'generate_executing',
    GENERATE_SUCCEED = 'generate_succeed',
    GENERATE_FAILED = 'generate_failed',
}

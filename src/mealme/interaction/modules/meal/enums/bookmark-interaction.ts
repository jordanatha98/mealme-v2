export enum BookmarkInteraction {
    IDLE = 'idle',
    CREATE_EXECUTING = 'create_executing',
    CREATE_SUCCEED = 'create_succeed',
    CREATE_FAILED = 'create_failed',
    DELETE_EXECUTING = 'delete_executing',
    DELETE_SUCCEED = 'delete_succeed',
    DELETE_FAILED = 'delete_failed',
}

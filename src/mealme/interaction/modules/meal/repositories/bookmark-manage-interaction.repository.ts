import { Injectable } from '@angular/core';
import { Repository } from '@ubud/ngrx';
import { Observable } from 'rxjs';
import { MealmeInteractionStore } from '@mealme/src/interaction/stores/mealme-interaction.store';
import { MealmeInteractionState } from '@mealme/src/interaction/stores/states';
import { BookmarkInteraction } from '@mealme/src/interaction/modules/meal/enums/bookmark-interaction';

@Injectable()
export class BookmarkManageInteractionRepository extends Repository<MealmeInteractionState> {
    public constructor(store: MealmeInteractionStore) {
        super(store);
    }

    public isProcess$(): Observable<boolean> {
        return this.select(state => state.meal.bookmark.manage.process);
    }

    public getError$(): Observable<string> {
        return this.select(state => state.meal.bookmark.manage.error);
    }

    public getSuccess$(): Observable<string> {
        return this.select(state => state.meal.bookmark.manage.success);
    }

    public getInteraction$(): Observable<BookmarkInteraction> {
        return this.select(state => state.meal.bookmark.manage.interaction);
    }
}

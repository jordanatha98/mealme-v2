import { Injectable } from '@angular/core';
import { Repository } from '@ubud/ngrx';
import { Observable } from 'rxjs';
import { MealmeInteractionStore } from '@mealme/src/interaction/stores/mealme-interaction.store';
import { MealmeInteractionState } from '@mealme/src/interaction/stores/states';
import { GeneratePlanInteraction } from '@mealme/src/interaction/modules/meal/enums/generate-plan-interaction';

@Injectable()
export class PlanMenusGenerateInteractionRepository extends Repository<MealmeInteractionState> {
    public constructor(store: MealmeInteractionStore) {
        super(store);
    }

    public isProcess$(): Observable<boolean> {
        return this.select(state => state.meal.planMenus.generate.process);
    }

    public getError$(): Observable<string> {
        return this.select(state => state.meal.planMenus.generate.error);
    }

    public getSuccess$(): Observable<string> {
        return this.select(state => state.meal.planMenus.generate.success);
    }

    public getInteraction$(): Observable<GeneratePlanInteraction> {
        return this.select(state => state.meal.planMenus.generate.interaction);
    }
}

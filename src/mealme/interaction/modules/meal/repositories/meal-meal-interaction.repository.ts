import { Injectable } from '@angular/core';
import { Repository } from '@ubud/ngrx';
import { Observable } from 'rxjs';
import { MealmeInteractionStore } from '@mealme/src/interaction/stores/mealme-interaction.store';
import { MealmeInteractionState } from '@mealme/src/interaction/stores/states';

@Injectable()
export class MealMealInteractionRepository extends Repository<MealmeInteractionState> {
    public constructor(store: MealmeInteractionStore) {
        super(store);
    }

    public isProcess$(): Observable<boolean> {
        return this.select(state => state.meal.meal.process);
    }

    public getError$(): Observable<string> {
        return this.select(state => state.meal.meal.error);
    }
}

import { Injectable } from '@angular/core';
import { Repository } from '@ubud/ngrx';
import { Observable } from 'rxjs';
import { MealmeInteractionStore } from '@mealme/src/interaction/stores/mealme-interaction.store';
import { MealmeInteractionState } from '@mealme/src/interaction/stores/states';

@Injectable()
export class MenuDetailInteractionRepository extends Repository<MealmeInteractionState> {
    public constructor(store: MealmeInteractionStore) {
        super(store);
    }

    public isProcess$(): Observable<boolean> {
        return this.select(state => state.meal.menu.detail.process);
    }

    public getError$(): Observable<string> {
        return this.select(state => state.meal.menu.detail.error);
    }

    public isNutrientsProcess$(): Observable<boolean> {
        return this.select(state => state.meal.menu.nutrients.process);
    }

    public getNutrientsError$(): Observable<string> {
        return this.select(state => state.meal.menu.nutrients.error);
    }

    public isIngredientsProcess$(): Observable<boolean> {
        return this.select(state => state.meal.menu.ingredients.process);
    }

    public getIngredientsError$(): Observable<string> {
        return this.select(state => state.meal.menu.ingredients.error);
    }

    public isInstructionsProcess$(): Observable<boolean> {
        return this.select(state => state.meal.menu.instructions.process);
    }

    public getInstructionsError$(): Observable<string> {
        return this.select(state => state.meal.menu.instructions.error);
    }
}

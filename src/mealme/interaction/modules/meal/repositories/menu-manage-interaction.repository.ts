import { Injectable } from '@angular/core';
import { Repository } from '@ubud/ngrx';
import { Observable } from 'rxjs';
import { MealmeInteractionStore } from '@mealme/src/interaction/stores/mealme-interaction.store';
import { MealmeInteractionState } from '@mealme/src/interaction/stores/states';
import { BookmarkInteraction } from '@mealme/src/interaction/modules/meal/enums/bookmark-interaction';
import { MenuInteraction } from '@mealme/src/interaction/modules/meal/enums/menu-interaction';

@Injectable()
export class MenuManageInteractionRepository extends Repository<MealmeInteractionState> {
    public constructor(store: MealmeInteractionStore) {
        super(store);
    }

    public isProcess$(): Observable<boolean> {
        return this.select(state => state.meal.menu.manage.process);
    }

    public getError$(): Observable<string> {
        return this.select(state => state.meal.menu.manage.error);
    }

    public getSuccess$(): Observable<string> {
        return this.select(state => state.meal.menu.manage.success);
    }

    public getInteraction$(): Observable<MenuInteraction> {
        return this.select(state => state.meal.menu.manage.interaction);
    }
}

import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { from, Observable, of, throwError } from 'rxjs';
import { retryWhen, switchMap } from 'rxjs/operators';
import { AuthService } from '@mealme/src/domain/modules/auth/services/auth.service';
import { InterceptorConfig } from '@mealme/src/api/interceptors/interceptor.config';
import { Signature } from '@mealme/src/domain/modules/auth/models/signature';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    public constructor(
        private service: AuthService,
        private config: InterceptorConfig,
    ) {
    }

    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let needRetry: boolean = true;

        return from(this.service.getSignature()).pipe(
            switchMap((signature: Signature | null) => {
                if (!signature) {
                    needRetry = false;
                    return next.handle(req);
                }

                const endpoint = this.config?.endpoint;

                const excludes = [];

                const url = req.url;
                const split = url.split('/');
                let needExclude = false;

                excludes.forEach(exclude => {
                    if (exclude.method.toUpperCase() === req.method.toUpperCase()) {
                        const excludeSplit = exclude.url.split('/');
                        if (excludeSplit.length === split.length) {
                            const mapping = excludeSplit.map((item, index) => {
                                const findStart = item.indexOf('{');
                                const findEnd = item.indexOf('}');
                                if (findStart !== -1 && findEnd !== -1) {
                                    if (split[index]) {
                                        item = split[index];
                                    }
                                }
                                return item;
                            });

                            if (mapping.join('/') === url) {
                                needExclude = true;
                            }
                        }
                    }
                });

                if (needExclude) {
                    return next.handle(req);
                }

                return next.handle(
                    req.clone({
                        setHeaders: {
                            Authorization: `${signature.type} ${signature.token}`,
                        },
                    }),
                );
            }),
            retryWhen((error: Observable<HttpErrorResponse>) => {
                return error.pipe(
                    switchMap((err: HttpErrorResponse) => {
                        if (err.status !== 401 || !needRetry) {
                            return throwError(err);
                        }

                        needRetry = false;
                        return this.service.refreshUser().pipe(
                            switchMap((user: any) => {
                                if (user) {
                                    // TODO: When there is another service that need fetch detail user for each service
                                }

                                if (!user) {
                                    return of(null);
                                }

                                return throwError(err);
                            }),
                        );
                    }),
                );
            }),
        );
    }
}

import { HttpHandler } from '@angular/common/http';
import { ubudHttpHandlerFactory } from '@ubud/http';
import { MealmeApiClient } from '@mealme/src/api/clients/api.client';
import { MealmeApiConfig } from '@mealme/src/api/clients/config';

export function ApiClientFactory(handler: HttpHandler, config: MealmeApiConfig) {
    return new MealmeApiClient(ubudHttpHandlerFactory(handler, config.interceptors, { url: config.endpoint }));
}

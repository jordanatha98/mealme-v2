import { HttpInterceptor } from '@angular/common/http';

export class MealmeApiConfig {
    endpoint: string;
    interceptors?: HttpInterceptor[];
}

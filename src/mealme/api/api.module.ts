import { HTTP_INTERCEPTORS, HttpHandler } from '@angular/common/http';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { CamelResponseTransformerInterceptor, SnakeRequestTransformerInterceptor } from '@ubud/http';
import { MealmeApiClient } from '@mealme/src/api/clients/api.client';
import { ApiClientFactory } from '@mealme/src/api/clients/api-client.factory';
import { MealmeApiConfig } from '@mealme/src/api/clients/config';
import { AuthInterceptor } from '@mealme/src/api/interceptors/auth.interceptor';
import { InterceptorConfig } from '@mealme/src/api/interceptors/interceptor.config';

@NgModule()
export class MealmeApiModule {
    public static forRoot(endpoint: string, interceptorConfig?: InterceptorConfig): ModuleWithProviders<MealmeApiModule> {
        return {
            ngModule: MealmeApiModule,
            providers: [
                {
                    provide: MealmeApiConfig,
                    useValue: {
                        endpoint,
                    },
                },
                {
                    provide: MealmeApiClient,
                    useFactory: ApiClientFactory,
                    deps: [HttpHandler, MealmeApiConfig],
                },
                {
                    provide: InterceptorConfig,
                    useValue: interceptorConfig
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: SnakeRequestTransformerInterceptor,
                    multi: true,
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: CamelResponseTransformerInterceptor,
                    multi: true,
                },
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: AuthInterceptor,
                    multi: true,
                },
            ],
        };
    }
}

import { Injectable } from '@angular/core';
import { Store, UbudStore } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';

@Injectable()
@UbudStore('mealme')
export class MealmeStore extends Store<MealmeState> {
}

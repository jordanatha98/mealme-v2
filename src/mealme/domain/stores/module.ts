import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { mealmeReducer } from '@mealme/src/domain/stores/mealme.reducer';
import { MealmeStore } from '@mealme/src/domain/stores/mealme.store';

@NgModule({
    imports: [
        StoreModule.forFeature('mealme', mealmeReducer),
    ],
    providers: [MealmeStore],
})
export class MealmeDomainMealmeStoreModule {}

import { createReducer, Message } from '@ubud/ngrx';
import { MealmeState } from './states';
import { MEALME_PRODUCT_MESSAGES } from '@mealme/src/domain/modules/products/messages';
import { MEALME_AUTH_MESSAGES } from '@mealme/src/domain/modules/auth/messages';
import { MEALME_MEAL_MESSAGES } from '@mealme/src/domain/modules/meal/messages';
import { MEALME_COMMON_MESSAGES } from '@mealme/src/domain/modules/common/messages';
import { MEALME_ACCOUNT_MESSAGES } from '@mealme/src/domain/modules/account/messages';

const INITIAL_STATE: MealmeState = {
    product: {
        products: null,
    },
    user: {
        user: null,
        authenticated: null,
    },
    auth: {
        login: {
            formState: null,
        },
        signature: null,
        refreshToken: null,
    },
    meal: {
        meal: null,
        meals: null,
        plans: null,
        yesterdayPlans: null,
        homePlans: null,
        menu: null,
        menuNutrients: null,
        menuIngredients: null,
        menuInstructions: null,
        bookmarks: null,
    },
    plan: {
        plan: null,
    }
};

export function mealmeReducer(state: MealmeState = INITIAL_STATE, action: Message<MealmeState>): MealmeState {
    const messages: any[] = [
        ...MEALME_PRODUCT_MESSAGES,
        ...MEALME_AUTH_MESSAGES,
        ...MEALME_MEAL_MESSAGES,
        ...MEALME_COMMON_MESSAGES,
        ...MEALME_ACCOUNT_MESSAGES,
    ];

    return createReducer<MealmeState>(...messages)(state, action);
}

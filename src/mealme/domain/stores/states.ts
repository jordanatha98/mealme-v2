import { User } from '@mealme/src/domain/modules/auth/models/user';
import { FormState } from '@ubud/form';
import { LoginDto } from '@mealme/src/domain/modules/auth/dtos/login-dto';
import { Signature } from '@mealme/src/domain/modules/auth/models/signature';
import { Menu } from '@mealme/src/domain/modules/meal/models/menu';
import { Plan } from '@mealme/src/domain/modules/plan/models/plan';
import { Collection } from '@shared/types/collection';
import { PlanMenu } from '@mealme/src/domain/modules/plan/models/plan-menu';
import { MenuInstruction } from '@mealme/src/domain/modules/meal/models/menu-instruction';
import { MenuIngredient } from '@mealme/src/domain/modules/meal/models/menu-ingredient';
import { MenuNutrient } from '@mealme/src/domain/modules/meal/models/menu-nutrient';
import { Bookmark } from '@mealme/src/domain/modules/meal/models/bookmark';

interface AuthState {
    login: {
        formState: FormState<LoginDto>;
    };
    signature: Signature;
    refreshToken: Signature;
}

interface ProductState {
    products: any[];
}

interface UserState {
    user: User;
    authenticated: User;
}

interface MealState {
    meal: Menu;
    meals: Menu[];
    plans: Collection<Plan>;
    yesterdayPlans: Collection<Plan>;
    homePlans: Collection<Plan>;
    menu: Menu;
    menuInstructions: MenuInstruction[];
    menuIngredients: MenuIngredient[];
    menuNutrients: MenuNutrient[];
    bookmarks: Collection<Bookmark>;
}

interface PlanState {
    plan: Plan;
}

export interface MealmeState {
    product: ProductState;
    user: UserState;
    auth: AuthState;
    meal: MealState;
    plan: PlanState;
}

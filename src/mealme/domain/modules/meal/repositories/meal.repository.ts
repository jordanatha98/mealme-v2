import { Injectable } from '@angular/core';
import { Repository } from '@ubud/ngrx';
import { MealmeState } from '../../../stores/states';
import { Observable } from 'rxjs';
import { MealmeStore } from '@mealme/src/domain/stores/mealme.store';
import { Menu } from '@mealme/src/domain/modules/meal/models/menu';
import { Plan } from '@mealme/src/domain/modules/plan/models/plan';
import { Collection } from '@shared/types/collection';
import { MenuNutrient } from '@mealme/src/domain/modules/meal/models/menu-nutrient';
import { MenuIngredient } from '@mealme/src/domain/modules/meal/models/menu-ingredient';
import { MenuInstruction } from '@mealme/src/domain/modules/meal/models/menu-instruction';
import { Bookmark } from '@mealme/src/domain/modules/meal/models/bookmark';

@Injectable()
export class MealRepository extends Repository<MealmeState> {
    public constructor(store: MealmeStore) {
        super(store);
    }

    public getMeal$(): Observable<any> {
        return this.select(state => state.meal.meal);
    }

    public getMeals$(): Observable<Menu[]> {
        return this.select(state => state.meal.meals);
    }

    public getPlans$(): Observable<Collection<Plan>> {
        return this.select(state => state.meal.plans);
    }

    public getYesterdayPlans$(): Observable<Collection<Plan>> {
        return this.select(state => state.meal.yesterdayPlans);
    }

    public getHomePlans$(): Observable<Collection<Plan>> {
        return this.select(state => state.meal.homePlans);
    }

    public getMenu$(): Observable<Menu> {
        return this.select(state => state.meal.menu);
    }

    public getMenuNutrients$(): Observable<MenuNutrient[]> {
        return this.select(state => state.meal.menuNutrients)
    }

    public getMenuIngredients$(): Observable<MenuIngredient[]> {
        return this.select(state => state.meal.menuIngredients)
    }

    public getMenuInstructions$(): Observable<MenuInstruction[]> {
        return this.select(state => state.meal.menuInstructions)
    }

    public getBookmarks$(): Observable<Collection<Bookmark>> {
        return this.select(state => state.meal.bookmarks);
    }
}

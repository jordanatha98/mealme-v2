import { Injectable } from '@angular/core';
import { Effects, Message, ubudType } from '@ubud/ngrx';
import { Actions, Effect } from '@ngrx/effects';
import { MealmeStore } from '@mealme/src/domain/stores/mealme.store';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { MessageUtil } from '@shared/utils/message.util';
import { RouterUtil } from '@shared/utils/router.util';
import { HttpErrorResponse } from '@angular/common/http';
import { MealService } from '@mealme/src/domain/modules/meal/services/meal.service';
import { FetchNextMeal } from '@mealme/src/domain/modules/meal/messages/commands/fetch-next-meal';
import { FetchNextMealSucceed } from '@mealme/src/domain/modules/meal/messages/events/fetch-next-meal-succeed';
import { FetchNextMealFailed } from '@mealme/src/domain/modules/meal/messages/events/fetch-next-meal-failed';
import { MealsEntity } from '@mealme/src/domain/modules/meal/messages/documents/meals-entity';
import { FetchMeals } from '@mealme/src/domain/modules/meal/messages/commands/fetch-meals';
import { FetchMealsSucceed } from '@mealme/src/domain/modules/meal/messages/events/fetch-meals-succeed';
import { FetchMealsFailed } from '@mealme/src/domain/modules/meal/messages/events/fetch-meals-failed';
import { FetchPlans } from '@mealme/src/domain/modules/meal/messages/commands/fetch-plans';
import { FetchPlansSucceed } from '@mealme/src/domain/modules/meal/messages/events/fetch-plans-succeed';
import { PlansEntity } from '@mealme/src/domain/modules/meal/messages/documents/plans-entity';
import { FetchPlansFailed } from '@mealme/src/domain/modules/meal/messages/events/fetch-plans-failed';
import { FetchMenu } from '@mealme/src/domain/modules/meal/messages/commands/fetch-menu';
import { FetchMenuSucceed } from '@mealme/src/domain/modules/meal/messages/events/fetch-menu-succeed';
import { MenuEntity } from '@mealme/src/domain/modules/meal/messages/documents/menu-entity';
import { FetchMenuFailed } from '@mealme/src/domain/modules/meal/messages/events/fetch-menu-failed';
import { FetchMenuNutrients } from '@mealme/src/domain/modules/meal/messages/commands/fetch-menu-nutrients';
import { FetchMenuNutrientsSucceed } from '@mealme/src/domain/modules/meal/messages/events/fetch-menu-nutrients-succeed';
import { MenuNutrientsEntity } from '@mealme/src/domain/modules/meal/messages/documents/menu-nutrients-entity';
import { FetchMenuNutrientsFailed } from '@mealme/src/domain/modules/meal/messages/events/fetch-menu-nutrients-failed';
import { FetchMenuIngredients } from '@mealme/src/domain/modules/meal/messages/commands/fetch-menu-ingredients';
import { FetchMenuIngredientsSucceed } from '@mealme/src/domain/modules/meal/messages/events/fetch-menu-ingredients-succeed';
import { MenuIngredientsEntity } from '@mealme/src/domain/modules/meal/messages/documents/menu-ingredients-entity';
import { FetchMenuIngredientsFailed } from '@mealme/src/domain/modules/meal/messages/events/fetch-menu-ingredients-failed';
import { FetchMenuInstructions } from '@mealme/src/domain/modules/meal/messages/commands/fetch-menu-instructions';
import { FetchMenuInstructionsFailed } from '@mealme/src/domain/modules/meal/messages/events/fetch-menu-instructions-failed';
import { FetchMenuInstructionsSucceed } from '@mealme/src/domain/modules/meal/messages/events/fetch-menu-instructions-succeed';
import { MenuInstructionsEntity } from '@mealme/src/domain/modules/meal/messages/documents/menu-instructions-entity';
import { GeneratePlan } from '@mealme/src/domain/modules/meal/messages/commands/generate-plan';
import { GeneratePlanSucceed } from '@mealme/src/domain/modules/meal/messages/events/generate-plan-succeed';
import { GeneratePlanFailed } from '@mealme/src/domain/modules/meal/messages/events/generate-plan-failed';
import { FetchBookmarks } from '@mealme/src/domain/modules/meal/messages/commands/fetch-bookmarks';
import { FetchBookmarksSucceed } from '@mealme/src/domain/modules/meal/messages/events/fetch-bookmarks-succeed';
import { BookmarksEntity } from '@mealme/src/domain/modules/meal/messages/documents/bookmarks-entity';
import { FetchBookmarksFailed } from '@mealme/src/domain/modules/meal/messages/events/fetch-bookmarks-failed';
import { CreateBookmark } from '@mealme/src/domain/modules/meal/messages/commands/create-bookmark';
import { CreateBookmarkSucceed } from '@mealme/src/domain/modules/meal/messages/events/create-bookmark-succeed';
import { CreateBookmarkFailed } from '@mealme/src/domain/modules/meal/messages/events/create-bookmark-failed';
import { DeleteBookmark } from '@mealme/src/domain/modules/meal/messages/commands/delete-bookmark';
import { DeleteBookmarkSucceed } from '@mealme/src/domain/modules/meal/messages/events/delete-bookmark-succeed';
import { DeleteBookmarkFailed } from '@mealme/src/domain/modules/meal/messages/events/delete-bookmark-failed';
import { CreateMenu } from '@mealme/src/domain/modules/meal/messages/commands/menu/create-menu';
import { CreateMenuSucceed } from '@mealme/src/domain/modules/meal/messages/events/menu/create-menu-succeed';
import { CreateMenuFailed } from '@mealme/src/domain/modules/meal/messages/events/menu/create-menu-failed';
import { CustomerMenu } from '@mealme/src/domain/modules/meal/models/customer-menu';
import { CreateMenuInstruction } from '@mealme/src/domain/modules/meal/messages/commands/menu/create-menu-instruction';
import { MenuInstruction } from '@mealme/src/domain/modules/meal/models/menu-instruction';
import { CreateMenuInstructionSucceed } from '@mealme/src/domain/modules/meal/messages/events/menu/create-menu-instruction-succeed';
import { CreateMenuInstructionFailed } from '@mealme/src/domain/modules/meal/messages/events/menu/create-menu-instruction-failed';
import { CreateMenuIngredient } from '@mealme/src/domain/modules/meal/messages/commands/menu/create-menu-ingredient';
import { CreateMenuIngredientSucceed } from '@mealme/src/domain/modules/meal/messages/events/menu/create-menu-ingredient-succeed';
import { MenuIngredient } from '@mealme/src/domain/modules/meal/models/menu-ingredient';
import { CreateMenuIngredientFailed } from '@mealme/src/domain/modules/meal/messages/events/menu/create-menu-ingredient-failed';
import { CreateMenuNutrient } from '@mealme/src/domain/modules/meal/messages/commands/menu/create-menu-nutrient';
import { MenuNutrient } from '@mealme/src/domain/modules/meal/models/menu-nutrient';
import { CreateMenuNutrientSucceed } from '@mealme/src/domain/modules/meal/messages/events/menu/create-menu-nutrient-succeed';
import { CreateMenuNutrientFailed } from '@mealme/src/domain/modules/meal/messages/events/menu/create-menu-nutrient-failed';

@Injectable()
export class MealEffect extends Effects {
    public constructor(actions$: Actions, private service: MealService, private store: MealmeStore) {
        super(actions$);
    }

    @Effect()
    public fetchNextMeal$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchNextMeal),
        map(message => MessageUtil.getPayload<FetchNextMeal>(message)),
        mergeMap(({ queries }) => {
            return this.service.getNextMeal(RouterUtil.queryParamsExtractor(queries)).pipe(
                mergeMap(response => {
                    if (response) {
                        return of(
                            new FetchNextMealSucceed(),
                            new MealsEntity({ meals: response })
                        )
                    }

                    return of(
                        new FetchNextMealFailed({
                            exception: new HttpErrorResponse({
                                error: `Something Error`,
                                status: 400,
                            }),
                        })
                    )
                }),
                catchError((e: HttpErrorResponse) => of(new FetchNextMealFailed({exception: e}))),
            );
        }),
    );

    @Effect()
    public fetchMeals$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchMeals),
        map(message => MessageUtil.getPayload<FetchMeals>(message)),
        mergeMap(({ queries }) => {
            return this.service.getMeals(RouterUtil.queryParamsExtractor(queries)).pipe(
                mergeMap(response => {
                    if (response) {
                        return of(
                            new FetchMealsSucceed(),
                            new MealsEntity({ meals: response })
                        )
                    }

                    return of(
                        new FetchMealsFailed({
                            exception: new HttpErrorResponse({
                                error: `Something Error`,
                                status: 400,
                            }),
                        })
                    )
                }),
                catchError((e: HttpErrorResponse) => of(new FetchMealsFailed({exception: e}))),
            );
        }),
    );

    @Effect()
    public fetchPlanMenus$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchPlans),
        map(message => MessageUtil.getPayload<FetchPlans>(message)),
        mergeMap(({ customer, queries, yesterday, home }) => {
            return this.service.getPlans(customer, RouterUtil.queryParamsExtractor(queries)).pipe(
                mergeMap(response => {
                    if (response) {
                        return of(
                            new FetchPlansSucceed(),
                            new PlansEntity({ yesterday, plans: response, home })
                        )
                    }

                    return of(
                        new FetchPlansFailed({
                            exception: new HttpErrorResponse({
                                error: `Something Error`,
                                status: 400,
                            }),
                        })
                    )
                }),
                catchError((e: HttpErrorResponse) => of(new FetchPlansFailed({exception: e}))),
            );
        }),
    );

    @Effect()
    public fetchMenu$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchMenu),
        map(message => MessageUtil.getPayload<FetchMenu>(message)),
        mergeMap(({ menu }) => {
            return this.service.getMenu(menu).pipe(
                mergeMap(response => {
                    if (response) {
                        return of(
                            new FetchMenuSucceed(),
                            new MenuEntity({ menu: response })
                        )
                    }

                    return of(
                        new FetchMenuFailed({
                            exception: new HttpErrorResponse({
                                error: `Something Error`,
                                status: 400,
                            }),
                        })
                    )
                }),
                catchError((e: HttpErrorResponse) => of(new FetchMenuFailed({exception: e}))),
            );
        }),
    );

    @Effect()
    public fetchMenuNutrients$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchMenuNutrients),
        map(message => MessageUtil.getPayload<FetchMenuNutrients>(message)),
        mergeMap(({ menu }) => {
            return this.service.getMenuNutrients(menu).pipe(
                mergeMap(response => {
                    if (response) {
                        return of(
                            new FetchMenuNutrientsSucceed(),
                            new MenuNutrientsEntity({ menuNutrients: response })
                        )
                    }

                    return of(
                        new FetchMenuNutrientsFailed({
                            exception: new HttpErrorResponse({
                                error: `Something Error`,
                                status: 400,
                            }),
                        })
                    )
                }),
                catchError((e: HttpErrorResponse) => of(new FetchMenuNutrientsFailed({exception: e}))),
            );
        }),
    );

    @Effect()
    public fetchMenuIngredients$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchMenuIngredients),
        map(message => MessageUtil.getPayload<FetchMenuIngredients>(message)),
        mergeMap(({ menu }) => {
            return this.service.getMenuIngredients(menu).pipe(
                mergeMap(response => {
                    if (response) {
                        return of(
                            new FetchMenuIngredientsSucceed(),
                            new MenuIngredientsEntity({ menuIngredients: response })
                        )
                    }

                    return of(
                        new FetchMenuIngredientsFailed({
                            exception: new HttpErrorResponse({
                                error: `Something Error`,
                                status: 400,
                            }),
                        })
                    )
                }),
                catchError((e: HttpErrorResponse) => of(new FetchMenuIngredientsFailed({exception: e}))),
            );
        }),
    );

    @Effect()
    public fetchMenuInstructions$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchMenuInstructions),
        map(message => MessageUtil.getPayload<FetchMenuInstructions>(message)),
        mergeMap(({ menu }) => {
            return this.service.getMenuInstructions(menu).pipe(
                mergeMap(response => {
                    if (response) {
                        return of(
                            new FetchMenuInstructionsSucceed(),
                            new MenuInstructionsEntity({ menuInstructions: response })
                        )
                    }

                    return of(
                        new FetchMenuInstructionsFailed({
                            exception: new HttpErrorResponse({
                                error: `Something Error`,
                                status: 400,
                            }),
                        })
                    )
                }),
                catchError((e: HttpErrorResponse) => of(new FetchMenuInstructionsFailed({exception: e}))),
            );
        }),
    );

    @Effect()
    public generatePlan$: Observable<Message> = this.actions$.pipe(
        ubudType(GeneratePlan),
        map(message => MessageUtil.getPayload<GeneratePlan>(message)),
        mergeMap(({ customer, payload }) => {
            return this.service.generatePlans(customer, payload).pipe(
                mergeMap(response => {
                    console.log(response)
                    if (response) {
                        return of(
                            new GeneratePlanSucceed(),
                        )
                    }

                    return of(
                        new GeneratePlanFailed({
                            exception: new HttpErrorResponse({
                                error: `Something Error`,
                                status: 400,
                            }),
                        })
                    )
                }),
                catchError((e: HttpErrorResponse) => {
                    return of(new GeneratePlanFailed({exception: e}))
                }),
            );
        }),
    );

    @Effect()
    public fetchBookmarks$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchBookmarks),
        map(message => MessageUtil.getPayload<FetchBookmarks>(message)),
        mergeMap(({ customer }) => {
            return this.service.getBookmarks(customer).pipe(
                mergeMap(response => {
                    if (response) {
                        return of(
                            new FetchBookmarksSucceed(),
                            new BookmarksEntity({ bookmarks: response })
                        )
                    }

                    return of(
                        new FetchBookmarksFailed({
                            exception: new HttpErrorResponse({
                                error: `Something Error`,
                                status: 400,
                            }),
                        })
                    )
                }),
                catchError((e: HttpErrorResponse) => of(new FetchBookmarksFailed({exception: e}))),
            );
        }),
    );

    @Effect()
    public createBookmark$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateBookmark),
        map(message => MessageUtil.getPayload<CreateBookmark>(message)),
        mergeMap(({ customer, menu }) => {
            return this.service.createBookmark(customer, menu).pipe(
                mergeMap(response => {
                    if (response) {
                        return of(
                            new CreateBookmarkSucceed(),
                        );
                    }

                    return of(
                        new CreateBookmarkFailed({
                            exception: new HttpErrorResponse({
                                status: 400,
                                error: $localize`There was an error processing`,
                            }),
                        })
                    );
                }),
                catchError((e: HttpErrorResponse) => {
                    return of(new CreateBookmarkFailed({ exception: e }));
                }),
            );
        }),
    );

    @Effect()
    public deleteBookmark$: Observable<Message> = this.actions$.pipe(
        ubudType(DeleteBookmark),
        map(message => MessageUtil.getPayload<DeleteBookmark>(message)),
        mergeMap(({ bookmark }) => {
            return this.service.deleteBookmark(bookmark).pipe(
                mergeMap(response => {
                    if (response) {
                        return of(
                            new DeleteBookmarkSucceed(),
                        );
                    }

                    return of(
                        new DeleteBookmarkFailed({
                            exception: new HttpErrorResponse({
                                status: 400,
                                error: $localize`There was an error processing`,
                            }),
                        })
                    );
                }),
                catchError((e: HttpErrorResponse) => {
                    return of(new DeleteBookmarkFailed({ exception: e }));
                }),
            );
        }),
    );

    @Effect()
    public createMenu$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateMenu),
        map(message => MessageUtil.getPayload<CreateMenu>(message)),
        mergeMap(({ customer, payload }) => {
            return this.service.createMenu(customer, payload).pipe(
                mergeMap(response => {
                    if (response instanceof CustomerMenu) {
                        return of(
                            new CreateMenuSucceed({ response }),
                        );
                    }

                    return of(
                        new CreateMenuFailed({
                            exception: new HttpErrorResponse({
                                status: 400,
                                error: $localize`There was an error processing`,
                            }),
                        })
                    );
                }),
                catchError((e: HttpErrorResponse) => {
                    return of(new CreateMenuFailed({ exception: e }));
                }),
            );
        }),
    );

    @Effect()
    public createMenuInstructions$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateMenuInstruction),
        map(message => MessageUtil.getPayload<CreateMenuInstruction>(message)),
        mergeMap(({ menu, payload }) => {
            return this.service.createMenuInstruction(menu, payload).pipe(
                mergeMap(response => {
                    if (response instanceof MenuInstruction) {
                        return of(
                            new CreateMenuInstructionSucceed(),
                        );
                    }

                    return of(
                        new CreateMenuInstructionFailed({
                            exception: new HttpErrorResponse({
                                status: 400,
                                error: $localize`There was an error processing`,
                            }),
                        })
                    );
                }),
                catchError((e: HttpErrorResponse) => {
                    return of(new CreateMenuInstructionFailed({ exception: e }));
                }),
            );
        }),
    );

    @Effect()
    public createMenuIngredient$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateMenuIngredient),
        map(message => MessageUtil.getPayload<CreateMenuIngredient>(message)),
        mergeMap(({ menu, payload }) => {
            return this.service.createMenuIngredient(menu, payload).pipe(
                mergeMap(response => {
                    if (response instanceof MenuIngredient) {
                        return of(
                            new CreateMenuIngredientSucceed(),
                        );
                    }

                    return of(
                        new CreateMenuIngredientFailed({
                            exception: new HttpErrorResponse({
                                status: 400,
                                error: $localize`There was an error processing`,
                            }),
                        })
                    );
                }),
                catchError((e: HttpErrorResponse) => {
                    return of(new CreateMenuIngredientFailed({ exception: e }));
                }),
            );
        }),
    );

    @Effect()
    public createMenuNutrient$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateMenuNutrient),
        map(message => MessageUtil.getPayload<CreateMenuNutrient>(message)),
        mergeMap(({ menu, payload }) => {
            return this.service.createMenuNutrient(menu, payload).pipe(
                mergeMap(response => {
                    if (response instanceof MenuNutrient) {
                        return of(
                            new CreateMenuNutrientSucceed(),
                        );
                    }

                    return of(
                        new CreateMenuNutrientFailed({
                            exception: new HttpErrorResponse({
                                status: 400,
                                error: $localize`There was an error processing`,
                            }),
                        })
                    );
                }),
                catchError((e: HttpErrorResponse) => {
                    return of(new CreateMenuNutrientFailed({ exception: e }));
                }),
            );
        }),
    );
}

import { Injectable } from '@angular/core';
import { MealmeApiClient } from '@mealme/src/api/clients/api.client';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { Menu } from '@mealme/src/domain/modules/meal/models/menu';
import { Collection } from '@shared/types/collection';
import { HttpUtil } from '@shared/utils/http.util';
import { mapToArrayClass, mapToClass, mapToCollection, mapToData } from '@shared/transformers/responses.transformer';
import { Plan } from '@mealme/src/domain/modules/plan/models/plan';
import { MenuNutrient } from '@mealme/src/domain/modules/meal/models/menu-nutrient';
import { MenuIngredient } from '@mealme/src/domain/modules/meal/models/menu-ingredient';
import { MenuInstruction } from '@mealme/src/domain/modules/meal/models/menu-instruction';
import { GeneratePlanDto } from '@mealme/src/domain/modules/meal/dtos/generate-plan-dto';
import { Bookmark } from '@mealme/src/domain/modules/meal/models/bookmark';
import { CustomerMenu } from '@mealme/src/domain/modules/meal/models/customer-menu';
import { MenuDto } from '@mealme/src/domain/modules/meal/dtos/menu-dto';
import { MenuInstructionDto } from '@mealme/src/domain/modules/meal/dtos/menu-instruction-dto';
import { MenuIngredientDto } from '@mealme/src/domain/modules/meal/dtos/menu-ingredient-dto';
import { MenuNutrientDto } from '@mealme/src/domain/modules/meal/dtos/menu-nutrient-dto';

@Injectable()
export class MealService {
    public mealDummy: Menu[] = [
        new Menu({
            id: '1',
            name: 'Avocado Toast',
            cover: 'assets/food-1.png'
        }),
        new Menu({
            id: '2',
            name: 'Creamy Salad Toast',
            cover: 'assets/food-2.png'
        }),
        new Menu({
            id: '3',
            name: 'Bowl of Hell',
            cover: 'assets/food-1.png'
        }),
        new Menu({
            id: '4',
            name: 'Bowl of Heaven',
            cover: 'assets/food-2.png'
        })
    ]

    public constructor(private client: MealmeApiClient) {
    }

    public getNextMeal(queries?: any): Observable<Menu[]> {
        return of(this.mealDummy).pipe(
            delay(3000)
        );
    }

    public getMeal(queries?: any): Observable<Menu> {
        return of(this.mealDummy[0]).pipe(
            delay(3000)
        );
    }

    public getMeals(queries?: any): Observable<Menu[]> {
        return of(this.mealDummy).pipe(
            delay(1000)
        )
    }

    public getPlans(customer: string, queries?: any): Observable<Collection<Plan>> {
        return this.client.get(`customers/${customer}/plans`, { params: HttpUtil.queryParamsExtractor(queries) }).pipe(
            mapToCollection(Plan)
        );
    }

    public getMenu(menu: string): Observable<Menu> {
        return this.client.get(`menus/${menu}`).pipe(
            mapToData(),
            mapToClass(Menu)
        );
    }

    public getMenuNutrients(menu: string): Observable<MenuNutrient[]> {
        return this.client.get(`menus/${menu}/nutrients`).pipe(
            mapToData(),
            mapToArrayClass(MenuNutrient)
        );
    }

    public getMenuIngredients(menu: string): Observable<MenuIngredient[]> {
        return this.client.get(`menus/${menu}/ingredients`).pipe(
            mapToData(),
            mapToArrayClass(MenuIngredient)
        );
    }

    public getMenuInstructions(menu: string): Observable<MenuInstruction[]> {
        return this.client.get(`menus/${menu}/instructions`).pipe(
            mapToData(),
            mapToArrayClass(MenuInstruction)
        );
    }

    public generatePlans(customer: string, payload: GeneratePlanDto): Observable<any> {
        return this.client.post(`customers/${customer}/plans/generate`, payload).pipe(
            mapToData(),
        );
    }

    public getBookmarks(customer: string): Observable<Collection<Bookmark>> {
        return this.client.get(`customers/${customer}/menu-bookmarks`).pipe(
            mapToCollection(Bookmark)
        )
    }

    public createBookmark(customer: string, menu: string): Observable<any> {
        return this.client.post(`customers/${customer}/menus/${menu}/bookmark`, {}).pipe(
            mapToData()
        )
    }

    public deleteBookmark(bookmark: string): Observable<any> {
        return this.client.delete(`menu-bookmarks/${bookmark}`).pipe(
            mapToData(),
        )
    }

    public createMenu(customer: string, payload: MenuDto): Observable<CustomerMenu> {
        return this.client.post(`customers/${customer}/menus`, HttpUtil.payloadToFormData(payload)).pipe(
            mapToData(),
            mapToClass(CustomerMenu)
        )
    }

    public createMenuInstruction(menu: string, payload: MenuInstructionDto): Observable<MenuInstruction> {
        return this.client.post(`menus/${menu}/instructions`, payload).pipe(
            mapToData(),
            mapToClass(MenuInstruction)
        )
    }

    public createMenuIngredient(menu: string, payload: MenuIngredientDto): Observable<MenuIngredient> {
        return this.client.post(`menus/${menu}/ingredients`, payload).pipe(
            mapToData(),
            mapToClass(MenuIngredient)
        )
    }
    public createMenuNutrient(menu: string, payload: MenuNutrientDto): Observable<MenuNutrient> {
        return this.client.post(`menus/${menu}/nutrients`, payload).pipe(
            mapToData(),
            mapToClass(MenuNutrient)
        )
    }
}

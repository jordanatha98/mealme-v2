import { UUIDModel } from '@shared/models/uuid-model';
import { Type } from 'class-transformer';
import { forwardRef } from '@angular/core';
import { Ingredient } from '@mealme/src/domain/modules/common/models/ingredient';
import { Menu } from '@mealme/src/domain/modules/meal/models/menu';

export class MenuIngredient extends UUIDModel<MenuIngredient> {
    @Type(forwardRef(() => Ingredient) as any)
    public ingredient: Ingredient;
    public menuId: string;
    public name: string;
    public amount: number;
    public unit: string;
    @Type(forwardRef(() => Menu) as any)
    public menu: Menu
}
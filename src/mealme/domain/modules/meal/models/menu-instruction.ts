import { UUIDModel } from '@shared/models/uuid-model';
import { Type } from 'class-transformer';
import { forwardRef } from '@angular/core';
import { Menu } from '@mealme/src/domain/modules/meal/models/menu';

export class MenuInstruction extends UUIDModel<MenuInstruction> {
    public menuId: string;
    public step: number;
    public instruction: string;
    @Type(forwardRef(() => Menu) as any)
    public menu: Menu
}
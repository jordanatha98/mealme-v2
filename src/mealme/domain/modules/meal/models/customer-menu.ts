import { UUIDModel } from '@shared/models/uuid-model';
import { Type } from 'class-transformer';
import { forwardRef } from '@angular/core';
import { Customer } from '@mealme/src/domain/modules/customer/models/customer';

export class CustomerMenu extends UUIDModel<CustomerMenu> {
    @Type(forwardRef(() => Customer) as any)
    public customer: Customer;
    public title: string;
    public calorieAmount: number;
    public description: string;
    public duration: number;
    public healthScore: number;
    public customerId: string;
}
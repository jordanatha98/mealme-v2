import { UUIDModel } from '@shared/models/uuid-model';

export class Menu extends UUIDModel<Menu> {
    public name: string;
    public cover: string;
    public title: string;
    public imageUrl: string;
    public calorieAmount: number;
    public duration: number;
    public description: string;
    public healthScore: number;
    public rating: string;
    public verified: boolean;
}

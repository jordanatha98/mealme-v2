import { UUIDModel } from '@shared/models/uuid-model';
import { Type } from 'class-transformer';
import { forwardRef } from '@angular/core';
import { Menu } from '@mealme/src/domain/modules/meal/models/menu';

export class MenuNutrient extends UUIDModel<MenuNutrient> {
    public menuId: string;
    public name: string;
    public amount: number;
    public unit: string;
    public dailyAmountPercentage: number;
    @Type(forwardRef(() => Menu) as any)
    public menu: Menu
}
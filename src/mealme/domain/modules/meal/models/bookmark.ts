import { UUIDModel } from '@shared/models/uuid-model';
import { Type } from 'class-transformer';
import { forwardRef } from '@angular/core';
import { Menu } from './menu';

export class Bookmark extends UUIDModel<Bookmark> {
    @Type(forwardRef(() => Menu) as any)
    public menu: Menu;
    public menuId: string;
    public customerId: string;
}
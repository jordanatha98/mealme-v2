import { Injectable } from '@angular/core';
import { Form, FormFactory } from '@ubud/form';
import { FormBuilder } from '@angular/forms';
import { MenuNutrientRule } from '@mealme/src/domain/modules/meal/rules/menu-nutrient.rule';

@Injectable()
export class MenuNutrientFormFactory implements FormFactory {
    public constructor(private fb: FormBuilder) {
    }

    public create(): Form {
        const formRule = new MenuNutrientRule();

        return {
            formGroup: this.fb.group(formRule.getFormControls()),
            rules: [formRule],
        };
    }
}

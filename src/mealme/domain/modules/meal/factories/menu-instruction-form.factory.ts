import { Injectable } from '@angular/core';
import { Form, FormFactory } from '@ubud/form';
import { FormBuilder } from '@angular/forms';
import { MenuInstructionRule } from '@mealme/src/domain/modules/meal/rules/menu-instruction.rule';

@Injectable()
export class MenuInstructionFormFactory implements FormFactory {
    public constructor(private fb: FormBuilder) {
    }

    public create(): Form {
        const formRule = new MenuInstructionRule();

        return {
            formGroup: this.fb.group(formRule.getFormControls()),
            rules: [formRule],
        };
    }
}

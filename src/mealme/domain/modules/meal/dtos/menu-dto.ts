export class MenuDto {
    public title: string;
    public image: any;
    public calorieAmount: number;
    public description: string;
    public duration: number;
    public healthScore: number;
    public dietTypes: string[];
}
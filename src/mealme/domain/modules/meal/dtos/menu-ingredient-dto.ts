export class MenuIngredientDto {
    public ingredient: string;
    public name: string;
    public amount: string;
    public unit: string;
}
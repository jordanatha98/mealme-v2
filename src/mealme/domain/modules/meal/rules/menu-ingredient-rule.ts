import { ErrorMessages, Rule } from '@ubud/form';
import { ValidatorFn, Validators } from '@angular/forms';

export class MenuIngredientRule extends Rule {
    public get errorMessages(): ErrorMessages {
        return {
            ingredient: {
                required: () => 'Ingredient cannot be empty',
            },
            name: {
                required: () => 'Name cannot be empty',
            },
            amount: {
                required: () => 'Amount cannot be empty',
            },
            unit: {
                required: () => 'Unit cannot be empty',
            },
        };
    }

    public get ingredient(): ValidatorFn {
        return Validators.required;
    }

    public get name(): ValidatorFn {
        return Validators.required;
    }

    public get amount(): ValidatorFn {
        return Validators.required;
    }

    public get unit(): ValidatorFn {
        return Validators.required;
    }

    public getFormControls(): { [p: string]: any } {
        return {
            ingredient: ['', this.ingredient],
            name: ['', this.name],
            amount: ['', this.amount],
            unit: ['', this.unit],
        };
    }
}

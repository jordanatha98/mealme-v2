import { ErrorMessages, Rule } from '@ubud/form';

export class MenuNutrientRule extends Rule {
    public get errorMessages(): ErrorMessages {
        return {
        };
    }

    public getFormControls(): { [p: string]: any } {
        return {
            name: [''],
            amount: [''],
            unit: [''],
            dailyAmountPercentage: [''],
        };
    }
}

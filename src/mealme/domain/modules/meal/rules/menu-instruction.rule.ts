import { ErrorMessages, Rule } from '@ubud/form';
import { ValidatorFn, Validators } from '@angular/forms';

export class MenuInstructionRule extends Rule {
    public get errorMessages(): ErrorMessages {
        return {
            instruction: {
                required: () => 'Ingredient cannot be empty',
            },
        };
    }

    public get instruction(): ValidatorFn {
        return Validators.required;
    }

    public getFormControls(): { [p: string]: any } {
        return {
            instruction: ['', this.instruction],
        };
    }
}

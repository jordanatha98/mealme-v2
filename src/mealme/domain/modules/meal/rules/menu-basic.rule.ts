import { ErrorMessages, Rule } from '@ubud/form';
import { ValidatorFn, Validators } from '@angular/forms';

export class MenuBasicRule extends Rule {
    public get errorMessages(): ErrorMessages {
        return {
            title: {
                required: () => 'Food Name cannot be empty',
            },
            image: {
                required: () => 'Image cannot be empty',
            },
            description: {
                required: () => 'Description cannot be empty',
            },
            duration: {
                required: () => 'Duration cannot be empty',
            },
        };
    }

    public get title(): ValidatorFn {
        return Validators.required;
    }

    public get image(): ValidatorFn {
        return Validators.required;
    }

    public get description(): ValidatorFn {
        return Validators.required;
    }

    public get duration(): ValidatorFn {
        return Validators.required;
    }

    public getFormControls(): { [p: string]: any } {
        return {
            title: ['', this.title],
            image: ['', this.image],
            description: ['', this.description],
            duration: ['', this.duration],
            calorieAmount: [''],
            healthScore: [''],
            dietTypes: [[]]
        };
    }
}

import { FetchNextMeal } from '@mealme/src/domain/modules/meal/messages/commands/fetch-next-meal';
import { FetchNextMealSucceed } from '@mealme/src/domain/modules/meal/messages/events/fetch-next-meal-succeed';
import { FetchNextMealFailed } from '@mealme/src/domain/modules/meal/messages/events/fetch-next-meal-failed';
import { MealEntity } from '@mealme/src/domain/modules/meal/messages/documents/meal-entity';
import { MealsEntity } from '@mealme/src/domain/modules/meal/messages/documents/meals-entity';
import { FetchMeals } from '@mealme/src/domain/modules/meal/messages/commands/fetch-meals';
import { FetchMealsSucceed } from '@mealme/src/domain/modules/meal/messages/events/fetch-meals-succeed';
import { FetchMealsFailed } from '@mealme/src/domain/modules/meal/messages/events/fetch-meals-failed';
import { FetchPlans } from '@mealme/src/domain/modules/meal/messages/commands/fetch-plans';
import { FetchPlansFailed } from '@mealme/src/domain/modules/meal/messages/events/fetch-plans-failed';
import { FetchPlansSucceed } from '@mealme/src/domain/modules/meal/messages/events/fetch-plans-succeed';
import { PlansEntity } from '@mealme/src/domain/modules/meal/messages/documents/plans-entity';
import { FetchMenuIngredients } from '@mealme/src/domain/modules/meal/messages/commands/fetch-menu-ingredients';
import {
    FetchMenuIngredientsFailed
} from '@mealme/src/domain/modules/meal/messages/events/fetch-menu-ingredients-failed';
import {
    FetchMenuIngredientsSucceed
} from '@mealme/src/domain/modules/meal/messages/events/fetch-menu-ingredients-succeed';
import { FetchMenuInstructions } from '@mealme/src/domain/modules/meal/messages/commands/fetch-menu-instructions';
import {
    FetchMenuInstructionsFailed
} from '@mealme/src/domain/modules/meal/messages/events/fetch-menu-instructions-failed';
import { FetchMenuNutrients } from '@mealme/src/domain/modules/meal/messages/commands/fetch-menu-nutrients';
import { FetchMenuNutrientsFailed } from '@mealme/src/domain/modules/meal/messages/events/fetch-menu-nutrients-failed';
import {
    FetchMenuNutrientsSucceed
} from '@mealme/src/domain/modules/meal/messages/events/fetch-menu-nutrients-succeed';
import { FetchMenu } from '@mealme/src/domain/modules/meal/messages/commands/fetch-menu';
import { FetchMenuFailed } from '@mealme/src/domain/modules/meal/messages/events/fetch-menu-failed';
import { FetchMenuSucceed } from '@mealme/src/domain/modules/meal/messages/events/fetch-menu-succeed';
import { MenuEntity } from '@mealme/src/domain/modules/meal/messages/documents/menu-entity';
import { MenuNutrientsEntity } from '@mealme/src/domain/modules/meal/messages/documents/menu-nutrients-entity';
import { MenuIngredientsEntity } from '@mealme/src/domain/modules/meal/messages/documents/menu-ingredients-entity';
import { MenuInstructionsEntity } from '@mealme/src/domain/modules/meal/messages/documents/menu-instructions-entity';
import { GeneratePlan } from '@mealme/src/domain/modules/meal/messages/commands/generate-plan';
import { GeneratePlanSucceed } from '@mealme/src/domain/modules/meal/messages/events/generate-plan-succeed';
import { GeneratePlanFailed } from '@mealme/src/domain/modules/meal/messages/events/generate-plan-failed';
import { FetchBookmarks } from '@mealme/src/domain/modules/meal/messages/commands/fetch-bookmarks';
import { FetchBookmarksFailed } from '@mealme/src/domain/modules/meal/messages/events/fetch-bookmarks-failed';
import { FetchBookmarksSucceed } from '@mealme/src/domain/modules/meal/messages/events/fetch-bookmarks-succeed';
import { CreateBookmark } from '@mealme/src/domain/modules/meal/messages/commands/create-bookmark';
import { CreateBookmarkSucceed } from '@mealme/src/domain/modules/meal/messages/events/create-bookmark-succeed';
import { CreateBookmarkFailed } from '@mealme/src/domain/modules/meal/messages/events/create-bookmark-failed';
import { DeleteBookmark } from '@mealme/src/domain/modules/meal/messages/commands/delete-bookmark';
import { DeleteBookmarkSucceed } from '@mealme/src/domain/modules/meal/messages/events/delete-bookmark-succeed';
import { DeleteBookmarkFailed } from '@mealme/src/domain/modules/meal/messages/events/delete-bookmark-failed';
import { BookmarksEntity } from '@mealme/src/domain/modules/meal/messages/documents/bookmarks-entity';
import { CreateMenu } from '@mealme/src/domain/modules/meal/messages/commands/menu/create-menu';
import { CreateMenuSucceed } from '@mealme/src/domain/modules/meal/messages/events/menu/create-menu-succeed';
import { CreateMenuFailed } from '@mealme/src/domain/modules/meal/messages/events/menu/create-menu-failed';
import { CreateMenuIngredientSucceed } from '@mealme/src/domain/modules/meal/messages/events/menu/create-menu-ingredient-succeed';
import { CreateMenuIngredient } from '@mealme/src/domain/modules/meal/messages/commands/menu/create-menu-ingredient';
import { CreateMenuIngredientFailed } from '@mealme/src/domain/modules/meal/messages/events/menu/create-menu-ingredient-failed';
import { CreateMenuInstruction } from '@mealme/src/domain/modules/meal/messages/commands/menu/create-menu-instruction';
import { CreateMenuInstructionSucceed } from '@mealme/src/domain/modules/meal/messages/events/menu/create-menu-instruction-succeed';
import { CreateMenuInstructionFailed } from '@mealme/src/domain/modules/meal/messages/events/menu/create-menu-instruction-failed';
import { CreateMenuNutrient } from '@mealme/src/domain/modules/meal/messages/commands/menu/create-menu-nutrient';
import { CreateMenuNutrientSucceed } from '@mealme/src/domain/modules/meal/messages/events/menu/create-menu-nutrient-succeed';
import { CreateMenuNutrientFailed } from '@mealme/src/domain/modules/meal/messages/events/menu/create-menu-nutrient-failed';

export const MEALME_MEAL_MESSAGES: any[] = [
    FetchNextMeal,
    FetchNextMealSucceed,
    FetchNextMealFailed,
    MealEntity,
    MealsEntity,
    FetchMeals,
    FetchMealsSucceed,
    FetchMealsFailed,
    FetchPlans,
    FetchPlansFailed,
    FetchPlansSucceed,
    PlansEntity,

    FetchMenu,
    FetchMenuFailed,
    FetchMenuSucceed,
    FetchMenuIngredients,
    FetchMenuIngredientsFailed,
    FetchMenuIngredientsSucceed,
    FetchMenuInstructions,
    FetchMenuInstructionsFailed,
    FetchMenuInstructions,
    FetchMenuNutrients,
    FetchMenuNutrientsFailed,
    FetchMenuNutrientsSucceed,

    MenuEntity,
    MenuNutrientsEntity,
    MenuIngredientsEntity,
    MenuInstructionsEntity,

    GeneratePlan,
    GeneratePlanSucceed,
    GeneratePlanFailed,

    FetchBookmarks,
    FetchBookmarksFailed,
    FetchBookmarksSucceed,

    CreateBookmark,
    CreateBookmarkSucceed,
    CreateBookmarkFailed,

    DeleteBookmark,
    DeleteBookmarkSucceed,
    DeleteBookmarkFailed,

    BookmarksEntity,

    CreateMenu,
    CreateMenuSucceed,
    CreateMenuFailed,

    CreateMenuIngredient,
    CreateMenuIngredientSucceed,
    CreateMenuIngredientFailed,

    CreateMenuInstruction,
    CreateMenuInstructionSucceed,
    CreateMenuInstructionFailed,

    CreateMenuNutrient,
    CreateMenuNutrientSucceed,
    CreateMenuNutrientFailed,

];

import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';
import { Collection } from '@shared/types/collection';
import { Plan } from '@mealme/src/domain/modules/plan/models/plan';

@UbudMessage('Mealme.Menu.PlansEntity')
export class PlansEntity extends Message<MealmeState> {
    public plans: Collection<Plan>;
    public yesterday?: boolean;
    public home?: boolean;

    public handle(state: MealmeState): MealmeState {
        if (this.yesterday) {
            return {
                ...state,
                meal: {
                    ...state.meal,
                    yesterdayPlans: this.plans,
                },
            };
        }
        if (this.home) {
            return {
                ...state,
                meal: {
                    ...state.meal,
                    homePlans: this.plans,
                },
            };
        }
        return {
            ...state,
            meal: {
                ...state.meal,
                plans: this.plans,
            },
        };
    }
}

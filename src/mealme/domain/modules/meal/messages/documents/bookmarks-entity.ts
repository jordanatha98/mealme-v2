import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';
import { Collection } from '@shared/types/collection';
import { Bookmark } from '@mealme/src/domain/modules/meal/models/bookmark';

@UbudMessage('Mealme.Menu.BookmarksEntity')
export class BookmarksEntity extends Message<MealmeState> {
    public bookmarks: Collection<Bookmark>;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
            meal: {
                ...state.meal,
                bookmarks: this.bookmarks,
            },
        };
    }
}

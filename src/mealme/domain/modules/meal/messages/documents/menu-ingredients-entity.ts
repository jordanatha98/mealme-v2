import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';
import { MenuIngredient } from '@mealme/src/domain/modules/meal/models/menu-ingredient';

@UbudMessage('Mealme.Menu.MenuIngredientsEntity')
export class MenuIngredientsEntity extends Message<MealmeState> {
    public menuIngredients: MenuIngredient[];

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
            meal: {
                ...state.meal,
                menuIngredients: this.menuIngredients,
            },
        };
    }
}

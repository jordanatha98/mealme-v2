import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';
import { MenuInstruction } from '@mealme/src/domain/modules/meal/models/menu-instruction';

@UbudMessage('Mealme.Menu.MenuInstructionsEntity')
export class MenuInstructionsEntity extends Message<MealmeState> {
    public menuInstructions: MenuInstruction[];

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
            meal: {
                ...state.meal,
                menuInstructions: this.menuInstructions,
            },
        };
    }
}

import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';
import { MenuNutrient } from '@mealme/src/domain/modules/meal/models/menu-nutrient';

@UbudMessage('Mealme.Menu.MenuNutrientsEntity')
export class MenuNutrientsEntity extends Message<MealmeState> {
    public menuNutrients: MenuNutrient[];

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
            meal: {
                ...state.meal,
                menuNutrients: this.menuNutrients,
            },
        };
    }
}

import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';
import { Menu } from '@mealme/src/domain/modules/meal/models/menu';

@UbudMessage('Mealme.Menu.MenuEntity')
export class MenuEntity extends Message<MealmeState> {
    public menu: Menu;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
            meal: {
                ...state.meal,
                menu: this.menu,
            },
        };
    }
}

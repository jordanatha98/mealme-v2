import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';
import { Menu } from '@mealme/src/domain/modules/meal/models/menu';

@UbudMessage('Mealme.Menu.MealEntity')
export class MealEntity extends Message<MealmeState> {
    public meal: Menu;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
            meal: {
                ...state.meal,
                meal: this.meal,
            },
        };
    }
}

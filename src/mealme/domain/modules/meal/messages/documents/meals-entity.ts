import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';
import { Menu } from '@mealme/src/domain/modules/meal/models/menu';

@UbudMessage('Mealme.Menu.MealsEntity')
export class MealsEntity extends Message<MealmeState> {
    public meals: Menu[];

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
            meal: {
                ...state.meal,
                meals: this.meals,
            },
        };
    }
}

import { UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '../../../../stores/states';
import { Failed } from '@shared/messages/events/failed';

@UbudMessage('Mealme.Menu.FetchMealsFailed')
export class FetchMealsFailed extends Failed<MealmeState> {
}

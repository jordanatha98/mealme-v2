import { UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '../../../../stores/states';
import { Failed } from '@shared/messages/events/failed';

@UbudMessage('Mealme.Menu.FetchNextMealFailed')
export class FetchNextMealFailed extends Failed<MealmeState> {
}

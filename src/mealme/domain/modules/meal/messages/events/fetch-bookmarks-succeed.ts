import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';

@UbudMessage('Mealme.Menu.FetchBookmarksSucceed')
export class FetchBookmarksSucceed extends Message<MealmeState> {
    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

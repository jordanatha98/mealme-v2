import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';

@UbudMessage('Mealme.Menu.FetchMenuSucceed')
export class FetchMenuSucceed extends Message<MealmeState> {
    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

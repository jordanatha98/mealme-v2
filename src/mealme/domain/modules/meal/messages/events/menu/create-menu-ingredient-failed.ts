import { UbudMessage } from '@ubud/ngrx';
import { Failed } from '@shared/messages/events/failed';
import { MealmeState } from '@mealme/src/domain/stores/states';

@UbudMessage('Mealme.Meal.Menu.CreateMenuIngredientFailed')
export class CreateMenuIngredientFailed extends Failed<MealmeState> {
}

import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';
import { CustomerMenu } from '@mealme/src/domain/modules/meal/models/customer-menu';

@UbudMessage('Mealme.Meal.Menu.CreateMenuSucceed')
export class CreateMenuSucceed extends Message<MealmeState> {
    public response?: CustomerMenu;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';

@UbudMessage('Mealme.Menu.FetchBookmarks')
export class FetchBookmarks extends Message<MealmeState> {
    public customer: string;
    public queries?: any;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';

@UbudMessage('Mealme.Menu.FetchNextMeal')
export class FetchNextMeal extends Message<MealmeState> {
    public queries?: any;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

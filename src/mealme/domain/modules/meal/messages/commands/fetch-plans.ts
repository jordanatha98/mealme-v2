import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';

@UbudMessage('Mealme.Menu.FetchPlans')
export class FetchPlans extends Message<MealmeState> {
    public customer: string;
    public queries?: any;
    public yesterday?: boolean;
    public home?: boolean;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

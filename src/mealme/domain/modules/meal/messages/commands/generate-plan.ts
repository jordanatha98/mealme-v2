import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';
import { GeneratePlanDto } from '@mealme/src/domain/modules/meal/dtos/generate-plan-dto';

@UbudMessage('Mealme.Menu.GeneratePlan')
export class GeneratePlan extends Message<MealmeState> {
    public customer: string
    public payload: GeneratePlanDto;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

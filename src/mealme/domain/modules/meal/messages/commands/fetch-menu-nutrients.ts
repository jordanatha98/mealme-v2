import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';

@UbudMessage('Mealme.Menu.FetchMenuNutrients')
export class FetchMenuNutrients extends Message<MealmeState> {
    public menu: string;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

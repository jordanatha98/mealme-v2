import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';

@UbudMessage('Mealme.Menu.FetchMenu')
export class FetchMenu extends Message<MealmeState> {
    public menu: any;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

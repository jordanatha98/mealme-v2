import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';

@UbudMessage('Mealme.Menu.DeleteBookmark')
export class DeleteBookmark extends Message<MealmeState> {
    public bookmark: string;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

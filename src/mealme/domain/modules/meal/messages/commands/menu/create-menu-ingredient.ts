import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';
import { MenuIngredientDto } from '@mealme/src/domain/modules/meal/dtos/menu-ingredient-dto';

@UbudMessage('Mealme.Meal.Menu.CreateMenuIngredient')
export class CreateMenuIngredient extends Message<MealmeState> {
    public menu: string;
    public payload: MenuIngredientDto

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';
import { MenuDto } from '@mealme/src/domain/modules/meal/dtos/menu-dto';

@UbudMessage('Mealme.Meal.Menu.CreateMenu')
export class CreateMenu extends Message<MealmeState> {
    public customer: string;
    public payload: MenuDto;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

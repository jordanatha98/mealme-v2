import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';
import { MenuNutrientDto } from '@mealme/src/domain/modules/meal/dtos/menu-nutrient-dto';

@UbudMessage('Mealme.Meal.Menu.CreateMenuNutrient')
export class CreateMenuNutrient extends Message<MealmeState> {
    public menu: string;
    public payload: MenuNutrientDto

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

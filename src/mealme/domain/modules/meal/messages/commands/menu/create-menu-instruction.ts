import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';
import { MenuInstructionDto } from '@mealme/src/domain/modules/meal/dtos/menu-instruction-dto';

@UbudMessage('Mealme.Meal.Menu.CreateMenuInstruction')
export class CreateMenuInstruction extends Message<MealmeState> {
    public menu: string;
    public payload: MenuInstructionDto

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

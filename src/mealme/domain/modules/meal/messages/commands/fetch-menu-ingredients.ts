import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';

@UbudMessage('Mealme.Menu.FetchMenuIngredients')
export class FetchMenuIngredients extends Message<MealmeState> {
    public menu: string;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

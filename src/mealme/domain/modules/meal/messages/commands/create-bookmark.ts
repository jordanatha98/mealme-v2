import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';

@UbudMessage('Mealme.Menu.CreateBookmark')
export class CreateBookmark extends Message<MealmeState> {
    public customer: string;
    public menu: string;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

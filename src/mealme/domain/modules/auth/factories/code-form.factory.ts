import { Injectable } from '@angular/core';
import { Form, FormFactory } from '@ubud/form';
import { FormBuilder } from '@angular/forms';
import { CodeRule } from '@mealme/src/domain/modules/auth/rules/code.rule';

@Injectable()
export class CodeFormFactory implements FormFactory {
    public constructor(private fb: FormBuilder) {
    }

    public create(): Form {
        const formRule = new CodeRule();

        return {
            formGroup: this.fb.group(formRule.getFormControls()),
            rules: [formRule],
        };
    }
}

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { AuthRepository } from '@mealme/src/domain/modules/auth/repositories/auth.repository';

@Injectable()
export class GuestGuard implements CanActivate, CanActivateChild {
    public constructor(private authRepo: AuthRepository, private router: Router) {
    }

    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.authRepo.getSignature$().pipe(
            first(),
            map(signature => {
                if (signature) {
                    const { prefix } = route.data;
                    this.router.navigateByUrl(prefix.tabs + '/account').then();

                    return false;
                }

                return !signature;
            }),
        );
    }

    public canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.canActivate(childRoute, state);
    }
}

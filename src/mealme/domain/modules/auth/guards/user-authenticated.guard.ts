import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, first, map, mergeMap } from 'rxjs/operators';
import { UserService } from '../services/user.service';
import { HttpErrorResponse } from '@angular/common/http';
import { MealmeStore } from '@mealme/src/domain/stores/mealme.store';
import { AuthenticatedUserEntity } from '@mealme/src/domain/modules/auth/messages/entities/user/authenticated-user-entity';
import { AuthRepository } from '@mealme/src/domain/modules/auth/repositories/auth.repository';

@Injectable()
export class UserAuthenticatedGuard implements CanActivate, CanActivateChild {
    public constructor(private store: MealmeStore, private service: UserService, private repo: AuthRepository, private router: Router) {
    }

    public canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
        const url = window.location.href;

        return this.store.select(state => state.user.authenticated).pipe(
            first(),
            mergeMap(user => {
                if (user) {
                    return of(user);
                }

                return this.store.select(state => state.auth.signature).pipe(
                    first(),
                    mergeMap(signature => {
                        if (!signature) {
                            // TODO: Refresh user (logout first)
                            this.router.navigateByUrl('app/auth-blank/auth/landing').then();

                            return of(null);
                        }

                        return this.service.getAuthenticatedUser().pipe(
                            map(response => {
                                this.store.dispatch(new AuthenticatedUserEntity({ authenticated: response }));
                                return response;
                            }),
                            catchError((e: HttpErrorResponse) => {
                                if (401 === e.status) {
                                    // TODO: Refresh user (logout first)
                                    // window.location.href = `/account/auth/log-out?redirect_url=${encodeURIComponent(url)}`;

                                    return of(null);
                                }

                                return of(null);
                            }),
                        )
                    }),
                );
            }),
            map(user => {
                return !!user;
            }),
        );
    }

    public canActivateChild(childRoute: ActivatedRouteSnapshot): Observable<boolean> {
        return this.canActivate(childRoute);
    }
}

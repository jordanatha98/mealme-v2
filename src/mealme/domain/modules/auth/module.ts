import { NgModule } from '@angular/core';
import { MealmeDomainMealmeStoreModule } from '../../stores/module';
import { EffectsModule } from '@ngrx/effects';
import { AuthService } from '@mealme/src/domain/modules/auth/services/auth.service';
import { UserService } from '@mealme/src/domain/modules/auth/services/user.service';
import { UserEffect } from '@mealme/src/domain/modules/auth/effects/user.effect';
import { AuthEffect } from '@mealme/src/domain/modules/auth/effects/auth.effect';
import { GuestGuard } from '@mealme/src/domain/modules/auth/guards/guest.guard';
import { UserAuthenticatedGuard } from '@mealme/src/domain/modules/auth/guards/user-authenticated.guard';

@NgModule({
    imports: [
        MealmeDomainMealmeStoreModule,
        EffectsModule.forFeature([
            UserEffect,
            AuthEffect,
        ]),
    ],
    providers: [
        AuthService,
        UserService,
        GuestGuard,
        UserAuthenticatedGuard,
    ]
})
export class MealmeDomainAuthModule {}

import { Injectable } from '@angular/core';
import { Effects, Message, ubudType } from '@ubud/ngrx';
import { Actions, Effect } from '@ngrx/effects';
import { MealmeStore } from '@mealme/src/domain/stores/mealme.store';
import { Observable, of } from 'rxjs';
import { catchError, filter, map, mergeMap, take } from 'rxjs/operators';
import { MessageUtil } from '@shared/utils/message.util';
import { HttpErrorResponse } from '@angular/common/http';
import { UserService } from '@mealme/src/domain/modules/auth/services/user.service';
import { CreateUser } from '@mealme/src/domain/modules/auth/messages/commands/user/create-user';
import { CreateUserSucceed } from '@mealme/src/domain/modules/auth/messages/events/user/create-user-succeed';
import { CreateUserFailed } from '@mealme/src/domain/modules/auth/messages/events/user/create-user-failed';
import { User } from '@mealme/src/domain/modules/auth/models/user';
import { UserEntity } from '@mealme/src/domain/modules/auth/messages/entities/user/user-entity';
import { VerifyUser } from '@mealme/src/domain/modules/auth/messages/commands/user/verify-user';
import { VerifyUserSucceed } from '@mealme/src/domain/modules/auth/messages/events/user/verify-user-succeed';
import { VerifyUserFailed } from '@mealme/src/domain/modules/auth/messages/events/user/verify-user-failed';
import { CreateCustomer } from '@mealme/src/domain/modules/auth/messages/commands/user/create-customer';
import { Customer } from '@mealme/src/domain/modules/customer/models/customer';
import { CreateCustomerSucceed } from '@mealme/src/domain/modules/auth/messages/events/user/create-customer-succeed';
import { CreateCustomerFailed } from '@mealme/src/domain/modules/auth/messages/events/user/create-customer-failed';
import { FetchAuthenticatedUser } from '@mealme/src/domain/modules/auth/messages/commands/user/fetch-authenticated-user';
import { FetchAuthenticatedUserFailed } from '@mealme/src/domain/modules/auth/messages/events/user/fetch-authenticated-user-failed';
import { FetchAuthenticatedUserSucceed } from '@mealme/src/domain/modules/auth/messages/events/user/fetch-authenticated-user-succeed';
import { AuthenticatedUserEntity } from '@mealme/src/domain/modules/auth/messages/entities/user/authenticated-user-entity';

@Injectable()
export class UserEffect extends Effects {
    public constructor(actions$: Actions, private service: UserService, private store: MealmeStore) {
        super(actions$);
    }

    @Effect()
    public createUser$: Observable<Message> = this.actions$.pipe(
        ubudType(CreateUser),
        map(message => MessageUtil.getPayload<CreateUser>(message)),
        mergeMap(({ payload }) => {
            return this.service.createUser(payload).pipe(
                mergeMap(response => {
                    if (response instanceof User) {
                        return of(
                            new CreateUserSucceed(),
                            new UserEntity({
                                user: response,
                            }),
                        );
                    }

                    return of(
                        new CreateUserFailed({
                            exception: new HttpErrorResponse({
                                status: 400,
                                error: $localize`There was an error processing`,
                            }),
                        })
                    );
                }),
                catchError((e: HttpErrorResponse) => {
                    return of(new CreateUserFailed({ exception: e }));
                }),
            );
        }),
    );

    @Effect()
    public verifyUser$: Observable<Message> = this.actions$.pipe(
        ubudType(VerifyUser),
        map(message => MessageUtil.getPayload<VerifyUser>(message)),
        mergeMap(({ user, payload }) => {
            return this.service.verifyUser(user, payload).pipe(
                mergeMap(response => {
                    if (response instanceof User) {
                        return of(
                            new  VerifyUserSucceed(),
                            new UserEntity({
                                user: response,
                            }),
                        );
                    }

                    return of(
                        new VerifyUserFailed({
                            exception: new HttpErrorResponse({
                                status: 400,
                                error: $localize`There was an error processing`,
                            }),
                        })
                    );
                }),
                catchError((e: HttpErrorResponse) => {
                    return of(new VerifyUserFailed({ exception: e }));
                }),
            );
        }),
    );

    @Effect()
    public createCustomer: Observable<Message> = this.actions$.pipe(
        ubudType(CreateCustomer),
        map(message => MessageUtil.getPayload<CreateCustomer>(message)),
        mergeMap(({ user, payload }) => {
            return this.service.createCustomer(user, payload).pipe(
                mergeMap(response => {
                    if (response instanceof Customer) {
                        return of(
                            new CreateCustomerSucceed(),
                        );
                    }

                    return of(
                        new CreateCustomerFailed({
                            exception: new HttpErrorResponse({
                                status: 400,
                                error: $localize`There was an error processing`,
                            }),
                        })
                    );
                }),
                catchError((e: HttpErrorResponse) => {
                    return of(new CreateCustomerFailed({ exception: e }));
                }),
            );
        }),
    );

    @Effect()
    public fetchAuthenticatedUser$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchAuthenticatedUser),
        map(message => MessageUtil.getPayload<FetchAuthenticatedUser>(message)),
        mergeMap(({ cacheable }) => {
            const obs = this.service.getAuthenticatedUser().pipe(
                mergeMap(response => {
                    if (response instanceof User) {
                        return [new FetchAuthenticatedUserSucceed({ user: response }), new AuthenticatedUserEntity({ authenticated: response })];
                    }

                    return of(new FetchAuthenticatedUserFailed({
                        exception: new HttpErrorResponse({
                            error: 'Something error',
                            status: 400
                        })
                    }));
                }),
                catchError((e: HttpErrorResponse) => of(new FetchAuthenticatedUserFailed({ exception: e }))),
            );

            if (cacheable) {
                return this.store.select(state => state.user).pipe(
                    filter(user => !!user),
                    take(1),
                    mergeMap(({ authenticated }) => {
                        if (authenticated) {
                            return [
                                new FetchAuthenticatedUserSucceed({ user: authenticated }),
                                new AuthenticatedUserEntity({ user: authenticated }),
                            ];
                        }

                        return obs;
                    }),
                );
            }

            return obs;
        }),
    );
}

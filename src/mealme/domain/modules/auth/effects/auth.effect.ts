import { Injectable } from '@angular/core';
import { Effects, Message, ubudType } from '@ubud/ngrx';
import { Actions, Effect } from '@ngrx/effects';
import { AuthService } from '@mealme/src/domain/modules/auth/services/auth.service';
import { Storage } from '@ionic/storage';
import { Observable, of } from 'rxjs';
import { Authenticate } from '@mealme/src/domain/modules/auth/messages/commands/auth/authenticate';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { MessageUtil } from '@shared/utils/message.util';
import { Signature } from '@mealme/src/domain/modules/auth/models/signature';
import { AuthenticateSucceed } from '@mealme/src/domain/modules/auth/messages/events/auth/authenticate-succeed';
import { AuthenticateFailed } from '@mealme/src/domain/modules/auth/messages/events/auth/authenticate-failed';
import { HttpErrorResponse } from '@angular/common/http';
import { SignOut } from '@mealme/src/domain/modules/auth/messages/commands/auth/sign-out';
import { AuthStorage } from '@mealme/src/domain/modules/auth/enums/auth-storage';
import { SignOutSucceed } from '@mealme/src/domain/modules/auth/messages/events/auth/sign-out-succeed';

@Injectable()
export class AuthEffect extends Effects {
    public constructor(actions$: Actions, private service: AuthService, private storage: Storage) {
        super(actions$);
    }

    @Effect()
    public authenticate$: Observable<Message> = this.actions$.pipe(
        ubudType(Authenticate),
        map(message => MessageUtil.getPayload<Authenticate>(message)),
        mergeMap(({payload}: Authenticate) => {
            return this.service.authenticate(payload).pipe(
                map(response => {
                    if (response instanceof Signature) {
                        this.service.setSignature(response);
                        this.service.setRefreshToken(response);

                        return new AuthenticateSucceed({signature: response});
                    }

                    return new AuthenticateFailed({
                        exception: new HttpErrorResponse({
                            error: 'Something error',
                            status: 400
                        })
                    });
                }),
                catchError((e: HttpErrorResponse) => of(new AuthenticateFailed({exception: e}))),
            );
        }),
    );

    @Effect()
    public signOut$: Observable<Message> = this.actions$.pipe(
        ubudType(SignOut),
        mergeMap(() => {
            return Promise.all([
                this.storage.remove(AuthStorage.SIGNATURE),
                this.storage.remove(AuthStorage.REFRESH_TOKEN),
            ]).then(() => {
                this.service.setSignature(null);
                this.service.setRefreshToken(null);

                return new SignOutSucceed();
            })
        })
    )

}

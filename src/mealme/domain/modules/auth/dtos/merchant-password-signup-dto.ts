export class MerchantPasswordSignupDto {
    public password: string;
    public passwordConfirmation: string;
    public receiveOffer: number;
    public token: string;
}

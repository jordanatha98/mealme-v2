export class MerchantSignupDto {
    public email: string;
    public firstName: string;
    public lastName: string;
    public phoneNumber: string;
    public yearOfBirth: number;
    public companyName: string;
    public companyAddress: string;
    public companyDescription: string;
    public position: string;
    public additionalInformation: string;
    public password: string;
    public passwordConfirmation: string;
}

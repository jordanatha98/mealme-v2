export class UserDto {
    public email: string;
    public firstName: string;
    public lastName: string;
    public phoneNumber: string;
    public yearOfBirth: number;
}

export class MerchantCompanySignupDto {
    public companyName: string;
    public companyAddress: string;
    public companyDescription: string;
    public position: string;
    public additionalInformation: string;
}

export class MerchantBasicSignupDto {
    public email: string;
    public firstName: string;
    public lastName: string;
    public phoneNumber: string;
    public yearOfBirth: string;
}

import { ErrorMessages, Rule } from '@ubud/form';
import { ValidatorFn, Validators } from '@angular/forms';

export class CodeRule extends Rule {
    public get errorMessages(): ErrorMessages {
        return {
            otp: {
                required: () => 'Verification code cannot be empty',
            },
        };
    }

    public get otp(): ValidatorFn {
        return Validators.required;
    }

    public getFormControls(): { [p: string]: any } {
        return {
            otp: ['', this.otp],
            token: ['']
        };
    }
}

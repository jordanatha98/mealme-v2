import { ErrorMessages, Rule } from '@ubud/form';
import { ValidatorFn, Validators } from '@angular/forms';
import { ValidatorUtil } from '@shared/utils/validator.util';

export class PasswordSignupRule extends Rule {
    public get errorMessages(): ErrorMessages {
        return {
            password: {
                required: () => 'Password cannot be empty',
            },
            passwordConfirmation: {
                required: () => 'Password Confirmation cannot be empty',
                match: () => 'The password confirmation does not match the password',
            },
        };
    }

    public get password(): ValidatorFn {
        return Validators.required;
    }

    public get passwordConfirmation(): ValidatorFn[] {
        return [Validators.required, ValidatorUtil.matchValidator('password')];
    }

    public getFormControls(): { [p: string]: any } {
        return {
            passwordConfirmation: ['', this.passwordConfirmation],
            password: ['', this.password],
            receiveOffer: [''],
            token: ['']
        };
    }
}

import { ErrorMessages, Rule } from '@ubud/form';
import { ValidatorFn, Validators } from '@angular/forms';

export class BasicSignupRule extends Rule {
    public get errorMessages(): ErrorMessages {
        return {
            email: {
                required: () => 'Email cannot be empty',
            },
            firstName: {
                required: () => 'First Name cannot be empty',
            },
            lastName: {
                required: () => 'Last Name cannot be empty',
            },
            phone: {
                required: () => 'Phone Number cannot be empty',
            },
            yearOfBirth: {
                required: () => 'Year of Birth cannot be empty',
            },
        };
    }

    public get email(): ValidatorFn {
        return Validators.required;
    }

    public get firstName(): ValidatorFn {
        return Validators.required;
    }

    public get lastName(): ValidatorFn {
        return Validators.required;
    }

    public get phone(): ValidatorFn {
        return Validators.required;
    }

    public get yearOfBirth(): ValidatorFn {
        return Validators.required;
    }

    public getFormControls(): { [p: string]: any } {
        return {
            email: ['', this.email],
            firstName: ['', this.firstName],
            lastName: ['', this.lastName],
            phone: ['', this.phone],
            yearOfBirth: ['', this.yearOfBirth],
        };
    }
}

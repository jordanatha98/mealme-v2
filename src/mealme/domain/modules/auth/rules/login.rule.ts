import { ErrorMessages, Rule } from '@ubud/form';
import { ValidatorFn, Validators } from '@angular/forms';

export class LoginRule extends Rule {
    public get errorMessages(): ErrorMessages {
        return {
            username: {
                required: () => 'Email cannot be empty',
            },
            password: {
                required: () => 'Password cannot be empty',
            },
        };
    }

    public get username(): ValidatorFn {
        return Validators.required;
    }

    public get password(): ValidatorFn {
        return Validators.required;
    }

    public getFormControls(): { [p: string]: any } {
        return {
            username: ['', this.username],
            password: ['', this.password],
        };
    }
}

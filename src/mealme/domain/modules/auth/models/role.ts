import { UUIDModel } from '@shared/models/uuid-model';

export class Role extends UUIDModel<Role> {
    public name: string;
    public label: string;
}

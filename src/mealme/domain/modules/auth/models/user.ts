import { UUIDModel } from '@shared/models/uuid-model';
import { Role } from '@mealme/src/domain/modules/auth/models/role';
import { Type } from 'class-transformer';
import { forwardRef } from '@angular/core';
import { Customer } from '@mealme/src/domain/modules/customer/models/customer';

export class User extends UUIDModel<User> {
    public name: string;
    @Type(forwardRef(() => Customer) as any)
    public user: Customer;
    public status: string;
    public email: string;
    public phone: string;
    public firstName: string;
    public lastName: string;
    public verified: boolean;
    public yearOfBirth: number;
}

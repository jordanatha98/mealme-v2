export class Signature {
    public type: string;
    public token: string;
    public expiresIn: number;
    public tokenType?: string;
}

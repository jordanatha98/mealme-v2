import { Injectable } from '@angular/core';
import { MealmeApiClient } from '@mealme/src/api/clients/api.client';
import { Observable } from 'rxjs';
import { User } from '@mealme/src/domain/modules/auth/models/user';
import { CodeDto } from '@mealme/src/domain/modules/auth/dtos/code-dto';
import { MerchantPasswordSignupDto } from '@mealme/src/domain/modules/auth/dtos/merchant-password-signup-dto';
import { Customer } from '@mealme/src/domain/modules/customer/models/customer';
import { mapToClass, mapToData } from '@shared/transformers/responses.transformer';

@Injectable()
export class UserService {
    public constructor(private client: MealmeApiClient) {
    }

    public createUser(payload: any): Observable<any> {
        return this.client.post(`users/register`, payload).pipe(
            mapToData(),
            mapToClass(User),
        );
    }

    public verifyUser(user: string, payload: CodeDto): Observable<User> {
        return this.client.patch(`users/${user}/verify`, payload).pipe(
            mapToData(),
            mapToClass(User),
        );
    }

    public createCustomer(user: string, payload: MerchantPasswordSignupDto): Observable<Customer> {
        return this.client.post(`users/${user}/customer`, payload).pipe(
            mapToData(),
            mapToClass(Customer),
        );
    }

    public getAuthenticatedUser(): Observable<User> {
        return this.client.get(`auth/me`).pipe(
            mapToData(),
            mapToClass(User),
        );
    }
}

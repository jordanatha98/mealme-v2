import { Injectable } from '@angular/core';
import { MealmeApiClient } from '@mealme/src/api/clients/api.client';
import { Storage } from '@ionic/storage';
import { Signature } from '@mealme/src/domain/modules/auth/models/signature'
import { MealmeStore } from '@mealme/src/domain/stores/mealme.store';
import { AuthStorage } from '@mealme/src/domain/modules/auth/enums/auth-storage';
import { SignatureEntity } from '@mealme/src/domain/modules/auth/messages/entities/auth/signature-entity';
import { RefreshTokenEntity } from '@mealme/src/domain/modules/auth/messages/entities/auth/refresh-token-entity';
import { LoginDto } from '@mealme/src/domain/modules/auth/dtos/login-dto';
import { Observable, of } from 'rxjs';
import { mapToClass, mapToData } from '@shared/transformers/responses.transformer';

@Injectable()
export class AuthService {
    public signature: Signature | null = null;
    public refreshToken: Signature | null = null;

    public constructor(
        private client: MealmeApiClient,
        private storage: Storage,
        private store: MealmeStore,
    ) {
    }

    public getSignature(): Promise<Signature | null> {
        if (this.signature) {
            return Promise.resolve(this.signature);
        }

        return this.storage.get(AuthStorage.SIGNATURE);
    }

    public getRefreshToken(): Promise<Signature | null> {
        if (null !== this.refreshToken) {
            return Promise.resolve(this.refreshToken);
        }

        return this.storage.get(AuthStorage.REFRESH_TOKEN);
    }

    public setSignature(signature: Signature | null): void {
        this.storage.set('id', 123123).then();
        this.signature = signature;
        this.store.dispatch(new SignatureEntity({ signature }));
        this.storage.set(AuthStorage.SIGNATURE, signature).then();
    }

    public setRefreshToken(refreshToken: Signature): void {
        this.refreshToken = refreshToken;
        this.store.dispatch(new RefreshTokenEntity({ refreshToken }));
        this.storage.set(AuthStorage.REFRESH_TOKEN, refreshToken).then();
    }

    public authenticate(payload: LoginDto): Observable<Signature> {
        return this.client.post(`auth`, payload).pipe(
            mapToData(),
            mapToClass(Signature),
        );
    }

    public refreshUser(): Observable<void> {
        // TODO: Refresh user when token expired
        return of(null);
    }
}

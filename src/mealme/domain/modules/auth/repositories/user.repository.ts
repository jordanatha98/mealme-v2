import { Injectable } from '@angular/core';
import { Repository } from '@ubud/ngrx';
import { MealmeState } from '../../../stores/states';
import { Observable } from 'rxjs';
import { MealmeStore } from '@mealme/src/domain/stores/mealme.store';
import { User } from '@mealme/src/domain/modules/auth/models/user';

@Injectable()
export class UserRepository extends Repository<MealmeState> {
    public constructor(store: MealmeStore) {
        super(store);
    }

    public getUser$(): Observable<any> {
        return this.select(state => state.user.user);
    }

    public getAuthenticatedUser$(): Observable<User> {
        return this.select(state => state.user.authenticated);
    }
}

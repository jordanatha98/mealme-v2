import { Injectable } from '@angular/core';
import { Repository } from '@ubud/ngrx';
import { Observable } from 'rxjs';
import { FormState } from '@ubud/form';
import { LoginDto } from '../dtos/login-dto';
import { Signature } from '../models/signature';
import { MealmeState } from '@mealme/src/domain/stores/states';
import { MealmeStore } from '@mealme/src/domain/stores/mealme.store';

@Injectable()
export class AuthRepository extends Repository<MealmeState> {
    public constructor(store: MealmeStore) {
        super(store);
    }

    public getLoginFormState$(): Observable<FormState<LoginDto>> {
        return this.select(state => state.auth.login.formState);
    }

    public getSignature$(): Observable<Signature> {
        return this.select(state => state.auth.signature);
    }
}

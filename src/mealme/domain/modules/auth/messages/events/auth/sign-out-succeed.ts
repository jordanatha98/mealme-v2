import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';

@UbudMessage('Auth.Auth.SignOutSucceed')
export class SignOutSucceed extends Message<MealmeState> {
    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
            auth: {
                ...state.auth,
                signature: null,
                refreshToken: null
            },
            user: {
                ...state.user,
                authenticated: null,
                user: null,
            }
        };
    }
}

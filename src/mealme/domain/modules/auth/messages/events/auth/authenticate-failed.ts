import { UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';
import { Failed } from '@shared/messages/events/failed';

@UbudMessage('Auth.Auth.AuthenticateFailed')
export class AuthenticateFailed extends Failed<MealmeState> {
    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
            auth: {
                ...state.auth,
                login: {
                    ...state.auth.login,
                    formState: {
                        ...state.auth.login.formState,
                        pristine: false,
                        disabled: false,
                    },
                },
            },
        };
    }
}

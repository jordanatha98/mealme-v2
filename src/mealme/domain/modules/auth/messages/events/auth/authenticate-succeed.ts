import { Message, UbudMessage } from '@ubud/ngrx';
import { Signature } from '../../../models/signature';
import { MealmeState } from '@mealme/src/domain/stores/states';

@UbudMessage('Auth.Auth.AuthenticateSucceed')
export class AuthenticateSucceed extends Message<MealmeState> {
    public signature: Signature;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
            auth: {
                ...state.auth,
                login: {
                    ...state.auth.login,
                    formState: {
                        ...state.auth.login.formState,
                        pristine: false,
                        disabled: false,
                    },
                },
                signature: this.signature,
            },
        };
    }
}

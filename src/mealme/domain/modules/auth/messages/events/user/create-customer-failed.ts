import { UbudMessage } from '@ubud/ngrx';
import { Failed } from '@shared/messages/events/failed';
import { MealmeState } from '@mealme/src/domain/stores/states';

@UbudMessage('Auth.User.CreateCustomerFailed')
export class CreateCustomerFailed extends Failed<MealmeState> {
}

import { UbudMessage } from '@ubud/ngrx';
import { Failed } from '@shared/messages/events/failed';
import { MealmeState } from '@mealme/src/domain/stores/states';

@UbudMessage('Auth.User.VerifyUserFailed')
export class VerifyUserFailed extends Failed<MealmeState> {
}

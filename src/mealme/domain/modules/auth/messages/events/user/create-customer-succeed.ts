import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';

@UbudMessage('Auth.User.CreateCustomerSucceed')
export class CreateCustomerSucceed extends Message<MealmeState> {
    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';
import { UserDto } from '@mealme/src/domain/modules/auth/dtos/user-dto';

@UbudMessage('Auth.User.CreateUser')
export class CreateUser extends Message<MealmeState> {
    public payload: UserDto;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

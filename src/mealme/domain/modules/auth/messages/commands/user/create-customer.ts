import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';
import { MerchantPasswordSignupDto } from '@mealme/src/domain/modules/auth/dtos/merchant-password-signup-dto';

@UbudMessage('Auth.User.CreateCustomer')
export class CreateCustomer extends Message<MealmeState> {
    public user: string;
    public payload: MerchantPasswordSignupDto;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

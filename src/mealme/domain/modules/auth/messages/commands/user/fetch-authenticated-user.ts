import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';

@UbudMessage('Auth.User.FetchAuthenticatedUser')
export class FetchAuthenticatedUser extends Message<MealmeState> {
    public cacheable?: boolean;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

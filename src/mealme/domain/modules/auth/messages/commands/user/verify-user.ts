import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';
import { CodeDto } from '@mealme/src/domain/modules/auth/dtos/code-dto';

@UbudMessage('Auth.User.VerifyUser')
export class VerifyUser extends Message<MealmeState> {
    public user: string;
    public payload: CodeDto;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

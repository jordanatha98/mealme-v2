import { Message, UbudMessage } from '@ubud/ngrx';
import { LoginDto } from '../../../dtos/login-dto';
import { MealmeState } from '@mealme/src/domain/stores/states';

@UbudMessage('Auth.Auth.Authenticate')
export class Authenticate extends Message<MealmeState> {
    public payload: LoginDto;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

import { CreateUser } from '@mealme/src/domain/modules/auth/messages/commands/user/create-user';
import { CreateUserSucceed } from '@mealme/src/domain/modules/auth/messages/events/user/create-user-succeed';
import { CreateUserFailed } from '@mealme/src/domain/modules/auth/messages/events/user/create-user-failed';
import { UserEntity } from '@mealme/src/domain/modules/auth/messages/entities/user/user-entity';
import { VerifyUser } from '@mealme/src/domain/modules/auth/messages/commands/user/verify-user';
import { VerifyUserSucceed } from '@mealme/src/domain/modules/auth/messages/events/user/verify-user-succeed';
import { VerifyUserFailed } from '@mealme/src/domain/modules/auth/messages/events/user/verify-user-failed';
import { CreateCustomer } from '@mealme/src/domain/modules/auth/messages/commands/user/create-customer';
import { CreateCustomerSucceed } from '@mealme/src/domain/modules/auth/messages/events/user/create-customer-succeed';
import { CreateCustomerFailed } from '@mealme/src/domain/modules/auth/messages/events/user/create-customer-failed';
import { RefreshTokenEntity } from '@mealme/src/domain/modules/auth/messages/entities/auth/refresh-token-entity';
import { SignatureEntity } from '@mealme/src/domain/modules/auth/messages/entities/auth/signature-entity';
import { Authenticate } from '@mealme/src/domain/modules/auth/messages/commands/auth/authenticate';
import { AuthenticateSucceed } from '@mealme/src/domain/modules/auth/messages/events/auth/authenticate-succeed';
import { AuthenticateFailed } from '@mealme/src/domain/modules/auth/messages/events/auth/authenticate-failed';
import { FetchAuthenticatedUser } from '@mealme/src/domain/modules/auth/messages/commands/user/fetch-authenticated-user';
import { FetchAuthenticatedUserFailed } from '@mealme/src/domain/modules/auth/messages/events/user/fetch-authenticated-user-failed';
import { FetchAuthenticatedUserSucceed } from '@mealme/src/domain/modules/auth/messages/events/user/fetch-authenticated-user-succeed';
import { AuthenticatedUserEntity } from '@mealme/src/domain/modules/auth/messages/entities/user/authenticated-user-entity';
import { SignOut } from '@mealme/src/domain/modules/auth/messages/commands/auth/sign-out';
import { SignOutSucceed } from '@mealme/src/domain/modules/auth/messages/events/auth/sign-out-succeed';

export const MEALME_AUTH_MESSAGES: any[] = [
    UserEntity,
    RefreshTokenEntity,
    SignatureEntity,

    CreateUser,
    CreateUserSucceed,
    CreateUserFailed,

    VerifyUser,
    VerifyUserSucceed,
    VerifyUserFailed,

    CreateCustomer,
    CreateCustomerSucceed,
    CreateCustomerFailed,

    Authenticate,
    AuthenticateSucceed,
    AuthenticateFailed,

    FetchAuthenticatedUser,
    FetchAuthenticatedUserFailed,
    FetchAuthenticatedUserSucceed,

    AuthenticatedUserEntity,

    SignOut,
    SignOutSucceed,
];

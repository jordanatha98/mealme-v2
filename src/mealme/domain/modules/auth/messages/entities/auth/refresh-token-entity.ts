import { Message, UbudMessage } from '@ubud/ngrx';
import { Signature } from '@mealme/src/domain/modules/auth/models/signature';
import { MealmeState } from '@mealme/src/domain/stores/states';

@UbudMessage('Auth.Auth.RefreshTokenEntity')
export class RefreshTokenEntity extends Message<MealmeState> {
    public refreshToken: Signature;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
            auth: {
                ...state.auth,
                refreshToken: this.refreshToken,
            },
        };
    }
}

import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';
import { Signature } from '@mealme/src/domain/modules/auth/models/signature';

@UbudMessage('Auth.Auth.SignatureEntity')
export class SignatureEntity extends Message<MealmeState> {
    public signature: Signature;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
            auth: {
                ...state.auth,
                signature: this.signature,
            },
        };
    }
}

import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';
import { User } from '@mealme/src/domain/modules/auth/models/user';

@UbudMessage('Auth.User.AuthenticatedUserEntity')
export class AuthenticatedUserEntity extends Message<MealmeState> {
    public authenticated: User;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
            user: {
                ...state.user,
                authenticated: this.authenticated
            },
        };
    }
}

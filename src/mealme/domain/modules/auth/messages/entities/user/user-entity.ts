import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';
import { UserDto } from '@mealme/src/domain/modules/auth/dtos/user-dto';
import { User } from '@mealme/src/domain/modules/auth/models/user';

@UbudMessage('Auth.User.UserEntity')
export class UserEntity extends Message<MealmeState> {
    public user: User;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
            user: {
                ...state.user,
                user: this.user
            },
        };
    }
}

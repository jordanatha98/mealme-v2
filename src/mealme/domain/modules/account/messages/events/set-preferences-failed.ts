import { UbudMessage } from '@ubud/ngrx';
import { Failed } from '@shared/messages/events/failed';
import { MealmeState } from '@mealme/src/domain/stores/states';

@UbudMessage('Account.SetPreferencesFailed')
export class SetPreferencesFailed extends Failed<MealmeState> {
}

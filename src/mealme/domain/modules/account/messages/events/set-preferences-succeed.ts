import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';

@UbudMessage('Account.SetPreferencesSucceed')
export class SetPreferencesSucceed extends Message<MealmeState> {
    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

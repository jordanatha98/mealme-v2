import { SetPreferences } from '@mealme/src/domain/modules/account/messages/commands/set-preferences';
import { SetPreferencesSucceed } from '@mealme/src/domain/modules/account/messages/events/set-preferences-succeed';
import { SetPreferencesFailed } from '@mealme/src/domain/modules/account/messages/events/set-preferences-failed';

export const MEALME_ACCOUNT_MESSAGES: any[] = [
    SetPreferences,
    SetPreferencesSucceed,
    SetPreferencesFailed,
];

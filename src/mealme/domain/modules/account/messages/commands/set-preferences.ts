import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';
import { PreferencesDto } from '@mealme/src/domain/modules/account/dtos/preferences-dto';

@UbudMessage('Account.SetPreferences')
export class SetPreferences extends Message<MealmeState> {
    public customer: string;
    public payload: PreferencesDto;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

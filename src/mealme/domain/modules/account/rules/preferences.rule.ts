import { ErrorMessages, Rule } from '@ubud/form';

export class PreferencesRule extends Rule {
    public get errorMessages(): ErrorMessages {
        return {};
    }

    public getFormControls(): { [p: string]: any } {
        return {
            likes: [''],
            dislikes: [''],
            allergies: [''],
            dietType: [''],
            maxCaloriePerMeal: [''],
        };
    }
}

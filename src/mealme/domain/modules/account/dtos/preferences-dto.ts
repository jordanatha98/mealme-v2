export class PreferencesDto {
    public likes: string[];
    public dislikes: string[];
    public allergies: string[];
    public dietType: string;
    public maxCaloriePerMeal: number;
}

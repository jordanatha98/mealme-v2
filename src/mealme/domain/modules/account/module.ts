import { NgModule } from '@angular/core';
import { MealmeDomainMealmeStoreModule } from '../../stores/module';
import { EffectsModule } from '@ngrx/effects';
import { AccountEffect } from '@mealme/src/domain/modules/account/effects/account.effect';
import { AccountService } from '@mealme/src/domain/modules/account/services/account.service';

@NgModule({
    imports: [
        MealmeDomainMealmeStoreModule,
        EffectsModule.forFeature([
            AccountEffect,
        ]),
    ],
    providers: [
        AccountService
    ]
})
export class MealmeDomainAccountModule {}

import { Injectable } from '@angular/core';
import { Form, FormFactory } from '@ubud/form';
import { FormBuilder } from '@angular/forms';
import { PreferencesRule } from '@mealme/src/domain/modules/account/rules/preferences.rule';

@Injectable()
export class PreferencesFormFactory implements FormFactory {
    public constructor(private fb: FormBuilder) {
    }

    public create(): Form {
        const formRule = new PreferencesRule();

        return {
            formGroup: this.fb.group(formRule.getFormControls()),
            rules: [formRule],
        };
    }
}

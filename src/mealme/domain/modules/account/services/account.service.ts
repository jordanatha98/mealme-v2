import { Injectable } from '@angular/core';
import { MealmeApiClient } from '@mealme/src/api/clients/api.client';
import { Observable } from 'rxjs';
import { mapToClass, mapToData } from '@shared/transformers/responses.transformer';
import { PreferencesDto } from '@mealme/src/domain/modules/account/dtos/preferences-dto';
import { Customer } from '@mealme/src/domain/modules/customer/models/customer';

@Injectable()
export class AccountService {
    public constructor(private client: MealmeApiClient) {
    }

    public setPreferences(customer: string, payload: PreferencesDto): Observable<Customer> {
        return this.client.patch(`customers/${customer}/set-preferences`, payload).pipe(
            mapToData(),
            mapToClass(Customer),
        );
    }
}

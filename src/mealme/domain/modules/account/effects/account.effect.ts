import { Injectable } from '@angular/core';
import { Effects, Message, ubudType } from '@ubud/ngrx';
import { Actions, Effect } from '@ngrx/effects';
import { MealmeStore } from '@mealme/src/domain/stores/mealme.store';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { MessageUtil } from '@shared/utils/message.util';
import { HttpErrorResponse } from '@angular/common/http';
import { Customer } from '@mealme/src/domain/modules/customer/models/customer';
import { AccountService } from '@mealme/src/domain/modules/account/services/account.service';
import { SetPreferences } from '@mealme/src/domain/modules/account/messages/commands/set-preferences';
import { SetPreferencesSucceed } from '@mealme/src/domain/modules/account/messages/events/set-preferences-succeed';
import { SetPreferencesFailed } from '@mealme/src/domain/modules/account/messages/events/set-preferences-failed';

@Injectable()
export class AccountEffect extends Effects {
    public constructor(actions$: Actions, private service: AccountService, private store: MealmeStore) {
        super(actions$);
    }

    @Effect()
    public setPreferences$: Observable<Message> = this.actions$.pipe(
        ubudType(SetPreferences),
        map(message => MessageUtil.getPayload<SetPreferences>(message)),
        mergeMap(({ customer, payload }) => {
            return this.service.setPreferences(customer, payload).pipe(
                mergeMap(response => {
                    if (response instanceof Customer) {
                        return of(
                            new SetPreferencesSucceed(),
                        );
                    }

                    return of(
                        new SetPreferencesFailed({
                            exception: new HttpErrorResponse({
                                status: 400,
                                error: $localize`There was an error processing`,
                            }),
                        })
                    );
                }),
                catchError((e: HttpErrorResponse) => {
                    return of(new SetPreferencesFailed({ exception: e }));
                }),
            );
        }),
    );
}

import { UUIDModel } from '@shared/models/uuid-model';
import { User } from '@mealme/src/domain/modules/auth/models/user';
import { DietType } from '@mealme/src/domain/modules/common/models/diet-type';
import { Like } from '@mealme/src/domain/modules/common/models/like';
import { Dislike } from '@mealme/src/domain/modules/common/models/dislike';
import { Allergy } from '@mealme/src/domain/modules/common/models/allergy';
import { Type } from 'class-transformer';
import { forwardRef } from '@angular/core';

export class Customer extends UUIDModel<Customer> {
    public user: User;
    public userId: string;
    @Type(forwardRef(() => Like) as any)
    public likes?: Like[];
    @Type(forwardRef(() => Dislike) as any)
    public dislikes?: Dislike[];
    @Type(forwardRef(() => Allergy) as any)
    public allergies?: Allergy[];
    @Type(forwardRef(() => DietType) as any)
    public dietType?: DietType;
    public preferencesSet: boolean;
    public maxCaloriePerMenu: number;
    public dietTypeId: string;
}

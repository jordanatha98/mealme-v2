import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';

@UbudMessage('Mealme.Product.FetchProducts')
export class FetchProducts extends Message<MealmeState> {
    public queries?: any;

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

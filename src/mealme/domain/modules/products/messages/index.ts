import { FetchProducts } from '@mealme/src/domain/modules/products/messages/commands/fetch-products';
import { FetchProductsFailed } from '@mealme/src/domain/modules/products/messages/events/fetch-products-failed';
import { FetchProductsSucceed } from '@mealme/src/domain/modules/products/messages/events/fetch-products-succeed';

export const MEALME_PRODUCT_MESSAGES: any[] = [
    FetchProducts,
    FetchProductsFailed,
    FetchProductsSucceed,
];

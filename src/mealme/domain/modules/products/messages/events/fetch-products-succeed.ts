import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '../../../../stores/states';

@UbudMessage('Mealme.Product.FetchProductsSucceed')
export class FetchProductsSucceed extends Message<MealmeState> {
    public products: any[];

    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
            product: {
                ...state.product,
                products: this.products,
            },
        };
    }
}

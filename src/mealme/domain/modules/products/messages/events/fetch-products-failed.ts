import { UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '../../../../stores/states';
import { Failed } from '@shared/messages/events/failed';

@UbudMessage('Mealme.Product.FetchProductsFailed')
export class FetchProductsFailed extends Failed<MealmeState> {
}

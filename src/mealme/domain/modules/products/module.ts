import { NgModule } from '@angular/core';
import { MealmeDomainMealmeStoreModule } from '../../stores/module';
import { EffectsModule } from '@ngrx/effects';
import { ProductEffect } from '@mealme/src/domain/modules/products/effects/product.effect';
import { ProductService } from '@mealme/src/domain/modules/products/services/product.service';

@NgModule({
    imports: [
        MealmeDomainMealmeStoreModule,
        EffectsModule.forFeature([
            ProductEffect,
        ]),
    ],
    providers: [
        ProductService,
    ]
})
export class MealmeDomainMealmeProductModule {}

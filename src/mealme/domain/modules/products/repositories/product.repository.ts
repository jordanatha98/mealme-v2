import { Injectable } from '@angular/core';
import { Repository } from '@ubud/ngrx';
import { MealmeState } from '../../../stores/states';
import { Observable } from 'rxjs';
import { MealmeStore } from '@mealme/src/domain/stores/mealme.store';

@Injectable()
export class ProductRepository extends Repository<MealmeState> {
    public constructor(store: MealmeStore) {
        super(store);
    }

    public getProducts$(): Observable<any> {
        return this.select(state => state.product.products);
    }
}

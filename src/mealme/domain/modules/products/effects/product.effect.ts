import { Injectable } from '@angular/core';
import { Effects, Message, ubudType } from '@ubud/ngrx';
import { Actions, Effect } from '@ngrx/effects';
import { ProductService } from '@mealme/src/domain/modules/products/services/product.service';
import { MealmeStore } from '@mealme/src/domain/stores/mealme.store';
import { Observable, of } from 'rxjs';
import { FetchProducts } from '@mealme/src/domain/modules/products/messages/commands/fetch-products';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { MessageUtil } from '@shared/utils/message.util';
import { RouterUtil } from '@shared/utils/router.util';
import { FetchProductsSucceed } from '@mealme/src/domain/modules/products/messages/events/fetch-products-succeed';
import { FetchProductsFailed } from '@mealme/src/domain/modules/products/messages/events/fetch-products-failed';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class ProductEffect extends Effects {
    public constructor(actions$: Actions, private service: ProductService, private store: MealmeStore) {
        super(actions$);
    }

    @Effect()
    public fetchProducts$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchProducts),
        map(message => MessageUtil.getPayload<FetchProducts>(message)),
        mergeMap(({ queries }) => {
            return this.service.getProducts(RouterUtil.queryParamsExtractor(queries)).pipe(
                map(response => {
                    if (response) {
                        return new FetchProductsSucceed({products: response});
                    }

                    return new FetchProductsFailed({
                        exception: new HttpErrorResponse({
                            error: `Something Error`,
                            status: 400,
                        }),
                    });
                }),
                catchError((e: HttpErrorResponse) => of(new FetchProductsFailed({exception: e}))),
            );
        }),
    );
}

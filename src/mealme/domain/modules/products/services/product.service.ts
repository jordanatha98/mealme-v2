import { Injectable } from '@angular/core';
import { MealmeApiClient } from '@mealme/src/api/clients/api.client';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable()
export class ProductService {
    public constructor(private client: MealmeApiClient) {
    }

    public getProducts(queries?: any): Observable<any[]> {
        return of([{ id: '1', name: 'asd' }, { id: '2', name: '2' }]).pipe(
            delay(3000)
        );
    }
}

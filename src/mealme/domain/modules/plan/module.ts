import { NgModule } from '@angular/core';
import { MealmeDomainMealmeStoreModule } from '../../stores/module';
import { EffectsModule } from '@ngrx/effects';
import { MealEffect } from '@mealme/src/domain/modules/meal/effects/meal.effect';
import { MealService } from '@mealme/src/domain/modules/meal/services/meal.service';

@NgModule({
    imports: [
        MealmeDomainMealmeStoreModule,
        EffectsModule.forFeature([
        ]),
    ],
    providers: [
    ]
})
export class MealmeDomainPlanModule {}

import { UUIDModel } from '@shared/models/uuid-model';
import { Type } from 'class-transformer';
import { forwardRef } from '@angular/core';
import { Menu } from '@mealme/src/domain/modules/meal/models/menu';
import { PlanMenu } from '@mealme/src/domain/modules/plan/models/plan-menu';

export class Plan extends UUIDModel<Plan> {
    public date: string;
    @Type(forwardRef(() => Menu) as any)
    public meals: Menu[];
    @Type(forwardRef(() => PlanMenu) as any)
    public planMenus: PlanMenu[];
    public customerId: string;
    public mealTime: string;
}

import { UUIDModel } from '@shared/models/uuid-model';
import { Type } from 'class-transformer';
import { forwardRef } from '@angular/core';
import { Customer } from '@mealme/src/domain/modules/customer/models/customer';

export class GeneratedPlan extends UUIDModel<GeneratedPlan> {
    @Type(forwardRef(() => Customer) as any)
    public customer: Customer;

    public date: string;
    public customerId: string;
}
import { UUIDModel } from '@shared/models/uuid-model';
import { Type } from 'class-transformer';
import { forwardRef } from '@angular/core';
import { Menu } from '@mealme/src/domain/modules/meal/models/menu';

export class PlanMenu extends UUIDModel<PlanMenu> {
    @Type(forwardRef(() => Menu) as any)
    public menu: Menu;
    public planId: string;
    public menuId: string;
    public mealTime: string;
}
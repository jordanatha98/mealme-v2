import { NgModule } from '@angular/core';
import { MealmeDomainMealmeStoreModule } from '../../stores/module';
import { EffectsModule } from '@ngrx/effects';
import { PreferencesEffect } from '@mealme/src/domain/modules/common/effects/preferences.effect';
import { PreferencesService } from '@mealme/src/domain/modules/common/services/preferences.service';

@NgModule({
    imports: [
        MealmeDomainMealmeStoreModule,
        EffectsModule.forFeature([
            PreferencesEffect,
        ]),
    ],
    providers: [
        PreferencesService,
    ]
})
export class MealmeDomainCommonModule {}

import { Injectable } from '@angular/core';
import { Effects, Message, ubudType } from '@ubud/ngrx';
import { Actions, Effect } from '@ngrx/effects';
import { MealmeStore } from '@mealme/src/domain/stores/mealme.store';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { MessageUtil } from '@shared/utils/message.util';
import { RouterUtil } from '@shared/utils/router.util';
import { HttpErrorResponse } from '@angular/common/http';
import { PreferencesService } from '@mealme/src/domain/modules/common/services/preferences.service';
import { FetchIngredients } from '@mealme/src/domain/modules/common/messages/commands/fetch-ingredients';
import { FetchIngredientsSucceed } from '@mealme/src/domain/modules/common/messages/events/fetch-ingredients-succeed';
import { FetchIngredientsFailed } from '@mealme/src/domain/modules/common/messages/events/fetch-ingredients-failed';
import { FetchDietTypes } from '@mealme/src/domain/modules/common/messages/commands/fetch-diet-types';
import { FetchDietTypesSucceed } from '@mealme/src/domain/modules/common/messages/events/fetch-diet-types-succeed';
import { FetchDietTypesFailed } from '@mealme/src/domain/modules/common/messages/events/fetch-diet-types-failed';

@Injectable()
export class PreferencesEffect extends Effects {
    public constructor(actions$: Actions, private service: PreferencesService, private store: MealmeStore) {
        super(actions$);
    }

    @Effect()
    public fetchIngredients$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchIngredients),
        map(message => MessageUtil.getPayload<FetchIngredients>(message)),
        mergeMap(({ queries }) => {
            return this.service.getIngredients(RouterUtil.queryParamsExtractor(queries)).pipe(
                mergeMap(response => {
                    if (response) {
                        return of(
                            new FetchIngredientsSucceed(),
                        )
                    }

                    return of(
                        new FetchIngredientsFailed({
                            exception: new HttpErrorResponse({
                                error: `Something Error`,
                                status: 400,
                            }),
                        })
                    )
                }),
                catchError((e: HttpErrorResponse) => of(new FetchIngredientsFailed({exception: e}))),
            );
        }),
    );

    @Effect()
    public fetchDietTypes$: Observable<Message> = this.actions$.pipe(
        ubudType(FetchDietTypes),
        map(message => MessageUtil.getPayload<FetchDietTypes>(message)),
        mergeMap(({ queries }) => {
            return this.service.getDietTypes(RouterUtil.queryParamsExtractor(queries)).pipe(
                mergeMap(response => {
                    if (response) {
                        return of(
                            new FetchDietTypesSucceed(),
                        )
                    }

                    return of(
                        new FetchDietTypesFailed({
                            exception: new HttpErrorResponse({
                                error: `Something Error`,
                                status: 400,
                            }),
                        })
                    )
                }),
                catchError((e: HttpErrorResponse) => of(new FetchDietTypesFailed({exception: e}))),
            );
        }),
    );
}

import { APP_INITIALIZER, Injector, Provider } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AuthStorage } from '@mealme/src/domain/modules/auth/enums/auth-storage';
import { SignatureEntity } from '@mealme/src/domain/modules/auth/messages/entities/auth/signature-entity';
import { RefreshTokenEntity } from '@mealme/src/domain/modules/auth/messages/entities/auth/refresh-token-entity';
import { MealmeStore } from '@mealme/src/domain/stores/mealme.store';
import { subscribeToPromise } from 'rxjs/internal-compatibility';

export function appInitializer(injector: Injector): () => Promise<void> {
    return () => {
        return new Promise<void>(async resolve => {
            const storage = injector.get(Storage);
            const store = injector.get(MealmeStore);

            return Promise.all([
                storage.get(AuthStorage.SIGNATURE),
                storage.get(AuthStorage.REFRESH_TOKEN),
            ]).then(([signature, refreshToken]) => {
                if (signature) {
                    store.dispatch(new SignatureEntity({ signature }));
                    store.dispatch(new RefreshTokenEntity({ refreshToken }));
                }

                return resolve();
            });
        });
    };
}

export const MEALME_APP_INITIALIZER: Provider = {
    useFactory: appInitializer,
    provide: APP_INITIALIZER,
    deps: [Injector],
    multi: true,
};

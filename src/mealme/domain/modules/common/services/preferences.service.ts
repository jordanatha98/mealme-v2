import { Injectable } from '@angular/core';
import { MealmeApiClient } from '@mealme/src/api/clients/api.client';
import { Observable } from 'rxjs';
import { mapToCollection } from '@shared/transformers/responses.transformer';
import { Collection } from '@shared/types/collection';
import { Ingredient } from '@mealme/src/domain/modules/common/models/ingredient';
import { HttpUtil } from '@shared/utils/http.util';
import { DietType } from '@mealme/src/domain/modules/common/models/diet-type';

@Injectable()
export class PreferencesService {
    public constructor(private client: MealmeApiClient) {
    }

    public getIngredients(queries: string): Observable<Collection<Ingredient>> {
        return this.client.get(`ingredients`, { params: HttpUtil.queryParamsExtractor(queries) }).pipe(
            mapToCollection(Ingredient)
        );
    }

    public getDietTypes(queries: string): Observable<Collection<DietType>> {
        return this.client.get(`diet-types`, { params: HttpUtil.queryParamsExtractor(queries) }).pipe(
            mapToCollection(DietType)
        );
    }
}

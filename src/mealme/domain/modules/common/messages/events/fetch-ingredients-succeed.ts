import { Message, UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '@mealme/src/domain/stores/states';

@UbudMessage('Common.FetchIngredientsSucceed')
export class FetchIngredientsSucceed extends Message<MealmeState> {
    public handle(state: MealmeState): MealmeState {
        return {
            ...state,
        };
    }
}

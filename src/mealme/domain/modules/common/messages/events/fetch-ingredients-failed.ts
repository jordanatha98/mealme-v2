import { UbudMessage } from '@ubud/ngrx';
import { MealmeState } from '../../../../stores/states';
import { Failed } from '@shared/messages/events/failed';

@UbudMessage('Common.FetchIngredientsFailed')
export class FetchIngredientsFailed extends Failed<MealmeState> {
}

import { FetchIngredients } from '@mealme/src/domain/modules/common/messages/commands/fetch-ingredients';
import { FetchIngredientsFailed } from '@mealme/src/domain/modules/common/messages/events/fetch-ingredients-failed';
import { FetchIngredientsSucceed } from '@mealme/src/domain/modules/common/messages/events/fetch-ingredients-succeed';
import { FetchDietTypes } from '@mealme/src/domain/modules/common/messages/commands/fetch-diet-types';
import { FetchDietTypesFailed } from '@mealme/src/domain/modules/common/messages/events/fetch-diet-types-failed';
import { FetchDietTypesSucceed } from '@mealme/src/domain/modules/common/messages/events/fetch-diet-types-succeed';

export const MEALME_COMMON_MESSAGES: any[] = [
    FetchIngredients,
    FetchIngredientsFailed,
    FetchIngredientsSucceed,

    FetchDietTypes,
    FetchDietTypesFailed,
    FetchDietTypesSucceed
];

import { UUIDModel } from '@shared/models/uuid-model';

export class Ingredient extends UUIDModel<Ingredient> {
    public name: string;
    public imageUrl: string;
}

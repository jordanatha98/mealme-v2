import { UUIDModel } from '@shared/models/uuid-model';

export class Allergy extends UUIDModel<Allergy> {
    public ingredientId: string;
    public customerId: string
}
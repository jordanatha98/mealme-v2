import { UUIDModel } from '@shared/models/uuid-model';

export class DietType extends UUIDModel<DietType> {
    public name: string;
    public imageUrl: string;
    public key: string;
}

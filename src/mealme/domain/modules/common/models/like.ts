import { UUIDModel } from '@shared/models/uuid-model';

export class Like extends UUIDModel<Like> {
    public customerId: string;
    public ingredientId: string;
}
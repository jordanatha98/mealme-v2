import { UUIDModel } from '@shared/models/uuid-model';

export class Dislike extends UUIDModel<Dislike> {
    public ingredientId: string;
    public customerId: string
}
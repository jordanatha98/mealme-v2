const fs = require('fs');

const SOURCE_ICONS = 'resources/';

/* ---- ANDROID --- */
const TARGET_ANDROID = 'android/app/src/main/res/';
const TARGET_ANDROID_DIRS = ['drawable-hdpi', 'drawable-mdpi', 'drawable-xhdpi', 'drawable-xxhdpi', 'drawable-xxxhdpi'];

const ANDROID_CLOSE_ICONS = [
    { source: 'close-android.png', target: 'drawable-hdpi/close.png' },
    { source: 'close-android.png', target: 'drawable-mdpi/close.png' },
    { source: 'close-android.png', target: 'drawable-xhdpi/close.png' },
    { source: 'close-android.png', target: 'drawable-xxhdpi/close.png' },
    { source: 'close-android.png', target: 'drawable-xxxhdpi/close.png' },
];

const ANDROID_BACK_ICONS = [
    { source: 'back-android.png', target: 'drawable-hdpi/back.png' },
    { source: 'back-android.png', target: 'drawable-mdpi/back.png' },
    { source: 'back-android.png', target: 'drawable-xhdpi/back.png' },
    { source: 'back-android.png', target: 'drawable-xxhdpi/back.png' },
    { source: 'back-android.png', target: 'drawable-xxxhdpi/back.png' },
];

const ANDROID_FORWARD_ICONS = [
    { source: 'forward-android.png', target: 'drawable-hdpi/forward.png' },
    { source: 'forward-android.png', target: 'drawable-mdpi/forward.png' },
    { source: 'forward-android.png', target: 'drawable-xhdpi/forward.png' },
    { source: 'forward-android.png', target: 'drawable-xxhdpi/forward.png' },
    { source: 'forward-android.png', target: 'drawable-xxxhdpi/forward.png' },
];

/* --- IOS --- */
const TARGET_IOS = 'ios/App/App/Assets.xcassets/';
const TARGET_IOS_DIRS = ['close.imageset', 'back.imageset', 'forward.imageset'];

const IOS_CLOSE_ICONS = [
    { source: 'close-ios.png', target: 'close.imageset/close.png' },
    { source: 'close-ios.png', target: 'close.imageset/close-1.png' },
    { source: 'close-ios.png', target: 'close.imageset/close-2.png' },
];

const IOS_BACK_ICONS = [
    { source: 'back-ios.png', target: 'back.imageset/back.png' },
    { source: 'back-ios.png', target: 'back.imageset/back-1.png' },
    { source: 'back-ios.png', target: 'back.imageset/back-2.png' },
];

const IOS_FORWARD_ICONS = [
    { source: 'forward-ios.png', target: 'forward.imageset/forward.png' },
    { source: 'forward-ios.png', target: 'forward.imageset/forward-1.png' },
    { source: 'forward-ios.png', target: 'forward.imageset/forward-2.png' },
];

function initialize() {
    for (const dir of TARGET_ANDROID_DIRS) {
        const target = TARGET_ANDROID + dir;

        if (!fs.existsSync(target)) {
            fs.mkdirSync(target);
        }
    }

    for (const dir of TARGET_IOS_DIRS) {
        const target = TARGET_IOS + dir;

        if (!fs.existsSync(target)) {
            fs.mkdirSync(target);
        }
    }
}

function copyImages(sourcePath, targetPath, images) {
    for (const icon of images) {
        let source = sourcePath + icon.source;
        let target = targetPath + icon.target;
        fs.copyFile(source, target, err => {
            if (err) throw err;
            console.log(`${source} >> ${target}`);
        });
    }
}

initialize();

copyImages(SOURCE_ICONS, TARGET_ANDROID, ANDROID_CLOSE_ICONS);
copyImages(SOURCE_ICONS, TARGET_ANDROID, ANDROID_BACK_ICONS);
copyImages(SOURCE_ICONS, TARGET_ANDROID, ANDROID_FORWARD_ICONS);

copyImages(SOURCE_ICONS, TARGET_IOS, IOS_CLOSE_ICONS);
copyImages(SOURCE_ICONS, TARGET_IOS, IOS_BACK_ICONS);
copyImages(SOURCE_ICONS, TARGET_IOS, IOS_FORWARD_ICONS);
